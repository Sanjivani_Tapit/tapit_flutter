import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:tapit/Activities/EditContainerActivity.dart';
import 'package:tapit/Activities/IntroSlider.dart';
import 'package:tapit/Activities/HomeScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/SignInClass.dart';
import 'package:tapit/global.dart'as global;
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  String LOGTAG="SplashScreen";

  @override
  void initState()
  {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async
    {
      double dpr=MediaQuery.of(context).devicePixelRatio;
      double unitHeightValue;
      if(dpr<=2.5)
      {
        unitHeightValue = 2.5;
      }
      else
      {
        double width=MediaQuery.of(context).size.width;
        double height=MediaQuery.of(context).size.height;
        if(height<700)
        {
          unitHeightValue = 2.4;
        }
        else{
          unitHeightValue = 2.6;
        }
      }

      double mul10 = 4.6;
      double mul12 = 4.8;
      double mul13 = 5.0;
      double mul14 = 5.6;
      double mul15 = 5.8;
      double mul16 = 6.4;
      double mul17 = 6.8;
      double mul18 = 7;
      double mul20 = 7.5;
      double mul22 = 8;
      double mul24 = 8.5;
      double mul26 = 9;

      global.font10=mul10*unitHeightValue;
      global.font12=mul12*unitHeightValue;
      global.font13=mul13*unitHeightValue;
      global.font14=mul14*unitHeightValue;
      global.font15=mul15*unitHeightValue;
      global.font16=mul16*unitHeightValue;
      global.font17=mul17*unitHeightValue;
      global.font18=mul18*unitHeightValue;
      global.font20=mul20*unitHeightValue;
      global.font22=mul22*unitHeightValue;
      global.font24=mul24*unitHeightValue;
      global.font26=mul26*unitHeightValue;
      global.nameTextFont=global.font16;
      global.amtFont=global.font15;
    });

    getStatus();
  }

  Future<void> getLocationData() async
  {
    Location location = new Location();
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {

      }
      else if(_permissionGranted == PermissionStatus.granted)
      {
        var lat,long,first,addresses,coordinates;
        location.getLocation().then((value) async => {
          _locationData=value,
          lat=_locationData.latitude,
          long=_locationData.longitude,
          coordinates = new Coordinates(lat, long),
          addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates),
          first = addresses.first,
          setState(() {
            global.initialCountryCode=first.countryCode;
          }),
        }).catchError((onError){
          return null;
        });
      }

    }
    else if(_permissionGranted == PermissionStatus.granted)
    {
      var lat,long,first,addresses,coordinates;
      location.getLocation().then((value) async => {
        _locationData=value,
        lat=_locationData.latitude,
        long=_locationData.longitude,
        coordinates = new Coordinates(lat, long),
        addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates),
        first = addresses.first,
        setState(() {
          global.initialCountryCode=first.countryCode;
        }),
      }).catchError((onError){
        return null;
      });

    }
  }

  void getStatus()async
  {
    String firstname,lastname;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool('UserLoggedInStatus');
    firstname = prefs.getString('CurrentFirstName');
    lastname = prefs.getString('CurrentLastName');

    if(firstname==null)
    {
      firstname="";
    }
    if(lastname==null)
    {
      lastname="";
    }

    await getLocationData();
    setState(() {
      Timer(
        Duration(milliseconds: 2000),
            () async => {
          if(global.notiClicked){
            Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => EditContainerActivity(),),),
          }
          else
            {
              if(boolValue==null)
                {

                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => IntroductionSlider(),),),
                }
              else if(!boolValue)
                {
                  Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) =>
                        SignInClass(
                            main_text: "Hi " + firstname + " " + lastname,
                            sub_text: "Mobile verification is mandatory"),),),
                }
              else if(boolValue)
                  {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen(),),),
                  }

            }
        },
      );
    });
  }


  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: new Container(
          padding: EdgeInsets.fromLTRB(80, 0, 80, 0),
          width:MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: global.splashScreenGradient,
                  tileMode: TileMode.clamp
              )
          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 60,
                child:  Image.asset('assets/justap_splash_logo.png'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

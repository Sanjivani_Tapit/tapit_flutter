// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'responsebean.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseBean _$ResponseBeanFromJson(Map<String, dynamic> json) {
  return ResponseBean(
    payLoad: json['payLoad'],
    timestamp: json['timestamp'] as int,
    status: json['status'] as int,
    error: json['error'] as String,
    message: json['message'] as String,
    path: json['path'] as String,
  );
}

Map<String, dynamic> _$ResponseBeanToJson(ResponseBean instance) =>
    <String, dynamic>{
      'payLoad': instance.payLoad,
      'timestamp': instance.timestamp,
      'status': instance.status,
      'error': instance.error,
      'message': instance.message,
      'path': instance.path,
    };

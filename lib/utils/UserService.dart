import 'dart:collection';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/helpers/Constants.dart';
import 'package:tapit/api_client.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:tapit/utils/responsebean.dart';
import 'package:tapit/global.dart' as global;
class UserService
{
  Future<ResponseBean> getAllUsers() async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;
    List<User> userList;
    try {
      response = await apiClient.getAllUsers(Constants.BASE_PLATFORM);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getUserInfo(String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;
    List<User> userList;
    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getUserData(Constants.BASE_PLATFORM,userid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> otpGenerate(String mobno,String cc) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      OTPGenerate otpGenerate=new OTPGenerate(mobileNo: mobno,countryCode: cc);
      var jsonbody=jsonEncode(otpGenerate);
      response = await apiClient.otpGenerate(Constants.BASE_PLATFORM,jsonbody);
    }
    on DioError catch (error, stacktrace)
    {
      int statusCode = error.response.statusCode;
      if (statusCode == 500)
      {
        var encodedStr = jsonEncode(error.response.data);
        Map<String, dynamic> valueMap = jsonDecode(encodedStr);
        response = ResponseBean.fromJson(valueMap);

        var resBody = json.decode(encodedStr);
        var user = jsonEncode(resBody['status']);
        if (user.toString().contains("500"))
        {
          return response;
        }
        return null;
      }
    }
    return response;
  }

  Future<ResponseBean> otpVerify(String mobno,String OTP,String cc) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      OTPVerify otpVerify=new OTPVerify(mobileNo: mobno,otp:OTP,countryCode: cc);
      var jsonbody=jsonEncode(otpVerify);
      response = await apiClient.otpVerify(Constants.BASE_PLATFORM,jsonbody);
      if(response.status==200)
      {
        var payload = response.payLoad;
        String token = payload['access_token'];
        String expiry = payload['expires_in'].toString();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool('UserLoggedInStatus', true);
        prefs.setString('AuthToken', token);
        prefs.setInt('Expiry', int.parse(expiry));
      }
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> checkMobileExist(String mobno,String cc) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("mobile", () => mobno);
      params.putIfAbsent("countryCode", () => cc);
      response = await apiClient.checkMobileExist(Constants.BASE_PLATFORM,params);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> UpdateUserData(String jsonBody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      response = await apiClient.UpdateUserData(Constants.BASE_PLATFORM,jsonBody);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> getCounter(String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getCounter(Constants.BASE_PLATFORM,userid,auth_token);
    }
    on DioError catch (error, stacktrace)
    {
      int statusCode=error.response.statusCode;
      if(statusCode==500)
      {
        var encodedStr=jsonEncode(error.response.data);
        Map<String, dynamic> valueMap = jsonDecode(encodedStr);
        response=ResponseBean.fromJson(valueMap);

        var resBody = json.decode(encodedStr);
        var user=jsonEncode(resBody['status']);
        if(user.toString().contains("403"))
        {
          return null;
        }
        if(user.toString().contains("500"))
        {
          return response;
        }
      }
      return null;
    }
    return response;
  }

  Future<ResponseBean> postContactToCloud(String jsonBody,String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.postContactToCloud(Constants.BASE_PLATFORM,jsonBody,userid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getContactSync(String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getContactSync(Constants.BASE_PLATFORM,userid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getHistoryData(String userid,String start,String end) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("start", () => start);
      params.putIfAbsent("limit", () => end);
      response = await apiClient.getHistoryData(Constants.BASE_PLATFORM,userid,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getAlertData(String userid,String start,String end) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("start", () => start);
      params.putIfAbsent("limit", () => end);
      response = await apiClient.getAlertData(Constants.BASE_PLATFORM,userid,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> updateAlertStatus(String userid,String alertid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.updateAlertStatus(Constants.BASE_PLATFORM,userid,alertid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getGroups(String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getGroups(Constants.BASE_PLATFORM,userid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> updateGroup(String userid,String grpid,String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.updateGroup(Constants.BASE_PLATFORM,userid,grpid,jsonbody,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> addGroup(String userid,String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.addGroup(Constants.BASE_PLATFORM,jsonbody,userid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> initiateBillSplit(String userid,String receiptid,String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.initiateBillSplit(Constants.BASE_PLATFORM,jsonbody,userid,receiptid,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> settleAPI(String receiptID,String senderID,String receiverID,String amount,int mode,String settlementType) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {

      String auth_token="Bearer "+global.authToken;

      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("senderid", () => senderID);
      params.putIfAbsent("receiverid", () => receiverID);
      params.putIfAbsent("amount", () => amount);
      params.putIfAbsent("paymentMode", () => mode.toString());
      params.putIfAbsent("settlementType", () => settlementType);

      response = await apiClient.settleAPI(Constants.BASE_PLATFORM,receiptID,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> rejectAPI(String receiptID,String senderID,String receiverID,String comment,String rejectType) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {

      String auth_token="Bearer "+global.authToken;

      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("senderid", () => senderID);
      params.putIfAbsent("receiverid", () => receiverID);
      params.putIfAbsent("comment", () => comment);
      params.putIfAbsent("rejectType", () => rejectType);

      response = await apiClient.rejectAPI(Constants.BASE_PLATFORM,receiptID,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> payupAPI(String receiptID,String senderID,String receiverID,String amount,int mode,String settlementType) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {

      String auth_token="Bearer "+global.authToken;

      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("senderid", () => senderID);
      params.putIfAbsent("receiverid", () => receiverID);
      params.putIfAbsent("amount", () => amount);
      params.putIfAbsent("paymentMode", () => mode.toString());
      params.putIfAbsent("settlementType", () => settlementType);

      response = await apiClient.payupAPI(Constants.BASE_PLATFORM,receiptID,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> reminderAPI(String receiptID,String senderID,String receiverID,String hotelname,String type) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;

      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("senderid", () => senderID);
      params.putIfAbsent("receiverid", () => receiverID);
      params.putIfAbsent("merchantName", () => hotelname);
      params.putIfAbsent("type", () => type);
      response = await apiClient.reminderAPI(Constants.BASE_PLATFORM,receiptID,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getReceipt(String receiptID) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getReceipt(Constants.BASE_PLATFORM,receiptID,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> requestSettlement(String receiptID) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.requestSettlement(Constants.BASE_PLATFORM,receiptID,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> getLogs(String userID,String receiptID) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getLogs(Constants.BASE_PLATFORM,userID,receiptID,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> verifyEmail(String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.verifyEmail(Constants.BASE_PLATFORM,jsonbody,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getImageMethod(String filename,String userid) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try
    {
      String auth_token="Bearer "+global.authToken;
      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("fileName", () => filename);
      params.putIfAbsent("userId", () => userid);
      response = await apiClient.getImageMethod(Constants.BASE_PLATFORM,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> putImageMethod(String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.putImageMethod(Constants.BASE_PLATFORM,jsonbody,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }


  Future<ResponseBean> getTapAPI(String jsonbody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.getTapAPI(jsonbody,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> getMerchantInfo(String ePrinterID) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      Map<String, String> params = new HashMap<String, String>();
      params.putIfAbsent("eprinterId", () => ePrinterID);
      response = await apiClient.getMerchantInfo(ePrinterID,params,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }

  Future<ResponseBean> acceptReceipt(String jsonBody) async {
    Dio dio = new Dio();
    ApiClient apiClient = new ApiClient(dio, Constants.BASE_URL);
    ResponseBean response;

    try {
      String auth_token="Bearer "+global.authToken;
      response = await apiClient.acceptReceipt(Constants.BASE_PLATFORM,jsonBody,auth_token);
    }
    catch (error, stacktrace)
    {
      return null;
    }
    return response;
  }
}
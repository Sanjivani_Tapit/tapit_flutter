import 'package:json_annotation/json_annotation.dart';
part 'responsebean.g.dart';
@JsonSerializable()
class ResponseBean {
  dynamic payLoad;
//  List<dynamic> test;

  ResponseBean(
      {this.payLoad,
        this.timestamp,
        this.status,
        this.error,
        this.message,
        this.path});

  int timestamp;

  int status;

  String error;

  String message;

  String path;

  factory ResponseBean.fromJson(Map<String, dynamic> json) =>
      _$ResponseBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseBeanToJson(this);
}

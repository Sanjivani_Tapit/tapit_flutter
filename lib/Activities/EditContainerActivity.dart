import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/CircularProgressbar.dart';
import 'package:tapit/Activities/GroupsActivity.dart';
import 'package:tapit/Activities/ReceiptScreen.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ExtraBillDetails.dart';
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/helpers/ModeOfPayment.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/helpers/ListForEditContainer.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'SplitDetailsActivity.dart';
import 'package:tapit/helpers/ListForEditContainerMember.dart';
import 'EditBillSplitActivity.dart';
import 'package:tapit/helpers/RejectionRadio.dart';
import 'package:tapit/helpers/TextFieldForDialog.dart';
import 'package:page_transition/page_transition.dart';

class EditContainerActivity extends StatefulWidget
{
  EditContainerActivity({Key key}) : super(key: key);
  EditContainerActivityState createState()=>EditContainerActivityState();
}

class EditContainerActivityState extends State<EditContainerActivity>
{
  BuildContext mContext;
  List<Fooditems> listofFoodItems=new List();
  List<ExtraBillDetails> listOfExtraBillDetails=new List();
  int modeOfPayemnt=0;
  Random random=new Random();
  double myAmount=0;
  bool isSettled=false;
  bool isAdmin=false;
  bool isPayee=false;
  bool isPayeeSettled=false;
  bool isPayeeRejected=false;
  double payeeBalAmt=0;
  int SettledUserCount=0;
  int modeOfPayment=0;
  String settleAmount="";
  bool isSplitGroupPresent=false;
  SharedPreferences prefs;
  bool receivedResponse=false;
  String splitType="";
  int splitTypeInt=0;
  double receiptSubtotal=0;
  String comment="";
  int receiptType=2;
  String receiptDescription="";
  List<ContactData> contact_list=new List();

  String LOGTAG="EditContainerActivity";

  @override
  void initState()
  {
    getReceipt();
    super.initState();
    global.isSplitDetails=false;
  }

  Future<void> _showSnackBar(BuildContext context, String text) async
  {
    if(text.toString().compareTo("REMIND")==0)
    {
      bool flag=await global.helperClass.sendRemainder(global.receiptID, global.UserID, global.adminUserID, global.hotelname,"payee");
      if(flag)
      {
        if (global.adminName == null || global.adminName.isEmpty)
        {
          showInfoFlushbarHelper(context, "Remainder sent to " + global.adminName.toString());
        }
        else
        {
          showInfoFlushbarHelper(context, "Remainder sent to " + global.adminName.toString());
        }
      }
      else
      {
        showInfoFlushbarHelper(context, "Remainder is already sent to " + global.adminName.toString()+". Please wait for 24hrs.");
      }
    }
    else if(text.toString().compareTo("SETTLE")==0)
    {
      settleForDialog();
    }
    else if(text.toString().compareTo("PAYUP")==0){
      payupDialog();
    }
    else if(text.toString().compareTo("REJECT")==0){
      rejectionDialog();
    }
  }

  void rejectionDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child:new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Reject request", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                          width: 20,
                                          height: 20,
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Image.asset(
                                              'assets/black_cross_icon.png',
                                              fit: BoxFit.contain,
                                              color:Color.fromRGBO(0, 0, 0, 0.3),
                                            ),
                                          )
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            new Column(
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("I can't accept this request because", style: TextStyle(fontSize: global.font17,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(height:17),
                            RejectionRadio(isSel: (int value){
                              if(value==0)
                              {
                                setState(() {
                                  comment="I want adjustment in my share";
                                });
                              }
                              else
                              {
                                setState(() {
                                  comment="I have already paid in other mode";
                                });
                              }
                            },)
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(color: Color(0xffdcdcdc), width: 1.0,),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize:  global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async
                                  {
                                    if(comment!=null && comment.isNotEmpty)
                                    {
                                      bool flag = await global.helperClass.rejectAPI(global.receiptID,global.UserID,global.adminUserID,comment,"member");
                                      if (flag) {
                                        setState(() {});
                                      }
                                      Navigator.of(context).pop();
                                      getReceipt();
                                    }
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  void payupDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          modeOfPayemnt=0;
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                        child:new Column(
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Pay to", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                          width: 20,
                                          height: 20,
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Image.asset(
                                                'assets/black_cross_icon.png',
                                                fit: BoxFit.contain,
                                                color:global.nameTextColor
                                            ),
                                          )
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 20),
                            new Stack(
                              children: <Widget>[
                                new Container(
                                  width: 50.0,
                                  height: 50.0,
                                  child:
                                  global.adminImage!=null?((global.adminImageType.toString().contains("true"))?
                                  CircleAvatar(
                                    backgroundImage: MemoryImage(global.adminImage),
                                    backgroundColor: global.imageBackColor,
                                  ):CircleAvatar(
                                    child: Text(global.adminImageString,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                    backgroundColor: global.colors.elementAt(2),
                                  )):
                                  CircleAvatar(
                                    backgroundImage: AssetImage('assets/dummy_user.png'),
                                    backgroundColor: global.appbargreycolor,
                                  ),
                                ),
                                new Positioned(
                                    top:0.0,
                                    right: 0.0,
                                    child: Container(
                                      height: 15,
                                      width: 15,
                                      child: Image.asset(
                                        'assets/admin_symbol.png',
                                        fit: BoxFit.contain,
                                        color:Color.fromRGBO(0, 0, 0, 0.3),
                                      ),
                                    )
                                ),
                              ],
                            ),
                            new Text(global.adminName.toString(), style: TextStyle(fontSize:  global.font18,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            new Text("(Admin)", style: TextStyle(fontSize:  global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height:17),
                            new Text("an amount of", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height: 2,),
                            new Text("\$ "+global.formatter.format(payeeBalAmt), style: TextStyle(fontSize:  global.font26,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            SizedBox(height: 20,),
                            new Text("Mode of Payment", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height: 10,),
                            ModeOfPayment(dailogType:0,isSel: (int value)  {
                              setState(() {
                                modeOfPayemnt=value;
                              });
                            },),
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize:  global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize:  global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async {
                                    String amt=double.parse((payeeBalAmt).toStringAsFixed(2)).toString();
                                    bool flag=await global.helperClass.payupAPI(global.receiptID,global.UserID,global.adminUserID,amt,modeOfPayemnt,"member");
                                    if(flag)
                                    {
                                      setState(()
                                      {
                                        isPayeeSettled=true;
                                      });
                                    }
                                    Navigator.of(context).pop();
                                    getReceipt();
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  void settleForDialog()
  {
    bool flag=false;
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          final textController = TextEditingController();
          myAmount=double.parse((myAmount).toStringAsFixed(2));
          textController.text=myAmount.toString();
          if(!flag)
          {
            settleAmount = myAmount.toString();
            flag=true;
          }
          modeOfPayment=3;

          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: SingleChildScrollView(
              child: Container(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          new Container(
                              padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                              child:new Column(
                                children: <Widget>[
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Flexible(
                                        flex:1,
                                        child:new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Text("Received from", style: TextStyle(fontSize:  global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex:1,
                                        child:new Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            Container(
                                                width: 20,
                                                height: 20,
                                                child: GestureDetector(
                                                  onTap: (){
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Image.asset(
                                                    'assets/black_cross_icon.png',
                                                    fit: BoxFit.contain,
                                                    color:Color.fromRGBO(0, 0, 0, 0.3),
                                                  ),
                                                )
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  new Row(
                                    children: <Widget>[
                                      new Stack(
                                        children: <Widget>[
                                          new Container(
                                            width: 48.0,
                                            height: 48.0,
                                            child:
                                            global.adminImage!=null?((global.adminImageType.toString().contains("true"))?
                                            CircleAvatar(
                                              backgroundImage: MemoryImage(global.adminImage),
                                              backgroundColor: global.imageBackColor,
                                            ):(global.adminImageString!=null?CircleAvatar(
                                              child: Text(global.adminImageString,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                              backgroundColor: global.colors.elementAt(2),
                                            ):CircleAvatar(
                                              backgroundImage: AssetImage('assets/dummy_user.png'),
                                              backgroundColor: global.appbargreycolor,
                                            )
                                            )
                                            ):
                                            CircleAvatar(
                                              backgroundImage: AssetImage('assets/dummy_user.png'),
                                              backgroundColor: global.appbargreycolor,
                                            ),
                                          ),
                                          new Positioned(
                                              top:0.0,
                                              right: 0.0,
                                              child: Container(
                                                height: 15,
                                                width: 15,
                                                child: Image.asset(
                                                    'assets/admin_symbol.png',
                                                    fit: BoxFit.contain
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                      SizedBox(width: 10,),
                                      new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Text(global.adminName.toString(), style: TextStyle(fontSize:  global.font16,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                          new Text("(Admin)", style: TextStyle(fontSize:  global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(height:20),
                                  TextFieldForDialog(initialVal:myAmount,isSel:(double value){
                                    setState(() {
                                      value=double.parse((value).toStringAsFixed(2));
                                      settleAmount=value.toString();
                                    });
                                  }),
                                  SizedBox(height: 23,),
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                          padding: EdgeInsets.fromLTRB(2, 0, 0, 0),
                                          child:  Opacity(
                                            opacity: 0.9,
                                            child:new Text("I received payment by", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                          )
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 15,),
                                  ModeOfPayment(dailogType:1,isSel: (int value)  {
                                    setState(() {
                                      modeOfPayment=value;
                                    });
                                  },),
                                ],
                              )
                          ),
                          SizedBox(height: 35,),
                          new Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  color: Color(0xffdcdcdc),
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ),
                          new Row(

                            children: <Widget>[
                              new Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      FlatButton(
                                        child: Text("Cancel", style: TextStyle(fontSize:  global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                        onPressed: (){
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  )
                              ),
                              new Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      FlatButton(
                                        child: Text("Confirm", style: TextStyle(fontSize:  global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                        onPressed: ()async
                                        {
                                          bool flag=await global.helperClass.SettleAPI(global.receiptID,global.UserID,global.adminUserID,settleAmount,modeOfPayment,"payee");
                                          if(flag)
                                          {
                                            setState(()
                                            {
                                              isPayeeSettled=true;
                                            });
                                          }
                                          Navigator.of(context).pop();
                                          getReceipt();
                                        },
                                      )
                                    ],
                                  )
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                ),
              ),
            ),
          );
        });
  }


  void showInfoFlushbarHelper(BuildContext context,String text)
  {
    Flushbar(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(10),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [global.darkgreycolor, global.darkgreycolor],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      duration: Duration(seconds: 2),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      message:text,
    )..show(context);
  }

  Future<void> getReceipt()async
  {
    global.UserIdAndNameMap.clear();
    global.sel_contact_list.clear();
    global.finalUserList.clear();
    global.imageList.clear();
    global.multiple_parts=0;

    setState(() {
      SettledUserCount=0;
    });

    if(!global.ereceiptToContainer)
    {
      setState(() {
        receivedResponse = false;
      });

      bool flag = await getReceiptData();
      if (flag)
      {
        setState(()
        {
          receivedResponse = true;
        });
      }
    }
    else
    {
      setState(()
      {
        receivedResponse = true;
      });
      bool flag = await getReceiptData();
    }
  }

  Future<bool> getReceiptData()async
  {
    Future<bool> flag=Future<bool>.value(true);
    prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");
    String userid=prefs.getString("UserID");
    global.UserID=userid;

    if(map!=null)
    {
      setState(()
      {
        global.searchitem = "";
        contact_list = (json.decode(map) as List).map((i) =>
            ContactData.fromJson(i)).toList();
      });
    }

    UserService userService=new UserService();
    ResponseBean response=await userService.getReceipt(global.receiptID);
    if(response!=null)
    {
      int statusCode = response.status;
      var payload = response.payLoad;
      if (statusCode == 200)
      {
        int unsettledPayeeCount = 0;
        String merchantURL;
        String hotelName="";
        receiptType=payload['receiptType'];
        String grpid = payload['groupId'];
        String splittype = payload['splitType'];
        String grpname = payload['groupName'];
        double receiptAmt = payload['totalAmount'];
        double serviceCharges = payload['serviceTax'];
        double gratuity = payload['gratuity'];

        var merchantobj=payload['merchant'];
        if(merchantobj!=null)
        {
          merchantURL = payload['merchant']['merchantUrl'];
          hotelName = payload['merchant']['merchantName'];
        }

        if(receiptType!=null)
        {
          if (receiptType == 1)
          {
            hotelName = payload['merchantName'];
            receiptDescription = payload['description'];
            if (hotelName == null)
            {
              hotelName = "Any Restaurent";
            }
          }
        }
        else
        {
          receiptType=2;
        }

        if (merchantURL == null || merchantURL.toString().compareTo("/url") == 0)
        {
          merchantURL="https://image.freepik.com/free-photo/chicken-steak-with-lemon-tomato-chili-carrot-white-plate_1150-25886.jpg";
        }

        global.receiptImageURL = merchantURL;
        global.grpname = grpname;

        ExtraBillDetails extraBillDetails = new ExtraBillDetails("ServiceCharges", 0, serviceCharges);
        ExtraBillDetails extraBillDetails1 = new ExtraBillDetails("gratuity", 0, gratuity);

        listOfExtraBillDetails.add(extraBillDetails);
        listOfExtraBillDetails.add(extraBillDetails1);

        if (serviceCharges != null && gratuity != null)
        {
          receiptSubtotal = receiptAmt - serviceCharges - gratuity;
        }
        else
        {
          receiptSubtotal = receiptAmt;
        }

        List<dynamic> foodlist = payload['items'];
        if (foodlist != null)
        {
          for (int k = 0; k < foodlist.length; k++)
          {
            String name = foodlist[k]["name"];
            double rate = foodlist[k]["rate"];
            double quan = foodlist[k]["quantity"];
            int quantity = quan.toInt();
            double amount = foodlist[k]["amount"];
            Fooditems fooditems = new Fooditems(name, rate, quantity, amount);
            listofFoodItems.add(fooditems);
          }
        }

        if (receiptAmt == 0)
        {
          receiptAmt = 60;
        }

        if (hotelName == null || hotelName.isEmpty)
        {
          hotelName = "Twinkal Indian Resto";
        }

        global.hotelname = hotelName;
        global.receiptAmount = receiptAmt;

        if (mounted)
        {
          setState(()
          {
            if (splittype != null)
            {
              if (splittype.compareTo("equal") == 0)
              {
                splitType = "equal";
                splitTypeInt = 0;
              }
              else if (splittype.compareTo("custom_dollar") == 0)
              {
                splitType = "dollar";
                splitTypeInt = 1;
              }
              else
              {
                splitType = "multiple";
                splitTypeInt = 2;
              }
            }
          });
        }
        bool isSettledStatus = payload['settlementStatus'];
        if (mounted)
        {
          setState(()
          {
            isSettled = isSettledStatus;
            global.isSettled = isSettledStatus;
          });
        }
        int multipleParts = 0;

        List<dynamic> splitGroups = payload['splitGroups'];
        if (splitGroups.length > 0)
        {
          if (mounted)
          {
            setState(()
            {
              isSplitGroupPresent = true;
            });
          }
        }
        else
        {
          if (mounted)
          {
            setState(()
            {
              isSplitGroupPresent = false;
            });
          }
        }

        for (int i = 0; i < splitGroups.length; i++)
        {
          String userId = splitGroups[i]['userId'];
          int userRole = splitGroups[i]['userRole'];
          bool payee = splitGroups[i]['payee'];
          double payeeAmt = splitGroups[i]['payeeAmount'];
          String memberStatus = splitGroups[i]['memberStatus'];
          String mobNumber = splitGroups[i]['mobileNo'];
          String registrationStatus = splitGroups[i]['registrationStatus'];
          int sharePart = splitGroups[i]['amtSharePart'];
          List<dynamic> splitOwes = splitGroups[i]['splitOwes'];
          String role;
          double shareAmt = splitOwes[0]['amount'];
          multipleParts += sharePart;
          bool istapitUser = false;
          double balanceAmt = splitOwes[0]['balanceAmount'];

          if (registrationStatus != null)
          {
            if (registrationStatus.compareTo("REGISTERED") == 0)
            {
              istapitUser = true;
            }
            else if (registrationStatus.compareTo("PARTIAL") == 0)
            {
              istapitUser = false;
            }
          }

          if (memberStatus.compareTo("settle") == 0 || memberStatus.compareTo("admin_settle") == 0 || memberStatus.compareTo("payee_settle") == 0)
          {
            setState(()
            {
              SettledUserCount++;
            });
          }


          if (userId.toString().compareTo(global.UserID) == 0)
          {
            if (userRole == 1)
            {
              role = "admin";
              setState(() {
                isAdmin = true;
                global.adminUserID = userId;
              });
            }
            else if (userRole == 0 && payee)
            {
              role = "payee";
              setState(() {
                isAdmin = false;
                isPayee = true;
                payeeBalAmt = balanceAmt;
                global.isPayee = true;
              });
            }
            else if (userRole == 0 && !payee)
            {
              role = "member";
              setState(() {
                isAdmin = false;
                isPayee = false;
                global.isPayee = false;
              });
            }

            String type = "true";
            String username = prefs.getString("CurrentFirstName") + " " + prefs.getString("CurrentLastName");
            String mobnumber = prefs.getString("MobileNo");
            String image = prefs.getString("UserImage");

            if (image == null || image.isEmpty)
            {
              var arr = username.split(" ");
              if (arr.length >= 2)
              {
                if (arr[0] != null && arr[0].isNotEmpty)
                {
                  image = arr[0].substring(0, 1);
                }

                if (arr[1] != null && arr[1].isNotEmpty)
                {
                  image = image + arr[1].substring(0, 1);
                }
              }
              else
              {
                image = arr[0];
              }
              type = "false";
            }

            if (memberStatus.compareTo("pending") == 0 || memberStatus.compareTo("admin_partial_settle") == 0 || memberStatus.compareTo("payee_partial_settle") == 0 || memberStatus.compareTo("reject") == 0)
            {
              setState(() {
                isPayeeSettled = false;
              });
              if(memberStatus.compareTo("reject") == 0)
              {
                setState(() {
                  isPayeeRejected=true;
                });
              }
              else
              {
                setState(() {
                  isPayeeRejected=false;
                });
              }
            }
            else
            {
              setState(() {
                isPayeeSettled = true;
                isPayeeRejected=false;
              });
            }

            UserAmountData userAmountData = new UserAmountData();
            userAmountData.userID = userId;
            userAmountData.isTapitUser = istapitUser;
            userAmountData.type = type;
            userAmountData.colordata = random.nextInt(6);
            userAmountData.image = image;
            userAmountData.name = username;
            userAmountData.number = mobnumber;
            userAmountData.isSelected = true;
            userAmountData.isPayee = payee;
            userAmountData.payeeAmount = payeeAmt;
            userAmountData.shareAmount = shareAmt;
            userAmountData.balanceAmount = balanceAmt;
            userAmountData.sharepart = sharePart;
            userAmountData.settlementStatus = memberStatus;
            userAmountData.usertype = role;
            userAmountData.cc = prefs.getString("countryCode");

            String name = "";
            if (username == null || username.isEmpty)
            {
              name = mobNumber;
            }
            else
            {
              name = username;
            }

            String temprole = "";
            setState(()
            {
              if (role.toString().compareTo("admin") == 0)
              {
                temprole = "(A)";
              }
              else if (role.toString().compareTo("payee") == 0)
              {
                temprole = "(P)";
              }
              else
              {
                temprole = "(M)";
              }

              global.UserIdAndNameMap.putIfAbsent(userId, () => name + temprole);
              myAmount = balanceAmt.abs();
            });

            Uint8List MainprofileImg;
            if (type.contains("true"))
            {
              MainprofileImg = base64Decode(image);
            }
            else if (type.contains("false"))
            {
              MainprofileImg = new Uint8List(0);
            }
            else if (type.contains("admin"))
            {
              MainprofileImg = new Uint8List(0);
            }
            else
            {
              MainprofileImg = null;
            }


            if (role.compareTo("admin") == 0)
            {
              global.adminName = username;
              global.adminImageString = image;
              global.adminImage = MainprofileImg;
              global.adminImageType = type;
            }

            bool numberAlreadyPresence = false;
            for (int s = 0; s < global.finalUserList.length; s++)
            {
              UserAmountData ud = global.finalUserList.elementAt(s);
              if (ud.number.toString().compareTo(userAmountData.number.toString()) == 0 && ud.cc.toString().compareTo(userAmountData.cc.toString()) == 0)
              {
                numberAlreadyPresence = true;
              }
            }
            if (!numberAlreadyPresence)
            {
              if (userId.toString().compareTo(global.UserID) == 0)
              {
                global.finalUserList.insert(0, userAmountData);
                global.imageList.insert(0, MainprofileImg);
              }
              else
              {
                global.finalUserList.add(userAmountData);
                global.imageList.add(MainprofileImg);
              }
            }
          }
          else
          {
            bool alreadyPresent = false;
            bool userfound = false;
            for (int j = 0; j < contact_list.length; j++)
            {
              ContactData cd = contact_list.elementAt(j);
              if (cd.number != null && cd.number.isNotEmpty)
              {
                if (cd.number.contains(mobNumber) || mobNumber.contains(cd.number))
                {
                  for (int p = 0; p < global.finalUserList.length; p++)
                  {
                    UserAmountData ud = global.finalUserList.elementAt(p);
                    if (ud.number.contains(mobNumber) || mobNumber.contains(ud.number))
                    {
                      if (!alreadyPresent)
                      {
                        alreadyPresent = true;
                      }
                    }
                  }

                  if (!alreadyPresent)
                  {
                    if (!userfound)
                    {
                      userfound = true;
                    }

                    if (userRole == 1)
                    {
                      role = "admin";
                      global.adminUserID = userId;
                    }
                    else if (userRole == 0 && payee)
                    {
                      role = "payee";
                    }
                    else if (userRole == 0 && !payee)
                    {
                      role = "member";
                    }

                    UserAmountData userAmountData = new UserAmountData();
                    userAmountData.userID = userId;
                    userAmountData.isTapitUser = istapitUser;
                    userAmountData.type = cd.type;
                    userAmountData.colordata = cd.colordata;
                    Uint8List temp = await global.helperClass.convertImg(cd);
                    global.imageList.add(temp);
                    userAmountData.image = cd.image;
                    userAmountData.name = cd.name;
                    userAmountData.number = cd.number;
                    userAmountData.isSelected = cd.isSelected;
                    userAmountData.isPayee = payee;
                    userAmountData.payeeAmount = payeeAmt;
                    userAmountData.shareAmount = shareAmt;
                    userAmountData.sharepart = sharePart;
                    userAmountData.balanceAmount = balanceAmt;
                    userAmountData.settlementStatus = memberStatus;
                    userAmountData.usertype = role;
                    userAmountData.cc = cd.cc;

                    String name = "";
                    if (cd.name == null || cd.name.isEmpty)
                    {
                      name = mobNumber;
                    }
                    else
                    {
                      name = cd.name;
                    }
                    String temprole = "";
                    setState(() {
                      if (role.toString().compareTo("admin") == 0)
                      {
                        temprole = "(A)";
                      }
                      else if (role.toString().compareTo("payee") == 0)
                      {
                        temprole = "(P)";
                      }
                      else
                      {
                        temprole = "(M)";
                      }
                      global.UserIdAndNameMap.putIfAbsent(userId, () => name + temprole);
                    });

                    if (role.compareTo("admin") == 0)
                    {
                      global.adminName = cd.name;
                      global.adminImageString = cd.image;
                      global.adminImage = await global.helperClass.convertImg(cd);
                      global.adminImageType = cd.type;
                    }

                    bool numberAlreadyPresence = false;
                    for (int s = 0; s < global.finalUserList.length; s++)
                    {
                      UserAmountData ud = global.finalUserList.elementAt(s);
                      if (ud.number.toString().compareTo(userAmountData.number.toString()) == 0 && ud.cc.toString().compareTo(userAmountData.cc.toString()) == 0)
                      {
                        numberAlreadyPresence = true;
                      }
                    }
                    if (!numberAlreadyPresence)
                    {
                      if (userId.toString().compareTo(global.UserID) == 0)
                      {
                        global.finalUserList.insert(0, userAmountData);
                      }
                      else
                      {
                        global.finalUserList.add(userAmountData);
                      }
                    }
                  }
                }
              }
            }
            if (!userfound)
            {
              if (!alreadyPresent)
              {
                if (userRole == 1)
                {
                  role = "admin";
                  global.adminUserID = userId;
                }
                else if (userRole == 0 && payee)
                {
                  role = "payee";
                }
                else if (userRole == 0 && !payee)
                {
                  role = "member";
                }


                UserAmountData userAmountData = new UserAmountData();
                userAmountData.userID = userId;
                userAmountData.isTapitUser = istapitUser;
                userAmountData.type = null;
                userAmountData.colordata = random.nextInt(6);
                Uint8List temp = null;
                global.imageList.add(temp);
                userAmountData.image = null;
                userAmountData.name = null;
                userAmountData.number = mobNumber;
                userAmountData.isSelected = false;
                userAmountData.isPayee = payee;
                userAmountData.payeeAmount = payeeAmt;
                userAmountData.shareAmount = shareAmt;
                userAmountData.sharepart = sharePart;
                userAmountData.balanceAmount = balanceAmt;
                userAmountData.settlementStatus = memberStatus;
                userAmountData.usertype = role;
                userAmountData.cc = prefs.getString("countryCode");

                String name = mobNumber;
                String temprole = "";
                setState(()
                {
                  if (role.toString().compareTo("admin") == 0)
                  {
                    temprole = "(A)";
                  }
                  else if (role.toString().compareTo("payee") == 0)
                  {
                    temprole = "(P)";
                  }
                  else
                  {
                    temprole = "(M)";
                  }

                  global.UserIdAndNameMap.putIfAbsent(userId, () => name + temprole);
                });

                bool numberAlreadyPresence = false;
                for (int s = 0; s < global.finalUserList.length; s++)
                {
                  UserAmountData ud = global.finalUserList.elementAt(s);
                  if (ud.number.toString().compareTo(userAmountData.number.toString()) == 0 && ud.cc.toString().compareTo(userAmountData.cc.toString()) == 0)
                  {
                    numberAlreadyPresence = true;
                  }
                }
                if (!numberAlreadyPresence)
                {
                  if (userId.toString().compareTo(global.UserID) == 0)
                  {
                    global.finalUserList.insert(0, userAmountData);
                  }
                  else
                  {
                    global.finalUserList.add(userAmountData);
                  }
                }

                if (role.compareTo("admin") == 0)
                {
                  global.adminName = mobNumber;
                  global.adminImageString = mobNumber.substring(0, 1);
                  global.adminImage = null;
                  global.adminImageType = null;
                }
              }
            }
          }
        }

        if (splitGroups != null)
        {
          if (splitGroups.length != SettledUserCount)
          {
            global.unsettledUserCount = splitGroups.length - SettledUserCount - 1;
          }
        }

        global.non_tapit_users = 0;
        for (int p = 0; p < global.finalUserList.length; p++)
        {
          UserAmountData ud = global.finalUserList.elementAt(p);
          if (!ud.isTapitUser)
          {
            global.non_tapit_users = global.non_tapit_users + 1;
          }
          if (ud.settlementStatus.toString().compareTo("pending") == 0 && ud.isPayee)
          {
            unsettledPayeeCount++;
          }
        }

        global.unsettledPayeeCount = unsettledPayeeCount - 1;
        global.grpid = grpid;
        global.multiple_parts = multipleParts;
      }
    }
    return flag;

  }

  void dialog_circular_progressbar()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return StatefulBuilder(builder: (context, setState)
          {
            return Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                "Sending payment requests" ,
                                style: TextStyle(fontSize: global.font20,
                                    color: global.blackcolor,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'BalooChetanSemiBold')),
                          ],
                        ),
                        SizedBox(height: 20,),
                        Column(
                          children: <Widget>[
                            SizedBox(
                              height: 200.0,
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: CircularProgressbar(totalusers: global.finalUserList.length,isSel: (bool value)  {
                                      if(value){
                                        Navigator.of(context).pop();
                                      }
                                    },),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20,),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: 18,
                                height: 18,
                                child: Image.asset(
                                    'assets/non_tapit_symbol.png',
                                    fit: BoxFit.contain
                                )
                            ),
                            new Text(global.non_tapit_users.toString()+" user invited to tapit",
                                style: TextStyle(fontSize: global.font17,
                                    color: global.darkgreycolor,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'BalooChetanRegular')),
                          ],
                        ),
                      ]
                  ),
                ),
              ),
            );
          });
        });
  }

  Future<bool> requestPayment()async{
    Future<bool> flag=Future<bool>.value(false);

    UserService userService=new UserService();
    ResponseBean response=await userService.requestSettlement(global.receiptID);
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 204 || statusCode==200)
      {
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    return flag;
  }

  Future<bool> _onBackPressed()
  {
    if(isSettled)
    {
      if(!global.receiptLastScreen)
      {
        global.homeFragment = 1;
        Navigator.of(context).pushNamedAndRemoveUntil('/Home', ModalRoute.withName('/Splash'));
      }
      else
      {
        Navigator.of(context).pop();
      }
    }
    else
    {
      if(global.ereceiptToContainer)
      {
        global.homeFragment = 1;
        Navigator.of(context).pushNamedAndRemoveUntil('/Home', ModalRoute.withName('/Splash'));
      }
      else
      {
        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    mContext=context;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: new Scaffold(
        resizeToAvoidBottomPadding:false,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            titleSpacing:0.0,
            automaticallyImplyLeading: false,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    _onBackPressed();
                  },
                  child: Container(
                    padding:  EdgeInsets.fromLTRB(15,0,0,0),
                    child: Image.asset(
                      'assets/back_arrow.png',
                      fit: BoxFit.contain,
                      height: 30,
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(30,12,0,12),
                    child:  Text("E-Receipt summary",style: new TextStyle(
                        fontSize: global.font18,
                        color: global.appbarTextColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'BalooChetanMedium'
                    )
                    ))
              ],
            ),
            actions: <Widget>[
              Hero(
                  tag:'ereceipt',
                  child:
                  global.receiptAmount!=0?GestureDetector(
                      onTap: (){
                        global.isReceiptFromContainer=true;
                        Navigator.push(context, PageTransition(
                            duration: Duration(milliseconds: 500),
                            alignment: Alignment.center,
                            curve: Curves.easeOut,
                            type: PageTransitionType.leftToRight,
                            child: ReceiptScreen(listofFoodItems: listofFoodItems,listOfExtraBillDetails: listOfExtraBillDetails,subTotal: receiptSubtotal,totalAmount: global.receiptAmount,receiptType: receiptType,receiptDescription: receiptDescription,)));
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                        child: new Row(
                          children: <Widget>[
                            Opacity(
                              opacity: 0.6,
                              child:Text("\$"+global.formatter.format(global.receiptAmount).toString(), style: TextStyle(fontSize:  global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            ),
                            Container(
                              width:25,
                              height: 25,
                              child:Image.asset(
                                  'assets/receipt_symbol.png',
                                  fit: BoxFit.contain
                              ),
                            ),
                          ],
                        ),
                      )
                  ):new Container(width: 0,height: 0,)
              )
            ],
            backgroundColor: global.appbarBackColor),
        body: receivedResponse?RefreshIndicator(
          color: global.mainColor,
          child: Container(
            color: global.whitecolor,
            child: CustomScrollView(
              scrollDirection: Axis.vertical,
              shrinkWrap: false,
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      new Row(
                        children: <Widget>[
                          new Flexible(
                            flex:1,
                            child:Hero(
                              tag:'receiptinfo',
                              child: Container(
                                decoration: BoxDecoration(
                                    color: global.whitecolor,
                                    shape: BoxShape.rectangle,
                                    boxShadow: [
                                      BoxShadow(
                                          color:Color.fromRGBO(0, 0, 0, 0.15),
                                          blurRadius: 1.0,
                                          offset: Offset(0.0, 1.0)
                                      ),]
                                ),
                                margin: EdgeInsets.fromLTRB(0,0,0,0),
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.width<600?(MediaQuery.of(context).size.height<800?MediaQuery.of(context).size.height/7:MediaQuery.of(context).size.height/8):MediaQuery.of(context).size.height/10,
                                  width: MediaQuery.of(context).size.width,
                                  child:Padding(
                                    padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        new Flexible(
                                            flex:1,
                                            fit:FlexFit.loose,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7.5:MediaQuery.of(context).size.width/10,
                                              height:MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7.5:MediaQuery.of(context).size.width/10,
                                              decoration: new BoxDecoration(
                                                color:  global.imageBackColor,
                                                image: new DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: NetworkImage( global.receiptImageURL),
                                                ),
                                                border: Border.all(color: global.imageBackColor, width: 1.0,),
                                                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                              ),
                                            )),
                                        new Flexible(
                                            flex:2,
                                            fit:FlexFit.tight,
                                            child: Container(
                                              child: new Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Material(
                                                    type: MaterialType.transparency,
                                                    child: Text(global.hotelname,
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(fontSize: global.font18,color: global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),

                                                  ),
                                                  Opacity(
                                                      opacity: 0.6,
                                                      child:Material(
                                                        type: MaterialType.transparency,
                                                        child: Text("Bill#12345678",
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,
                                                            style: TextStyle(fontSize: global.font14,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                      )
                                                  ),
                                                ],
                                              ),

                                            )),
                                        global.finalUserList.length>0?Container(
                                          height: 50,
                                          decoration: new BoxDecoration(
                                            color: Colors.white,
                                            border: Border(
                                              left: BorderSide(
                                                color:Color(0xff979797),
                                                width: 1.0,
                                              ),
                                            ),
                                          ),
                                        ):new Container(width: 0, height: 0,),
                                        global.finalUserList.length>0?new Flexible(
                                          flex:1,
                                          child: Container(
                                            child: new Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Material(
                                                  type: MaterialType.transparency,
                                                  child:
                                                  Text(SettledUserCount.toString()+"/"+global.finalUserList.length.toString(), style: TextStyle(fontSize: global.font18,color:Color(0xff47A90C),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                )
                                              ],
                                            ),
                                          ),
                                        ):new Container(width: 0,height: 0,)
                                      ],
                                    ),

                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                receivedResponse?isSplitGroupPresent?SliverList(delegate: SliverChildListDelegate(
                    [
                      Container(
                        child: new Column(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                !isSettled && !isAdmin?new Flexible(
                                  flex: 2,
                                  fit: FlexFit.tight,
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(30, 19, 0, 19),
                                    child: new Text("Split not confirmed yet by Admin", style: TextStyle(fontSize: global.font15,color:Color(0xffE02020),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ),
                                ):new Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(30, 19, 20, 19),
                                    child: splitType.toString().contains("equal")?new Row(
                                      children: <Widget>[
                                        new Text("Split Equally", style: TextStyle(fontSize: global.font15,color:Color(0xff000000),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                        Opacity(
                                            opacity:0.6,
                                            child:new Text(" (Default)", style: TextStyle(fontSize: global.font12,color:Color(0xff000000),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'))
                                        )
                                      ],
                                    ):
                                    new Text("Split by "+splitType, style: TextStyle(fontSize: global.font15,color:Color(0xff000000),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ),
                                ),
                                new Flexible(
                                  flex:1,
                                  fit:FlexFit.tight,
                                  child: !isSettled && isAdmin?
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      GestureDetector(
                                          onTap: (){
                                            Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => EditBillSplitActivity(receiptAmount:global.receiptAmount,groupname:global.grpname,sel_contact_list: global.finalUserList,spliTypeInt:splitTypeInt),
                                              ),
                                            );
                                          },
                                          child:
                                          new Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                child: new Container(
                                                    width: 15,
                                                    height: 15,
                                                    child: Image.asset(
                                                        'assets/edit.png',
                                                        fit: BoxFit.contain
                                                    )
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.fromLTRB(5, 0, 15, 0),
                                                child: new Text("Edit", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                              ),
                                            ],
                                          )
                                      )
                                    ],
                                  ):isSettled?new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: (){
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => SplitDetailsActivity(receiptAmount:global.receiptAmount,groupname:global.grpname,sel_contact_list: global.finalUserList,fragmentIndex: splitTypeInt,),
                                            ),
                                          );
                                        },
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: new Container(
                                                  width: 15,
                                                  height: 15,
                                                  child: Image.asset(
                                                      'assets/details.png',
                                                      fit: BoxFit.contain
                                                  )
                                              ),
                                            ),
                                            new Container(
                                              padding: EdgeInsets.fromLTRB(5, 0, 15, 0),
                                              child: new Text("Details", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ):new Container(
                                    width: 0,
                                    height: 0,
                                  ),
                                )
                              ],
                            ),
                            new Container(
                              padding: EdgeInsets.fromLTRB(10,0, 10,10),
                              decoration: new BoxDecoration(
                                color: global.whitecolor,
                                border: Border(
                                  bottom: BorderSide(color: Color(0xffdcdcdc), width: 1.0,),
                                ),
                              ),
                              child:new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Flexible(
                                    flex:2,
                                    fit:FlexFit.loose,
                                    child:new Container(
                                        margin: EdgeInsets.all(3),
                                        child: CircleAvatar(
                                          child: Text("i",style: TextStyle(color:new Color(0xffffffff)),),
                                          backgroundColor: global.whitecolor,
                                        )
                                    ),
                                  ),
                                  Flexible(
                                      flex: 11,
                                      fit: FlexFit.tight,
                                      child:new Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  new Flexible
                                                    (
                                                      flex:6,
                                                      fit:FlexFit.tight,
                                                      child:new Container(
                                                        padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                        child: new Text("Share holder", style: TextStyle(fontSize: global.font12,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                      )
                                                  ),
                                                  new Flexible
                                                    (
                                                      flex:1,
                                                      fit:FlexFit.loose,
                                                      child:new Container(
                                                        width: global.nontapitsymbolSize,
                                                        height: global.nontapitsymbolSize,
                                                      )
                                                  ),
                                                  new Flexible
                                                    (
                                                      flex:6,
                                                      fit:FlexFit.tight,
                                                      child:new Row(
                                                        children: <Widget>[
                                                          !isSettled && !isAdmin?new Container(width: 0,height: 0,):new Flexible
                                                            (
                                                            flex: 1,
                                                            fit: FlexFit.tight,
                                                            child: new Row
                                                              (
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              children: <Widget>[
                                                                new Container(
                                                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                  child: new Text("Paid", style: TextStyle(fontSize: global.font12,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          !isSettled && !isAdmin?new Container(width: 0,height: 0,):new Flexible
                                                            (
                                                            flex: 1,
                                                            fit: FlexFit.tight,
                                                            child: new Column
                                                              (
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              crossAxisAlignment: CrossAxisAlignment.end,
                                                              children: <Widget>[
                                                                new Container(
                                                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                  child: new Text("Balance", style: TextStyle(fontSize: global.font12,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                  ),
                                                ])
                                          ]
                                      )
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ]
                )
                ):SliverList(delegate: SliverChildListDelegate([
                  new Container(
                    color:global.whitecolor,
                    height: MediaQuery.of(context).size.width<600?(MediaQuery.of(context).size.height-(MediaQuery.of(context).size.height/7)*4):(MediaQuery.of(context).size.height/10-(MediaQuery.of(context).size.height/10)*4),
                    width: MediaQuery.of(context).size.width,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text("You can split this bill with a group",style: new TextStyle(
                            fontSize: global.font15,
                            color: Color.fromRGBO(0, 0, 0, 0.87),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'BalooChetanRegular'
                        )
                        ),
                        new FlatButton(
                          padding:EdgeInsets.symmetric(vertical: 8.0, horizontal: 35.0),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    GroupsActivity(source:0),
                              ),
                            ).then((value) => {
                              getReceipt()
                            });
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16.0),
                              side: BorderSide(color: global.mainColor,width: 1.5)
                          ),
                          color: global.whitecolor,
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          child: Text('ADD SPLIT GROUP', style: TextStyle(fontSize: global.font15, color: Color(0xffF57C00),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanMedium')),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width/6,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                color: global.appbargreycolor,
                                width: 2.0,
                              ),
                            ),
                          ),
                        ),
                        Container(
                            child:new Column(
                              children: <Widget>[
                                Opacity(
                                  opacity:0.7,
                                  child:Text("Settlement modes for members",style: new TextStyle(
                                      fontSize: global.font15,
                                      color: Color.fromRGBO(0, 0, 0, 0.87),
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'BalooChetanMedium'
                                  )
                                  ),
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex:1,
                                      fit: FlexFit.loose,
                                      child:Container(
                                        child:Image.asset(
                                            'assets/venmo.png',
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      flex:1,
                                      fit: FlexFit.loose,
                                      child: Container(
                                        child:Image.asset(
                                            'assets/gpay.png',
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      flex:1,
                                      fit: FlexFit.loose,
                                      child: Container(
                                        child:Image.asset(
                                            'assets/samsungpay.png',
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                        flex:1,
                                        fit: FlexFit.loose,
                                        child:Container(
                                          child:Image.asset(
                                              'assets/applepay.png',
                                              fit: BoxFit.contain
                                          ),
                                        )
                                    )
                                  ],
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex:1,
                                      fit: FlexFit.loose,
                                      child:Container(
                                        child:Image.asset(
                                            'assets/paytm.png',
                                            fit: BoxFit.contain
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Flexible(
                                        flex:1,
                                        fit: FlexFit.loose,
                                        child:Container(
                                          child:Image.asset(
                                              'assets/phonepe.png',
                                              fit: BoxFit.contain
                                          ),
                                        )
                                    )
                                  ],
                                ),
                              ],
                            )
                        ),
                      ],
                    ),
                  )
                ]),) :SliverList(delegate: SliverChildListDelegate(
                    [new Container(color:global.whitecolor,width: 0,height: 0,)])),
                receivedResponse?(!isAdmin?SliverList(
                    delegate: SliverChildBuilderDelegate((context, index)
                    {
                      return Flex(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            child: ListForEditContainerMember(index:index,userAmountData:global.finalUserList[index],profileImg: global.imageList[index],isSettledSplit:isSettled,isSel: (bool value)  {
                              getReceipt();
                            },),
                          ),
                        ],
                      );
                    }, childCount: global.finalUserList.length)
                ):SliverList(
                    delegate: SliverChildBuilderDelegate((context, index)
                    {
                      return isSettled?
                      Flex(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            child: ListForEditContainer(index:index,userAmountData:global.finalUserList[index],profileImg: global.imageList[index],isSettledSplit:isSettled,isSel: (bool value)  {
                              getReceipt();
                            },),
                          ),
                        ],
                      ):Flex(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            child: ListForEditContainer(index:index,userAmountData:global.finalUserList[index],profileImg: global.imageList[index],isSettledSplit:isSettled,isSel: (bool value)  {
                              getReceipt();
                            },),
                          ),
                        ],
                      );
                    }, childCount: global.finalUserList.length)
                )):SliverList(delegate: SliverChildListDelegate(
                    [new Container(color:global.whitecolor,width: 0,height: 0,)])),
                receivedResponse?(isPayee && isSettled && !isPayeeSettled?
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        child: new Column(
                          children: <Widget>[
                            payeeBalAmt<0? (Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: global.whitecolor,
                                border: Border.all(
                                    color: global.mainColor,
                                    width: 1.0),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5.0)),
                              ),
                              margin: EdgeInsets.fromLTRB(20,20,20,20),
                              padding: EdgeInsets.fromLTRB(5,2,5,2),
                              child: FlatButton(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                child: Text('Send reminder to admin',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                textColor: global.mainColor,
                                onPressed: () {
                                  _showSnackBar(context,"REMIND");
                                },
                              ),
                            )
                            ):(!isPayeeRejected?new Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: global.whitecolor,
                                border: Border.all(color: global.rejectButtoncolor, width: 1.0),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5.0)),
                              ),
                              margin: EdgeInsets.fromLTRB(20,20,20,20),
                              padding: EdgeInsets.fromLTRB(5,2,5,2),
                              child: FlatButton(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                child: Text('Reject admin',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                textColor: global.rejectButtoncolor,
                                onPressed: () {
                                  _showSnackBar(context,"REJECT");
                                },
                              ),):new Container(
                                width:0,
                                height:0
                            )),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: payeeBalAmt>0 && isPayeeRejected?global.whitecolor:global.payupButtoncolor,
                                border: Border.all(
                                    color: payeeBalAmt>0 && isPayeeRejected?global.whitecolor:global.payupButtoncolor,
                                    width: 1.0),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5.0)),
                              ),
                              padding: EdgeInsets.fromLTRB(5,2,5,2),
                              margin: payeeBalAmt>0 && isPayeeRejected?EdgeInsets.fromLTRB(0,0,0,0):EdgeInsets.fromLTRB(20,0,20,20),
                              child: FlatButton(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                child: payeeBalAmt<0?(Text(
                                  'Settle with admin',
                                  style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),
                                )):(!isPayeeRejected?Text(
                                  'PayUp to admin',
                                  style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),
                                ):new Container(color: global.whitecolor, height: 0, width: 0,
                                )
                                ),
                                textColor: global.whitecolor,
                                onPressed: () {
                                  if(payeeBalAmt<0)
                                  {
                                    _showSnackBar(context, "SETTLE");
                                  }
                                  else
                                  {
                                    _showSnackBar(context, "PAYUP");
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ):SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      isSplitGroupPresent?Container(color: global.whitecolor, height: 0, width: 0,):Container(color: global.whitecolor, height: 0, width: MediaQuery.of(context).size.width,
                      )
                    ],
                  ),
                )):SliverList(
                    delegate: SliverChildListDelegate(
                        [
                          new Container(color:global.whitecolor,width: 0,height: 0,)
                        ])
                ),
              ],
            ),
          ),
          onRefresh: getReceipt,
        ):new Container(
            child:Center(
              child:new Container(
                height: 50,
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                  backgroundColor: global.mainColor,
                  strokeWidth: 5,),
              ),
            )
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.all(0.0),
          child:receivedResponse?(isSplitGroupPresent?
          isAdmin?(
              !isSettled?new Container(
                width:MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                      colors: global.buttonGradient,
                      tileMode: TileMode.clamp
                  ),
                ),

                child: FlatButton(
                  child: Text("REQUEST PAYMENTS",style: TextStyle(fontSize:global.font14,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                              shape: RoundedRectangleBorder(borderRadius:
                              BorderRadius.circular(10.0)),
                              child: new Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          new Container(
                                              padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                                              child:new Column(
                                                children: <Widget>[
                                                  new Row(
                                                    children: <Widget>[
                                                      Flexible(
                                                        flex:1,
                                                        child:new Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: <Widget>[
                                                            new Text("Confirmation", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                          ],
                                                        ),
                                                      ),
                                                      Flexible(
                                                        flex:1,
                                                        child:new Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: <Widget>[
                                                            Container(
                                                                width: 20,
                                                                height: 20,
                                                                child: GestureDetector(
                                                                  onTap: (){
                                                                    Navigator.of(context).pop();
                                                                  },
                                                                  child: Image.asset(
                                                                    'assets/black_cross_icon.png',
                                                                    fit: BoxFit.contain,
                                                                    color:Color.fromRGBO(0, 0, 0, 0.3),
                                                                  ),
                                                                )
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(height: 20),
                                                  new Text("This means you are sending a payment link to the participants to settle their balances.", style: TextStyle(fontSize: global.font16,color:global.lightgreytextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                  SizedBox(height: 17),
                                                  new Text("You can't go back and edit the group members/split later.", style: TextStyle(fontSize: global.font16,color:global.lightgreytextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                  SizedBox(height:17),
                                                  new Text("Once the members payup, you can settle other payees.", style: TextStyle(fontSize: global.font16,color:global.lightgreytextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                  SizedBox(
                                                    height: 25,
                                                  ),
                                                ],
                                              )
                                          ),
                                          new Container(
                                            width: MediaQuery.of(context).size.width,
                                            decoration: new BoxDecoration(
                                              color: Colors.white,
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: Color(0xffdcdcdc),
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                          new Row(
                                            children: <Widget>[
                                              new Flexible(
                                                  flex: 1,
                                                  fit: FlexFit.tight,
                                                  child:new Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      FlatButton(
                                                        child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                        onPressed: (){
                                                          Navigator.of(context).pop();
                                                        },
                                                      )
                                                    ],
                                                  )
                                              ),
                                              new Flexible(
                                                  flex: 1,
                                                  fit: FlexFit.tight,
                                                  child:new Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      FlatButton(
                                                        child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                        onPressed: () async {
                                                          bool flag=await requestPayment();
                                                          if(flag)
                                                          {
                                                            setState(()
                                                            {
                                                              isSettled = true;
                                                              global.isSettled = true;
                                                            });
                                                            Navigator.of(context).pop();
                                                            dialog_circular_progressbar();
                                                          }
                                                          else
                                                          {
                                                            Navigator.of(context).pop();
                                                            global.helperClass.showAlertDialog(mContext, "Error", "Can't settle your request for now, Try again later");
                                                          }
                                                        },
                                                      )
                                                    ],
                                                  )
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          );
                        });
                  },
                ),
              ): new Container(color:global.whitecolor,width: 0,height: 0,))
              :new Container(color:global.whitecolor,width: 0,height: 0,):
          new Container(
            color: global.whitecolor,
            height: MediaQuery.of(context).size.width<600?(MediaQuery.of(context).size.height/7)*2:(MediaQuery.of(context).size.height/10)*2,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
                'assets/footer.png',
                fit: BoxFit.fill
            ),
          )
          ) :new Container(color:global.whitecolor,width: 0,height: 0,),
        ),
      ),
    );
  }
}
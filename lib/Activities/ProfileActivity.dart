import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/AlertTypeList.dart';
import 'package:tapit/helpers/Constants.dart';
import 'package:tapit/helpers/NotificationPrefList.dart';
import 'package:tapit/helpers/GenderRadio.dart';
import 'package:tapit/helpers/UserDetails.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class ProfileActivity extends StatefulWidget
{
  ProfileActivityState createState()=> ProfileActivityState();
}

class ProfileActivityState extends State<ProfileActivity>
{
  BuildContext mContext;
  final emailController = TextEditingController();
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final mobnoController = TextEditingController();
  final genderController = TextEditingController();
  final birthdayController=TextEditingController();

  bool firstname_validate = false;
  bool lastname_validate = false;
  bool mobileno_validate = false;
  bool emailadd_validate = false;
  bool gender_validate = false;
  bool birthday_validate=false;

  bool show_missing_text=false;
  SharedPreferences prefs;
  List<NotificationPreferencesList> notiPrefList=new List();
  String firstname,lastname,emailaddress,mobileNo,gender,birthday;

  Uint8List tempimg;
  String initialCountry;
  File userSelectedImg;
  bool isImageRemoved=false;
  String emailVerified="";
  String userID;
  String image;
  String cc;
  String ccName;
  String fcmToken;
  bool offerNoti=false;
  File tempfile;
  bool isResponsereceived=true;

  double cameraWidth = 70.0;
  double cameraHeight=70.0;
  double initial_curve=50.0;
  String userimage="";
  String LOGTAG="ProfileActivity";
  var selectedImage;

  @override
  void initState(){
    getData();
    super.initState();
  }

  void CameraImage() async
  {
    await ImagePicker.pickImage(source: ImageSource.camera).then((value) => ({
      selectedImage=value,
      tempfile=selectedImage,
      setState(()
      {
        userSelectedImg=tempfile;
        cameraWidth=70;
        cameraHeight=70;
        initial_curve=50;
        isImageRemoved=true;
      }),
    })).catchError((onError){
      setState(()
      {
        selectedImage=null;
        userSelectedImg=null;
        cameraWidth=70;
        cameraHeight=70;
        initial_curve=50;
        isImageRemoved=true;
        if(firstname!=null && firstname.isNotEmpty)
        {
          userimage = firstname.substring(0, 1);
        }

        if(lastname!=null && lastname.isNotEmpty){
          userimage = userimage + lastname.substring(0, 1);
        }
      });
    });
    Navigator.pop(mContext);
  }

  void GalleryImage() async
  {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((value) => {
      selectedImage=value,
      tempfile=selectedImage,
      setState(()
      {
        userSelectedImg=tempfile;
        cameraWidth=70;
        cameraHeight=70;
        initial_curve=50;
        isImageRemoved=true;
      }),
    }).catchError((onError){
      setState(()
      {
        selectedImage=null;
        userSelectedImg=null;
        cameraWidth=70;
        cameraHeight=70;
        initial_curve=50;
        isImageRemoved=true;
        if(firstname!=null && firstname.isNotEmpty)
        {
          userimage = firstname.substring(0, 1);
        }

        if(lastname!=null && lastname.isNotEmpty){
          userimage = userimage + lastname.substring(0, 1);
        }
      });
    });

    Navigator.pop(mContext);
  }

  void removeImage() async
  {
    setState(()
    {
      selectedImage=null;
      userSelectedImg=null;
      cameraWidth=70;
      cameraHeight=70;
      initial_curve=50;
      isImageRemoved=true;
      if(firstname!=null && firstname.isNotEmpty)
      {
        userimage = firstname.substring(0, 1);
      }
      if(lastname!=null && lastname.isNotEmpty){
        userimage = userimage + lastname.substring(0, 1);
      }
    });
    Navigator.pop(mContext);
  }

  Future<void> getData()async
  {
    prefs = await SharedPreferences.getInstance();
    userID=prefs.getString("UserID");
    fcmToken=prefs.getString("FCMToken");
    firstname=prefs.getString("CurrentFirstName");
    lastname=prefs.getString("CurrentLastName");
    mobileNo=prefs.getString("MobileNo");
    emailaddress=prefs.getString("email");
    emailVerified=prefs.getString("emailVerified");
    birthday=prefs.getString("BirthDate");
    gender=prefs.getString("Gender");
    image=prefs.getString("UserImage");
    cc= prefs.getString("countryCode");
    ccName= prefs.getString("countryCodeName");
    offerNoti=prefs.getBool("offerNoti");

    if(offerNoti==null)
    {
      setState(()
      {
        offerNoti=false;
      });
    }

    if(birthday!=null)
    {
      setState(()
      {
        birthday=birthday.substring(0,10);
        birthdayController.text=birthday;
      });
    }

    if(gender!=null)
    {
      setState(()
      {
        genderController.text=gender;
      });
    }

    if(emailVerified==null || emailVerified.isEmpty)
    {
      emailVerified="false";
    }

    setState(() {
      initialCountry=ccName;
    });

    if(image==null || image.isEmpty)
    {
      userSelectedImg=null;
      if(firstname!=null && firstname.isNotEmpty)
      {
        userimage = firstname.substring(0, 1);
      }
      if(lastname!=null && lastname.isNotEmpty)
      {
        userimage = userimage + lastname.substring(0, 1);
      }

    }
    else
    {
      Uint8List MainprofileImg=base64Decode(image);
      final tempDir = await getTemporaryDirectory();
      final file = await new File('${tempDir.path}/image'+DateTime.now().millisecondsSinceEpoch.toString()+'.jpg').create();
      file.writeAsBytesSync(MainprofileImg);
      setState(()
      {
        userSelectedImg=file;
      });
    }

    if(firstname==null || firstname.isEmpty)
    {
    }
    else
    {
      firstnameController.text = firstname;
    }

    if(lastname==null || lastname.isEmpty)
    {
    }
    else
    {
      lastnameController.text = lastname;
    }

    if(mobileNo==null || mobileNo.isEmpty)
    {
    }
    else
    {
      mobnoController.text = mobileNo;
    }

    if(emailaddress==null || emailaddress.isEmpty)
    {
    }
    else
    {
      emailController.text = emailaddress;
    }

    NotificationPreferencesList notificationPreferencesList=new NotificationPreferencesList(title:"App/Bill split activity",isSelected: true );
    NotificationPreferencesList notificationPreferencesList1=new NotificationPreferencesList(title:"Offers/Promotions",isSelected: offerNoti );

    setState(() {
      notiPrefList.add(notificationPreferencesList);
      notiPrefList.add(notificationPreferencesList1);
    });

  }

  Future<void> updateImage()async
  {
    bool getImageFlag=false;
    if(selectedImage!=null)
    {
      getImageFlag = await getImage();
    }

    if (isImageRemoved)
    {
      prefs.remove("UserImage");
      if (tempfile != null)
      {
        Uint8List temp_img = tempfile.readAsBytesSync();
        String avatar_string = base64Encode(temp_img);
        prefs.setString("UserImage", avatar_string);
      }
      else
      {
        String avatar_string = null;
        prefs.setString("UserImage", avatar_string);
      }
    }
  }

  Future<int> sendFileRestAPI(String url, File file) async
  {
    var len = await file.length();
    Response response;
    response = await http.put(url, body: file.readAsBytesSync());
    return response.statusCode;
  }

  Future<bool> getImage() async
  {
    Future<bool> flag=Future<bool>.value(false);
    String fileName = selectedImage.path.split('/').last;
    UserService userService=new UserService();
    ResponseBean response=await userService.getImageMethod(fileName.toString(),global.UserID);
    if(response!=null)
    {
      int statusCode = response.status;

      var payload = response.payLoad;
      var decoded_payload = Uri.decodeFull(payload);
      if (statusCode == 200)
      {
        flag = Future<bool>.value(true);
        await uploadImage(decoded_payload);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    else
    {
      global.helperClass.showAlertDialog(mContext, "Error", "Not able to update profile information.");
    }
    return flag;
  }

  Future<bool> uploadImage(String awsurl) async
  {
    Future<bool> flag=Future<bool>.value(false);
    var statusCode=await sendFileRestAPI(awsurl, selectedImage);
    if(statusCode==200)
    {
      flag=Future<bool>.value(true);
      await setImage();
    }
    else
    {
      flag=Future<bool>.value(false);
    }
    return flag;
  }

  Future<bool> setImage() async
  {
    Future<bool> flag=Future<bool>.value(false);
    String fileName = selectedImage.path.split('/').last;
    updateImageObject uimgObj=new updateImageObject(global.UserID,fileName);
    var jsonBody=jsonEncode(uimgObj);

    UserService userService=new UserService();
    ResponseBean response=await userService.putImageMethod(jsonBody);
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    else
    {
      global.helperClass.showAlertDialog(mContext, "Error", "Not able to update profile information.");
    }
    return flag;
  }



  Future<void> updateProfile() async
  {
    setState(() {
      show_missing_text=false;
      firstname_validate = false;
      lastname_validate = false;
      mobileno_validate = false;
      emailadd_validate = false;
      gender_validate=false;
      birthday_validate=false;

      firstname = firstnameController.text;
      lastname = lastnameController.text;
      mobileNo = mobnoController.text;
      emailaddress = emailController.text;
      gender=genderController.text;
      birthday=birthdayController.text;

      if (firstname.isEmpty || firstname=="")
      {
        show_missing_text=true;
        firstname_validate = true;
      }
      if(lastname.isEmpty || lastname=="")
      {
        show_missing_text=true;
        lastname_validate = true;
      }
      if(mobileNo.isEmpty || mobileNo=="")
      {
        show_missing_text=true;
        mobileno_validate = true;
      }
      if(emailaddress.isEmpty || emailaddress=="")
      {
        show_missing_text=true;
        emailadd_validate=true;
      }
      if(gender.isEmpty || gender=="")
      {
        show_missing_text=true;
        gender_validate=true;
      }
      if(birthday.isEmpty || birthday=="")
      {
        show_missing_text=true;
        birthday_validate=true;
      }
    });
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailaddress);

    if(emailadd_validate==false && mobileno_validate==false &&  lastname_validate==false && firstname_validate==false && gender_validate==false && birthday_validate==false)
    {
      if (!emailValid)
      {
        emailadd_validate = true;
      }
      else
      {
        setState(()
        {
          isResponsereceived=false;
        });

        String username=firstname+" "+lastname;

        List<AlertTypeList> alertSettingTypeList=new List();
        AlertTypeList alertTypeList=new AlertTypeList("BILL_SPLIT",true,true,true,true);
        alertSettingTypeList.add(alertTypeList);
        if(offerNoti)
        {
          AlertTypeList alertTypeList=new AlertTypeList("OFFERS",true,true,true,true);
          alertSettingTypeList.add(alertTypeList);
        }

        String emailVerifiedStatus;
        if(emailVerified.toString().compareTo("pending")==0)
        {
          emailVerifiedStatus="false";
        }
        else
        {
          emailVerifiedStatus=emailVerified;
        }

        birthday=birthday+"T00:00:00.0+0000";
        UserDetails userDetails=new UserDetails(id:userID,name:username,picture:"",email:emailaddress,mobileNo:mobileNo,countryCode:cc,deviceType:Constants.BASE_PLATFORM,fcmToken:fcmToken,emailVerified:emailVerifiedStatus,mobileVerified:"true",gender:gender,dob:birthday,alertSettingTypeList:alertSettingTypeList);
        var jsonBody=jsonEncode(userDetails);
        UserService userService=new UserService();
        ResponseBean response=await userService.UpdateUserData(jsonBody);

        if(response!=null)
        {
          int statusCode = response.status;
          if (statusCode == 200 || statusCode == 201)
          {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.remove("CurrentFirstName");
            prefs.remove("CurrentLastName");
            prefs.remove("email");
            prefs.remove("BirthDate");
            prefs.remove("Gender");
            prefs.remove("offerNoti");
            prefs.setString("CurrentFirstName", firstname);
            prefs.setString("CurrentLastName", lastname);
            prefs.setString("email", emailaddress);
            prefs.setString("BirthDate", birthday);
            prefs.setString("Gender", gender);
            prefs.setBool("offerNoti", offerNoti);

            await updateImage();
            setState(() {
              notiPrefList[1].isSelected=offerNoti;
              isResponsereceived = true;
            });

            global.helperClass.showAlertDialog(mContext, "Success", "Profile updated successfully");
          }
          else if (statusCode == 204) {
            global.helperClass.showAlertDialog(mContext, "Error", "No Content");
          }
          else if (statusCode == 400) {
            global.helperClass.showAlertDialog(mContext, "Error", "Bad Request");
          }
          else if (statusCode == 404) {
            global.helperClass.showAlertDialog(mContext, "Error", "Not Found");
          }
          else if (statusCode == 500) {
            global.helperClass.showAlertDialog(mContext, "Error", "Internal Server Error");
          }
        }
        else
        {
          global.helperClass.showAlertDialog(mContext, "Error", "Not able to update profile information.");
          setState(() {
            isResponsereceived = true;
          });
        }
      }
    }
  }

  Future<Null> show_dialog(BuildContext context)
  {
    setState(() {
      gender_validate=false;
    });

    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          final textController = TextEditingController();
          String comment="";
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                        child:new Column(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Text("Please select gender", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                              ],
                            ),
                            SizedBox(height: 20),
                            GenderRadio(isSel: (String value)
                            {
                              setState(() {
                                genderController.text=value;
                              });

                            },)
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<Null> _selectDate(BuildContext context) async
  {
    DateTime selectedDate;
    if(birthdayController.text!=null && birthday!=null && birthday.isNotEmpty)
    {
      selectedDate = new DateFormat("yyyy-MM-dd").parse(birthday);
    }
    else
    {
      selectedDate=DateTime(new DateTime.now().year-13);
    }

    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1950, 1),
      lastDate: DateTime(new DateTime.now().year-13),
      helpText: " ",
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData(primarySwatch: Colors.orange),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        birthday_validate=false;
        DateFormat formatter2 = DateFormat('yyyy-MM-dd');
        birthday=formatter2.format(picked).toString();
        birthdayController.value = TextEditingValue(text: formatter2.format(picked).toString());
      });
  }
  Future<bool> _onBackPressed()
  {
    Navigator.of(mContext).pop();
  }

  @override
  Widget build(BuildContext context) {

    mContext=context;
    final genderField = TextField(
      enabled: true,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: genderController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration: !gender_validate?InputDecoration(
        filled: true,
        fillColor: Color(0xffF7F7F7),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Gender",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Gender",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );

    final birthdayField = TextField(
      enabled: true,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: birthdayController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration: !birthday_validate?InputDecoration(
        filled: true,
        fillColor: Color(0xffF7F7F7),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Your Birthday",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Your Birthday",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );

    final firstnameField = TextField(
      onTap: () {
        setState(() {
          firstname_validate = false;
        });
      },
      cursorColor: global.mainColor,
      obscureText: false,
      controller: firstnameController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration: !firstname_validate?InputDecoration(
        filled: true,
        fillColor: Color(0xffF7F7F7),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "First name",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "First name",
        hintStyle:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );

    final lastnameField = TextField(
      onTap: () {
        setState(() {
          lastname_validate = false;
        });
      },
      cursorColor: global.mainColor,
      obscureText: false,
      controller: lastnameController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration:!lastname_validate? InputDecoration(
        filled: true,
        fillColor: Color(0xffF7F7F7),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Last name",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Last name",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );

    final mobilenoField = TextField(

      enabled: false,
      keyboardType: TextInputType.number,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: mobnoController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration: !mobileno_validate?InputDecoration(
        suffixIcon:  Container(
          child: Image.asset(
            'assets/account_verified.png',
          ),
        ),
        filled: true,
        fillColor: Color(0xffF7F7F7),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Mobile number",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width: 0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Mobile number",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );

    final emailaddressField = TextField(
      onTap: () {
        setState(() {
          emailadd_validate = false;
        });
      },
      cursorColor: global.mainColor,
      obscureText: false,
      controller: emailController,
      style:TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
      decoration: !emailadd_validate?InputDecoration(
        suffixIcon:  emailVerified.toString().compareTo("true")==0?Container(
          child: Image.asset(
            'assets/account_verified.png',
          ),
        ):Container(
          child: Image.asset(
            'assets/acc_ver_pending.png',
          ),
        ),
        filled: true,
        fillColor: Color(0xffF7F7F7),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffc8c8c8), width: 0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Email address",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffBF4C4C), width: 0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc8c8c8), width:0.75),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        hintText: "Email address",
        hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );
    // TODO: implement build
    return WillPopScope(
      onWillPop: _onBackPressed,
      child:Scaffold(
        appBar: AppBar(
            titleSpacing: 0.0,
            automaticallyImplyLeading: false,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: ()
                  {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    padding:  EdgeInsets.fromLTRB(15,0,0,0),
                    child: Image.asset(
                      'assets/back_arrow.png',
                      fit: BoxFit.contain,
                      height: 20,
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child:  Text("Profile",style: new TextStyle(
                        fontSize: global.font18,
                        color: global.appbarTextColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'BalooChetanSemiBold')
                    )
                )
              ],
            ),
            backgroundColor: global.appbarBackColor),
        body: isResponsereceived?Container(
          color: global.whitecolor,
          child: new Container(
            color: global.whitecolor,
            child: CustomScrollView(
              scrollDirection: Axis.vertical,
              shrinkWrap: false,
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      new Container(
                          height:MediaQuery.of(context).size.width,
                          width:MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(20),
                          child: new Container(
                            decoration: userSelectedImg!=null?new BoxDecoration(
                              color: global.imageBackColor,
                              image:  DecorationImage(
                                image:  FileImage(File(userSelectedImg.path)),
                                fit: BoxFit.cover,
                              ),
                              border: Border.all(
                                color: global.whitecolor,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(20.0)),
                            ):userimage!=null?new BoxDecoration(
                              color: global.adminBackcolor,
                              border: Border.all(
                                color: global.whitecolor,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(20.0)),
                            ):new BoxDecoration(
                              color: global.introRoundColorGrey,
                              border: Border.all(
                                color: global.whitecolor,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(20.0)),
                            ),
                            child: Stack(
                              children: <Widget>[
                                userSelectedImg==null && userimage!=null?Center(
                                  child:new Text(userimage,style: TextStyle(fontSize: 45,color:global.adminTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                ):new Container(width: 0,height: 0,),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Opacity(
                                      opacity: 0.8,
                                      child: new AnimatedContainer(
                                        width: cameraWidth,
                                        height: cameraHeight,
                                        duration: Duration (seconds: 1),
                                        curve:  Curves.easeOutCirc,
                                        decoration: new BoxDecoration(
                                          color: Color(0xFFF57C00),
                                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(0),bottomRight:  Radius.circular(20),topLeft:  Radius.circular(initial_curve),topRight:  Radius.circular(0)),
                                        ),
                                        child: IconButton(
                                            padding: EdgeInsets.fromLTRB(15, 15 , 15,15 ),
                                            alignment: Alignment.bottomRight,
                                            icon: Icon(Icons.camera_alt),
                                            color: global.whitecolor,
                                            disabledColor: Colors.orangeAccent,
                                            highlightColor: Colors.orangeAccent,
                                            onPressed: () {

                                              setState(() {
                                                cameraWidth=MediaQuery.of(context).size.width-100;
                                                cameraHeight=300;
                                                initial_curve=500;
                                              });

                                              showModalBottomSheet(
                                                  isDismissible: false,
                                                  context: context,
                                                  builder: (BuildContext bc)
                                                  {
                                                    return new Wrap(
                                                      children: <Widget>[
                                                        new Column(
                                                          children: <Widget>[
                                                            new Container(
                                                              color:global.whitecolor,
                                                              child: new Row(
                                                                children: <Widget>[
                                                                  Flexible(
                                                                      child: GestureDetector(
                                                                        onTap:GalleryImage,
                                                                        child: new Column(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: <Widget>[
                                                                            new Container(
                                                                              height:50,
                                                                              width:50,
                                                                              margin: EdgeInsets.fromLTRB(20,25,30,5),
                                                                              decoration: new BoxDecoration(
                                                                                color: global.whitecolor,
                                                                                border: Border.all(color: global.whitecolor, width: 1.0),
                                                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                                              ),
                                                                              child: Image.asset(
                                                                                  'assets/gallery.png',
                                                                                  fit: BoxFit.cover
                                                                              ),
                                                                            ),
                                                                            new Container(
                                                                              margin: EdgeInsets.fromLTRB(20,0,30,0),
                                                                              child: new Text('Gallery',style: TextStyle(fontSize: global.font12,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      )
                                                                  ),
                                                                  Flexible(
                                                                      child: GestureDetector(
                                                                        onTap: CameraImage,
                                                                        child: new Column(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: <Widget>[
                                                                            new Container(
                                                                              height:50,
                                                                              width:50,
                                                                              margin: EdgeInsets.fromLTRB(0,25,30,5),
                                                                              decoration: new BoxDecoration(
                                                                                color: global.whitecolor,
                                                                                border: Border.all(color: global.whitecolor, width: 1.0),
                                                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                                              ),
                                                                              child: Image.asset(
                                                                                  'assets/camera.png',
                                                                                  fit: BoxFit.cover
                                                                              ),
                                                                            ),
                                                                            new Container(
                                                                              margin: EdgeInsets.fromLTRB(0,0,30,0),
                                                                              child:   new Text('Camera',style: TextStyle(fontSize: global.font12,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      )
                                                                  ),
                                                                  Flexible(
                                                                      child: GestureDetector(
                                                                        onTap: removeImage,
                                                                        child: new Column(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: <Widget>[
                                                                            new Container(
                                                                              height:50,
                                                                              width:50,
                                                                              margin: EdgeInsets.fromLTRB(0,25,0,5),
                                                                              decoration: new BoxDecoration(
                                                                                color: global.whitecolor,
                                                                                border: Border.all(color: global.whitecolor, width: 1.0),
                                                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                                              ),
                                                                              child: Image.asset(
                                                                                  'assets/remove.png',
                                                                                  fit: BoxFit.cover
                                                                              ),
                                                                            ),
                                                                            new Container(
                                                                              margin: EdgeInsets.fromLTRB(0,0,0,0),
                                                                              child: new Text('Remove',style: TextStyle(fontSize: global.font12,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      )
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            new Container(
                                                              color: global.whitecolor,
                                                              width: MediaQuery.of(context).size.width,
                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                  color: global.whitecolor,
                                                                  border: Border.all(color: Color.fromRGBO(0, 0, 0, 0.54), width: 1.0),
                                                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                                                ),
                                                                //padding: EdgeInsets.fromLTRB(12, 0, 12, 0),
                                                                margin: EdgeInsets.fromLTRB(20,20,20,20),
                                                                child: FlatButton(
                                                                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                                                  child: Text('CANCEL',style:TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                                                  textColor: Colors.white,
                                                                  onPressed: () {
                                                                    setState(() {
                                                                      cameraWidth=70;
                                                                      cameraHeight=70;
                                                                      initial_curve=50;
                                                                    });
                                                                    Navigator.pop(context);
                                                                  },
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    );
                                                  }
                                              ).then((value) => {
                                                setState(()
                                                {
                                                  cameraWidth=70;
                                                  cameraHeight=70;
                                                  initial_curve=50;
                                                })
                                              });
                                            }
                                        ),
                                      )
                                  ),
                                )
                              ],
                            ),
                          )
                      )
                    ],
                  ),
                ),

                SliverList(delegate: SliverChildListDelegate(
                    [
                      new Container(
                        padding: const EdgeInsets.all(20.0),
                        child:new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child:  Text("First Name", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                                SizedBox(width: 20,),
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child: Text("Last Name", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    child: SizedBox(
                                      height: 36,
                                      child: firstnameField,
                                    )),
                                SizedBox(width: 20,),
                                new Flexible(
                                    child: SizedBox(
                                      height: 36,
                                      child: lastnameField,
                                    )),
                              ],
                            ),
                            SizedBox(height: 15,),
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child:  Text("Gender", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                                SizedBox(
                                  width: 20,
                                ),
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child: Text("Your Birthday", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    child: SizedBox(
                                      height: 36,
                                      child: GestureDetector(
                                        onTap:()=>show_dialog(context),
                                        child: AbsorbPointer(
                                            child:genderField
                                        ),
                                      ),
                                    )),
                                SizedBox(width: 20,),
                                new Flexible(
                                    child: SizedBox(
                                        height: 36,
                                        child: GestureDetector(
                                          onTap:()=> _selectDate(context),
                                          child: AbsorbPointer(
                                              child:birthdayField
                                          ),
                                        )
                                      // birthdayField,
                                    )),
                              ],
                            ),
                            SizedBox(height: 15,),
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child:  Text("Mobile No", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                              ],
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Flexible(
                                  flex: 1,
                                  fit:FlexFit.tight,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xfff7f7f7),
                                      border: Border.all(color: Color(0xffc8c8c8), width: 0.75),
                                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                    ),
                                    alignment: Alignment.topCenter,
                                    child: SizedBox(
                                      height: 36,
                                      child: Material(
                                        elevation: 0.0,
                                        color: Color(0xfff7f7f7),
                                        shape: OutlineInputBorder(
                                          borderSide: BorderSide(color: Color(0xfff7f7f7)),
                                          borderRadius: BorderRadius.circular(6.0),
                                        ),
                                        child: CountryCodePicker(
                                          enabled: false,
                                          textStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                          initialSelection: initialCountry,
                                          favorite: ['+91', 'IN'],
                                          showCountryOnly: false,
                                          showOnlyCountryWhenClosed: false,
                                          alignLeft: false,
                                          padding: EdgeInsets.all(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 10,),
                                new Flexible(
                                    flex: 3,
                                    fit:FlexFit.tight,
                                    child: SizedBox(
                                      height: 36,
                                      child: mobilenoField,
                                    )),
                              ],
                            ),
                            SizedBox(height: 15,),
                            new Row(
                              children: <Widget>[
                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Container(
                                      child:  Text("Email address", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 36,
                              child: emailaddressField,
                            ),
                            SizedBox(height: 15,),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Offstage(
                                  offstage: !show_missing_text,
                                  child: new Text(
                                    "Missing Mandtory Fields",
                                    textAlign: TextAlign.left,
                                    style: new TextStyle(
                                        fontSize: global.font12,
                                        color: global.errorColor,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'BalooChetanRegular'
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5,),
                            emailVerified.toString().compareTo("pending")==0?new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  child:  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Email verification is pending", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                                      SizedBox(height: 5,),
                                      Text("Click on the link we have sent you in the email, subject \"Verify your email\" from no-reply@tapit.live", style: TextStyle(fontSize: global.font14,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                    ],
                                  ),
                                )
                              ],
                            ):new Container(width: 0,height: 0,)
                          ],
                        ),
                      ),
                    ]
                )),

                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      new Container(
                        padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                        child: Text("Notification Preferences", style: TextStyle(fontSize: global.font13,color:Color.fromRGBO(0, 0, 0, 0.56),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                      )
                    ],
                  ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, index)
                    {
                      return Center
                        (
                        child:Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: NotificationPrefList(index:index,title: notiPrefList[index].title,isSelected:notiPrefList[index].isSelected,isSel: (bool value)  {
                            setState(() {
                              offerNoti=value;
                            });
                          },),
                        ),
                      );
                    }, childCount: notiPrefList.length)
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      new Container(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ):new Container(
            child:Center(
              child:new Container(
                height: 50,
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                  backgroundColor: global.mainColor,
                  strokeWidth: 5,),
              ),
            )
        ),
        bottomNavigationBar:isResponsereceived? Padding(
            padding: EdgeInsets.all(0.0),
            child:new Container(
              width:MediaQuery.of(context).size.width,
              decoration: new BoxDecoration(gradient: new LinearGradient(
                  colors: global.buttonGradient,
                  tileMode: TileMode.clamp
              ),
              ),
              child: FlatButton(
                child: Text("SAVE CHANGES",style: TextStyle(color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                onPressed: () {
                  updateProfile();
                },
              ),
            )
        ):new Container(width: 0,height: 0,),
      ),
    );
  }
}


class NotificationPreferencesList {
  NotificationPreferencesList({this.title,this.isSelected});

  String title;
  bool isSelected;
}

class updateImageObject {
  String userId;
  String fileKey;
  updateImageObject(this.userId, this.fileKey);

  Map toJson() => {
    'userId': userId,
    'fileKey': fileKey,
  };
}


import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/GroupsActivity.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'EditContactActivity.dart';

class UpdateGroupActivity extends StatefulWidget
{
  UpdateGroupActivity({Key key,this.groupID,this.groupname,this.sel_contact_list,this.isAdminOwner,this.ownerID}) : super(key: key);
  List<ContactData> sel_contact_list=new List();
  String groupname;
  String groupID;
  bool isAdminOwner;
  String ownerID;
  UpdateGroupActivityState createState()=> UpdateGroupActivityState();
}

class UpdateGroupActivityState extends State<UpdateGroupActivity>
{
  List<ContactData> contact_list=new List();
  List<ContactData> items=new List();
  List<ContactData> alreadyitems=new List();
  List<int> absentusers=new List();
  List<String> userIdList=new List();
  final textController = TextEditingController();
  int nonTapitUsersCount=0;
  bool isChangeSaved=false;
  Random random=new Random();
  Uint8List MainprofileImg;
  String LOGTAG="UpdateGroupActivity";

  @override
  void initState(){
    getData();
    super.initState();
  }

  Future<void> getData() async
  {
    setState(() {
      isChangeSaved=false;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");

    if(map!=null)
    {
      setState(()
      {
        contact_list = (json.decode(map) as List).map((i) => ContactData.fromJson(i)).toList();
      });
    }

    items.addAll(contact_list);
    alreadyitems.addAll(widget.sel_contact_list);

    for(int j=0;j<widget.sel_contact_list.length;j++)
    {
      bool alreadyChanged=false;
      ContactData newcontactData=widget.sel_contact_list.elementAt(j);

      for(int i=0;i<items.length;i++)
      {
        ContactData contactData=items.elementAt(i);
        if(contactData.number.contains(newcontactData.number) || newcontactData.number.contains(contactData.number))
        {
          alreadyChanged = true;
          ContactData cd = new ContactData();
          cd.userID = newcontactData.userID;
          cd.number = newcontactData.number;

          if (newcontactData.name != null && newcontactData.name.isNotEmpty)
          {
            cd.name = newcontactData.name;
          }
          else
          {
            if (contactData.name == null || contactData.name.isEmpty)
            {
            }
            else
            {
              cd.name = contactData.name;
            }
          }

          if (newcontactData.userID == null)
          {
            cd.userID = contactData.userID;
            cd.isTapitUser = true;
          }
          else
          {
            cd.userID = newcontactData.userID;
            cd.isTapitUser = false;
          }

          if (newcontactData.image == null || newcontactData.image.isEmpty)
          {
            if (contactData.type == null || contactData.type.isEmpty)
            {
              cd.type = contactData.type;
              cd.image = "PR";
            }
            else
            {
              cd.type = contactData.type;
              cd.image = contactData.image;
            }
          }
          else
          {
            if (newcontactData.type == null || newcontactData.type.isEmpty)
            {
              cd.type = contactData.type;
              cd.image = "Ab";

            }
            else {
              cd.type = newcontactData.type;
              cd.image = newcontactData.image;

            }
          }

          cd.isTapitUser = newcontactData.isTapitUser;
          cd.isSelected = true;
          cd.colordata = newcontactData.colordata;
          cd.cc=newcontactData.cc;

          setState(() {
            widget.sel_contact_list[j] = cd;
          });
        }
      }
    }

    for(int s=0;s<widget.sel_contact_list.length;s++)
    {
      ContactData finalcd=widget.sel_contact_list.elementAt(s);
      if(!finalcd.isTapitUser)
      {
        nonTapitUsersCount++;
      }
    }
  }

  void updateGroup()async
  {
    bool flag=false;
    flag= await addGroupToCloud(widget.groupname);
    if(flag)
    {
      setState(() {
        isChangeSaved=true;
      });
    }
  }

  Future<bool> addGroupToCloud(String groupname) async
  {
    Future<bool> flag=Future<bool>.value(false);
    List<String> users=new List();
    List<NonUserItem> nonUserList=new List();

    for(int i=1;i<widget.sel_contact_list.length;i++)
    {
      ContactData contactData=widget.sel_contact_list.elementAt(i);
      if(contactData.isSelected)
      {
        if (contactData.userID == null || contactData.userID.isEmpty)
        {
          NonUserItem nonUserItem=new NonUserItem(contactData.number, contactData.cc);
          nonUserList.add(nonUserItem);
        }
        else
        {
          users.add(contactData.userID);
        }
      }
    }

    GroupData groupData = new GroupData(groupname,users,nonUserList);
    var jsonbody=jsonEncode(groupData);

    UserService userService=new UserService();
    ResponseBean response=await userService.updateGroup(global.UserID,widget.groupID,jsonbody);
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    return flag;
  }

  Future<bool> _onBackPressed()
  {
    Navigator.of(context).pop();
    Navigator.push(context, MaterialPageRoute(builder: (context) => GroupsActivity(source:1),),);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          appBar: AppBar(
              titleSpacing: 0.0,
              automaticallyImplyLeading: false,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){
                      _onBackPressed();
                    },
                    child: Container(
                      padding:  EdgeInsets.fromLTRB(15,0,0,0),
                      child: Image.asset(
                        'assets/back_arrow.png',
                        fit: BoxFit.contain,
                        height: 20,
                      ),
                    ),
                  ),
                  Expanded(
                      child:Container(
                        padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: new Text(widget.groupname,style: TextStyle(fontSize: global.font18, color: global.blackcolor,fontWeight: FontWeight.normal,
                            fontFamily: 'BalooChetanMedium')),)
                  )
                ],
              ),
              backgroundColor: global.appbarBackColor),
          body: Container(
              color: global.whitecolor,
              child: Stack(
                children: <Widget>[
                  CustomScrollView(
                    slivers: <Widget>[
                      SliverList(
                          delegate: SliverChildBuilderDelegate((context, index) {
                            return Center(
                              child:Container(
                                child: GroupSummaryContact(index:index,contactData: widget.sel_contact_list[index],profileImg: global.selectedImageList[index],isAdminOwner:widget.isAdminOwner,ownerID:widget.ownerID,isSelected: (bool value) {
                                  setState(() {
                                    widget.sel_contact_list.removeAt(index);
                                  });
                                },
                                ),
                              ),
                            );
                          }, childCount: widget.sel_contact_list.length)),
                      SliverList(delegate: SliverChildListDelegate(
                          [
                            Container(
                              padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                              width: MediaQuery.of(context).size.width,
                              decoration: new BoxDecoration(
                                color: Colors.white,
                                border: Border(
                                  bottom: BorderSide(
                                    color: Color(0xffdcdcdc),
                                    width: 1.5,
                                  ),
                                ),
                              ),
                            ),
                          ]
                      )
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [
                            widget.isAdminOwner?SizedBox(
                                height: 70,
                                child: Container(
                                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    color: global.whitecolor,
                                    child: GestureDetector(
                                      onTap: (){
                                        Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                EditContactActivity(sourceID:1,groupID:widget.groupID,groupname:widget.groupname,sel_contact_list:widget.sel_contact_list,isAdminOwner: widget.isAdminOwner,),
                                          ),
                                        );
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          Image.asset(
                                            'assets/add_member_brown.png',
                                            fit: BoxFit.contain,
                                            height: 20,
                                          ),
                                          Container(
                                              color: global.whitecolor,
                                              padding: EdgeInsets.fromLTRB(20,0,0,0),
                                              child:  Text("ADD MEMBER",style: new TextStyle(
                                                  fontSize: global.font16,
                                                  color: Color(0xff381b1b),
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'BalooChetanMedium'
                                              )
                                              ))
                                        ],
                                      ),
                                    )
                                )
                            ):new Container(width:0,height:0),
                          ],
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [
                            SizedBox(
                              height: 100,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  widget.isAdminOwner?Positioned(
                    bottom: 0,
                    right: 0,
                    left: 0,
                    child: new Column(
                      children: <Widget>[

                        !isChangeSaved?new Container(
                          width:MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(gradient: new LinearGradient(
                              colors: global.buttonGradient,
                              tileMode: TileMode.clamp
                          ),
                          ),
                          child: FlatButton(
                            child: Text("SAVE GROUP",style: TextStyle(color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                            onPressed: () {
                              updateGroup();
                            },
                          ),
                        ):new Container(width: 0,height: 0,),
                      ],
                    ),
                  ):new Container(width: 0,height: 0,)
                ],
              )
          ),
        )
    );
  }

}
class GroupSummaryContact extends StatefulWidget
{
  const GroupSummaryContact({Key key,this.index, this.contactData,this.profileImg,this.isSelected,this.isAdminOwner,this.ownerID}) : super(key: key);
  final ContactData contactData;
  final Uint8List profileImg;
  final int index;
  final bool isAdminOwner;
  final String ownerID;
  final ValueChanged<bool> isSelected;

  GroupSummaryContactState createState()=>GroupSummaryContactState();
}

class GroupSummaryContactState extends State<GroupSummaryContact> {

  bool isSelected = false;
  bool checkboxVal = true;


  @override
  void initState(){

  }

  @override
  Widget build(BuildContext context)
  {
    return new Row(
      children: <Widget>[
        Flexible(
          child:Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10,10, 10, 10),
              child: Center(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Flexible(
                          flex:1,
                          fit: FlexFit.tight,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                child:widget.profileImg!=null?((widget.contactData.type.toString().contains("true"))?CircleAvatar(
                                  backgroundImage: MemoryImage(widget.profileImg),
                                  backgroundColor: global.imageBackColor,
                                ):(widget.contactData.image!=null?CircleAvatar(
                                  child: Text(widget.contactData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                  backgroundColor: widget.index==0?global.adminBackcolor:(widget.contactData.colordata!=null?global.colors.elementAt(widget.contactData.colordata):global.colors.elementAt(3)),
                                ):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ))):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ),
                              ),
                            ],
                          )
                      ),
                      new Flexible(
                          flex: 5,
                          fit: FlexFit.tight,
                          child: Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child:new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  widget.contactData.name!=null?
                                  Text(widget.contactData.name, style: TextStyle(fontSize: global.font16,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                  (widget.contactData.cc!=null?(
                                      Text(widget.contactData.cc+" "+widget.contactData.number, style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))):
                                  (Text(widget.contactData.number.toString(),
                                      style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')))
                                  ),
                                ],
                              )
                          )
                      ),
                      new Flexible(
                          flex:1,
                          fit: FlexFit.tight,
                          child:new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  child: !widget.contactData.isTapitUser?Image.asset(
                                    'assets/non_tapit_symbol.png',
                                    fit: BoxFit.contain,
                                    color: Color.fromRGBO(0,0,0,1).withOpacity(0.6),
                                    height: global.nontapitsymbolSize,
                                  ):null
                              )
                            ],
                          )
                      ),
                      new Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child:global.grpOwnerID.toString().compareTo(widget.contactData.userID.toString())==0?new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  child: Image.asset(
                                      'assets/lock_key_symbol.png',
                                      fit: BoxFit.contain
                                  ))
                            ],
                          ):new Container(
                            child:widget.isAdminOwner?GestureDetector(
                                onTap: ()
                                {
                                  widget.isSelected(false);
                                },
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      child:Image.asset(
                                        'assets/trash_symbol.png',
                                        fit: BoxFit.contain,
                                        color: global.nameTextColor,
                                      ),
                                    ),
                                  ],
                                )
                            ):new Container(width:0 ,height: 0,),
                          )
                      ),
                    ]
                ),
              )
          ),
        ),
      ],
    );
  }
}

class GroupData {
  String name;
  List<String> users;
  List<NonUserItem> nonusers;

  GroupData(this.name, this.users,this.nonusers);

  Map toJson() => {
    'name': name,
    'users': users,
    'nonusers':nonusers,
  };
}

class NonUserItem {
  String mobileNo;
  String countryCode;
  NonUserItem(this.mobileNo, this.countryCode);

  Map toJson() => {
    'mobileNo': mobileNo,
    'countryCode': countryCode,
  };
}
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/Constants.dart';
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'BillSplitActivity.dart';
import 'package:tapit/helpers/BillSplit.dart';
import 'package:tapit/helpers/SplitOwes.dart';
import 'package:tapit/helpers/SplitGroups.dart';
import 'package:tapit/helpers/Fooditems.dart';

class AddGroupSummary extends StatefulWidget
{
  AddGroupSummary({Key key,this.sel_contact_list}) : super(key: key);
  List<ContactData> sel_contact_list=new List();
  AddGroupSummaryState createState()=> AddGroupSummaryState();
}

class AddGroupSummaryState extends State<AddGroupSummary>
{
  String LOGTAG="AddGroupSummary";

  bool isButtonClicked=false;
  Uint8List MainprofileImg;
  String groupID="";
  String groupname;
  int nonTapitUsersCount=0;
  SharedPreferences prefs;
  final textController = TextEditingController();
  List<String> userIDList=new List();

  @override
  void initState()
  {
    getData();
    super.initState();
  }

  void getData()
  {
    for(int p=0;p<widget.sel_contact_list.length;p++)
    {
      ContactData cd=widget.sel_contact_list.elementAt(p);
      if(!cd.isTapitUser)
      {
        nonTapitUsersCount++;
      }
    }
  }

  void saveGroup() async
  {
    setState(()
    {
      isButtonClicked=true;
    });

    groupname=textController.text;
    if(groupname==null || groupname.isEmpty)
    {
      groupname=" ";
      bool flag=false;

      setState(()
      {
        isButtonClicked=false;
      });

      flag= await addGroupToSession("",widget.sel_contact_list);
      bool flag2=await collectData();

      if(flag)
      {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BillSplitActivity(receiptAmount:global.receiptAmount,groupname:"",sel_contact_list: widget.sel_contact_list,),
          ),
        );
      }
      else
      {
        setState(()
        {
          isButtonClicked=false;
        });
        global.helperClass.showAlertDialog(context, "Error","Bill Split Session Initialisation failed");
      }
    }
    else
    {
      bool flag=false;
      flag= await addGroupToCloud(groupname);

      if(flag)
      {
        bool flag=await addGroupToSession(groupID,widget.sel_contact_list);
        bool flag2=await collectData();

        setState(() {
          isButtonClicked=false;
        });

        if(flag)
        {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BillSplitActivity(receiptAmount:global.receiptAmount,groupname:groupname,sel_contact_list: widget.sel_contact_list,),
            ),
          );
        }
        else
        {
          setState(() {
            isButtonClicked=false;
          });
          global.helperClass.showAlertDialog(context, "Error","Bill Split Session Initialisation failed");
        }
      }
      else
      {
        setState(()
        {
          isButtonClicked=false;
        });
        global.helperClass.showAlertDialog(context, "Error","Bill Split Session Initialisation failed");
      }
    }
  }

  Future<bool> collectData() async
  {
    Future<bool> flag=Future<bool>.value(true);

    global.sel_contact_list.clear();
    global.imageList.clear();
    global.finalUserList.clear();
    global.grpname=groupname;
    global.grpid=groupID;
    global.multiple_parts=widget.sel_contact_list.length;

    for(int s=0;s<widget.sel_contact_list.length;s++)
    {
      ContactData obj = widget.sel_contact_list.elementAt(s);
      if(obj.isSelected)
      {
        global.sel_contact_list.add(obj);
        Uint8List temp = await global.helperClass.convertImg(obj);
        global.imageList.add(temp);
      }
    }
    return flag;
  }

  Future<bool> addGroupToCloud(String groupname) async
  {
    Future<bool> flag=Future<bool>.value(false);
    List<String> users=new List();
    List<NonUserItem> nonUserList=new List();

    for(int i=1;i<widget.sel_contact_list.length;i++)
    {
      ContactData contactData=widget.sel_contact_list.elementAt(i);
      if(contactData.isSelected)
      {
        if (contactData.userID == null || contactData.userID.isEmpty)
        {
          NonUserItem nonUserItem=new NonUserItem(contactData.number, contactData.cc);
          nonUserList.add(nonUserItem);
        }
        else
        {
          users.add(contactData.userID);
        }
      }
    }

    GroupData groupData = new GroupData(groupname,users,nonUserList);
    var jsonbody=jsonEncode(groupData);

    String url = Constants.BASE_URL+'/api/users/'+global.UserID+'/group';
    UserService userService=new UserService();
    ResponseBean response=await userService.addGroup(global.UserID,jsonbody);

    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        var userdecode = response.payLoad;
        String grpid = userdecode['id'];
        String createdby = userdecode['created_by'];
        List<dynamic> userlist = userdecode['users'];

        if (grpid == null || grpid.isEmpty)
        {
          grpid = "";
        }
        else
        {
          groupID = grpid;
        }

        userIDList.add(createdby);
        for (int s = 0; s < userlist.length; s++)
        {
          userIDList.add(userlist[s].toString());
        }

        for (int s = 0; s < widget.sel_contact_list.length; s++)
        {
          ContactData cd = widget.sel_contact_list.elementAt(s);
          cd.userID = userIDList.elementAt(s);
        }
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }

    return flag;
  }

  Future<bool> addGroupToSession(String groupid,List<ContactData> userid_list) async
  {
    Future<bool> flag=Future<bool>.value(false);
    prefs = await SharedPreferences.getInstance();

    List<SplitGroups> unregistredSplitGroups = new List();
    List<SplitGroups> splitGroups = new List();
    List<Fooditems> fooditems = new List();
    bool registredSatus=false;

    for(int i=0;i<userid_list.length;i++)
    {
      if(userid_list.elementAt(i).isSelected)
      {
        List<SplitOwes> splitOwes = new List();
        registredSatus = true;
        String cc = "";
        double balanceAmt=0;
        String userid = userid_list.elementAt(i).userID;

        if (userid == null || userid.isEmpty)
        {
          registredSatus = false;
          userid = userid_list.elementAt(i).number;
          cc = userid_list.elementAt(i).cc;
        }

        SplitGroups splitGroup;
        if (i == 0)
        {
          SplitOwes splitOwe;

          double userAmt=global.receiptAmount / userid_list.length;
          userAmt=double.parse((userAmt).toStringAsFixed(2));
          double offset=(global.receiptAmount-(userAmt*userid_list.length));
          offset=double.parse((offset).toStringAsFixed(2));
          userAmt=double.parse((userAmt+offset).toStringAsFixed(2));
          balanceAmt=(userAmt)-global.receiptAmount;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));

          splitOwe = new SplitOwes(global.UserID, userAmt,balanceAmt);
          splitOwes.add(splitOwe);

          if (registredSatus)
          {
            splitGroup = new SplitGroups(global.UserID, 1, "1", true, global.receiptAmount, 0, "pending", fooditems, splitOwes, "", cc);
            splitGroups.add(splitGroup);
          }
          else
          {
            splitGroup = new SplitGroups("", 1, "1", true, global.receiptAmount, 0, "pending", fooditems, splitOwes, global.UserID, cc);
            unregistredSplitGroups.add(splitGroup);
          }
        }
        else
        {
          balanceAmt=global.receiptAmount/userid_list.length;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));
          if (registredSatus)
          {
            SplitOwes splitOwe = new SplitOwes(global.UserID, balanceAmt,balanceAmt);
            splitOwes.add(splitOwe);
            splitGroup = new SplitGroups(userid, 0, "1", false, 0, 0, "pending", fooditems, splitOwes, "", cc);
            splitGroups.add(splitGroup);
          }
          else
          {
            SplitOwes splitOwe = new SplitOwes(global.UserID, balanceAmt,balanceAmt);
            splitOwes.add(splitOwe);
            splitGroup = new SplitGroups("", 0, "1", false, 0, 0, "pending", fooditems, splitOwes, userid, cc);
            unregistredSplitGroups.add(splitGroup);
          }
        }
      }

    }

    BillSplit billSplit=new BillSplit(groupid, 0, "equal", "0", splitGroups,unregistredSplitGroups);
    var jsonbody=jsonEncode(billSplit);
    UserService userService=new UserService();
    ResponseBean response=await userService.initiateBillSplit(global.UserID,global.receiptID,jsonbody);
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    return flag;
  }

  void changeNonUserCount()
  {
    setState(() {
      nonTapitUsersCount=0;
    });
    for(int s=0;s<widget.sel_contact_list.length;s++)
    {
      ContactData finalcd=widget.sel_contact_list.elementAt(s);
      if(finalcd.isSelected)
      {
        if (!finalcd.isTapitUser)
        {
          setState(()
          {
            nonTapitUsersCount = nonTapitUsersCount + 1;
          });
        }
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding:false,
      appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding:  EdgeInsets.fromLTRB(15,0,0,0),
                  child: Image.asset(
                    'assets/back_arrow.png',
                    fit: BoxFit.contain,
                    height: 20,
                  ),
                ),
              ),
              Expanded(
                  child:Container(
                    padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child: TextField(
                      enabled: true,
                      controller: textController,
                      decoration: InputDecoration(border: InputBorder.none, hintText: 'Type group name',
                        hintStyle: TextStyle( fontSize:global.font18,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                      ),
                    ),
                  )
              )
            ],
          ),
          backgroundColor: global.appbarBackColor),
      body:   Container(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
        color: global.whitecolor,
        child: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: <Widget>[
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, index)
                    {
                      return Center(
                        child:Container(
                          child: GroupSummaryContact(index:index,contactData: widget.sel_contact_list[index],profileImg: global.selectedImageList[index],isSelected: (bool value) {
                            widget.sel_contact_list.elementAt(index).isSelected=value;
                            changeNonUserCount();
                          },),
                        ),
                      );
                    }, childCount: widget.sel_contact_list.length)
                ),
                SliverList(delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xffdcdcdc),
                              width: 1.5,
                            ),
                          ),
                        ),
                      ),
                    ]
                )
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SizedBox(
                          height: 50,
                          child: Container(
                              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                              color: global.whitecolor,
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.of(context).pop();
                                },
                                child: new Row(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/add_member_orange.png',
                                      fit: BoxFit.contain,
                                      height: 20,
                                    ),
                                    Container(
                                        color: global.whitecolor,
                                        padding: const EdgeInsets.fromLTRB(20,0,0,0),
                                        child:  Text("ADD MEMBER",style: new TextStyle(
                                            fontSize: global.font16,
                                            color: global.mainColor,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'BalooChetanMedium'
                                        )
                                        ))
                                  ],
                                ),
                              )
                          )
                      ),
                    ],
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                ),
              ],
            ),

            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: new Column(
                children: <Widget>[
                  nonTapitUsersCount>0?new Container(
                    color:global.whitecolor,
                    padding: EdgeInsets.fromLTRB(0,5,0,20),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(" "+nonTapitUsersCount.toString()+" users will be invited to Tapit app",style: TextStyle(fontSize: global.font15,color: Color(0xff616161),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),)
                      ],
                    ),):new Container(width: 0,height: 0,),
                  new Container(
                    width:MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      gradient: new LinearGradient(
                          colors: global.buttonGradient,
                          tileMode: TileMode.clamp
                      ),
                    ),
                    child: FlatButton(
                      child: Text("PROCEED TO SPLIT",style: TextStyle(fontSize:global.font14,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                      onPressed: () {
                        if(!isButtonClicked)
                        {
                          saveGroup();
                        }
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}
class GroupSummaryContact extends StatefulWidget
{
  GroupSummaryContact({Key key,this.index, this.contactData,this.profileImg,this.isSelected}) : super(key: key);
  ContactData contactData;
  Uint8List profileImg;
  int index;
  ValueChanged<bool> isSelected;
  GroupSummaryContactState createState()=>GroupSummaryContactState();
}

class GroupSummaryContactState extends State<GroupSummaryContact> {

  bool isSelected = false;
  bool checkboxVal = true;

  void _onRememberMeChanged(bool newValue) => setState(() {

    checkboxVal = newValue;
    setState(()
    {
      isSelected = newValue;
      widget.isSelected(isSelected);
    });

  });

  @override
  Widget build(BuildContext context) {

    return new Row(
          children: <Widget>[
            Flexible(
              child:Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(10,5, 10, 5),
                  child: Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Flexible(
                              flex:1,
                              fit: FlexFit.tight,
                              child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                      height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                      child:widget.profileImg!=null?((widget.contactData.type.toString().contains("true"))?CircleAvatar(
                                        backgroundImage: MemoryImage(widget.profileImg),
                                        backgroundColor: global.imageBackColor,
                                      ):(widget.contactData.image!=null?CircleAvatar(
                                        child: Text(widget.contactData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                        backgroundColor: widget.index==0?global.adminBackcolor:(widget.contactData.colordata!=null?global.colors.elementAt(widget.contactData.colordata):global.colors.elementAt(3)),
                                      ):CircleAvatar(
                                        backgroundImage: AssetImage('assets/dummy_user.png'),
                                        backgroundColor: global.appbargreycolor,
                                      ))):CircleAvatar(
                                        backgroundImage: AssetImage('assets/dummy_user.png'),
                                        backgroundColor: global.appbargreycolor,
                                      ),
                                    ),
                                  ]
                              )
                          ),
                          new Flexible(
                              flex: 4,
                              fit: FlexFit.tight,
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  child:new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      widget.contactData.name!=null?
                                      (widget.index==0?new Text(widget.contactData.name+"(Me)", style: TextStyle(fontSize: global.font16,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):
                                      new Text(widget.contactData.name, style: TextStyle(fontSize: global.font16,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                                      ):Text(widget.contactData.cc+" "+widget.contactData.number, style: TextStyle(fontSize: global.font16,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                      widget.contactData.name!=null?Opacity(
                                        opacity: 0.6,
                                        child:widget.index==0?new Container(width: 0,height: 0,):new Text(widget.contactData.cc+" "+widget.contactData.number, style: TextStyle(fontSize: global.font13,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                      ):new Container(width:0,height:0)
                                    ],
                                  )
                              )
                          ),
                          new Flexible(
                              flex:1,
                              fit: FlexFit.tight,
                              child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        width:  global.nontapitsymbolSize,
                                        height:  global.nontapitsymbolSize,
                                        child: !widget.contactData.isTapitUser?Image.asset(
                                            'assets/non_tapit_symbol.png',
                                            fit: BoxFit.contain
                                        ):null
                                    )
                                  ]
                              )
                          ),
                          new Flexible(
                              flex: 1,
                              fit: FlexFit.tight,
                              child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    widget.index==0?Container(
                                        width: 20,
                                        height: 20,
                                        child: Image.asset(
                                            'assets/admin_symbol.png',
                                            fit: BoxFit.contain
                                        )):GestureDetector(
                                      onTap: (){
                                        _onRememberMeChanged(!checkboxVal);
                                      },
                                      child: new Container(
                                          width: 23,
                                          height: 23,
                                          child: checkboxVal?Image.asset(
                                              'assets/orange_checkbox.png',
                                              fit: BoxFit.contain
                                          ):Image.asset(
                                              'assets/empty_checkbox.png',
                                              fit: BoxFit.contain
                                          )
                                      ),
                                    )
                                  ]
                              )
                          ),
                        ]
                    ),
                  )
              ),
            )
          ]
      );
  }
}

class GroupData {
  String name;
  List<String> users;
  List<NonUserItem> nonusers;

  GroupData(this.name, this.users,this.nonusers);

  Map toJson() => {
    'name': name,
    'users': users,
    'nonusers':nonusers,
  };
}

class NonUserItem {
  String mobileNo;
  String countryCode;
  NonUserItem(this.mobileNo, this.countryCode);

  Map toJson() => {
    'mobileNo': mobileNo,
    'countryCode': countryCode,
  };
}







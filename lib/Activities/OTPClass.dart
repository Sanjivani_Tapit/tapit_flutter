import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tapit/global.dart' as global;
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:quiver/async.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class OTPClass extends StatefulWidget
{
  OTPClass({Key key, @required this.usermobileno,@required this.countryCode}) : super(key: key);
  String usermobileno;
  String countryCode;
  OTPClassState createState()=> OTPClassState();
}

class OTPClassState extends State<OTPClass>
{
  bool isResponseReceived=true;
  String _buttonText="RESEND(00:30)";
  String userOTP=null;
  String error_msg="Error message for verification";
  int _start = 30;
  bool resendEnabled=false;
  bool show_missing_text=false;
  var sub;
  final TextEditingController textController = TextEditingController();
  BuildContext mcontext;

  String LOGTAG="OTPClass";

  @override
  void initState()
  {
    super.initState();
    setState(() {
      textController.text="0000";
    });

    startTimer();
  }

  void startTimer()
  {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    sub = countDownTimer.listen(null);
    sub.onData((duration) {
      if(mounted) {
        setState(() {
          resendEnabled = false;
          _buttonText = "RESEND(00:" + (_start - duration.elapsed.inSeconds).toString() + ")";
        });
      }
    });

    sub.onDone(() {
      if(mounted)
      {

        setState(() {
          resendEnabled = true;
          _buttonText = "RESEND";
        });
      }
      sub.cancel();
    });
  }


  void getOTP(String otp)
  {
    userOTP=otp;
  }

  void resendOTP() async
  {
    if(resendEnabled)
    {
      if(widget.usermobileno!=null || widget.usermobileno=="")
      {
        setState(() {
          isResponseReceived=false;
        });

        UserService userService=new UserService();
        ResponseBean response=await userService.otpGenerate(widget.usermobileno, widget.countryCode);
        if(response!=null)
        {
          int statusCode = response.status;

          if (statusCode == 200)
          {
            setState(() {isResponseReceived=true;});
            startTimer();
            String body = response.payLoad;
            global.helperClass.showAlertDialog(mcontext, "Success", "OTP Sent Successfully");
          }
          else if (statusCode == 204) {

            setState(() {isResponseReceived=true;});
            global.helperClass.showAlertDialog(mcontext, "Error", "No Content");
          }
          else if (statusCode == 400) {

            setState(() {isResponseReceived=true;});
            global.helperClass.showAlertDialog(mcontext, "Error", "Bad Request");
          }
          else if (statusCode == 404) {

            setState(() {isResponseReceived=true;});
            global.helperClass.showAlertDialog(mcontext, "Error", "Not Found");
          }
          else if (statusCode == 500)
          {
            setState(() {isResponseReceived=true;});

            if(response.message.toString().contains("is not a mobile number"))
            {
              global.helperClass.showAlertDialog(context, "Error", "Not able to verify OTP. Please enter valid mobile number");
            }
            else
            {
              global.helperClass.showAlertDialog(context, "Error", "Check your mobile number");
            }
          }
          else
          {
            setState(() {isResponseReceived=true;});
            global.helperClass.showAlertDialog(mcontext, "Error", "Not able to resend OTP");
          }
        }
        else
        {
          setState(() {isResponseReceived=true;});
        }
      }
      else
      {
        setState(() {isResponseReceived=true;});
        global.helperClass.showAlertDialog(mcontext, "Error","Please provide mobile number");
      }
    }
  }

  void submitOTP() async
  {

    if(userOTP==null)
    {
      setState(()
      {
        this.show_missing_text=true;
        this.error_msg="Please provide OTP";
      });
    }
    else if(userOTP.length<4)
    {
      setState(() {
        this.show_missing_text=true;
        this.error_msg="Please provide full OTP";
      });
    }
    else
    {
      setState(() {isResponseReceived=false;});
      UserService userService=new UserService();
      ResponseBean response=await userService.otpVerify(widget.usermobileno,userOTP, widget.countryCode);
      if(response!=null)
      {
        int statusCode = response.status;
        if (statusCode == 200)
        {
          global.homeFragment = 0;
          Navigator.of(context).pushNamedAndRemoveUntil('/Home', (Route<dynamic> route) => false);
        }
        else if (statusCode == 204) {

          setState(() {isResponseReceived=true;});
          global.helperClass.showAlertDialog(mcontext, "Error", "No Content");
        }
        else if (statusCode == 400) {

          setState(() {isResponseReceived=true;});
          global.helperClass.showAlertDialog(mcontext, "Error", "Bad Request");
        }
        else if (statusCode == 404) {

          setState(() {isResponseReceived=true;});
          global.helperClass.showAlertDialog(mcontext, "Error", "Not Found");
        }
        else if (statusCode == 500) {

          setState(() {isResponseReceived=true;});
          global.helperClass.showAlertDialog(mcontext, "Error", "Internal Server Error");
        }
        else {

          setState(() {isResponseReceived=true;});
          global.helperClass.showAlertDialog(mcontext, "Error", "Not able to verify OTP");
        }
      }
      else
      {
        setState(() {isResponseReceived=true;});
        global.helperClass.showAlertDialog(mcontext, "Error", "Not able to verify OTP");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    mcontext=context;
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Image.asset(
                  'assets/back_arrow.png',
                  fit: BoxFit.contain,
                  height: 25,
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(30,12,0,12),
                  child:  Text("Verify OTP",style: new TextStyle(
                      fontSize: global.font18,
                      color: global.appbarTextColor,
                      fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'
                  )
                  ))
            ],
          ),
          backgroundColor: global.appbarBackColor),
      body: isResponseReceived?new Container(
        color: Color(0xFFffffff),
        child:new Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                width:MediaQuery.of(context).size.width/1.14,
                child:new Text("A One Time Password has been sent to your registred Email and Mobile Number.Please enter it here to verify yourself.",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: global.font12,
                      color: new Color(0xff000000),
                      fontWeight: FontWeight.normal,
                      fontFamily: 'BalooChetanRegular'
                  ),),
              ),
              SizedBox(height: 20,),
              Center(
                  child:new Container(
                    width: 200,
                    child: PinCodeTextField(
                      length: 4,
                      autoFocus: true,
                      obsecureText: false,
                      textInputType: TextInputType.numberWithOptions(decimal: false),
                      animationType: AnimationType.fade,
                      textStyle: TextStyle(fontSize:global.font20,color:global.blackcolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanMedium'),
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderWidth: 0.75,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 50,
                        fieldWidth: 40,
                        selectedFillColor: Color(0xfffcfcfc),
                        selectedColor: Color(0xffc0c0c0),
                        activeFillColor: Color(0xfff7f7f7),
                        activeColor: Color(0xffc0c0c0),
                        inactiveFillColor: Color(0xfff7f7f7),
                        inactiveColor: Color(0xffc0c0c0),

                      ),
                      enableActiveFill: true,
                      onCompleted: (v) {

                      },
                      onChanged: (value) {
                        setState(() {
                          userOTP = value;
                        });
                      },
                      beforeTextPaste: (text) {
                        return true;
                      },
                    ),
                  )
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Offstage(
                    offstage: !show_missing_text,
                    child: new Text(
                      this.error_msg,
                      textAlign: TextAlign.left,
                      style: new TextStyle(
                          fontSize: global.font12,
                          color: global.errorColor,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'BalooChetanRegular'
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Expanded(
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    !resendEnabled?new Flexible(
                        flex:1,
                        fit: FlexFit.tight,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: new RaisedButton(
                            color: Color.fromRGBO(153, 153, 153, 0.17),
                            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 5.0),
                            onPressed: () {
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: new Text(_buttonText, style: TextStyle(fontSize: global.font17,color: Color(0xff7f7f7f),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                          ),
                        )):
                    new Flexible(
                        flex:1,
                        fit: FlexFit.tight,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: new RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 25.0),
                            onPressed: () {
                              resendOTP();
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            color:global.mainColor,
                            child: new Text(_buttonText, style: TextStyle(fontSize: global.font17,color: Color(0xffffffff),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                          ),
                        )),
                    new Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: new RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 25.0),
                            onPressed: () {
                              submitOTP();
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            color:global.mainColor,
                            child: Text('SUBMIT', style: TextStyle(fontSize: global.font17,color: Color(0xffffffff),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                          ),
                        ))
                  ],
                ),
              ),
            ],
          ),
        ),
      ):new Container(
          child:Center(
            child:new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          )
      ),
    );
  }
}

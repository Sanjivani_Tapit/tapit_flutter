import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ListofPayee.dart';
import 'package:tapit/Fragments/EquallyFragment.dart';
import 'package:tapit/Fragments/MultipleFragment.dart';
import 'package:tapit/Fragments/DollarFragment.dart';
import 'package:tapit/helpers/navigation_bar.dart';
import 'package:tapit/helpers/navigation_bar_item.dart';
import 'package:tapit/helpers/UserAmountData.dart';

class SplitDetailsActivity extends StatefulWidget
{
  SplitDetailsActivity({Key key,this.receiptAmount,this.groupname,this.sel_contact_list,this.fragmentIndex}) : super(key: key);
  List<UserAmountData> sel_contact_list=new List();
  String groupname;
  double receiptAmount=0;
  int fragmentIndex=0;
  SplitDetailsActivityState createState()=> SplitDetailsActivityState();
}

class SplitDetailsActivityState extends State<SplitDetailsActivity> with SingleTickerProviderStateMixin
{
  bool listSize=true;
  int _currentIndex = 0;
  String grpname;
  Uint8List MainprofileImg;

  List<UserAmountData> amount_paid_items=new List();
  List<UserAmountData> finalUserList=new List();
  List<Tab> tabList = List();
  PageController _pageController;
  List<Widget> _children = [
  ];

  String LOGTAG="SplitDetailsActivity";

  @override
  Future<void> initState()
  {
    _pageController = PageController();
    storeToList();
    setState(() {
      _currentIndex=widget.fragmentIndex;
    });
    global.isSplitDetails=true;
    super.initState();
  }

  void storeToList() async
  {
    setState(()
    {
      if(widget.fragmentIndex==0)
      {
        EquallyFragment equallyFragment = new EquallyFragment();
        setState(() {
          _children.add(equallyFragment);
        });
      }
      else if(widget.fragmentIndex==1)
      {
        DollarFragment dollarFragment = new DollarFragment();
        setState(() {
          _children.add(dollarFragment);
        });
      }
      else if(widget.fragmentIndex==2)
      {
        MultipleFragment multipleFragment = new MultipleFragment();
        setState(() {
          _children.add(multipleFragment);
        });
      }

      amount_paid_items.addAll(widget.sel_contact_list);

      global.payee_list.clear();
      global.finalUserList.clear();

      global.payee_list.addAll(amount_paid_items);
      global.finalUserList.addAll(amount_paid_items);
    });
  }

  void onTabTapped(int index)
  {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding:  EdgeInsets.fromLTRB(15,0,0,0),
                  child: Image.asset(
                    'assets/back_arrow.png',
                    fit: BoxFit.contain,
                    height: 20,
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(30,0,0,0),
                  child:  Text("Bill Split",style: new TextStyle(
                      fontSize: global.font18,
                      color: global.appbarTextColor,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'BalooChetanSemiBold'
                  )
                  )),
            ],
          ),
          actions: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
              child: new Row(
                children: <Widget>[
                  Opacity(
                    opacity: 0.6,
                    child:Text("\$"+global.formatter.format(widget.receiptAmount).toString(), style: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                  ),
                  Container(
                    width:25,
                    height: 25,
                    child:Image.asset(
                        'assets/receipt_symbol.png',
                        fit: BoxFit.contain
                    ),
                  ),
                ],
              ),
            )
          ],
          backgroundColor: global.appbarBackColor,
        ),
        body: Container(
            color: global.whitecolor,
            height: MediaQuery.of(context).size.height,
            child:new Stack(
              children: <Widget>[
                new Container(
                    color: global.whitecolor,
                    child:new Column(
                      children: <Widget>[
                        new Container(
                          color: global.whitecolor,
                          height:MediaQuery.of(context).size.height>600?(MediaQuery.of(context).size.height>800?MediaQuery.of(context).size.height/12:MediaQuery.of(context).size.height/11):MediaQuery.of(context).size.height/10,
                          margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                          width: MediaQuery.of(context).size.width,
                          child: new Row(
                            children: <Widget>[
                              new Flexible(
                                flex:1,
                                fit:FlexFit.tight,
                                child: new Text("Amount Paid by", style: TextStyle(fontSize: global.font16,color:Color(0xff3b3b3b),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                              ),
                              new Flexible(
                                flex: 2,
                                fit: FlexFit.tight,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    listSize?
                                    ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: global.imageList.length,
                                      itemBuilder: (context, index)
                                      {
                                        return Container(
                                          child: ListofPayee(index: index,
                                            profileImg: global.imageList[index],
                                            userAmountData: global.payee_list[index],
                                            isSelected: (bool value) {
                                              setState(() {});
                                            },
                                          ),
                                        );
                                      },
                                    ):new Container(width: 0,height: 0,),
                                    GestureDetector(
                                      onTap: (){

                                      },
                                      child: !global.isSplitDetails?Image.asset(
                                        'assets/add_payee_symbol.png',
                                        fit: BoxFit.contain,
                                        height: 30,
                                      ):new Container(width: 0,height: 0,),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                            width: MediaQuery.of(context).size.width,
                            decoration:BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Color(0xff979797).withOpacity(0.1),
                                  width: 2.0,
                                ),
                              ),
                            )
                        ),
                        new Container(
                          width: MediaQuery.of(context).size.width,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.15),
                                    blurRadius: 2.0,
                                    offset: Offset(0.0, 1.0)
                                )
                              ],
                              color: global.whitecolor
                          ),
                          child: TitledBottomNavigationBar(
                            enableShadow: false,
                            currentIndex:_currentIndex ,
                            BAR_HEIGHT: 40,
                            INDICATOR_HEIGHT: 1.5,
                            onTap: onTabTapped,
                            reverse: true,
                            items: [
                              TitledNavigationBarItem(
                                icon: Icons.home,
                                title: _currentIndex==0?new Text('Equally',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Equally',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ),
                              TitledNavigationBarItem(
                                icon: Icons.home,
                                title: _currentIndex==1?new Text('Dollars(\$)',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Dollars(\$)',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ),
                              TitledNavigationBarItem(
                                  icon: Icons.home,
                                  title: _currentIndex==2?new Text('Multiples',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Multiples',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                              ),
                            ],
                            activeColor: Color(0xfff18642),
                            inactiveColor: Color.fromRGBO(0, 0, 0, 0.6),
                          ),
                        ),

                        Flexible(
                            flex: 3,
                            fit: FlexFit.tight,
                            child:Container(
                              decoration: BoxDecoration(
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.black54,
                                        blurRadius: 1.0,
                                        offset: Offset(10.0, 0.75)
                                    )
                                  ],
                                  color: global.whitecolor
                              ),
                              child:  PageView(
                                controller: _pageController,
                                onPageChanged: (index) {
                                  setState(() {
                                  });
                                },
                                children: _children,
                              ),
                            )
                        ),
                      ],
                    )
                ),
              ],
            )
        )
    );
  }
}


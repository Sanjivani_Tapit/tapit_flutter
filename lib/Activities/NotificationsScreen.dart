import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/EditContainerActivity.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ContactData.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class NotificationsScreen extends StatefulWidget
{
  NotificationsState createState()=> NotificationsState();
}

class NotificationsState extends State<NotificationsScreen>{

  List<ContactData> contact_list=new List();
  List<Noti> notificationList=new List();
  ScrollController scrollController = new ScrollController();
  String amount;
  String name;
  String status_comment;
  String merchant_comment;
  SharedPreferences prefs;
  bool isResponseReceived=false;
  int paginationTotalCount=0;
  int paginationCount=0;

  String LOGTAG="NotificationsScreen";

  @override
  void initState()
  {
    getLog();
    super.initState();
  }

  Future<void> getLog()
  {
    getLogs(0,11);
    paginationTotalCount=(global.totalAlertCount/10).toInt();
    int remain=(global.totalReceiptCount%10);

    setState(() {
      notificationList.clear();
    });

    scrollController.addListener(()
    {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent)
      {
        if(remain==0)
        {
          if (paginationCount < paginationTotalCount)
          {
            getLogs(paginationCount, 11);
          }
        }
        else
        {
          if (paginationCount <= paginationTotalCount)
          {
            getLogs(paginationCount, 11);
          }
        }
      }
    });
  }


  void getLogs(int start,int end) async
  {
    contact_list.clear();
    prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");
    if(map!=null)
    {
      setState(() {
        global.searchitem = "";
        contact_list = (json.decode(map) as List).map((i) => ContactData.fromJson(i)).toList();
      });
    }

    UserService userService=new UserService();
    ResponseBean response=await userService.getAlertData(global.UserID,start.toString(),end.toString());

    if(response!=null)
    {
      int statusCode = response.status;
      List<dynamic> logList = response.payLoad;
      if (statusCode == 200)
      {
        if (logList != null)
        {
          paginationCount++;
          for (int i = 0; i < logList.length; i++)
          {
            String title;
            String status;
            String receiptId;
            String currentDay;
            bool readStatus = false;
            String mobileno;
            String cc;
            String picture;
            bool isNetImage = false;
            String imageType = null;

            title = logList[i]["message"];
            status = logList[i]["status"];
            receiptId = logList[i]["receiptId"];
            String txnTimestamp = logList[i]["sentOn"];
            mobileno = logList[i]["mobileNo"];
            cc = logList[i]["countryCode"];
            picture = logList[i]["picture"];
            String alertid = logList[i]["id"];


            if (picture != null)
            {
              if (picture.isNotEmpty)
              {
                isNetImage = true;
                imageType = "false";
              }
              else
              {
                for (int s = 0; s < contact_list.length; s++)
                {
                  ContactData cd = contact_list.elementAt(s);
                  if (mobileno.toString().compareTo(cd.number.toString()) == 0 && cc.toString().compareTo(cd.cc.toString()) == 0)
                  {
                    if (cd.type == null)
                    {
                      picture = cd.image;
                      imageType = null;
                    }
                    else if (cd.type.toString().compareTo("true") == 0)
                    {
                      picture = cd.image;
                      imageType = "true";
                    }
                    else
                    {
                      picture = cd.image;
                      imageType = "false";
                    }
                  }
                }
              }
            }
            else
            {
              for (int s = 0; s < contact_list.length; s++)
              {
                ContactData cd = contact_list.elementAt(s);
                if (cd.number.toString().compareTo(mobileno.toString()) == 0 && cc.toString().compareTo(cd.cc.toString()) == 0)
                {
                  if (cd.type == null)
                  {
                    picture = cd.image;
                    imageType = null;
                  }
                  else if (cd.type.toString().compareTo("true") == 0)
                  {
                    picture = cd.image;
                    imageType = "true";
                  }
                  else
                  {
                    picture = cd.image;
                    imageType = "false";
                  }
                }
              }
            }

            String day = txnTimestamp.substring(0, 10);
            String time = txnTimestamp.substring(11, 19);

            var dateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse(day + " " + time, true);
            String alert_time = dateTime.toLocal().toString().substring(11, 16);
            String time_hh_mm = alert_time;
            final now = DateTime.now();
            final lastMidnight = new DateTime(now.year, now.month, now.day);
            int today_midnight = lastMidnight.millisecondsSinceEpoch;
            int current_timeinmillies = dateTime.toLocal().millisecondsSinceEpoch;

            bool timeFlag = false;
            int s = 0;
            while (!timeFlag)
            {
              if (current_timeinmillies > today_midnight)
              {
                if (s == 0)
                {
                  currentDay = "Today," + " " + time_hh_mm;
                  timeFlag = true;
                }
                else if (s == 1)
                {
                  currentDay = "Yesterday," + " " + time_hh_mm;
                  timeFlag = true;
                }
                else
                {
                  currentDay = s.toString() + " days ago," + " " + time_hh_mm;
                  timeFlag = true;
                }
              }
              else
              {
                today_midnight = today_midnight - 86400000;
              }
              s++;
            }

            if (status == null || status.isEmpty)
            {
              readStatus = false;
            }
            else
            {
              if (status.toString().compareTo("unread") == 0)
              {
                readStatus = false;
              }
              else
              {
                readStatus = true;
              }
            }

            title = "<p>" + title + "</p>";
            Noti noti = new Noti(title: title, timestamp: currentDay, receiptID: receiptId, profileImg: picture, isNetImage: isNetImage, imageType: imageType, readStatus: readStatus, alertid: alertid);
            setState(() {
              notificationList.add(noti);
            });
          }
        }
        setState(() {
          isResponseReceived = true;
        });
      }
      else if (statusCode == 204)
      {
        setState(() {
          isResponseReceived = true;
        });
      }
      else if (statusCode == 400)
      {
        global.helperClass.showAlertDialog(context, "Error", "Bad Request");
        setState(() {
          isResponseReceived = true;
        });
      }
      else if (statusCode == 404)
      {
        global.helperClass.showAlertDialog(context, "Error", "Not Found");
        setState(() {
          isResponseReceived = true;
        });
      }
      else if (statusCode == 500)
      {
        global.helperClass.showAlertDialog(context, "Error", "Internal Server Error");
        setState(() {
          isResponseReceived = true;
        });
      }
      else
      {
        global.helperClass.showAlertDialog(context, "Error", "Not able to get alerts");
        setState(() {
          isResponseReceived = true;
        });
      }
    }
    else
    {
      setState(() {
        isResponseReceived = true;
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: new Container(
                  padding:EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child:Image.asset(
                    'assets/black_cross_icon.png',
                    fit: BoxFit.contain,
                    height: 25,
                  ),
                ),
              ),
              Container(
                  padding: const EdgeInsets.all(8.0), child:  Text("Notifications",style: new TextStyle(
                  fontSize: global.font18,
                  color: global.appbarTextColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'BalooChetanSemiBold'
              )
              ))
            ],
          ),
          backgroundColor: global.appbarBackColor),
      body: Container(
        color: Colors.white,
        child: isResponseReceived?Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child:Center(
            child: notificationList.length>0?ListView.builder(
              controller: scrollController,
              itemCount: notificationList.length,
              itemBuilder: (context, index) {
                return Center(
                    child:Container(
                      decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 1.0,
                                color:Color(0x4D979797)
                            )
                        ),
                      ),
                      child: ChoiceCard(noti: notificationList[index]),
                    )
                );
              },
            ):new Container(
              child: new Text("You have no notifications yet",style: new TextStyle(
                  fontSize: global.font16,
                  color: global.nameTextColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'BalooChetanRegular'
              ),
              ),
            ),
          ),
        ):new Container(
            child:Center(
              child:new Container(
                height: 50,
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                  backgroundColor: global.mainColor,
                  strokeWidth: 5,),
              ),
            )
        ),
      ),
    );
  }
}

class Noti
{
  Noti({this.title,this.timestamp,this.receiptID,this.profileImg,this.isNetImage,this.imageType,this.readStatus,this.alertid});

  String title;
  String timestamp;
  String receiptID;
  String profileImg;
  bool isNetImage;
  String imageType;
  bool readStatus;
  String alertid;


}

class ChoiceCard extends StatefulWidget{
  ChoiceCard({Key key, this.noti}) : super(key: key);

  ChoiceCardState createState()=> ChoiceCardState();
  final Noti noti;
}

class ChoiceCardState extends State<ChoiceCard> {

  Future<bool> updateAlertStatus(String alertid)async
  {
    Future<bool> flag=Future<bool>.value(true);

    UserService userService=new UserService();
    ResponseBean response=await userService.updateAlertStatus(global.UserID,alertid.toString());
    int statusCode=response.status;
    if(statusCode==200)
    {
      flag=Future<bool>.value(true);
    }
    else
    {
      flag=Future<bool>.value(false);
    }
    return flag;
  }

  void updateState()
  {
    setState(() {
      widget.noti.readStatus=true;
    });

  }

  @override
  Widget build(BuildContext context)
  {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: new Container(
            color: widget.noti.readStatus?global.notireadBackColor:global.notiUnreadBackColor,
            padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
            child: GestureDetector(
              onTap: () async {
                global.receiptLastScreen=true;
                global.receiptID=widget.noti.receiptID;
                bool flag=await updateAlertStatus(widget.noti.alertid);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        EditContainerActivity(),
                  ),
                ).then((value) => {
                  updateState()
                });
              },
              child: Stack(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                      width: MediaQuery.of(context).size.width,
                      color: widget.noti.readStatus?global.notireadBackColor:global.notiUnreadBackColor,
                      child: Center(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              new Flexible(
                                flex:1,
                                fit:FlexFit.tight,
                                child: Center(
                                  child:new Container(
                                    width: 40.0,
                                    height: 40.0,
                                    child:
                                    widget.noti.imageType!=null?(
                                        widget.noti.isNetImage?(
                                            CircleAvatar(
                                              backgroundImage:
                                              NetworkImage(widget.noti.profileImg),
                                              backgroundColor: global.imageBackColor,
                                            )
                                        ):(widget.noti.imageType.toString().compareTo("true")==0?(
                                            CircleAvatar(
                                              backgroundImage: MemoryImage(base64Decode(widget.noti.profileImg)),
                                              backgroundColor: global.imageBackColor,
                                            )
                                        ) :
                                        (CircleAvatar(
                                          child:Text(
                                            widget.noti.profileImg.toString(),style: TextStyle(color: new Color(0xffffffff)),),
                                          backgroundColor: global.colors.elementAt(3),
                                        )
                                        )
                                        )
                                    ) : CircleAvatar(
                                      backgroundImage: AssetImage('assets/dummy_user.png'),
                                      backgroundColor: global.appbargreycolor,
                                    ),
                                  ),
                                ),
                              ),
                              new Flexible(
                                  flex: 6,
                                  fit: FlexFit.tight,
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                        child: Opacity(
                                          opacity: 0.87,
                                          child: Html(
                                              shrinkWrap: true,
                                              data: widget.noti.title,
                                              style: {
                                                "p": Style(
                                                    fontSize: FontSize(global.font15),
                                                    color: global.blackcolor,
                                                    fontFamily: 'BalooChetanRegular'
                                                ),
                                                "B": Style(
                                                    fontSize: FontSize(global.font15),
                                                    color: global.blackcolor,
                                                    fontFamily: 'BalooChetanSemiBold'
                                                ),
                                              }
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                              )
                            ]
                        ),
                      )
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child:new Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: Opacity(
                            opacity: 0.4,
                            child: Text(widget.noti.timestamp, style: TextStyle(fontSize: global.font10,color:Color(0xD9000000),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          )
                      )
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/SignInClass.dart';
import 'package:tapit/global.dart' as global;
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;
import 'package:tapit/helpers/AlertTypeList.dart';
import 'package:tapit/helpers/Constants.dart';
import 'OTPClass.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:tapit/helpers/UserDetails.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';

class SignUpClass extends StatefulWidget
{
  SignUpClassState createState() => SignUpClassState();
}

class SignUpClassState extends State<SignUpClass>
{
  BuildContext mcontext;

  bool show_missing_text=false;
  bool responseReceived=false;
  bool isUserAlreadyExist=false;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final facebookLogin = FacebookLogin();
  final emailController = TextEditingController();
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final mobnoController = TextEditingController();
  String firstname = null;
  String lastname = null;
  String mobileno = null;
  String emailaddress = null;
  String fcmToken="null";
  String country_code="";
  String picture="";
  String initialCountryCode="US";
  String countryCodeName="";
  String socialMediaEmail="";

  bool emailVerified=false;
  bool mobileVerified=false;
  Map userProfile;

  String LOGTAG="SignUpClass";
  bool firstname_validate = false;
  bool lastname_validate = false;
  bool mobileno_validate = false;
  bool emailadd_validate = false;
  String first_missing_text="";
  String last_missing_text="";
  String mobno_missing_text="";
  String email_missing_text="";

  @override
  void initState()
  {
    if(global.initialCountryCode!=null && global.initialCountryCode.isNotEmpty)
    {
      setState(()
      {
        initialCountryCode=global.initialCountryCode;
      });
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();

    initialiseUI();
  }

  void initialiseUI()
  {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    FocusManager.instance.primaryFocus.unfocus();
    getLocation();

    setState(()
    {
      responseReceived=false;
      emailController.text="";
      firstnameController.text="";
      lastnameController.text="";

      firstname = null;
      lastname = null;
      emailaddress = null;
      firstname_validate = false;
      lastname_validate = false;
      emailadd_validate = false;
      first_missing_text="";
      last_missing_text="";
      email_missing_text="";

      if(global.signUpMobNo!=null && global.signUpMobNo.isNotEmpty)
      {
        mobnoController.text=global.signUpMobNo;
        mobileno = global.signUpMobNo;
        mobileno_validate = false;
        mobno_missing_text="";
      }
      else
      {
        mobileno = null;
        mobnoController.text="";
        mobileno_validate = false;
        mobno_missing_text="";
      }
    });
  }

  void getLocation()async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cc = prefs.getString('countryCode');
    if(cc==null)
    {
      Location location = new Location();
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied)
      {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted)
        {

        }
        else if(_permissionGranted == PermissionStatus.granted)
        {
          _locationData = await location.getLocation();

          var lat=_locationData.latitude;
          var long=_locationData.longitude;

          final coordinates = new Coordinates(lat, long);
          var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          setState(()
          {
            initialCountryCode = first.countryCode;
            global.initialCountryCode=first.countryCode;
          });
        }
      }

      if(global.initialCountryCode.isEmpty || global.initialCountryCode==null)
      {
        _locationData = await location.getLocation();

        var lat = _locationData.latitude;
        var long = _locationData.longitude;
        final coordinates = new Coordinates(lat, long);
        var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
        var first = addresses.first;
        setState(()
        {
          initialCountryCode = first.countryCode;
          global.initialCountryCode=initialCountryCode;
        });
      }
    }
    else
    {
      setState(() {
        initialCountryCode=cc;
      });
    }
  }

  void checkMobile()
  {
    setState(()
    {
      firstname=firstnameController.text;
      lastname=lastnameController.text;
      mobileno = mobnoController.text;
      if (mobileno.length == 10)
      {
        getMobile();
      }
    });
  }

  Future<String> getMobile() async
  {
    isUserAlreadyExist=false;
    String mobiletext = mobnoController.text;
    String cc="+"+this.country_code.substring(1,this.country_code.length);

    UserService userService=new UserService();
    ResponseBean response=await userService.checkMobileExist(mobiletext,cc);
    int statusCode=response.status;

    if (statusCode == 200 || statusCode==204)
    {
      var payload=response.payLoad;
      isUserAlreadyExist=true;
      if (payload == "null" || payload==null)
      {

      }
      else
      {
        String mainText="";
        String subText="";
        if(firstname=="" && lastname=="")
        {
          mainText = "Great to see you back!";
          subText="Let us Log in you through OTP verification";
        }
        else
        {
          mainText = "Hi " + firstname + " " + lastname;
          subText="We found an existing account for your details";
        }

        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("User is Registered"),
                content: Text("Please procees to signin"),
                actions: [
                  FlatButton(
                    onPressed: () =>
                    {
                      SystemChannels.textInput.invokeMethod('TextInput.hide'),
                      Navigator.of(context).pop(),
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SignInClass(main_text: mainText,sub_text: subText,),
                        ),
                      ).then((value) => {

                        initialiseUI()
                      }),
                    },
                    child: Text("Signin"),
                  ),
                ],
              );
            });
      }
    }
    else
    {
      isUserAlreadyExist=true;
    }
    return "success";
  }

  void signup() async
  {

    fcmToken="";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token=prefs.getString("FCMToken");
    if(token==null || token.isEmpty)
    {

    }
    else
    {
      fcmToken=token;
    }

    setState(()
    {
      show_missing_text=false;
      firstname_validate = false;
      lastname_validate = false;
      mobileno_validate = false;
      emailadd_validate = false;

      firstname = firstnameController.text;
      lastname = lastnameController.text;
      mobileno = mobnoController.text;
      emailaddress = emailController.text;

      if (firstname.isEmpty || firstname==" " || firstname.length==0)
      {
        show_missing_text=true;
        first_missing_text="First name is mandatory";
        firstname_validate = true;
      }
      if(lastname.isEmpty || lastname==" " ||  lastname.length==0)
      {
        show_missing_text=true;
        last_missing_text="Last name is mandatory";
        lastname_validate = true;
      }
      if(mobileno.isEmpty || mobileno==" " || mobileno.length==0)
      {
        show_missing_text=true;
        mobno_missing_text="Mobile number is mandatory";
        mobileno_validate = true;
      }
      if(mobileno.length<10 || mobileno.length>10)
      {
        show_missing_text=true;
        mobno_missing_text="Enter valid Mobile number";
        mobileno_validate = true;
      }
      if(emailaddress.isEmpty || emailaddress==" " || emailaddress.length==0)
      {
        show_missing_text=true;
        email_missing_text="Email Address is mandatory";
        emailadd_validate=true;
      }
      else
      {
        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailaddress);
        if (!emailValid)
        {
          emailadd_validate = true;
          email_missing_text = "Email address not in standard format";
        }
      }
    });

    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailaddress);
    if(emailadd_validate==false && mobileno_validate==false &&  lastname_validate==false && firstname_validate==false)
    {
      if (!emailValid)
      {
        emailadd_validate = true;
        email_missing_text="Email address not in standard format";
      }
      else
      {
        if(socialMediaEmail.toString().compareTo(emailaddress)!=0)
        {
          if(socialMediaEmail.isNotEmpty)
          {
            setState(() {
              emailVerified=false;
            });
          }
        }

        setState(()
        {
          responseReceived=true;
        });


        String fullname = firstname + " " + lastname;
        String cc="+"+this.country_code.substring(1,this.country_code.length);
        String birthday,gender;
        bool offerNoti=false;

        List<AlertTypeList> alertSettingTypeList=new List();
        AlertTypeList alertTypeList=new AlertTypeList("BILL_SPLIT",true,true,true,true);
        alertSettingTypeList.add(alertTypeList);

        if(isUserAlreadyExist)
        {
          UserDetails userDetails = new UserDetails(name: fullname,
              email: emailaddress,
              countryCode: cc,
              mobileNo: mobileno,
              deviceType: Constants.BASE_PLATFORM,
              fcmToken: fcmToken,
              picture: picture,
              emailVerified: emailVerified.toString(),
              mobileVerified: mobileVerified.toString(),
              dob: birthday,
              gender: gender,
              alertSettingTypeList: alertSettingTypeList);
          var jsonBody = jsonEncode(userDetails);
          UserService userService = new UserService();
          ResponseBean response = await userService.UpdateUserData(jsonBody);
          int statusCode = response.status;

          if (statusCode == 201 || statusCode == 200)
          {
            var user = jsonEncode(response.payLoad);
            var userdecode = json.decode(user);

            String userid = userdecode['id'];
            SharedPreferences prefs = await SharedPreferences.getInstance();
            bool CheckValue = prefs.containsKey('CurrentFirstName');
            if (CheckValue) {
              prefs.remove("CurrentFirstName");
              prefs.remove("CurrentLastName");
              prefs.remove("MobileNo");
              prefs.remove("UserID");
              prefs.remove("UserImage");
              prefs.remove("countryCode");
              prefs.remove("email");
              prefs.remove("emailVerified");
              prefs.remove("countryCodeName");
            }
            prefs.setString("CurrentFirstName", firstname);
            prefs.setString("CurrentLastName", lastname);
            prefs.setString("UserID", userid);
            prefs.setString("MobileNo", mobileno);
            prefs.setString("countryCode", cc);
            prefs.setString("countryCodeName", countryCodeName);
            prefs.setString("email", emailaddress);
            prefs.setString("emailVerified", emailVerified.toString());
            prefs.setBool("offerNoti", offerNoti);

            if (picture == null || picture.isEmpty)
            {
              prefs.setString("UserImage", "");
            }
            else
            {
              String urlpic = picture;
              http.Response responsepic = await http.get(urlpic);
              Uint8List uimg = responsepic.bodyBytes;
              String img = base64Encode(uimg);
              prefs.setString("UserImage", img);
            }

            otpGenerate();
          }
          else if (statusCode == 204) {
            setState(() {
              responseReceived = false;
            });
            global.helperClass.showAlertDialog(context, "Error", "No Content");
          }
          else if (statusCode == 400) {
            setState(() {
              responseReceived = false;
            });
            global.helperClass.showAlertDialog(context, "Error", "Bad Request");
          }
          else if (statusCode == 404) {
            setState(() {
              responseReceived = false;
            });
            global.helperClass.showAlertDialog(context, "Error", "Not Found");
          }
          else if (statusCode == 500) {
            setState(() {
              responseReceived = false;
            });
            global.helperClass.showAlertDialog(context, "Error", "Internal Server Error");
          }
          else
          {
            setState(() {
              responseReceived = false;
            });
            global.helperClass.showAlertDialog(context, "Error", "Not able to sign up");
          }
        }
      }
    }
  }


  void otpGenerate() async
  {
    String cc="+"+this.country_code.substring(1,this.country_code.length);
    UserService userService=new UserService();
    ResponseBean response=await userService.otpGenerate(mobileno,cc);
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        setState(()
        {
          responseReceived = false;
        });

        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  OTPClass(usermobileno: mobileno, countryCode: cc)),
        );
      }
      else if (statusCode == 204)
      {
        setState(()
        {
          responseReceived = false;
        });
        global.helperClass.showAlertDialog(context, "Error", "No Content");
      }
      else if (statusCode == 400)
      {
        setState(() {
          responseReceived = false;
        });
        global.helperClass.showAlertDialog(context, "Error", "Bad Request");
      }
      else if (statusCode == 404)
      {
        setState(() {
          responseReceived = false;
        });
        global.helperClass.showAlertDialog(context, "Error", "Not Found");
      }
      else if (statusCode == 500)
      {
        setState(() {
          responseReceived = false;
        });

        if(response.message.toString().contains("is not a mobile number"))
        {
          global.helperClass.showAlertDialog(context, "Error", "Not able to sign up. Please enter valid mobile number");
        }
        else
        {
          global.helperClass.showAlertDialog(context, "Error", "Not able to sign up");
        }
      }
      else
      {
        setState(() {
          responseReceived = false;
        });

        global.helperClass.showAlertDialog(context, "Error", "Not able to sign up");
      }
    }
    else
    {
      setState(() {
        responseReceived = false;
      });
      global.helperClass.showAlertDialog(context, "Error", "Not able to sign up. Please try again");
    }
  }


  void _onCountryChange(CountryCode countryCode)
  {
    this.countryCodeName=countryCode.code.toString();
    this.country_code=countryCode.toString();
  }

  void google_signup() async
  {
    picture="";
    socialMediaEmail="";
    bool google_signed_in_status=await googleSignIn.isSignedIn();
    if(google_signed_in_status==true)
    {
      await googleSignIn.signOut();
    }

    setState(() {
      responseReceived=true;
    });

    GoogleSignInAccount googleSignInAccount;
    GoogleSignInAuthentication googleSignInAuthentication;
    AuthCredential credential;
    AuthResult authResult;
    FirebaseUser user;
    FirebaseUser currentUser;
    var arr;
    googleSignIn.signIn().then((value) async => {

      googleSignInAccount=value,
      googleSignInAuthentication = await googleSignInAccount.authentication,
      credential = GoogleAuthProvider.getCredential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      ),

      authResult = await _auth.signInWithCredential(credential),
      user = authResult.user,
      currentUser = await _auth.currentUser(),
      picture=currentUser.photoUrl,
      emailVerified=true,

      arr = currentUser.displayName.split(' '),
      socialMediaEmail=currentUser.email,
      setState(() {
        firstnameController.text=arr[0];
        lastnameController.text=arr[1];
        emailController.text=currentUser.email;
      }),
      setState(() {
        responseReceived=false;
      }),

    }).catchError((error, stackTrace) {
      setState(() {
        responseReceived=false;
      });
    });
  }

  void facebook_signup() async
  {
    picture="";
    socialMediaEmail="";
    final result = await facebookLogin.logIn(['email','public_profile', 'user_friends']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
        final profile = JSON.jsonDecode(graphResponse.body);

        setState(() {
          userProfile=profile;
        });

        String url=userProfile["picture"]["data"]["url"].toString();

        picture=url;
        var arr = profile['name'].split(' ');

        socialMediaEmail=profile['email'];
        setState(() {
          firstnameController.text=arr[0];
          lastnameController.text=arr[1];
          emailController.text=profile['email'];
        });

        emailVerified=true;
        break;

      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    mcontext = context;

    final firstnameField = TextField(
        onTap: () {
          setState(() {
            firstname_validate = false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        textCapitalization: TextCapitalization.words,
        cursorColor: global.mainColor,
        obscureText: false,
        controller: firstnameController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "First name",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    final lastnameField = TextField(
        onTap: () {
          setState(() {
            lastname_validate = false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        textCapitalization: TextCapitalization.words,
        cursorColor: global.mainColor,
        obscureText: false,
        controller: lastnameController,
        decoration:InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "Last name",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    final mobilenoField = TextField(
        onTap: () {
          setState(() {
            mobileno_validate = false;
          });
        },
        onChanged: (value) => {
          checkMobile(),
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        keyboardType: TextInputType.number,
        cursorColor: global.mainColor,
        obscureText: false,
        controller: mobnoController,
        decoration:InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "Mobile number",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    final emailaddressField = TextField(
        onTap: () {
          setState(() {
            emailadd_validate = false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        cursorColor: global.mainColor,
        obscureText: false,
        controller: emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "Email address",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                'assets/logo.png',
                fit: BoxFit.contain,
                height: 25,
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(30,12,0,12),
                  child: Text("Sign up",
                      style: new TextStyle(
                          fontSize: global.font18,
                          color: global.appbarTextColor,
                          fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'
                      )))
            ],
          ),
          backgroundColor: global.appbarBackColor),
      body:
      responseReceived?new Container(
          child:Center(
            child:new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          )
      ):
      new Container(
        height: MediaQuery.of(context).size.height,
        color: Color(0xFFffffff),
        child: new Padding(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: new Stack(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            child: SizedBox(
                              height: 36,
                              child: firstnameField,
                            )
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        new Flexible(
                            child: SizedBox(
                              height: 36,
                              child: lastnameField,
                            )
                        ),
                      ],
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            child: SizedBox(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                                    child: Offstage(
                                      offstage: !firstname_validate,
                                      child: new Text(
                                        first_missing_text,
                                        textAlign: TextAlign.left,
                                        style: new TextStyle(
                                            fontSize: global.font12,
                                            color: global.errorColor,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'BalooChetanRegular'
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        new Flexible(
                            child: SizedBox(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                                    child: Offstage(
                                      offstage: !lastname_validate,
                                      child: new Text(
                                        last_missing_text,
                                        textAlign: TextAlign.left,
                                        style: new TextStyle(
                                            fontSize: global.font12,
                                            color: global.errorColor,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'BalooChetanRegular'
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Color(0xfff7f7f7),
                              border: Border.all(color: Color(0xffc8c8c8), width: 0.75),
                              borderRadius: BorderRadius.all(
                                  Radius.circular(5.0)),
                            ),
                            alignment: Alignment.topCenter,
                            child: SizedBox(
                              height: 36,
                              child: Material(
                                elevation: 0.0,
                                color: Color(0xfff7f7f7),
                                shape: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xfff7f7f7)),
                                  borderRadius: BorderRadius.circular(6.0),
                                ),
                                child: CountryCodePicker(
                                  textStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 1),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                  onInit: _onCountryChange,
                                  onChanged: _onCountryChange,
                                  initialSelection: initialCountryCode,
                                  favorite: ['+91', 'IN','US'],
                                  showCountryOnly: false,
                                  showOnlyCountryWhenClosed: false,
                                  alignLeft: false,
                                  padding: EdgeInsets.all(0),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        new Flexible(
                            flex: 3,
                            child: SizedBox(
                              height: 36,
                              child: mobilenoField,
                            )),
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                          child: Offstage(
                            offstage: !mobileno_validate,
                            child: new Text(
                              mobno_missing_text,
                              textAlign: TextAlign.left,
                              style: new TextStyle(
                                  fontSize: global.font12,
                                  color: global.errorColor,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'BalooChetanRegular'
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 36,
                      child: emailaddressField,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Offstage(
                          offstage: !emailadd_validate,
                          child: new Text(
                            email_missing_text,
                            textAlign: TextAlign.left,
                            style: new TextStyle(
                                fontSize: global.font12,
                                color: global.errorColor,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'BalooChetanRegular'
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width:MediaQuery.of(context).size.width/2.5,
                          child:  new RaisedButton(
                            padding: EdgeInsets.fromLTRB(17,10,17,10),
                            onPressed: () {
                              signup();
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            color: global.mainColor,
                            child: Text('NEXT',
                                style: TextStyle(fontSize: global.font14, color: Color(0xffffffff),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold')),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Opacity(
                            opacity:0.8,
                            child: new Text(
                              "Or,Alternatively you can connect with",
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  fontSize: global.font12,
                                  color: new Color.fromRGBO(122, 122, 122, 0.87),
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'BalooChetanMedium'
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width:MediaQuery.of(context).size.width/2.5,
                            child:  RaisedButton(
                              onPressed: (){
                                google_signup();
                              },
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                              child: new Row(
                                children: <Widget>[
                                  Image.asset(
                                    'assets/google.png',
                                    fit: BoxFit.contain,
                                    height: 18,
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 16, top: 8, right: 5, bottom: 8),
                                    child:Text('Google', style: TextStyle(color: Color(0xff7f7f7f),fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                                  )
                                ],
                              ),
                              textColor:Color(0xff7f7f7f),
                              splashColor: Color(0xffffffff),
                              color: Color(0xfff6f6f6),
                            ),
                          ),
                          SizedBox(
                            width: 17,
                          ),
                          Container(
                            width:MediaQuery.of(context).size.width/2.5,
                            child: RaisedButton(
                              onPressed: (){
                                facebook_signup();
                              },
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                              child: new Row(
                                children: <Widget>[
                                  Image.asset(
                                    'assets/facebook.png',
                                    fit: BoxFit.contain,
                                    height: 18,
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 16, top: 8, right: 5, bottom: 8),
                                    child:Text('Facebook', style: TextStyle(color: Color(0xff7f7f7f),fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                                  )
                                ],
                              ),
                              textColor:Color(0xff7f7f7f),
                              splashColor: Color(0xffffffff),
                              color: Color(0xfff6f6f6),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height:  MediaQuery.of(context).size.height/15.7,
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new RichText(
                            text: new TextSpan(
                              children: [
                                new TextSpan(
                                  text: 'I have an account already.',
                                  style: new TextStyle(
                                      fontSize: global.font14, color: Color(0xff000000),fontWeight: FontWeight.normal,
                                      fontFamily: 'BalooChetanRegular'),
                                ),
                                new TextSpan(
                                  text: 'Sign in',
                                  style: new TextStyle(
                                      fontSize: global.font14,
                                      color: global.mainColor,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'BalooChetanRegular',
                                      decoration: TextDecoration.underline),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SignInClass(main_text: "Great to see you back!",sub_text: "Let us Log in you through OTP verification",)),
                                      );
                                    },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height/13.7,
                      ),
                    ],
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}

import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:tapit/Activities/SignUpClass.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/SliderModel.dart';

class IntroductionSlider extends StatefulWidget
{
  _IntroductionSliderState createState()=> _IntroductionSliderState();
}

class _IntroductionSliderState extends State<IntroductionSlider>
{
  List<SliderModel> mySLides = new List<SliderModel>();
  int slideIndex = 0;
  PageController controller;
  String LOGTAG="IntoSlider";

  @override
  void initState()
  {
    mySLides = getSlides();
    super.initState();
    controller = new PageController();
  }

  void onSkipPress()
  {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignUpClass()),);
  }

  Widget _buildPageIndicator(bool isCurrentPage)
  {
    return Container(
      margin: EdgeInsets.fromLTRB(5,10,5,10),
      height: isCurrentPage ? 12.0 : 12.0,
      width: isCurrentPage ? 12.0 : 12.0,
      decoration: BoxDecoration(
        gradient: isCurrentPage?new LinearGradient(
            colors: global.introButtonGradient,
            tileMode: TileMode.clamp
        ):new LinearGradient(
            colors: global.greyGradient,
            tileMode: TileMode.clamp
        ),
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  List<SliderModel> getSlides()
  {
    List<SliderModel> slides = new List<SliderModel>();
    SliderModel sliderModel = new SliderModel();
    sliderModel.setDesc("Thermal paper making involves BPA chemicals hazrdous to earth and human too");
    sliderModel.setTitle("Go Green with E`receipts");
    sliderModel.setImageAssetPath("assets/Onb-1.png");
    slides.add(sliderModel);

    sliderModel = new SliderModel();
    sliderModel.setDesc("Whether at a restaurant or a mall, split bills with your friends and settle later");
    sliderModel.setTitle("Split and Settle bills hasslefree");
    sliderModel.setImageAssetPath("assets/Onb-2.png");
    slides.add(sliderModel);

    sliderModel = new SliderModel();
    sliderModel.setDesc("Revisiting places rewards you with exclusive offers for amazing experiences");
    sliderModel.setTitle("Get rewards for staying connected");
    sliderModel.setImageAssetPath("assets/Onb-3.png");
    slides.add(sliderModel);

    return slides;
  }

  @override
  Widget build(BuildContext context) {

    if(Platform.isAndroid)
    {
      FlutterStatusbarcolor.setStatusBarColor(global.browncolor);
    }
    return  Scaffold(
      appBar: AppBar(
        title:new Text(""),
        elevation: 0,
        backgroundColor: global.appbarBackColor,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                  flex: 6,
                  fit:FlexFit.tight,
                  child: PageView(
                    controller: controller,
                    onPageChanged: (index) {
                      setState(() {
                        slideIndex = index;
                      });
                    },
                    children: <Widget>[
                      SlideTile(
                        context: context,
                        index:0,
                        imagePath: mySLides[0].getImageAssetPath(),
                        title: mySLides[0].getTitle(),
                        desc: mySLides[0].getDesc(),
                      ),
                      SlideTile(
                        context: context,
                        index:1,
                        imagePath: mySLides[1].getImageAssetPath(),
                        title: mySLides[1].getTitle(),
                        desc: mySLides[1].getDesc(),
                      ),
                      SlideTile(
                        context: context,
                        index:2,
                        imagePath: mySLides[2].getImageAssetPath(),
                        title: mySLides[2].getTitle(),
                        desc: mySLides[2].getDesc(),
                      )
                    ],
                  )
              ),
              Flexible(
                  flex: 2,
                  fit: FlexFit.tight,
                  child:  new Container(
                    color: global.whitecolor,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          flex:1,
                          fit: FlexFit.tight,
                          child:  Container(
                            color: global.whitecolor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                for (int i = 0; i < 3 ; i++) i == slideIndex ? _buildPageIndicator(true): _buildPageIndicator(false),
                              ],),
                          ),
                        ),
                        Flexible(
                            flex:2,
                            fit: FlexFit.tight,
                            child:new Column(
                              children: <Widget>[
                                new Container(
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                    height: 40,
                                    color: global.whitecolor,
                                    child: slideIndex==2?new Container(
                                        child:new RaisedButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                          color:  global.mainColor,
                                          padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 45.0),
                                          onPressed: () {
                                            onSkipPress();
                                          },
                                          child:new Text("GET STARTED", textAlign: TextAlign.center, style:TextStyle(color: global.whitecolor, fontSize: global.font15, fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                                        )
                                    ):
                                    GestureDetector(
                                        onTap: (){
                                          onSkipPress();
                                        },
                                        child:  new Container(
                                          color: global.whitecolor,
                                          child: new Opacity(
                                            opacity: 0.87,
                                            child:  new Text("SKIP", textAlign: TextAlign.center, style:TextStyle(color: Color(0xff000000), fontSize: global.font16, fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                          ),
                                        )
                                    )
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  )
              )
            ],
          )
      ),
    );
  }

}


class SlideTile extends StatelessWidget
{
  String imagePath, title, desc;
  int index;
  BuildContext context;
  SlideTile({this.context,this.index,this.imagePath, this.title, this.desc});

  @override
  Widget build(BuildContext context)
  {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: global.whitecolor,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            flex: 2,
            fit: FlexFit.loose,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Container(
                  width: index==0?MediaQuery.of(context).size.width/2.5:MediaQuery.of(context).size.width/1.5,
                  color: global.whitecolor,
                  child: Material(
                    color: global.whitecolor,
                    child: Text(title,
                      textAlign: TextAlign.center,
                      style:  TextStyle(color: Color(0xff000000), fontSize: global.font24, fontWeight: FontWeight.normal,fontFamily: 'BalooChetanSemiBold',),),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                new Container(
                  width: MediaQuery.of(context).size.width/1.2,
                  color: global.whitecolor,
                  child: Material(
                      color: global.whitecolor,
                      child:Opacity(
                        opacity: 0.6,
                        child: Text(desc,
                            textAlign: TextAlign.center,
                            style:TextStyle(color: Color(0xff000000), fontSize: global.font15, fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                      )
                  ),
                ),
              ],
            ),
          ),
          Flexible(
              flex: 4,
              fit: FlexFit.tight,
              child: new Container(
                padding: EdgeInsets.all(20),
                child: new Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: global.whitecolor,
                    image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        imagePath,
                      ),
                    ),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }
}
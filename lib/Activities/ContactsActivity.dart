import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/helpers/ContactList.dart';
import 'package:tapit/helpers/ImageHorizontalScroll.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'AddGroupSummary.dart';
import 'package:http/http.dart' as http;
import 'dart:math';

class ContactsActivity extends StatefulWidget
{
  ContactsActivityState createState()=> ContactsActivityState();
}

class ContactsActivityState extends State<ContactsActivity>
{
  bool searchStarted=false;
  static BuildContext mContext;
  String countryCode;

  Widget appBarTitle =  new Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      GestureDetector(
        onTap: (){
          Navigator.of(mContext).pop();
        },
        child: Container(
          padding:  EdgeInsets.fromLTRB(15,0,0,0),
          child: Image.asset(
            'assets/back_arrow.png',
            fit: BoxFit.contain,
            height: 20,
          ),
        ),
      ),
      Container(
          padding: EdgeInsets.fromLTRB(30,0,0,0),
          child:  Text("Contacts",style: new TextStyle(
              fontSize: global.font18,
              color: global.appbarTextColor,
              fontWeight: FontWeight.normal,
              fontFamily: 'BalooChetanSemiBold'
          )
          )
      ),
    ],
  );
  Icon actionIcon = new Icon(Icons.search, color: global.blackcolor,);

  List<ContactData> sel_contact_list=new List();
  List<ContactData> contact_list=new List();
  List<ContactData> items=new List();
  TextEditingController editingController = TextEditingController();
  ItemScrollController _scrollController = ItemScrollController();

  Uint8List MainprofileImg;
  Random random=new Random();
  bool showButton=false;
  bool isRefreshed=false;
  bool isAlphabet=false;
  bool userFound=false;
  bool userfoundtext=false;
  SharedPreferences prefs;

  String LOGTAG="ContactsActivity";

  @override
  void initState(){
    getData();
    super.initState();
  }

  bool isNumeric(String str) {
    RegExp _numeric = RegExp(r'^-?[0-9]+$');
    return _numeric.hasMatch(str);
  }

  void filterSearchResults(String query) async
  {
    List<ContactData> dummySearchList = new List();
    dummySearchList.addAll(contact_list);

    setState(() {
      searchStarted=true;
      global.searchitem=query;
    });

    if(query.length==1)
    {
      bool val=isNumeric(query);
      if(!val)
      {
        isAlphabet=true;
      }
      else
      {
        isAlphabet=false;
      }
    }

    if(query.isNotEmpty)
    {
      List<ContactData> dummyListData = List<ContactData>();
      dummySearchList.forEach((item)
      {
        if(isAlphabet)
        {
          if(item.name!=null)
          {
            if (item.name.toLowerCase().contains(query))
            {
              dummyListData.add(item);
            }
          }
        }
        else
        {
          if (item.number.contains(query))
          {
            dummyListData.add(item);
          }
        }
      });

      if(dummyListData.length==0)
      {
        if(query.length==10)
        {
          ContactData contactData=await checkAlreadyPresent(query);
          if(contactData!=null)
          {
            dummyListData.add(contactData);

            if(contactData.name!=null)
            {
              setState(()
              {
                userFound = true;
                userfoundtext = true;
              });
            }
            else{
              setState(()
              {
                userFound=true;
                userfoundtext=false;
              });
            }
          }
          else
          {
            setState(()
            {
              userFound=true;
              userfoundtext=false;
            });
          }
        }
      }

      items.clear();
      items.addAll(dummyListData);

      global.contactImageList.clear();
      for(int s=0;s<items.length;s++)
      {
        ContactData obj=dummyListData.elementAt(s);
        Uint8List temp=await global.helperClass.convertImg(obj);
        global.contactImageList.add(temp);
      }
      return;
    }
    else
    {
      items.clear();
      items.addAll(contact_list);
      global.contactImageList.clear();
      for(int s=0;s<items.length;s++)
      {
        ContactData obj=items.elementAt(s);
        Uint8List temp=await global.helperClass.convertImg(obj);
        global.contactImageList.add(temp);
      }
    }
  }

  Future<ContactData> checkAlreadyPresent(String number) async
  {
    global.searchItemNumber=false;

    ContactData contactData ;
    String usertype;
    String imageData;
    String username;
    String mobiletext = number;

    UserService userService=new UserService();
    ResponseBean response=await userService.checkMobileExist(mobiletext,countryCode);
    int statusCode=response.status;

    if (statusCode == 200)
    {
      var payload=response.payLoad;
      if (payload == "null" || payload==null)
      {
        usertype=null;
        username=null;
        imageData="null";
        global.searchItemNumber=true;

        contactData = new ContactData(
            userID:"",
            cc:countryCode,
            isTapitUser:false,
            type: usertype,
            colordata:random.nextInt(6),
            image: imageData,
            name: username,
            number: number.replaceAll(" ", ""),
            isSelected:false);
      }
      else
      {
        usertype="";
        username="";
        imageData="PR";

        global.searchItemNumber=false;
        var nameJson = response.payLoad;
        String nameString = jsonEncode(nameJson);
        Map<String, dynamic> map = jsonDecode(nameString);
        username = map['name'];
        String userid = map['id'];
        imageData=map['picture'];

        if(imageData==null || imageData.isEmpty)
        {
          imageData="null";
          contactData = new ContactData(userID:userid, cc:countryCode, isTapitUser:true, type: null, colordata:random.nextInt(6), image: imageData, name: username, number: number.replaceAll(" ", ""), isSelected:false);
        }
        else
        {
          http.Response response = await http.get(imageData);
          Uint8List uimg = response.bodyBytes;
          String image= new String.fromCharCodes(uimg);
          contactData = new ContactData(userID:userid, cc:countryCode, isTapitUser:true, type: "true", colordata:random.nextInt(6), image: image, name: username, number: number.replaceAll(" ", ""), isSelected:false);
        }
      }
    }
    else
    {
      contactData =null;
    }

    return contactData;
  }

  Future<void> getData() async
  {
    global.contactImageList.clear();
    global.selectedImageList.clear();

    prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");
    countryCode=prefs.getString("countryCode");

    if(map!=null)
    {
      setState(() {
        searchStarted = false;
        global.searchitem = "";
        contact_list = (json.decode(map) as List).map((i) =>
            ContactData.fromJson(i)).toList();
      });
    }
    items.addAll(contact_list);
    for(int s=0;s<items.length;s++)
    {
      ContactData obj=items.elementAt(s);
      Uint8List temp=await global.helperClass.convertImg(obj);
      global.contactImageList.add(temp);
    }
  }

  void getIndexFromList(int newindex)
  {
    int scrollindex=0;
    ContactData newcontactData=sel_contact_list.elementAt(newindex);
    for(int i=0;i<contact_list.length;i++)
    {
      ContactData contactData=contact_list.elementAt(i);
      if(contactData.number.contains(newcontactData.number))
      {
        scrollindex=i;
        break;
      }
    }
    _scrollController.jumpTo(index: scrollindex,alignment: 0);
  }

  Future<void> updateImageList() async
  {
    global.selectedImageList.clear();
    for(int s=0;s<sel_contact_list.length;s++)
    {
      ContactData obj=sel_contact_list.elementAt(s);
      Uint8List temp=await global.helperClass.convertImg(obj);
      global.selectedImageList.add(temp);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    mContext=context;
    return Scaffold(
      resizeToAvoidBottomPadding:false,
      appBar: buildBar(context),
      body: Container(
          color: Colors.white,
          child: Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child:Stack(
                  children: <Widget>[
                    new Container(
                        padding:  userFound?EdgeInsets.fromLTRB(0, 30, 0, 0):EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: ScrollablePositionedList.builder(
                          addAutomaticKeepAlives: true,
                          itemScrollController: _scrollController,
                          itemCount: items.length,
                          itemBuilder: (context, index)
                          {
                            return Center(
                                child:Container(
                                  child: ContactList(index:index,contactData: items[index],profileImg: global.contactImageList[index],isSel: (bool value) {
                                    setState(() {
                                      isRefreshed=true;
                                      if (value)
                                      {
                                        ContactData ncd=items[index];
                                        ncd.isSelected=value;
                                        bool alreadyPresent=false;
                                        for(int k=0;k<sel_contact_list.length;k++)
                                        {
                                          ContactData cd=sel_contact_list.elementAt(k);
                                          if(cd.number.toString().compareTo(ncd.number.toString())==0 && cd.cc.toString().compareTo(ncd.cc.toString())==0)
                                          {
                                            alreadyPresent=true;
                                          }
                                        }
                                        if(!alreadyPresent)
                                        {
                                          sel_contact_list.add(items[index]);
                                        }
                                        updateImageList();
                                      }
                                      else
                                      {
                                        ContactData ncd=items[index];
                                        ncd.isSelected=value;
                                        sel_contact_list.remove(items[index]);
                                        updateImageList();
                                      }
                                    });
                                  },),
                                )
                            );
                          },
                        )
                    ),
                    new Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: userFound?new Container(
                        color: global.whitecolor,
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            userfoundtext?Container(
                              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              child: Text("1 Tapit user", style: TextStyle(fontSize: global.font18,color: Color(0xff616161),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            ):
                            Container(
                              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              child: Text("No Matches", style: TextStyle(fontSize: global.font18,color: Color(0xff616161),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            ),
                            Container(
                              padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                              width: MediaQuery.of(context).size.width,
                              decoration: new BoxDecoration(
                                color: Colors.white,
                                border: Border(
                                  bottom: BorderSide(
                                    color: Color(0xffdcdcdc),
                                    width: 1.5,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ):new Container(
                        width: 0,
                        height: 0,
                      ),
                    ),
                    new Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child: sel_contact_list.length>0?
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(15,0, 10, 0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Color(0xffFFFBF9),
                                  boxShadow: [BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.2),
                                      offset: Offset(0,-1),
                                      blurRadius: 5.0,
                                      spreadRadius: 0
                                  ),]
                              ),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    height: 6,
                                  ),
                                  new Text("Selection",style: TextStyle(color: global.nameTextColor,fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanMedium'),),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    child:ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: sel_contact_list.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                          child: ImageHorizontalScroll(index:index,contactData:sel_contact_list[index],profileImg: global.selectedImageList[index],flag:1,isSelected: (bool value) {
                                            setState(() {
                                              if (value) {
                                                getIndexFromList(index);
                                              }
                                            });
                                          },),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              width:MediaQuery.of(context).size.width,
                              decoration: new BoxDecoration(
                                gradient: new LinearGradient(
                                    colors: global.buttonGradient,
                                    tileMode: TileMode.clamp
                                ),
                              ),
                              child: FlatButton(
                                child: Text("SAVE CHANGES",style: TextStyle(color: global.whitecolor,fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                                onPressed: () async {
                                  String type="true";
                                  String username=prefs.getString("CurrentFirstName")+" "+prefs.getString("CurrentLastName");
                                  String mobnumber=prefs.getString("MobileNo");
                                  String image=prefs.getString("UserImage");
                                  String userID=prefs.getString("UserID");
                                  String cc=prefs.getString("countryCode");

                                  if(image==null || image.isEmpty)
                                  {
                                    var arr=username.split(" ");
                                    if(arr.length>=2)
                                    {
                                      if(arr[0]!=null && arr[0].isNotEmpty)
                                      {
                                        image = arr[0].substring(0, 1);
                                      }
                                      if(arr[1]!=null && arr[1].isNotEmpty)
                                      {
                                        image = image + arr[1].substring(0, 1);
                                      }
                                    }
                                    else
                                    {
                                      image=arr[0];
                                    }
                                    type="false";
                                  }

                                  bool flag=false;
                                  ContactData con=new ContactData( userID:userID,cc:cc,isTapitUser:true, type:type ,colordata:random.nextInt(6), image: image, name: username, number: mobnumber,isSelected:true);

                                  for(int i=0;i<sel_contact_list.length;i++)
                                  {
                                    if(sel_contact_list.elementAt(i).number.contains(mobnumber))
                                    {
                                      flag=true;
                                      break;
                                    }
                                  }
                                  List<ContactData> new_sel_list=new List();
                                  new_sel_list.addAll(sel_contact_list);

                                  if(!flag)
                                  {
                                    new_sel_list.insert(0, con);
                                    Uint8List tempimg=await global.helperClass.convertImg(con);
                                    global.selectedImageList.insert(0,tempimg);
                                  }

                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          AddGroupSummary(sel_contact_list: new_sel_list),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        )
                            :new Container(
                          width: 0,
                          height: 0,
                        )
                    )
                  ]
              )
          )
      ),
    );
  }
  Widget buildBar(BuildContext context) {
    return new AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title:appBarTitle,
        backgroundColor: global.appbarBackColor,
        actions: <Widget>[
          new IconButton(icon: actionIcon, onPressed: () {
            setState(() {
              searchStarted=true;
              if (this.actionIcon.icon == Icons.search) {
                this.actionIcon = new Icon(Icons.close, color: global.blackcolor,);
                this.appBarTitle =
                new Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        Navigator.of(mContext).pop();
                      },
                      child: Container(
                        padding:  EdgeInsets.fromLTRB(15,0,0,0),
                        child: Image.asset(
                          'assets/back_arrow.png',
                          fit: BoxFit.contain,
                          height: 20,
                        ),
                      ),
                    ),
                    Expanded(
                        child:new TextField(
                          onChanged: (value){
                            filterSearchResults(value);
                          },
                          cursorColor: global.mainColor,
                          controller: editingController,
                          style: new TextStyle(
                            color: global.blackcolor,
                          ),
                          decoration: new InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: new Icon(Icons.search, color:  global.blackcolor),
                              hintText: "Name/Phone no",
                              hintStyle: new TextStyle(fontSize: global.font17, color:  global.appbargreycolor,fontWeight: FontWeight.normal,
                                  fontFamily: 'BalooChetanSemiBold')
                          ),
                        )
                    )
                  ],
                );
              }
              else {
                _handleSearchEnd();
              }
            });
          },),
        ]
    );
  }

  Future<void> _handleSearchEnd() async {

    global.searchItemNumber=false;

    setState(() {
      searchStarted=false;
      this.actionIcon = new Icon(Icons.search, color:  global.blackcolor,);
      this.appBarTitle =new Row(
        children: <Widget>[
          GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Container(
              padding:  EdgeInsets.fromLTRB(15,0,0,0),
              child: Image.asset(
                'assets/back_arrow.png',
                fit: BoxFit.contain,
                height: 20,
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(30,0,0,0),
              child:  Text("Contacts",style: new TextStyle(
                  fontSize: global.font18,
                  color: global.appbarTextColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'BalooChetanSemiBold'
              )
              )
          ),
        ],
      );

      userFound=false;
      global.searchitem="";
      editingController.clear();
    });

    items.clear();
    items.addAll(contact_list);
    global.contactImageList.clear();
    for(int s=0;s<items.length;s++)
    {
      ContactData obj=items.elementAt(s);
      Uint8List temp=await global.helperClass.convertImg(obj);
      global.contactImageList.add(temp);
    }
  }
}


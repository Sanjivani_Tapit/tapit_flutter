import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/helpers/SplitGroups.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'EditContainerActivity.dart';
import 'dart:convert';
import 'package:tapit/helpers/ReceiptData.dart';
import 'package:tapit/helpers/EdgeClipperBottom.dart';
import 'package:tapit/helpers/EdgeClipperTop.dart';

class ManualReceipt extends StatefulWidget
{
  ManualReceipt({Key key,this.receiptName,this.totalAmount,this.description}) : super(key: key);
  String receiptName;
  String totalAmount;
  String description;
  ManualReceiptState createState()=>ManualReceiptState();
}

class ManualReceiptState extends State<ManualReceipt>
{
  final String LOGTAG="ManualReceipt";

  @override
  void initState()
  {
    super.initState();
  }

  void saveReceipt() async
  {
    List<Fooditems> items;
    List<SplitGroups> splitGroups = new List();

    AddReceipt addReceipt = new AddReceipt( "", widget.totalAmount, "", "", "0", items, splitGroups, global.UserID,widget.description,1,widget.receiptName);
    var jsonbody = jsonEncode(addReceipt);
    UserService userService=new UserService();
    ResponseBean response=await userService.acceptReceipt(jsonbody);
    int statusCode = response.status;
    var respayload = response.payLoad;
    if (statusCode == 200)
    {
      global.receiptImageURL="https://images.pexels.com/photos/262978/pexels-photo-262978.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940";
      global.hotelname=widget.receiptName;
      global.receiptAmount=double.parse(widget.totalAmount);
      global.ereceiptToContainer=true;
      global.receiptLastScreen=false;
      global.receiptID=respayload['id'];

      Navigator.of(context).pushReplacement(
        PageRouteBuilder(
          transitionDuration: Duration(milliseconds: 500),
          pageBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return EditContainerActivity();
          },
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return Align(
              child: FadeTransition(
                opacity: animation,
                child: child,
              ),
            );
          },
        ),
      );
    }
    else
    {
      global.helperClass.showAlertDialog(context, "Error", "Can't create receipt right now.Please try again");
    }
  }


  Future<bool> _onBackPressed()
  {
    Navigator.of(context).pop();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: _onBackPressed,
        child:Scaffold(
            resizeToAvoidBottomPadding:false,
            appBar: AppBar(
                titleSpacing: 0.0,
                automaticallyImplyLeading: false,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        _onBackPressed();
                      },
                      child: Container(
                        padding:  EdgeInsets.fromLTRB(15,0,0,0),
                        child: Image.asset(
                          'assets/black_cross_icon.png',
                          fit: BoxFit.contain,
                          height: 20,
                        ),
                      ),
                    ),
                    Expanded(
                        child:Container(
                          padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                          child: new Text("E-Receipt",style: TextStyle(fontSize: global.font18, color: global.nameTextColor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold')),)
                    )
                  ],
                ),
                backgroundColor: global.appbarBackColor),
            body: new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: global.browncolor,
              child: CustomScrollView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  slivers: <Widget>[
                    SliverList(
                        delegate: SliverChildListDelegate(
                            [
                              new Column(
                                children: <Widget>[
                                  ClipPath(
                                    clipper: PointedEdgeClipperTop(),
                                    child: Container(
                                      height: 8,
                                      margin: EdgeInsets.fromLTRB(17,10,17,0),
                                      color: Colors.white,
                                    ),
                                  ),
                                  Container(
                                    color:global.whitecolor,
                                    margin: EdgeInsets.fromLTRB(17,0,17,0),
                                    child:ReceiptData(merchantName:global.hotelname,merchantURL:global.receiptImageURL,listofFoodItems:null,listOfExtraBillDetails: null,subTotal: 0,totalAmount: double.parse(widget.totalAmount),receiptType: 1,receiptDescription: widget.description,),
                                  ),
                                  ClipPath(
                                    clipper: PointedEdgeClipperBot(),
                                    child: Container(
                                      height: 8,
                                      margin: EdgeInsets.fromLTRB(17,0,17,0),
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ]
                        )
                    ),
                    SliverList(
                        delegate: SliverChildListDelegate(
                            [
                              Container(
                                  color:global.transparent,
                                  child: new Row(
                                    children: <Widget>[
                                      new Flexible(
                                        flex:1,
                                        fit:FlexFit.tight,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Color.fromRGBO(255, 255, 255, 0.1),
                                            border: Border.all(color: global.whitecolor, width: 1.0),
                                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                          ),
                                          margin: EdgeInsets.fromLTRB(40,20,20,20),
                                          child: FlatButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            child: Text('REJECT',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                            textColor: global.whitecolor,
                                            onPressed: ()
                                            {
                                              setState(() {});
                                              Navigator.pop(context);
                                            },
                                          ),
                                        ),
                                      ),

                                      new Flexible(
                                        flex:1,
                                        fit:FlexFit.tight,
                                        child:Container(
                                          decoration: BoxDecoration(
                                            color: global.mainColor,
                                            border: Border.all(color: global.mainColor, width: 1.0),
                                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                          ),
                                          margin: EdgeInsets.fromLTRB(20,20,40,20),
                                          child: FlatButton(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            child: Text('SAVE',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                            textColor: Colors.white,
                                            onPressed: ()
                                            {
                                              saveReceipt();
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                              )
                            ]
                        )
                    )
                  ]
              ),
            )
        )
    );
  }
}


class AddReceipt
{
  String eprinterId;
  String totalAmount;
  String serviceTax;
  String gratuity;
  String additionalTip;
  List<Fooditems> items;
  List<SplitGroups> splitGroups;
  String userId;
  String description;
  int receiptType;
  String merchantName;

  AddReceipt(this.eprinterId, this.totalAmount,this.serviceTax,this.gratuity,this.additionalTip,this.items,this.splitGroups,this.userId,this.description,this.receiptType,this.merchantName);

  Map toJson() => {
    'eprinterId': eprinterId,
    'totalAmount': totalAmount,
    'serviceTax': serviceTax,
    'gratuity': gratuity,
    'additionalTip': additionalTip,
    'items': items,
    'splitGroups': splitGroups,
    'userId': userId,
    'description': description,
    'receiptType': receiptType,
    'merchantName': merchantName,
  };
}

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quiver/async.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/Fragments//HomeFragmentClass.dart';
import 'package:tapit/Fragments/ReceiptsFragment.dart';
import 'package:tapit/Fragments/OffersFragment.dart';
import 'package:tapit/Fragments/AccountFragment.dart';
import 'package:tapit/helpers/ContactData.dart';
import 'package:http/http.dart' as http;
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class HomeScreen extends StatefulWidget
{
  HomeScreenState createState()=> HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver
{
  String countryCode;
  String emailVerified;
  bool onPauseCalled=false;
  bool screenInit=false;
  Random random=new Random();
  Iterable<Contact> _contacts;
  var contact_map = new Map();
  List<ContactData> contact_list=new List();
  List<ContactData> prev_contact_list=new List();
  List<ContactData> new_contact_list=new List();
  List<ContactData> temp_new_contact_list=new List();

  String LOGTAG="HomeScreen";

  @override
  void initState()
  {
    global.helperClass.getCount();
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: 1),
      new Duration(milliseconds: 500),
    );

    var sub = countDownTimer.listen(null);
    sub.onDone(() {
      sub.cancel();
      check_permissions();
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {

    if(state == AppLifecycleState.resumed)
    {
      checkEmailVerified();
    }else if(state == AppLifecycleState.inactive)
    {

    }else if(state == AppLifecycleState.paused)
    {
      onPauseCalled=true;
    }
  }

  Future<void> checkEmailVerified()async
  {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    emailVerified=prefs.getString("emailVerified");
    if(emailVerified.toString().compareTo("false")==0 || emailVerified.toString().compareTo("pending")==0)
    {
      UserService userService = new UserService();
      ResponseBean response = await userService.getUserInfo(global.UserID);
      if(response!=null)
      {
        var payload = response.payLoad;
        var emailver = payload['emailVerified'];
        if (emailver)
        {
          if(mounted)
          {
            setState(()
            {
              emailVerified = "true";
              global.isEmailVerified="true";
              prefs.setString("emailVerified", "true");
            });
          }
        }
      }
    }
  }

  void check_permissions() async
  {
    onPauseCalled=false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userID=prefs.getString("UserID");
    countryCode=prefs.getString("countryCode");
    emailVerified=prefs.getString("emailVerified");
    global.isEmailVerified=emailVerified;

    if(userID==null || userID.isEmpty)
    {

    }
    else
    {
      setState(() {
        global.UserID=userID;
      });
    }

    final PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted)
    {
      getContacts();
    }
    else
    {
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text('Permissions error',style: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
            content: Text('Please enable contacts access permission in system settings',style: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ));
    }
  }

  Future<void> getContacts() async
  {
    contact_list.clear();
    final Iterable<Contact> contacts = await ContactsService.getContacts();
    if (mounted)
    {
      setState(()
      {
        _contacts = contacts;
      });
    };

    for(int i=0;i<_contacts.length;i++)
    {
      Contact _con=_contacts.elementAt(i);
      ContactData contactData;
      bool flag=false;

      if(_con.avatar!=null && _con.avatar.isNotEmpty)
      {
        String avatar_string = base64Encode(_con.avatar);
        if(_con.phones!=null && _con.phones.toList().length>0)
        {
          List<String> numberList = new List();
          for(int s=0;s<_con.phones.toList().length;s++)
          {
            String number = _con.phones.toList()[s].value.toString().replaceAll(" ", "");
            number = number.toString().replaceAll("(", "");
            number = number.toString().replaceAll(")", "");
            number = number.toString().replaceAll("-", "");
            number = number.toString().replaceAll("*", "");

            if(numberList.length==0)
            {
              numberList.add(number);
            }
            else
            {
              bool alreadyPresent=false;
              for(int r=0;r<numberList.length;r++)
              {
                String numberListMember=numberList.elementAt(r).toString();
                if(number.toString().compareTo(numberListMember)==0)
                {
                  alreadyPresent=true;
                }
                else
                {
                  if(number.length>10)
                  {
                    String temp_number = number.toString().substring(number.length - 10, number.length);
                    if(temp_number.toString().compareTo(numberListMember)==0)
                    {
                      alreadyPresent=true;
                    }
                  }
                  else if(numberListMember.length>10)
                  {
                    String temp_number = numberListMember.toString().substring(numberListMember.length - 10, numberListMember.length);
                    if(temp_number.toString().compareTo(number)==0)
                    {
                      alreadyPresent=true;
                    }
                  }
                }
              }
              if(!alreadyPresent)
              {
                numberList.add(number);
              }
            }
          }

          for(int s=0;s<numberList.length;s++)
          {
            String number=numberList.elementAt(s);
            bool isNumber=global.helperClass.isNumeric(number);
            flag=false;

            if(isNumber)
            {
              String cc;
              if (number.length > 10) {
                cc = number.toString().substring(0, number.length - 10);
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
                number = number.toString().substring(number.length - 10, number.length);
              }
              else if (number.length == 10)
              {
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
              }
              else
              {
                flag = true;
              }

              if (!flag)
              {
                contactData = new ContactData(userID: "", cc: cc, isTapitUser: false, type: "true", colordata: null, image: avatar_string, name: _con.displayName, number: number, isSelected: false,);
              }
              if(!flag)
              {
                if(contactData!=null)
                {
                  contact_list.add(contactData);
                }
              }
            }
          }
        }
      }
      else
      {

        if(_con.phones != null && _con.phones.toList().length>0)
        {
          List<String> numberList=new List();
          for(int k=0;k<_con.phones.toList().length;k++)
          {
            String number=_con.phones.toList()[k].value.toString().replaceAll(" ", "");
            number=number.toString().replaceAll("(", "");
            number=number.toString().replaceAll(")", "");
            number=number.toString().replaceAll("-", "");
            number=number.toString().replaceAll("*", "");

            if(numberList.length==0)
            {
              numberList.add(number);
            }
            else
            {
              bool alreadyPresent=false;
              for(int r=0;r<numberList.length;r++)
              {
                String numberListMember = numberList.elementAt(r).toString();
                if (number.toString().compareTo(numberListMember) == 0)
                {
                  alreadyPresent = true;
                }
                else
                {
                  if (number.length > 10)
                  {
                    String temp_number = number.toString().substring(number.length - 10, number.length);
                    if (temp_number.toString().compareTo(numberListMember) == 0)
                    {
                      alreadyPresent = true;
                    }
                  }
                  else if (numberListMember.length > 10)
                  {
                    String temp_number = numberListMember.toString().substring(numberListMember.length - 10, numberListMember.length);
                    if (temp_number.toString().compareTo(number) == 0)
                    {
                      alreadyPresent = true;
                    }
                  }
                }
              }
              if(!alreadyPresent)
              {
                numberList.add(number);
              }
            }
          }

          for(int k=0;k<numberList.length;k++)
          {
            flag=false;
            String number=numberList.elementAt(k);
            bool isNumber=global.helperClass.isNumeric(number);

            if(isNumber)
            {
              String cc;

              if (number.length > 10)
              {
                cc = number.toString().substring(0, number.length - 10);
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
                number = number.toString().substring(number.length - 10, number.length);
              }
              else if (number.length == 10)
              {
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
              }
              else
              {
                flag = true;
              }

              String displayname = "";
              String type = "";
              int colordata;
              String image = "null";
              if (_con.displayName != null)
              {
                if (_con.initials() == null)
                {
                  image = _con.displayName.substring(0, 1);
                }
                else
                {
                  if (_con.initials().toString().isNotEmpty)
                  {
                    image = _con.initials();
                  }
                  else
                  {
                    image = _con.displayName.substring(0, 1);
                  }
                }

                if (_con.phones.toList()[0].value.toString().replaceAll(" ", "").toString().compareTo(_con.displayName.toString().replaceAll(" ", "")) == 0)
                {
                  displayname = null;
                  type = null;
                  colordata = null;
                }
                else
                {
                  displayname = _con.displayName;
                  type = "false";
                  colordata = random.nextInt(6);
                }
              }
              else
              {
                flag = true;
              }

              if (!flag)
              {
                contactData = new ContactData(userID: "", cc: cc, isTapitUser: false, type: type, colordata: colordata, image: image, name: displayname, number: number, isSelected: false,);
              }
              if(!flag)
              {
                if(contactData!=null)
                {
                  contact_list.add(contactData);
                }
              }
            }
          }
        }
        else
        {
          if(_con.displayName!=null)
          {
            String number = _con.displayName.replaceAll(" ", "");
            number = number.toString().replaceAll("(", "");
            number = number.toString().replaceAll(")", "");
            number = number.toString().replaceAll("-", "");
            number = number.toString().replaceAll("*", "");

            bool isNumber=global.helperClass.isNumeric(number);

            if(isNumber)
            {
              String cc;
              String image = "null";
              if (_con.initials() == null && _con.initials().isEmpty)
              {
                image = _con.displayName.substring(0, 1);
              }
              else
              {
                image = _con.initials();
              }

              if (number.length > 10)
              {
                cc = number.toString().substring(0, number.length - 10);
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
                number = number.toString().substring(number.length - 10, number.length);
              }
              else if (number.length == 10)
              {
                if (cc == null || cc.isEmpty || cc.toString().compareTo("0") == 0)
                {
                  cc = countryCode;
                }
              }
              else
              {
                flag = true;
              }

              if (!flag)
              {
                contactData = new ContactData(userID: "", cc: cc, isTapitUser: false, type: null, colordata: null, image: image, name: null, number: number, isSelected: false);
              }
              if(!flag)
              {
                if(contactData!=null)
                {
                  contact_list.add(contactData);
                }
              }
            }
          }
        }
      }

    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool CheckValue ;
    prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");
    if(map!=null)
    {
      CheckValue=true;
    }
    else
    {
      CheckValue=false;
    }

    if(!CheckValue)
    {
      bool flag=true;
      flag=await PostNewNumberToCloud(contact_list);
      if(flag)
      {
        await cloudSync();
      }
      prefs.setString("ContactMap", jsonEncode(contact_list));
    }
    else
    {
      String map=prefs.getString("ContactMap");
      prev_contact_list.clear();
      setState(()
      {
        prev_contact_list=(json.decode(map) as List).map((i) => ContactData.fromJson(i)).toList();
      });

      temp_new_contact_list.addAll(contact_list);

      for(int i=0;i<temp_new_contact_list.length;i++)
      {
        bool flag=false;
        for(int j=0;j<prev_contact_list.length;j++)
        {
          if(temp_new_contact_list.elementAt(i).number.toString().contains(prev_contact_list.elementAt(j).number.toString()))
          {
            if(!flag)
            {
              flag=true;
            }
          }
        }
        if(!flag)
        {
          new_contact_list.add(temp_new_contact_list.elementAt(i));
        }
      }

      if(new_contact_list!=null)
      {
        if(new_contact_list.length>0)
        {
          bool flag=true;
          flag=await PostNewNumberToCloud(new_contact_list);

          if(flag)
          {
            await cloudSync();
          }
        }
        else
        {
          bool syncflag=await cloudSync();
        }
      }
      prefs.remove("ContactMap");
      prefs.setString("ContactMap", jsonEncode(contact_list));
    }
  }

  Future<bool> PostNewNumberToCloud(List<ContactData> new_number_list) async
  {
    Future<bool> flag=Future<bool>.value(false);
    List<Map> PostContactJson = new List();
    for(int i=0;i<new_number_list.length;i++)
    {
      ContactData cd=new_number_list.elementAt(i);
      if(cd!=null)
      {
        PostContact postJson = new PostContact(cd.name, cd.number, "", "", cd.cc);
        PostContactJson.add(postJson.TojsonData());
      }
    }
    var jsonbody=jsonEncode(PostContactJson);
    UserService userService=new UserService();
    ResponseBean response=await userService.postContactToCloud(jsonbody, global.UserID);

    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    return flag;
  }

  Future<bool> cloudSync() async
  {
    Future<bool> flag=Future<bool>.value(false);
    UserService userService=new UserService();
    ResponseBean response=await userService.getContactSync(global.UserID);

    if(response!=null)
    {
      int statusCode = response.status;
      List<dynamic> list = response.payLoad;
      if (statusCode == 200)
      {
        if (list != null && list.length > 0)
        {
          for (int i = 0; i < list.length; i++)
          {
            String number = list[i]['mobileNo'];
            String userID = list[i]['userId'];
            String username = list[i]['name'];
            String url = list[i]['picture'];
            String cc = list[i]['countryCode'];

            for (int j = 0; j < contact_list.length; j++)
            {
              ContactData contactData = contact_list.elementAt(j);

              if (contactData.number.compareTo(number) == 0 && contactData.cc.compareTo(cc) == 0)
              {
                if (username != null)
                {
                  contactData.name = username;
                }

                if (url != null)
                {
                  if (url.isNotEmpty)
                  {
                    http.Response response = await http.get(url);
                    Uint8List uimg = response.bodyBytes;
                    contactData.image = base64Encode(uimg);
                    contactData.type = "true";
                    contactData.colordata = null;
                  }
                }

                if (userID != null && userID.isNotEmpty)
                {
                  contactData.userID = userID;
                  contactData.isTapitUser = true;
                }
                else
                {
                  contactData.isTapitUser = false;
                }

                if (cc != null)
                {
                  if (cc.isNotEmpty)
                  {
                    contactData.cc = cc;
                  }
                }
              }
            }
          }
        }
        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }
    return flag;
  }


  Future<PermissionStatus> _getPermission() async
  {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted)
    {
      final Map<Permission, PermissionStatus> permissionStatus = await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ?? PermissionStatus.undetermined;
    }
    else
    {
      return permission;
    }
  }

  int _currentIndex = global.homeFragment;
  final List<Widget> _children = [
    HomeFragmentClass(),
    ReceiptsFragment(),
    OffersFragment(),
    AccountFragment()
  ];

  void onTabTapped(int index) {
    setState(() {
      if(index!=2)
      {
        _currentIndex = index;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        showUnselectedLabels: true,
        selectedItemColor: global.mainColor,
        selectedLabelStyle: TextStyle(fontSize: global.font14,color:global.mainColor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),
        unselectedItemColor: Color(0xff808080),
        unselectedLabelStyle: TextStyle(fontSize: global.font13,color:Color(0xff808080),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),
        items: [
          BottomNavigationBarItem(
            icon:ImageIcon(AssetImage("assets/tap_symbol.png"),),
            title: new Text('Tap',style: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
          ),
          BottomNavigationBarItem(
            icon:ImageIcon(AssetImage("assets/receipt_symbol.png"),),
            title: new Text('Receipts',style: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
          ),
          BottomNavigationBarItem(
            icon:ImageIcon(AssetImage("assets/offer_symbol.png"),),
            title:new Container(
              padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
              decoration: new BoxDecoration(
                image:  DecorationImage(
                  image:  AssetImage('assets/offer_back.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
              child: Text('Coming Soon',style: TextStyle(fontSize:13,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
            ),
          ),
          BottomNavigationBarItem(
              icon:global.isEmailVerified.toString().compareTo("false")==0 || global.isEmailVerified.toString().compareTo("pending")==0?
              new Stack(
                children: <Widget>[
                  ImageIcon(AssetImage("assets/account_symbol.png"),),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: new Container(
                      width: 10,
                      height: 10,
                      child: ImageIcon(
                        AssetImage("assets/acc_ver_pending.png"),
                      ),
                    ),
                  )
                ],
              ) :ImageIcon(
                AssetImage("assets/account_symbol.png"),
              ),
              title: Text('Account',style: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),)
          )
        ],
      ),
    );
  }
}

class PostContact {
  String name;
  String mobileno;
  String email;
  String picture;
  String countryCode;
  PostContact(this.name,this.mobileno,this.email,this.picture,this.countryCode);

  Map<String, dynamic> TojsonData() {

    var map = new Map<String, dynamic>();
    map["name"] = this.name;
    map["mobileNo"] = this.mobileno;
    map["email"] = this.email;
    map["picture"] = this.picture;
    map["countryCode"] = this.countryCode;
    return map;
  }

}
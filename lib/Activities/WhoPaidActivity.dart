import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/helpers/ListofWhoPaid.dart';

class WhoPaidActivity extends StatefulWidget
{
  WhoPaidActivity({Key key,this.receiptAmount,this.groupname,this.sel_contact_list,this.selContactList}) : super(key: key);
  List<UserAmountData> sel_contact_list=new List();
  String groupname;
  double receiptAmount;
  int spliTypeInt;
  int pageID;
  WhoPaidState createState()=>WhoPaidState();
  final ValueChanged<List<UserAmountData>> selContactList;
}

class WhoPaidState extends State<WhoPaidActivity>
{
  final String LOGTAG="WhoPaidActivity";
  double _totalAmount=0;
  bool _isTotalMatched=true;
  List<double> _totalAmountList=new List();
  List<UserAmountData> current_payee_list=new List();

  @override
  void initState()
  {
    super.initState();

    setState(()
    {
      current_payee_list.addAll(widget.sel_contact_list);
      global.changedPayee=false;
      for(int j=0;j<widget.sel_contact_list.length;j++)
      {
        if(j==0)
        {
          _totalAmountList.add(global.receiptAmount);
        }
        else
        {
          _totalAmountList.add(0);
        }
      }
    });
  }

  Future<bool> _onBackPressed()
  {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: _onBackPressed,
        child:Scaffold(
            resizeToAvoidBottomPadding:false,
            appBar: AppBar(
                titleSpacing: 0.0,
                automaticallyImplyLeading: false,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        _onBackPressed();
                      },
                      child: Container(
                        padding:  EdgeInsets.fromLTRB(15,0,0,0),
                        child: Image.asset(
                          'assets/back_arrow.png',
                          fit: BoxFit.contain,
                          height: 20,
                        ),
                      ),
                    ),
                    Expanded(
                        child:Container(
                          padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                          child: new Text("Payees",style: TextStyle(fontSize: global.font18, color: global.nameTextColor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold')),)
                    )
                  ],
                ),
                actions: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                    child: new Row(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.6,
                          child:Text("\$"+widget.receiptAmount.toString(), style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                        ),
                        Container(
                          width:25,
                          height: 25,
                          child:Image.asset(
                              'assets/receipt_symbol.png',
                              fit: BoxFit.contain
                          ),
                        ),
                      ],
                    ),
                  )
                ],
                backgroundColor: global.appbarBackColor),
            body: new GestureDetector(
              onTap: (){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                color: global.whitecolor,
                child: Stack(
                  children: <Widget>[
                    new Container(
                      color: global.whitecolor,
                      child: CustomScrollView(
                        slivers: <Widget>[
                          SliverList(delegate: SliverChildListDelegate(
                              [
                                Container(
                                  child: new Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 10,
                                      ),
                                      new Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            padding: EdgeInsets.fromLTRB(30, 0, 20, 10),
                                            child: global.grpname!=null?new Text(global.grpname, style: TextStyle(fontSize: global.font16,color:Color(0xff3b3b3b),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Container(width:0,height:0),
                                          )
                                        ],
                                      ),
                                      new Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Flexible
                                            (
                                              flex:1,
                                              fit:FlexFit.tight,
                                              child:new Container(
                                                  padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                                                  child: Opacity(
                                                    opacity:0.5,
                                                    child: new Text("Share holder", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                  )
                                              )
                                          ),
                                          new Flexible
                                            (
                                            flex: 2,
                                            fit: FlexFit.tight,
                                            child: new Row
                                              (
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Container(
                                                    padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                                    child: Opacity(
                                                      opacity:0.5,
                                                      child: new Text("Amt paid", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                    )
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ]
                          )),
                          SliverList(
                              delegate: SliverChildBuilderDelegate((context, index) {
                                return Center(
                                  child:Container(
                                    child: ListofWhoPaid(index:index,userAmountData:current_payee_list[index],profileImg: global.imageList[index],amount:_totalAmountList[index],isSel: (double value)  {

                                      _totalAmountList[index]=value;
                                      _totalAmount=0;
                                      for(int i=0;i<_totalAmountList.length;i++)
                                      {
                                        _totalAmount+=_totalAmountList[i];
                                      }

                                      if(_totalAmount.compareTo(global.receiptAmount)==0)
                                      {
                                        setState(()
                                        {
                                          _isTotalMatched=true;
                                        });
                                      }
                                      else
                                      {
                                        setState(()
                                        {
                                          _isTotalMatched=false;
                                        });
                                      }
                                    },),
                                  ),
                                );
                              }, childCount: current_payee_list.length)),
                        ],
                      ),
                    ),
                    new Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child:
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            !_isTotalMatched?new Container(
                              width:MediaQuery.of(context).size.width,
                              padding: EdgeInsets.fromLTRB(20,3, 0, 3),
                              color: global.totalenotmatchcolor,
                              child: new Text("Total doesn't match",style: TextStyle(fontSize:global.font12,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
                            ):new Container(
                              height: 0,
                              width: 0,
                            ),
                            new Container(
                              width:MediaQuery.of(context).size.width,
                              decoration: new BoxDecoration(color: Color(0xff5D31DC)),
                              child: FlatButton(
                                child: Text("CONFIRM PAYEES",style: TextStyle(fontSize:global.font14,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                                onPressed: ()
                                {
                                  if(_isTotalMatched)
                                  {
                                    for(int i=0;i<_totalAmountList.length;i++)
                                    {
                                      if(_totalAmountList[i].compareTo(0)==0)
                                      {
                                        UserAmountData ud=global.finalUserList.elementAt(i);
                                        UserAmountData ud2=global.payee_list.elementAt(i);
                                        ud.isPayee=false;
                                        ud.usertype="member";
                                        ud.payeeAmount=_totalAmountList[i];
                                        ud2.isPayee=false;
                                        ud2.usertype="member";
                                        ud2.payeeAmount=_totalAmountList[i];
                                      }
                                      else
                                      {
                                        setState(()
                                        {
                                          UserAmountData ud=global.finalUserList.elementAt(i);
                                          UserAmountData ud2=global.payee_list.elementAt(i);

                                          ud.isPayee=true;
                                          if(i!=0)
                                          {
                                            ud.usertype = "payee";
                                          }
                                          ud.payeeAmount=_totalAmountList[i];

                                          ud2.isPayee=true;
                                          if(i!=0)
                                          {
                                            ud2.usertype = "payee";
                                          }
                                          ud2.payeeAmount=_totalAmountList[i];
                                        });
                                      }
                                    }

                                    widget.selContactList(widget.sel_contact_list);
                                    global.changedPayee=true;

                                    Navigator.of(context).pop();
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                ),
              ),
            )
        )
    );
  }
}
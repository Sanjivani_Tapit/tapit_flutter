import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:tapit/Activities/WhoPaidActivity.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/BillSplit.dart';
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/helpers/ListofPayee.dart';
import 'package:tapit/Fragments/EquallyFragment.dart';
import 'package:tapit/Fragments/MultipleFragment.dart';
import 'package:tapit/Fragments/DollarFragment.dart';
import 'package:tapit/helpers/SplitGroups.dart';
import 'package:tapit/helpers/SplitOwes.dart';
import 'package:tapit/helpers/navigation_bar.dart';
import 'package:tapit/helpers/navigation_bar_item.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/Activities/EditContainerActivity.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class BillSplitActivity extends StatefulWidget
{
  BillSplitActivity({Key key,this.receiptAmount,this.groupname,this.sel_contact_list}) : super(key: key);
  List<ContactData> sel_contact_list=new List();
  String groupname;
  double receiptAmount=0;
  BillSplitState createState()=> BillSplitState();
}

class BillSplitState extends State<BillSplitActivity> with SingleTickerProviderStateMixin
{

  String LOGTAG="BillSplitActivity";
  bool isButtonClicked=false;
  bool listSize=true;
  bool _isTotalMatched=true;
  int _currentIndex = 0;
  String grpname;
  Uint8List MainprofileImg;

  List<UserAmountData> amount_paid_items=new List();
  List<UserAmountData> finalUserList=new List();
  List<Tab> tabList = List();
  PageController _pageController;
  List<Widget> _children = [];

  @override
  Future<void> initState() {
    _pageController = PageController();
    storeToList();
    super.initState();
    global.receiptLastScreen=false;
  }

  void setTotalMatched(bool value)
  {
    if(value)
    {
      setState(()
      {
        _isTotalMatched=value;
      });
    }
    else
    {
      setState(()
      {
        _isTotalMatched=value;
      });
    }
  }

  void storeToList() async
  {
    setState(()
    {
      EquallyFragment equallyFragment=new EquallyFragment();
      DollarFragment dollarFragment=new DollarFragment();
      MultipleFragment multipleFragment=new MultipleFragment();

      setState(() {
        _children.add(equallyFragment);
        _children.add(dollarFragment);
        _children.add(multipleFragment);
      });

      equallyFragment.setOnMatchStateListener((value) {
        setState(() {
          _isTotalMatched=value;
        });
      });

      dollarFragment.setOnMatchStateListener((value) {
        setState(() {
          _isTotalMatched=value;
        });
      });

      multipleFragment.setOnMatchStateListener((value) {
        setState(() {
          _isTotalMatched=value;
        });
      });

      for(int i=0;i<widget.sel_contact_list.length;i++)
      {
        UserAmountData userAmountData=new UserAmountData();
        ContactData cd=widget.sel_contact_list.elementAt(i);
        if(cd.isSelected)
        {
          userAmountData.userID = cd.userID;
          userAmountData.isTapitUser = cd.isTapitUser;
          userAmountData.type = cd.type;
          userAmountData.colordata = cd.colordata;
          userAmountData.image = cd.image;
          userAmountData.name = cd.name;
          userAmountData.number = cd.number;
          userAmountData.isSelected = cd.isSelected;
          userAmountData.cc=cd.cc;

          if (i == 0)
          {
            userAmountData.isPayee = true;
            userAmountData.payeeAmount = global.receiptAmount;
            userAmountData.shareAmount = 0;
            userAmountData.sharepart = 1;
            userAmountData.settlementStatus = "pending";
            userAmountData.usertype = "admin";
          }
          else {
            userAmountData.isPayee = false;
            userAmountData.payeeAmount = 0;
            userAmountData.shareAmount = 0;
            userAmountData.sharepart = 1;
            userAmountData.settlementStatus = "pending";
            userAmountData.usertype = "member";
          }
          amount_paid_items.add(userAmountData);
        }
      }

      global.payee_list.clear();
      global.payee_list.addAll(amount_paid_items);
      global.finalUserList.addAll(amount_paid_items);
    });
  }

  Future<bool> addGroupToSession(String groupid,List<UserAmountData> userDetails) async
  {
    Future<bool> flag=Future<bool>.value(false);
    List<SplitGroups> splitGroups = new List();
    List<SplitGroups> unregistredSplitGroups = new List();
    List<Fooditems> fooditems = new List();
    String splittype="";
    double balanceAmt=0;
    bool registredSatus=false;

    setState(() {
      isButtonClicked=true;
    });

    for(int i=0;i<userDetails.length;i++)
    {
      int cnt=i;
      registredSatus=true;
      List<SplitOwes> splitOwes = new List();
      SplitGroups splitGroup;
      String cc="";
      String settlementStatus="pending";
      String userid=userDetails.elementAt(i).userID;

      if(userid==null || userid.isEmpty)
      {
        registredSatus=false;
        userid=userDetails.elementAt(i).number;
        cc=userDetails.elementAt(i).cc;
      }

      if(cnt==0)
      {
        UserAmountData userAmountData=global.finalUserList.elementAt(i);
        SplitOwes splitOwe;
        if(userAmountData.isPayee)
        {
          balanceAmt = userAmountData.shareAmount - userAmountData.payeeAmount;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));
        }
        else
        {
          balanceAmt = userAmountData.shareAmount;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));
        }
        splitOwe = new SplitOwes(global.UserID,double.parse((userAmountData.shareAmount).toStringAsFixed(2)),balanceAmt);
        splitOwes.add(splitOwe);
      }
      else
      {
        UserAmountData userAmountData=global.finalUserList.elementAt(i);
        SplitOwes splitOwe;
        if(userAmountData.isPayee)
        {
          balanceAmt = userAmountData.shareAmount - userAmountData.payeeAmount;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));
          if(balanceAmt==0)
          {
            settlementStatus="settle";
          }
          else
          {
            settlementStatus="pending";
          }
        }
        else
        {
          balanceAmt = userAmountData.shareAmount;
          balanceAmt=double.parse((balanceAmt).toStringAsFixed(2));
          if(balanceAmt==0)
          {
            settlementStatus="settle";
          }
          else
          {
            settlementStatus="pending";
          }
        }
        splitOwe = new SplitOwes(global.UserID, double.parse((userAmountData.shareAmount).toStringAsFixed(2)),balanceAmt);
        splitOwes.add(splitOwe);
      }

      int isAdmin=0;
      if(i==0)
      {
        isAdmin=1;
      }
      if(userDetails.elementAt(i).isPayee && userDetails.elementAt(i).payeeAmount!=0)
      {
        if(registredSatus)
        {
          splitGroup = new SplitGroups(userid,isAdmin,userDetails.elementAt(i).sharepart.toString(), true, userDetails.elementAt(i).payeeAmount, 0, settlementStatus,fooditems,splitOwes,"",cc);
          splitGroups.add(splitGroup);
        }
        else
        {
          splitGroup = new SplitGroups("",isAdmin,userDetails.elementAt(i).sharepart.toString(), true, userDetails.elementAt(i).payeeAmount, 0, settlementStatus,fooditems,splitOwes,userid,cc);
          unregistredSplitGroups.add(splitGroup);
        }
      }
      else
      {
        if(registredSatus)
        {
          splitGroup = new SplitGroups(userid,isAdmin,userDetails.elementAt(i).sharepart.toString(),false, userDetails.elementAt(i).shareAmount, 0, settlementStatus,fooditems,splitOwes,"",cc);
          splitGroups.add(splitGroup);
        }
        else
        {
          splitGroup = new SplitGroups("",isAdmin,userDetails.elementAt(i).sharepart.toString(),false, userDetails.elementAt(i).shareAmount, 0, settlementStatus,fooditems,splitOwes,userid,cc);
          unregistredSplitGroups.add(splitGroup);
        }
      }
    }

    if(_currentIndex==0)
    {
      splittype="equal";
    }
    else if(_currentIndex==1)
    {
      splittype="custom_dollar";
    }
    else{
      splittype="custom_multiple";
    }

    BillSplit billSplit=new BillSplit(groupid, 0, splittype, "0", splitGroups,unregistredSplitGroups);
    var jsonbody=jsonEncode(billSplit);
    UserService userService=new UserService();
    ResponseBean response=await userService.initiateBillSplit(global.UserID,global.receiptID,jsonbody);

    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {

        flag = Future<bool>.value(true);
      }
      else
      {
        flag = Future<bool>.value(false);
      }
    }

    setState(() {
      isButtonClicked=false;
    });

    return flag;
  }



  void onTabTapped(int index)
  {
    setState(()
    {
      _currentIndex = index;
      _pageController.jumpToPage(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomPadding:false,
        appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding:  EdgeInsets.fromLTRB(15,0,0,0),
                  child: Image.asset(
                    'assets/back_arrow.png',
                    fit: BoxFit.contain,
                    height: 20,
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(30,0,0,0),
                  child:  Text("Bill Split",style: new TextStyle(
                      fontSize: global.font18,
                      color: global.appbarTextColor,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'BalooChetanMedium')
                  )),
            ],
          ),
          actions: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
              child: new Row(
                children: <Widget>[
                  Opacity(
                    opacity: 0.6,
                    child:Text("\$"+global.formatter.format(widget.receiptAmount).toString(), style: TextStyle(fontSize:  global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                  ),
                  Container(
                    width:25,
                    height: 25,
                    child:Image.asset(
                        'assets/receipt_symbol.png',
                        fit: BoxFit.contain
                    ),
                  ),
                ],
              ),
            )
          ],
          backgroundColor: global.appbarBackColor,
        ),
        body: new GestureDetector(
            onTap: (){
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
                color: global.whitecolor,
                height: MediaQuery.of(context).size.height,
                child:new Stack(
                  children: <Widget>[
                    new Container(
                        color: global.whitecolor,
                        child:new Column(
                          children: <Widget>[
                            new Container(
                              color: global.whitecolor,
                              height:MediaQuery.of(context).size.height>600?(MediaQuery.of(context).size.height>800?MediaQuery.of(context).size.height/12:MediaQuery.of(context).size.height/11):MediaQuery.of(context).size.height/10,
                              margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                              width: MediaQuery.of(context).size.width,
                              child: new Row(
                                children: <Widget>[
                                  new Flexible
                                    (
                                    flex:1,
                                    fit:FlexFit.tight,
                                    child: new Text("Amount Paid by", style: TextStyle(fontSize: global.font16,color:Color(0xff3b3b3b),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ),
                                  new Flexible
                                    (
                                    flex: 2,
                                    fit: FlexFit.tight,
                                    child: new Row
                                      (
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        listSize? ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: global.imageList.length,
                                          itemBuilder: (context, index)
                                          {
                                            return Container(
                                              child: ListofPayee(index: index,
                                                profileImg: global.imageList[index],
                                                userAmountData: global.payee_list[index],
                                                isSelected: (bool value) {

                                                },
                                              ),
                                            );
                                          },
                                        ):new Container(width: 0,height: 0,),
                                        GestureDetector(
                                            onTap: (){
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => WhoPaidActivity(receiptAmount:widget.receiptAmount,groupname:global.grpname,sel_contact_list: global.payee_list,selContactList:(List<UserAmountData> list){
                                                    setState(() {
                                                      listSize=true;
                                                    });
                                                  }),
                                                ),
                                              );
                                            },
                                            child:   new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                  height: 50,
                                                  decoration: new BoxDecoration(
                                                    color: Colors.white,
                                                    border: Border(
                                                      right: BorderSide(color: Color.fromRGBO(151, 151, 151, 1), width: 1.0,),
                                                    ),
                                                  ),
                                                ),
                                                new Container(
                                                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                  child: Image.asset(
                                                    'assets/add_payee_symbol.png',
                                                    fit: BoxFit.contain,
                                                    height: 30,
                                                  ),
                                                )
                                              ],
                                            )
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                                width: MediaQuery.of(context).size.width,
                                decoration:BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(color: Color(0xff979797).withOpacity(0.1), width: 2.0,),
                                  ),
                                )
                            ),
                            new Container(
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.15),
                                        blurRadius: 2.0,
                                        offset: Offset(0.0, 1.0)
                                    )
                                  ],
                                  color: global.whitecolor
                              ),
                              child: TitledBottomNavigationBar(
                                enableShadow: false,
                                currentIndex:_currentIndex ,
                                onTap: onTabTapped,
                                BAR_HEIGHT: 40,
                                INDICATOR_HEIGHT: 1.5,
                                reverse: true,
                                items: [
                                  TitledNavigationBarItem(
                                    icon: Icons.home,
                                    title: _currentIndex==0?new Text('Equally',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Equally',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                  ),
                                  TitledNavigationBarItem(
                                    icon: Icons.home,
                                    title: _currentIndex==1?new Text('Dollars(\$)',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Dollars(\$)',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                  ),
                                  TitledNavigationBarItem(
                                      icon: Icons.home,
                                      title: _currentIndex==2?new Text('Multiples',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text('Multiples',style: TextStyle(fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                                  ),
                                ],
                                activeColor: Color(0xfff18642),
                                inactiveColor: Color.fromRGBO(0, 0, 0, 0.6),
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                fit: FlexFit.tight,
                                child:Container(
                                  decoration: BoxDecoration(
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: Colors.black54,
                                            blurRadius: 1.0,
                                            offset: Offset(10.0, 0.75)
                                        )
                                      ],
                                      color: global.whitecolor
                                  ),
                                  child:  PageView(
                                    controller: _pageController,
                                    onPageChanged: (index) {
                                      setState(() {
                                        _isTotalMatched=true;
                                        _currentIndex = index;
                                      });
                                    },
                                    children: _children,
                                  ),
                                )
                            ),
                          ],
                        )
                    ),
                    new Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child:
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            !_isTotalMatched?new Container(
                              width:MediaQuery.of(context).size.width,
                              padding: EdgeInsets.fromLTRB(20,3, 0, 3),
                              color: global.totalenotmatchcolor,
                              child: new Text("Total doesn't match",style: TextStyle(fontSize:global.font12,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
                            ):new Container(height: 0, width: 0,),
                            new Container(
                              width:MediaQuery.of(context).size.width,
                              decoration: !_isTotalMatched?new BoxDecoration(
                                  color: Color(0xffd3d3d3)
                              ):new BoxDecoration(
                                gradient: new LinearGradient(
                                    colors: global.buttonGradient,
                                    tileMode: TileMode.clamp
                                ),
                              ),
                              child: FlatButton(
                                child: !_isTotalMatched?Opacity(
                                    opacity:0.6,
                                    child:
                                    Text("SAVE CHANGES",style: TextStyle(fontSize:global.font14,color: Color(0xff7c7c7c),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),)
                                ):
                                Text("SAVE CHANGES",style: TextStyle(fontSize:global.font14,color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                                onPressed: ()
                                async {
                                  if(!isButtonClicked && _isTotalMatched)
                                  {
                                    bool flag = await addGroupToSession(global.grpid, global.finalUserList);
                                    if (flag)
                                    {
                                      global.ereceiptToContainer=false;
                                      global.receiptLastScreen = false;
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              EditContainerActivity(),
                                        ),
                                      );
                                    }
                                    else
                                    {
                                      global.helperClass.showAlertDialog(context, "Error", "Not able to confirm the bill split");
                                    }
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                )
            )
        )
    );
  }
}


import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ExtraBillDetails.dart';
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/helpers/ReceiptData.dart';
import 'package:tapit/helpers/EdgeClipperBottom.dart';
import 'package:tapit/helpers/EdgeClipperTop.dart';

class ReceiptScreen extends StatefulWidget
{
  ReceiptScreen({Key key,this.listofFoodItems,this.listOfExtraBillDetails,this.subTotal,this.totalAmount,this.receiptType,this.receiptDescription}) : super(key: key);
  ReceiptScreenState createState()=>ReceiptScreenState();
  List<Fooditems> listofFoodItems=new List();
  List<ExtraBillDetails> listOfExtraBillDetails=new List();
  double totalAmount;
  double subTotal;
  int receiptType;
  String receiptDescription;
}

class ReceiptScreenState extends State<ReceiptScreen>
{
  @override
  void initState(){
    super.initState();
  }

  Future<bool> _onBackPressed()
  {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            resizeToAvoidBottomPadding:false,
            appBar: AppBar(
                titleSpacing: 0.0,
                automaticallyImplyLeading: false,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: ()
                      {
                        Navigator.of(context).pop();
                      },
                      child: new Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        child: Image.asset(
                          'assets/black_cross_icon.png',
                          fit: BoxFit.contain,
                          height: 30,
                        ),
                      ),
                    ),
                    Expanded(
                        child:Container(
                          padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: new Text("E-Receipt",style: TextStyle(fontSize: global.font18, color: global.blackcolor,fontWeight: FontWeight.normal,
                              fontFamily: 'BalooChetanMedium')
                          ),
                        )
                    )
                  ],
                ),
                backgroundColor: global.appbarBackColor
            ),
            body: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: global.browncolor,
              child: CustomScrollView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                slivers: <Widget>[
                  SliverList(
                      delegate: SliverChildListDelegate(
                          [
                            new Column(
                              children: <Widget>[
                                ClipPath(
                                  clipper: PointedEdgeClipperTop(),
                                  child: Container(
                                    height: 8,
                                    margin: EdgeInsets.fromLTRB(17,10,17,0),
                                    color: Colors.white,
                                  ),
                                ),
                                Container(
                                  color:global.whitecolor,
                                  margin: EdgeInsets.fromLTRB(17,0,17,0),
                                  child:ReceiptData(merchantName:global.hotelname,merchantURL:global.receiptImageURL,listofFoodItems:widget.listofFoodItems,listOfExtraBillDetails: widget.listOfExtraBillDetails,subTotal: widget.subTotal,totalAmount: widget.totalAmount,receiptType: widget.receiptType,receiptDescription: widget.receiptDescription,),
                                ),
                                ClipPath(
                                  clipper: PointedEdgeClipperBot(),
                                  child: Container(
                                    height: 8,
                                    margin: EdgeInsets.fromLTRB(17,0,17,0),
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ]
                      )
                  ),
                ],
              ),
            )
        )
    );
  }
}
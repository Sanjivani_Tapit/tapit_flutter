import 'dart:async';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CircularProgressbar extends StatefulWidget
{
  CircularProgressbar({Key key,this.totalusers,this.isSel}) : super(key: key);
  final ValueChanged<bool> isSel;
  int totalusers=0;
  _CircleProgressState createState()=>_CircleProgressState();
}

class _CircleProgressState extends State<CircularProgressbar>
{
  int count=0;

  @override
  void initState()
  {
    // TODO: implement initState
    super.initState();

    int duration=(2000/widget.totalusers).ceil();
    new Timer.periodic(
      Duration(milliseconds: duration),
          (Timer timer) => setState(()
      {
        if(count==global.finalUserList.length)
        {
          timer.cancel();
          widget.isSel(true);
        }
        else
        {
          setState(() {
            count+=1;
          });
        }
      },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return
      Center(
        child:new CircularPercentIndicator(
            radius: 175.0,
            animation: true,
            animationDuration: 2000,
            lineWidth: 15.0,
            percent: 1,
            center: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(count.toString()+"/"+widget.totalusers.toString(), style: TextStyle(fontSize:23,color: global.darkgreycolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
                new Text(" members", style: TextStyle(fontSize:18,color: global.darkgreycolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),),
              ],
            ),
            circularStrokeCap: CircularStrokeCap.butt,
            backgroundColor: global.introRoundColorGrey,
            progressColor: global.mainColor
        ),
      );
  }
}


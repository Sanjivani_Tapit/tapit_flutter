import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tapit/global.dart' as global;
import 'ManualReceipt.dart';

class AddManualReceipt extends StatefulWidget
{
  AddManualReceiptState createState()=> AddManualReceiptState();
}

class AddManualReceiptState extends State<AddManualReceipt>
{

  final String LOGTAG="AddManualReceipt";
  final TextEditingController receiptNameController = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();

  bool receiptNameValidate=false;
  bool amountValidate=false;
  bool descriptionValidate=false;
  bool isResponseReceived=true;
  bool show_missing_text=false;

  String receiptName="";
  String totalAmount="";
  String description="";

  String missing_receipt_name="";
  String missing_amount="";
  String missing_dscription="";

  @override
  void initState(){
    super.initState();
  }

  Future<void> createReceipt() async
  {
    setState(() {

      show_missing_text=false;
      receiptNameValidate=false;
      amountValidate=false;
      descriptionValidate=false;

      receiptName = receiptNameController.text;
      totalAmount = amountController.text;
      description = descriptionController.text;

      if (receiptName.isEmpty || receiptName==" " || receiptName.length==0)
      {
        show_missing_text=true;
        missing_receipt_name="Receipt name is mandatory";
        receiptNameValidate = true;
      }
      if(totalAmount.isEmpty || totalAmount==" " ||  totalAmount.length==0)
      {
        show_missing_text=true;
        missing_amount="Total amount is mandatory";
        amountValidate = true;
      }
      if(description.isEmpty || description==" " || description.length==0)
      {
        show_missing_text=true;
        missing_dscription="Description is mandatory";
        descriptionValidate = true;
      }

    });

    if(receiptNameValidate==false && amountValidate==false &&  descriptionValidate==false)
    {
      global.hotelname=receiptName;
      global.receiptImageURL="https://image.freepik.com/free-photo/chicken-steak-with-lemon-tomato-chili-carrot-white-plate_1150-25886.jpg";
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => ManualReceipt(receiptName: receiptName,totalAmount: totalAmount,description: description,)));
    }
  }

  @override
  Widget build(BuildContext context)
  {
    final receiptNametextField=  TextField(
        onTap: () {
          setState(() {
            show_missing_text = false;
          });
        },
        onChanged: (value){
          setState(() {
            receiptNameValidate=false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        keyboardType: TextInputType.text,
        cursorColor: global.mainColor,
        obscureText: false,
        controller: receiptNameController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "Name of the receipt",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    final amounttextField=  TextField(
        onTap: () {
          setState(() {
            show_missing_text = false;
          });
        },
        onChanged: (value){
          setState(() {
            amountValidate=false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        cursorColor: global.mainColor,
        obscureText: false,
        controller: amountController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0.75, color: Color(0xffc8c8c8),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0.75, color: Color(0xffc8c8c8)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "\$ 0.00",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    final descriptiontextField=  TextField(
        onTap: () {
          setState(() {
            show_missing_text = false;
          });
        },
        onChanged: (value){
          setState(() {
            descriptionValidate=false;
          });
        },
        style:TextStyle( color:Color.fromRGBO(0, 0, 0, 0.87),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        cursorColor: global.mainColor,
        obscureText: false,
        keyboardType: TextInputType.multiline,
        maxLines: 10,
        controller: descriptionController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffF7F7F7),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Color(0xffF7F7F7),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(width: 0, color: Color(0xffF7F7F7),),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 8),
          hintText: "Enter details of items etc.",
          hintStyle: TextStyle( color:Color.fromRGBO(0, 0, 0, 0.4),fontSize:global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
        )
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Image.asset(
                  'assets/back_arrow.png',
                  fit: BoxFit.contain,
                  height: 25,
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(30,12,0,12),
                  child:  Text("New Receipt",style: new TextStyle(
                      fontSize: global.font18,
                      color: global.appbarTextColor,
                      fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'
                  )
                  ))
            ],
          ),
          backgroundColor: global.appbarBackColor
      ),
      body: new GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: (){
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: new Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    Flexible(
                      flex:1,
                      fit:FlexFit.tight,
                      child: new Container(
                        width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/9:MediaQuery.of(context).size.width/10,
                        height:MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/9:MediaQuery.of(context).size.width/10,
                        padding: EdgeInsets.all(5),
                        decoration: new BoxDecoration(
                          color: Color(0xffc4c4c4).withOpacity(0.3),
                          borderRadius: BorderRadius.all(Radius.circular(7.0)),
                          image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://image.freepik.com/free-photo/chicken-steak-with-lemon-tomato-chili-carrot-white-plate_1150-25886.jpg"),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Flexible(
                        flex:4,
                        fit:FlexFit.tight,
                        child: SizedBox(
                          height: 36,
                          child: receiptNametextField,
                        )
                    )
                  ],
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                      child: Offstage(
                        offstage: !receiptNameValidate,
                        child: new Text(
                          missing_receipt_name,
                          textAlign: TextAlign.left,
                          style: new TextStyle(
                              fontSize: global.font12,
                              color: global.errorColor,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'BalooChetanRegular'
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 23,
                ),
                new Row(
                  children: <Widget>[
                    Flexible(
                      flex:2,
                      fit:FlexFit.tight,
                      child: new Container(
                        child:Text("Amount Paid", style: TextStyle(fontSize: global.font14,fontWeight: FontWeight.normal,color: global.nameTextColor, fontFamily: 'BalooChetanRegular')),
                      ),
                    ),
                    Flexible(
                        flex:3,
                        fit:FlexFit.tight,
                        child:SizedBox(
                          height: 36,
                          child: amounttextField,
                        )
                    )
                  ],
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                      child: Offstage(
                        offstage: !amountValidate,
                        child: new Text(
                          missing_amount,
                          textAlign: TextAlign.left,
                          style: new TextStyle(
                              fontSize: global.font12,
                              color: global.errorColor,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'BalooChetanRegular'
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 23,
                ),
                new Row(
                  children: <Widget>[
                    Text("Description", style: TextStyle(fontSize: global.font14,fontWeight: FontWeight.normal,color: global.nameTextColor, fontFamily: 'BalooChetanRegular'))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                new Row(
                  children: <Widget>[
                    Container(
                      decoration: new BoxDecoration(
                        color: Color(0xfff7f7f7),
                        border: Border.all(
                            width: 0.75,
                            color: Color(0xffc8c8c8)
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                      ),
                      height: 150,
                      width: MediaQuery.of(context).size.width-40,
                      child: descriptiontextField,
                    )
                  ],
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 0, top: 8, right: 0, bottom: 0),
                      child: Offstage(
                        offstage: !descriptionValidate,
                        child: new Text(
                          missing_dscription,
                          textAlign: TextAlign.left,
                          style: new TextStyle(
                              fontSize: global.font12,
                              color: global.errorColor,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'BalooChetanRegular'
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
      ),
      bottomNavigationBar:isResponseReceived? Padding(
          padding: EdgeInsets.all(0.0),
          child:new Container(
            width:MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(gradient: new LinearGradient(
                colors: global.buttonGradient,
                tileMode: TileMode.clamp
            ),
            ),
            child: FlatButton(
              child: Text("CREATE RECEIPT",style: TextStyle(color: global.whitecolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanSemiBold'),),
              onPressed: () {
                createReceipt();
              },
            ),
          )
      ):new Container(width: 0,height: 0,),
    );
  }
}

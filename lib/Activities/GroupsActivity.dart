import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/ContactsActivity.dart';
import 'package:tapit/Activities/EditGroupSummary.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/helpers/ImageHorizontalScroll.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';
import 'UpdateGroupActivity.dart';

class GroupsActivity extends StatefulWidget
{
  GroupsActivity({Key key,this.source}) : super(key: key);
  final int source;
  GroupsActivityState createState()=>GroupsActivityState();
}

class GroupsActivityState extends State<GroupsActivity>
{
  bool responseReceived=false;
  Random random=new Random();
  String countryCode;
  String LOGTAG="GroupsActivity";

  static BuildContext mContext;
  Widget appBarTitle =  new Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      GestureDetector(
        onTap: (){
          Navigator.of(mContext).pop();
        },
        child: Container(
          padding:  EdgeInsets.fromLTRB(15,0,0,0),
          child: Image.asset(
            'assets/back_arrow.png',
            fit: BoxFit.contain,
            height: 20,
          ),
        ),
      ),
      Container(
          padding: EdgeInsets.fromLTRB(30,0,0,0),
          child:  Text("Split groups",style: new TextStyle(
              fontSize: global.font18,
              color: global.appbarTextColor,
              fontWeight: FontWeight.normal,
              fontFamily: 'BalooChetanSemiBold'
          )
          )
      ),
    ],
  );
  Icon actionIcon = new Icon(Icons.search, color: global.blackcolor,);
  TextEditingController editingController = TextEditingController();
  List<GroupData> list_of_groups=new List();
  List<GroupData> items =new List();
  List<ContactData> contact_list=new List();
  List<ContactData> items_contacts=new List();

  bool searchFlag=true;

  @override
  void initState(){
    super.initState();
    getGroupData();
  }

  void getGroupData() async
  {
    setState(() {
      bool responseReceived=false;
    });

    global.receiptLastScreen=false;
    contact_list.clear();
    list_of_groups.clear();
    items_contacts.clear();
    items.clear();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String map=prefs.getString("ContactMap");
    countryCode=prefs.getString("countryCode");

    if(map!=null)
    {
      setState(()
      {
        contact_list = (json.decode(map) as List).map((i) => ContactData.fromJson(i)).toList();
      });
    }

    items_contacts.addAll(contact_list);
    UserService userService=new UserService();
    ResponseBean response=await userService.getGroups(global.UserID);

    if(response!=null)
    {
      int statusCode = response.status;
      var payload = response.payLoad;

      if (statusCode == 200)
      {
        List<dynamic> adminOwnedList = payload['groupList'];
        List<dynamic> adminNotOwnedList = payload['otherGroupList'];

        for (int i = 0; i < adminOwnedList.length; i++)
        {

          bool isAdminOwner = false;
          String groupname = adminOwnedList.elementAt(i)['name'];
          String groupid = adminOwnedList.elementAt(i)['id'];
          String lastModified = adminOwnedList.elementAt(i)['modifiedDate'];
          String ownerID = adminOwnedList.elementAt(i)['created_by'];
          List<dynamic> users = adminOwnedList.elementAt(i)['users'];
          List<ContactData> sel_contact_list = new List();

          if (ownerID == null)
          {
            ownerID = "5f3b988e419b1b5cc8e751d6";
          }

          String day = lastModified.substring(0, 10);
          String time = lastModified.substring(11, 19);
          var dateTime = DateFormat("yyyy-MM-dd HH:mm").parse(day + " " + time, true);
          var dateLocal = dateTime.toLocal();
          String local = DateFormat("dd-MMM-yyyy").format(dateLocal);
          lastModified = local;

          for (int j = 0; j < users.length; j++)
          {
            bool imgfound = false;
            bool istapitUser = false;
            String type = "false";
            String img;
            int colorData;
            String cc;

            String userid = users[j]['id'];
            String username = users[j]['name'];
            String userrno = users[j]['mobileNo'];
            cc = users[j]['countryCode'];
            String resStatus = users[j]['registrationStatus'];
            String uname = username;

            if (cc == null || cc.isEmpty)
            {
              cc = countryCode;
            }

            if (resStatus != null)
            {
              if (resStatus.contains("REGISTERED"))
              {
                istapitUser = true;
              }
            }

            if (username == null || username.isEmpty)
            {
              username = null;
            }

            String picture = users[j]['picture'];

            for (int k = 0; k < items_contacts.length; k++)
            {
              ContactData prevContactData = items_contacts.elementAt(k);
              if(!imgfound)
              {
                if (prevContactData.number == userrno && prevContactData.cc == cc)
                {
                  username = prevContactData.name;
                  img = prevContactData.image;
                  colorData = prevContactData.colordata;

                  if (colorData == null)
                  {
                    if (username == null || username.isEmpty)
                    {
                      type = null;
                    }
                    else
                    {
                      type = "true";
                    }
                  }
                  else
                  {
                    type = "false";
                  }
                  imgfound = true;
                }
              }
              else
              {
                break;
              }
            }

            if (!imgfound)
            {
              if (username == null || username.isEmpty)
              {
                username = null;
                type = null;
              }
              else
              {
                type = "false";
                var arr = username.split(" ");
                if (arr[0] != null && arr[0].isNotEmpty)
                {
                  img = arr[0].substring(0, 1);
                }

                if (arr[1] != null && arr[1].isNotEmpty)
                {
                  img = img + arr[1].substring(0, 1);
                }
              }
            }


            ContactData contactData = new ContactData(
                userID: userid,
                cc: cc,
                isTapitUser: istapitUser,
                type: type,
                colordata: colorData,
                image: img,
                name: username,
                number: userrno,
                isSelected: true);
            sel_contact_list.add(contactData);
          }

          GroupData groupData=new GroupData(sourceID: widget.source,
              groupID: groupid,
              groupname: groupname,
              groupdetails: lastModified,
              sel_contact_list: sel_contact_list,
              isAdminOwner: true,
              ownerID: ownerID);

          list_of_groups.add(groupData);
        }


        for (int i = 0; i < adminNotOwnedList.length; i++)
        {
          String groupname = adminNotOwnedList.elementAt(i)['name'];
          String groupid = adminNotOwnedList.elementAt(i)['id'];
          String lastModified = adminNotOwnedList.elementAt(i)['modifiedDate'];
          String ownerID = adminNotOwnedList.elementAt(i)['created_by'];
          List<dynamic> users = adminNotOwnedList.elementAt(i)['users'];
          List<ContactData> sel_contact_list = new List();


          if (ownerID == null)
          {
            ownerID = "5f3b988e419b1b5cc8e751d6";
          }

          String day = lastModified.substring(0, 10);
          String time = lastModified.substring(11, 19);
          var dateTime = DateFormat("yyyy-MM-dd HH:mm").parse(day + " " + time, true);
          var dateLocal = dateTime.toLocal();
          String local = DateFormat("dd-MMM-yyyy").format(dateLocal);
          lastModified = local;

          for (int j = 0; j < users.length; j++)
          {
            bool imgfound = false;
            bool istapitUser = false;
            String type = "false";
            String img;
            int colorData;
            String cc;

            String userid = users[j]['id'];
            String username = users[j]['name'];
            String userrno = users[j]['mobileNo'];
            String resStatus = users[j]['registrationStatus'];
            cc = users[j]['countryCode'];

            if (cc == null || cc.isEmpty) {
              cc = countryCode;
            }

            if (resStatus != null) {
              if (resStatus.contains("REGISTERED")) {
                istapitUser = true;
              }
            }

            if (username == null || username.isEmpty)
            {
              username = null;
            }

            String picture = users[j]['picture'];
            for (int k = 0; k < items_contacts.length; k++)
            {
              ContactData prevContactData = items_contacts.elementAt(k);
              if(!imgfound)
              {
                if (prevContactData.number == userrno && prevContactData.cc == cc)
                {
                  username = prevContactData.name;
                  img = prevContactData.image;
                  colorData = prevContactData.colordata;
                  if (colorData == null)
                  {
                    if (username == null || username.isEmpty)
                    {
                      type = null;
                    }
                    else
                    {
                      type = "true";
                    }
                  }
                  else
                  {
                    type = "false";
                  }
                  imgfound = true;
                }
              }
              else
              {
                break;
              }

            }

            if (!imgfound)
            {
              if (username == null || username.isEmpty)
              {
                username = null;
                type = null;
              }
              else
              {
                type = "false";
                var arr = username.split(" ");
                if (arr[0] != null && arr[0].isNotEmpty)
                {
                  img = arr[0].substring(0, 1);
                }
                if (arr[1] != null && arr[1].isNotEmpty)
                {
                  img = img + arr[1].substring(0, 1);
                }
              }
            }

            ContactData contactData = new ContactData(
                userID: userid,
                cc: cc,
                isTapitUser: istapitUser,
                type: type,
                colordata: colorData,
                image: img,
                name: username,
                number: userrno,
                isSelected: true);
            sel_contact_list.add(contactData);
          }
          GroupData groupData=new GroupData(sourceID: widget.source,
              groupID: groupid,
              groupname: groupname,
              groupdetails: lastModified,
              sel_contact_list: sel_contact_list,
              isAdminOwner: false,
              ownerID: ownerID);

          list_of_groups.add(groupData);
        }

        if(mounted)
        {
          setState(()
          {
            responseReceived = true;
            items.addAll(list_of_groups);
            if (items.length > 0)
            {
              searchFlag = true;
            }
            else
            {
              searchFlag = false;
            }
          });
        }
      }
      else if (statusCode == 204)
      {
        setState(() {
          responseReceived = true;
        });
        global.helperClass.showAlertDialog(mContext, "Error", "No Content");
      }
      else if (statusCode == 400)
      {
        setState(() {
          responseReceived = true;
        });
        global.helperClass.showAlertDialog(mContext, "Error", "Bad Request");
      }
      else if (statusCode == 404)
      {
        setState(() {
          responseReceived = true;
        });
        global.helperClass.showAlertDialog(mContext, "Error", "Not Found");
      }
      else if (statusCode == 500)
      {
        setState(() {
          responseReceived = true;
        });
        global.helperClass.showAlertDialog(mContext, "Error", "Internal Server Error");
      }
    }
    else
    {
      setState(() {
        responseReceived = true;
      });
    }

  }

  void filterSearchResults(String query)
  {
    List<GroupData> dummySearchList = new List();
    dummySearchList.addAll(list_of_groups);

    if(query.isNotEmpty)
    {
      setState(() {
        searchFlag=true;
      });

      List<GroupData> dummyListData = List<GroupData>();
      dummySearchList.forEach((item)
      {
        if(item.groupname.toLowerCase().contains(query))
        {
          dummyListData.add(item);
        }
      });
      setState(()
      {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    }
    else
    {
      setState(()
      {
        items.clear();
        items.addAll(list_of_groups);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    mContext=context;

    return Scaffold(
      appBar: buildBar(context),
      body: Container(
        color: global.whitecolor,
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child:Center(
            child: responseReceived?(
                items.length>0?
                ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return Center(
                        child:Container(
                          child: GroupDataCard(groupData: items[index],mContext: this,),
                        )
                    );
                  },
                ):!searchFlag && widget.source==0?new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Text("You have no saved groups yet",style: new TextStyle(
                        fontSize: global.font16,
                        color: global.nameTextColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'BalooChetanRegular'
                    )
                    ),
                    SizedBox(height: 20,),
                    new FlatButton(
                      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 50.0),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                ContactsActivity(),
                          ),
                        );
                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0), side: BorderSide(color: global.mainColor,width: 1.5)),
                      color: global.whitecolor,
                      child: Text('CREATE NEW GROUP',
                          style: TextStyle(fontSize: global.font14, color: Color(0xffF57C00),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanMedium')),
                    ),
                  ],
                ) :new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Text("You have no saved groups yet",style: new TextStyle(
                        fontSize: global.font16,
                        color: global.nameTextColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'BalooChetanRegular'
                    )
                    ),
                  ],
                )):new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          ),
        ),
      ),
      floatingActionButton: widget.source==0?FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  ContactsActivity(),
            ),
          );
        },
        child: Icon(Icons.add),
        backgroundColor: global.mainColor,
        foregroundColor: global.whitecolor,
      ):new Container(width:0,height:0),

    );
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title:appBarTitle,
        backgroundColor: global.appbarBackColor,
        actions: <Widget>[
          new IconButton(icon: actionIcon, onPressed: () {
            setState(() {
              if (this.actionIcon.icon == Icons.search) {
                this.actionIcon = new Icon(Icons.close, color: global.blackcolor,);
                this.appBarTitle =

                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        Navigator.of(mContext).pop();
                      },
                      child: Container(
                        padding:  EdgeInsets.fromLTRB(15,0,0,0),
                        child: Image.asset(
                          'assets/back_arrow.png',
                          fit: BoxFit.contain,
                          height: 20,
                        ),
                      ),
                    ),
                    Expanded(
                      child: new TextField(
                        onChanged: (value){
                          filterSearchResults(value);
                        },
                        cursorColor: global.mainColor,
                        controller: editingController,
                        style: new TextStyle(
                          color: global.blackcolor,
                        ),
                        decoration: new InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: new Icon(Icons.search, color:  global.blackcolor),
                            hintText: "Search...",
                            hintStyle: new TextStyle(fontSize: global.font17, color:  global.appbargreycolor,fontWeight: FontWeight.normal,
                                fontFamily: 'BalooChetanSemiBold')
                        ),
                      ),
                    )
                  ],
                );
              }
              else {
                _handleSearchEnd();
              }
            });
          },),
        ]
    );
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(Icons.search, color:  global.blackcolor,);
      this.appBarTitle =new Row(
        children: <Widget>[
          GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Container(
              padding:  EdgeInsets.fromLTRB(15,0,0,0),
              child: Image.asset(
                'assets/back_arrow.png',
                fit: BoxFit.contain,
                height: 20,
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(30,0,0,0),
              child:  Text("Split groups",style: new TextStyle(
                  fontSize: global.font18,
                  color: global.appbarTextColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'BalooChetanSemiBold'
              )
              )
          ),
        ],
      );
      items.clear();
      items.addAll(list_of_groups);
      editingController.clear();
    });
  }
}

class GroupData
{
  const GroupData({this.groupID,this.groupname,this.groupdetails,this.sel_contact_list,this.isAdminOwner,this.sourceID,this.ownerID});

  final List<ContactData> sel_contact_list;
  final String groupname;
  final String groupdetails;
  final String groupID;
  final bool isAdminOwner;
  final int sourceID;
  final String ownerID;
}

class GroupDataCard extends StatefulWidget{
  const GroupDataCard({Key key, this.groupData,this.mContext}) : super(key: key);
  final GroupData groupData;
  final GroupsActivityState mContext;

  GroupDataCardState createState()=>GroupDataCardState();
}

class GroupDataCardState extends State<GroupDataCard>
{
  Uint8List MainprofileImg;

  void convertImg(ContactData obj)async{
    if(obj.type.toString().contains("true")){
      MainprofileImg=base64Decode(obj.image);
    }
    else if(obj.type.toString().contains("false")){
      MainprofileImg = new Uint8List(0);
    }
    else if(obj.type.toString().contains("admin")){
      MainprofileImg = new Uint8List(0);
    }
    else{
      MainprofileImg = null;
    }
  }

  @override
  Widget build(BuildContext context) {

    return new Row(
      children: <Widget>[
        Flexible(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0,horizontal: 13.0),
              child: Card(
                child:GestureDetector(
                  onTap: () async
                  {

                    String type="true";
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    String userid=prefs.getString("UserID");
                    String username=prefs.getString("CurrentFirstName")+" "+prefs.getString("CurrentLastName");
                    String mobnumber=prefs.getString("MobileNo");
                    String image=prefs.getString("UserImage");
                    String cc=prefs.getString("countryCode");

                    if(image==null || image.isEmpty)
                    {
                      var arr=username.split(" ");
                      if(arr.length>=2)
                      {
                        if(arr[0]!=null && arr[0].isNotEmpty)
                        {
                          image = arr[0].substring(0, 1);
                        }
                        if(arr[1]!=null && arr[1].isNotEmpty)
                        {
                          image = image + arr[1].substring(0, 1);
                        }
                      }
                      else
                      {
                        image=arr[0];
                      }
                      type="false";
                    }

                    bool flag=false;
                    Random random=new Random();
                    ContactData con=new ContactData( userID:userid,cc:cc,isTapitUser:true, type:type ,colordata:random.nextInt(6), image: image, name: username, number: mobnumber,isSelected:true);

                    for(int i=0;i<widget.groupData.sel_contact_list.length-1;i++)
                    {
                      if(widget.groupData.sel_contact_list.elementAt(i).number.contains(mobnumber))
                      {
                        flag=true;
                        break;
                      }
                    }
                    List<ContactData> new_sel_list=new List();
                    new_sel_list.addAll(widget.groupData.sel_contact_list);
                    global.selectedImageList.clear();

                    for(int s=0;s<widget.groupData.sel_contact_list.length;s++)
                    {
                      ContactData obj=widget.groupData.sel_contact_list.elementAt(s);
                      Uint8List temp=await global.helperClass.convertImg(obj);
                      global.selectedImageList.add(temp);
                    }

                    if(!flag)
                    {
                      new_sel_list.insert(0, con);
                      Uint8List tempimg=await global.helperClass.convertImg(con);
                      global.selectedImageList.insert(0,tempimg);
                    }

                    if(widget.groupData.sourceID==0)
                    {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => EditGroupSummary(groupID: widget.groupData.groupID, groupname: widget.groupData.groupname, sel_contact_list: new_sel_list,isAdminOwner:widget.groupData.isAdminOwner),),);
                    }
                    else
                    {
                      global.grpOwnerID=widget.groupData.ownerID;
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => UpdateGroupActivity(groupID: widget.groupData.groupID, groupname: widget.groupData.groupname, sel_contact_list: new_sel_list,isAdminOwner: widget.groupData.isAdminOwner,ownerID: widget.groupData.ownerID,),),);
                    }
                  },
                  child:Container(
                      padding: EdgeInsets.fromLTRB(10,10,10,10),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.2),
                            spreadRadius: 0,
                            offset: Offset(0,0),
                            blurRadius: 3.0,
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Flexible(
                                  flex: 6,
                                  fit: FlexFit.tight,
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child:Text(widget.groupData.groupname,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(fontSize: global.font16,fontWeight: FontWeight.normal,color: global.blackcolor,fontFamily: 'BalooChetanMedium' )),
                                  )
                              ),
                              new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child:  widget.groupData.isAdminOwner?new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Container(
                                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                        child: Image.asset(
                                          'assets/lock_icon_black.png',
                                          color: global.blackcolor,
                                        ))
                                  ],
                                ):new Container(width: 0,height: 0,),)
                            ],
                          ),
                          new Flexible(
                              child:new Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                    child:Opacity(
                                        opacity: 0.6,
                                        child:Icon(Icons.history,size: global.font15,)
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                    child:Opacity(
                                      opacity: 0.6,
                                      child:Text("Updated: "+widget.groupData.groupdetails, style: TextStyle(fontSize: global.font12,fontWeight: FontWeight.normal,color: global.blackcolor,fontFamily: 'BalooChetanRegular')),
                                    ),
                                  )
                                ],
                              )
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            height: 40,
                            child:ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: widget.groupData.sel_contact_list.length,
                              itemBuilder: (context, index) {
                                convertImg(widget.groupData.sel_contact_list[index]);
                                return Container(
                                  child: ImageHorizontalScroll(index:index,contactData:widget.groupData.sel_contact_list[index],profileImg: MainprofileImg,flag:0,isSelected: (bool value) {
                                    setState(() {});
                                  },
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      )
                  ),
                ),
              )
          ),
        )
      ],
    );
  }
}
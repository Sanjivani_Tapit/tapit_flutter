import 'package:json_annotation/json_annotation.dart';
import 'AlertTypeList.dart';

@JsonSerializable(nullable: false)
class UserDetails
{
  UserDetails(
      {this.id,
        this.name,
        this.picture,
        this.email,
        this.mobileNo,
        this.countryCode,
        this.deviceType,
        this.fcmToken,
        this.emailVerified,
        this.mobileVerified,
        this.gender,
        this.dob,
        this.alertSettingTypeList,
      });

  String id;
  String name;
  String picture;
  String email;
  String mobileNo;
  String countryCode;
  String deviceType;
  String fcmToken;
  String emailVerified;
  String mobileVerified;
  String gender;
  String dob;
  List<AlertTypeList> alertSettingTypeList;


  UserDetails.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        picture = json['picture'],
        email = json['email'],
        mobileNo = json['mobileNo'],
        countryCode = json['countryCode'],
        deviceType = json['deviceType'],
        fcmToken = json['fcmToken'],
        emailVerified=json['emailVerified'],
        mobileVerified=json['mobileVerified'],
        gender=json['gender'],
        dob=json['dob'],
        alertSettingTypeList=json['alertSettingTypeList'];

  Map<String, dynamic> toJson() {
    return {
      'id':id,
      'name':name,
      'picture':picture,
      'email':email,
      'mobileNo':mobileNo,
      'countryCode':countryCode,
      'deviceType': deviceType,
      'fcmToken': fcmToken,
      'emailVerified': emailVerified,
      'mobileVerified': mobileVerified,
      'gender': gender,
      'dob': dob,
      'alertSettingTypeList': alertSettingTypeList,
    };
  }
}
import 'package:tapit/helpers/SplitOwes.dart';
import 'package:tapit/helpers/Fooditems.dart';

class SplitGroups {
  String userId;
  int userRole;
  String amtSharePart;
  bool payee;
  double payeeAmount;
  int splitOweStatus;
  String memberStatus;
  List<Fooditems> items;
  List<SplitOwes> splitOwes;
  String mobileNo;
  String countryCode;

  SplitGroups(this.userId,this.userRole,this.amtSharePart, this.payee,this.payeeAmount,this.splitOweStatus,this.memberStatus,this.items,this.splitOwes,this.mobileNo,this.countryCode);

  Map toJson() => {
    'userId': userId,
    'userRole': userRole,
    'amtSharePart': amtSharePart,
    'payee': payee,
    'payeeAmount': payeeAmount,
    'splitOweStatus': splitOweStatus,
    'memberStatus': memberStatus,
    'items': items,
    'splitOwes': splitOwes,
    'mobileNo': mobileNo,
    'countryCode': countryCode,
  };
}
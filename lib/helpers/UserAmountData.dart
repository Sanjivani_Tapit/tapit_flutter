import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';
@JsonSerializable(nullable: false)
class UserAmountData
{
  UserAmountData({this.userID,this.cc,this.isTapitUser,this.type,this.colordata,this.image,this.name,this.number,this.isSelected,this.isPayee,this.shareAmount,this.payeeAmount,this.sharepart,this.usertype,this.settlementStatus});

  String userID;
  String cc;
  bool isTapitUser;
  String type;
  int colordata;
  String image;
  String name;
  String number;
  bool isSelected;
  bool isPayee;
  double shareAmount;
  double payeeAmount;
  double balanceAmount;
  int sharepart;
  String usertype;
  String settlementStatus;


  UserAmountData.fromJson(Map<String, dynamic> json)
      : userID = json['userID'],
        cc = json['cc'],
        isTapitUser = json['isTapitUser'],
        type = json['type'],
        colordata = json['colordata'],
        image = json['img'],
        name = json['name'],
        number = json['number'],
        isSelected=json['isSelected'],
        isPayee = json['isPayee'],
        shareAmount = json['shareAmount'],
        payeeAmount = json['payeeAmount'],
        balanceAmount = json['balanceAmount'],
        sharepart = json['sharepart'],
        usertype = json['usertype'],
        settlementStatus = json['settlementStatus'];


  Map<String, dynamic> toJson() {
    return {
      'userID':userID,
      'cc':cc,
      'isTapitUser':isTapitUser,
      'type':type,
      'colordata':colordata,
      'img': image,
      'name': name,
      'number': number,
      'isSelected': isSelected,
      'isPayee':isPayee,
      'shareAmount':shareAmount,
      'payeeAmount':payeeAmount,
      'balanceAmount':balanceAmount,
      'sharepart':sharepart,
      'usertype':usertype,
      'settlementStatus':settlementStatus,

    };
  }
}
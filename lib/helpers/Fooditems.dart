class Fooditems {
  String name;
  double rate;
  int quantity;
  double amount;
  Fooditems(this.name, this.rate,this.quantity,this.amount);

  Map toJson() => {
    'name': name,
    'rate': rate,
    'quantity': quantity,
    'amount': amount,
  };
}
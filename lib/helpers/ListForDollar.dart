import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/global.dart' as global;

class ListForDollar extends StatefulWidget
{
  const ListForDollar({Key key,this.index, this.userAmountData,this.profileImg,this.amount,this.isSel}) : super(key: key);
  final UserAmountData userAmountData;
  final Uint8List profileImg;
  final int index;
  final double amount;
  final ValueChanged<double> isSel;

  ListForDollarState createState()=>ListForDollarState();
}

class ListForDollarState extends State<ListForDollar>
{

  FocusNode focusNode = FocusNode();
  final textController = TextEditingController();
  bool isSel= false;
  String subtext="(Me)";
  String hintText="";

  @override
  void initState()
  {
    setState(() {
      hintText=global.formatter.format(widget.amount);
    });

    super.initState();

    focusNode.addListener(()
    {
      if (focusNode.hasFocus)
      {
        hintText = '';
      }
      else
      {
        hintText = global.formatter.format(widget.amount);
      }
      setState(() {});
    });

    if(widget.index==0)
    {
      subtext="(Me)";
    }
    else
    {
      subtext="";
    }

  }

  void _onRememberMeChanged(String newValue) => setState(()
  {
    textController.selection = TextSelection.collapsed(offset: textController.text.length);
    widget.isSel(double.parse(newValue));
  });

  @override
  Widget build(BuildContext context) {

    final textField=  TextField(

      focusNode: focusNode,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      style: TextStyle(fontSize:global.font14,color:global.blackcolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),
      inputFormatters: [ WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),],
      onChanged: _onRememberMeChanged,
      onTap: (){
        textController.selection = TextSelection.collapsed(offset: textController.text.length);
      },
      textAlignVertical: TextAlignVertical.center,
      textAlign: TextAlign.center,
      enabled: global.isSplitDetails?false:true,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: textController,
      decoration: InputDecoration(
          filled: false,
          fillColor:  Color(0xfffcfcfc),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xffc8c8c8c), width: 0.75),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusColor: global.whitecolor,
          focusedBorder:OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xffF57C00), width: 0.75),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(5, 5 , 5, 5),
          hintText: hintText,
          hintStyle: TextStyle(fontSize:global.font14,color:global.blackcolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular')
      ),
    );

    return new Row(
        children: <Widget>[
          Flexible(
            child:Padding(
              padding:  global.isSplitDetails?EdgeInsets.symmetric(vertical: 2.0,horizontal: 0.0):EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
              child:Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: global.whitecolor,
                    border: Border(
                      bottom: BorderSide(
                        color: widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails?Color(0xffe1e1e1):Color(0xffffffff),
                        width: 0.75,
                      ),
                      top:BorderSide(
                        color: widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails?Color(0xffe1e1e1):Color(0xffffffff),
                        width: 0.75,
                      ),
                    ),
                  ),
                  padding: EdgeInsets.fromLTRB(0,5, 0, 5),
                  child: Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Flexible(
                              flex:2,
                              fit: FlexFit.loose,
                              child: new Stack(
                                children: <Widget>[
                                  new Container(
                                    margin: EdgeInsets.all(3),
                                    width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                    height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                    child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                      backgroundImage: MemoryImage(widget.profileImg),
                                      backgroundColor: global.imageBackColor,
                                    ):(widget.userAmountData.image!=null?CircleAvatar(
                                      child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                      backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                                    ):CircleAvatar(
                                      backgroundImage: AssetImage('assets/dummy_user.png'),
                                      backgroundColor: global.appbargreycolor,
                                    ))):CircleAvatar(
                                      backgroundImage: AssetImage('assets/dummy_user.png'),
                                      backgroundColor: global.appbargreycolor,
                                    ),
                                  ),
                                  new Positioned(
                                    top:0.0,
                                    right: 0.0,
                                    child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                      height: 15,
                                      width: 15,
                                      child: Image.asset(
                                          'assets/admin_symbol.png',
                                          fit: BoxFit.contain
                                      ),
                                    ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                      height: 15,
                                      width: 15,
                                      child: Image.asset(
                                          'assets/payee_symbol.png',
                                          fit: BoxFit.contain
                                      ),
                                    ):new Container(
                                      width: 0,
                                      height: 0,
                                    ),
                                  ),
                                ],
                              )
                          ),
                          new Flexible(
                              flex: 6,
                              fit: FlexFit.tight,
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child:new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      widget.userAmountData.name!=null?
                                      (widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails? Text(
                                        "You",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),// user typed "et"
                                      ):
                                      new Text(
                                        widget.userAmountData.name.toString()+subtext,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),// user typed "et"
                                      )):new Text(
                                        widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),// user typed "et"
                                      ),
                                    ],
                                  )
                              )
                          ),
                          new Flexible(
                              flex:1,
                              fit: FlexFit.tight,
                              child:Container(
                                  width:  global.nontapitsymbolSize,
                                  height:  global.nontapitsymbolSize,
                                  child: !widget.userAmountData.isTapitUser?Image.asset(
                                      'assets/non_tapit_symbol.png',
                                      fit: BoxFit.contain
                                  ):null
                              )
                          ),
                          new Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: new Container(
                                padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child:  !global.isSplitDetails?new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text("\$",style: TextStyle(fontSize: global.font15,color: global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                    Expanded(
                                        child: new Container(
                                          padding: EdgeInsets.fromLTRB(3, 0, 0, 0),
                                          width: 60,
                                          height: 40,
                                          child:textField,
                                        )
                                    )
                                  ],
                                ):new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text("\$ "+widget.amount.toString(),style: TextStyle(fontSize: global.font15,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                  ],
                                )
                            ),
                          ),
                        ]
                    ),
                  )
              ),
            ),
          )
        ],
      );
  }
}
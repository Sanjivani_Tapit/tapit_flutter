import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/helpers/TaxReceipt.dart';
import 'ListForFoodItems.dart';
import 'package:tapit/helpers/MySeparator.dart';
import 'package:tapit/helpers/ExtraBillDetails.dart';

class ReceiptData extends StatefulWidget
{
  ReceiptData({Key key,this.merchantName,this.merchantURL,this.listofFoodItems,this.listOfExtraBillDetails,this.subTotal,this.totalAmount,this.receiptType,this.receiptDescription}) : super(key: key);
  final String merchantName;
  final String merchantURL;
  final List<Fooditems> listofFoodItems;
  final List<ExtraBillDetails> listOfExtraBillDetails;
  final double subTotal;
  final double totalAmount;
  final int receiptType;
  final String receiptDescription;
  ReceiptDataState createState()=>ReceiptDataState();
}

class ReceiptDataState extends State<ReceiptData> {

  ScrollController scrollController = new ScrollController();
  String LOGTAG="ReceiptData";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0,0,0,0),
        color: global.whitecolor,
        child:SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new Row(
                  children: <Widget>[
                    new Flexible(
                        flex:1,
                        child:
                        global.isReceiptFromContainer?Hero(
                            tag:'receiptinfo',
                            child: Material(
                                type: MaterialType.transparency,
                                child: new Container(
                                  child: SizedBox(
                                    height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.height/7:MediaQuery.of(context).size.height/9,
                                    width: MediaQuery.of(context).size.width,
                                    child:  new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        new Flexible(
                                            flex:1,
                                            fit:FlexFit.loose,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7:MediaQuery.of(context).size.width/10,
                                              height:MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7:MediaQuery.of(context).size.width/10,
                                              decoration: new BoxDecoration(
                                                color: global.imageBackColor,
                                                image: new DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: NetworkImage(widget.merchantURL),
                                                ),
                                                border: Border.all(
                                                  color: global.imageBackColor,
                                                  width: 1.0,
                                                ),
                                                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                              ),

                                            )),
                                        new Flexible(
                                            flex:2,
                                            fit:FlexFit.tight,
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: new Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Material(
                                                    type: MaterialType.transparency,
                                                    child: Text(widget.merchantName,
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(fontSize: global.font18,color: global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                  ),
                                                  Material(
                                                    type: MaterialType.transparency,
                                                    child: Opacity(
                                                        opacity: 0.6,
                                                        child:new Row(
                                                          children: <Widget>[
                                                            Text("Bill#12345678", style: TextStyle(fontSize: global.font15,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                            widget.receiptType==1?Text("(Manual)", style: TextStyle(fontSize: global.font15,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):new Container(width:0,height:0),
                                                          ],
                                                        )
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )),
                                      ],
                                    ),

                                  ),
                                )
                            )
                        ):new Container(
                          child: SizedBox(
                            height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.height/7:MediaQuery.of(context).size.height/9,
                            width: MediaQuery.of(context).size.width,
                            child:  new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[

                                new Flexible(
                                    flex:1,
                                    fit:FlexFit.loose,
                                    child: Container(
                                      width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7:MediaQuery.of(context).size.width/10,
                                      height:MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/7:MediaQuery.of(context).size.width/10,
                                      decoration: new BoxDecoration(
                                        color: Colors.white,
                                        image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(widget.merchantURL),
                                        ),
                                        border: Border.all(
                                          color: global.appbargreycolor,
                                          width: 1.0,
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                      ),
                                    )),
                                new Flexible(
                                    flex:2,
                                    fit:FlexFit.tight,
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(widget.merchantName,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: global.font18,color: global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                          Opacity(
                                              opacity: 0.6,
                                              child:new Row(
                                                children: <Widget>[
                                                  Text("Bill#12345678", style: TextStyle(fontSize: global.font15,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                  widget.receiptType==1?Text("(Manual)", style: TextStyle(fontSize: global.font15,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):new Container(width:0,height:0),
                                                ],
                                              )
                                          ),
                                        ],
                                      ),
                                    )),
                              ],
                            ),

                          ),
                        )
                    )
                  ]
              ),
              widget.receiptType==2?Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child:new Row(
                    children: <Widget>[
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child: Text("Items", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child: Text("Qty", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child: Text("Rate", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child:Text("Amount", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      )
                    ],
                  )
              ):Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child:new Row(
                    children: <Widget>[
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child: Text("Description", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              ),
              widget.receiptType==2?Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child:
                    ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemCount: widget.listofFoodItems.length,
                      itemBuilder: (context, index) {
                        return Container(
                          child: ListForFoodItems(index:index,fooditem:widget.listofFoodItems[index]),
                        );
                      },
                    ),
                  )
              ):new Container(width:0,height:0),


              widget.receiptType==2?Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child:new Row(
                    children: <Widget>[
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Opacity(
                              opacity:0.4,
                              child: Text("Subtotal", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      ),
                      Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: new Container(width: 0,height: 0,)
                      ),
                      Flexible(
                          flex: 2,
                          fit: FlexFit.tight,
                          child: new Container(width: 0,height: 0,)
                      ),
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: Text("\$"+global.formatter.format(widget.subTotal).toString(), style: TextStyle(fontSize: global.font13,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              ):new Container(width:0,height:0),
              widget.receiptType==2?Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child:Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemCount: widget.listOfExtraBillDetails.length,
                      itemBuilder: (context, index) {
                        return Container(
                          child: TaxReceipt(index:index,extraBillDetails: widget.listOfExtraBillDetails[index],),

                        );
                      },
                    ),
                  )
              ):new Container(width:0,height:0),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                child:widget.receiptType==1?(
                    new Row(
                      children: <Widget>[
                        new Container(
                          padding:EdgeInsets.fromLTRB(0,0,0,10),
                          child:Text(widget.receiptDescription.toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                        )
                      ],
                    )
                ):new Container(width:0,height:0),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                child: MySeparator(color: Color(0xff979797)),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child:new Row(
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                              child:widget.receiptType==2?Text("Total", style: TextStyle(fontSize: global.font16,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):Text("Amount paid", style: TextStyle(fontSize: global.font16,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            )
                          ],
                        ),
                      ),
                      Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: new Container(width: 0,height: 0,)
                      ),
                      Flexible(
                        flex: 1,
                        fit: FlexFit.loose,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: Text("\$"+global.formatter.format(widget.totalAmount).toString(), style: TextStyle(fontSize: global.font16,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              ),
            ],
          ),
        )
    );
  }
}


import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;

class ListForLog extends StatefulWidget
{
  const ListForLog({Key key,this.index,this.logData}) : super(key: key);
  final logData;
  final int index;
  ListForLogState createState()=>ListForLogState();
}

class ListForLogState extends State<ListForLog>
{
  @override
  void initState()
  {
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
            child:Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(0,10, 0, 0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          Text(widget.logData.txnTimestamp, style: TextStyle(fontSize:  global.font13,color:Color.fromRGBO(0, 0, 0, 0.7),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      new Row(
                        children: <Widget>[
                          Flexible(
                            child:widget.logData.txnStatus.toString().compareTo("reject")==0?Text(widget.logData.logTitle, style: TextStyle(fontSize: global.font15,color:global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):
                            (widget.logData.txnStatus.toString().compareTo("accept")==0?Text(widget.logData.logTitle, style: TextStyle(fontSize: global.font15,color:global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):
                            (widget.logData.txnStatus.toString().compareTo("pending")==0?Text(widget.logData.logTitle, style: TextStyle(fontSize: global.font15,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):
                            Text(widget.logData.logTitle, style: TextStyle(fontSize:  global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')))),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      widget.logData.txnStatus.toString().compareTo("reject")==0? widget.logData.txnComment!=null?new Row(
                        children: <Widget>[
                          Flexible(
                              child:Text(widget.logData.txnComment, style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                          )
                        ],
                      ):new Container(width: 0,height: 0,):new Container(width: 0,height: 0,),

                      widget.logData.txnStatus.toString().compareTo("settle")==0 || widget.logData.txnStatus.toString().compareTo("payee_settle")==0 || widget.logData.txnStatus.toString().compareTo("admin_settle")==0?widget.logData.txnMode!=null?new Row(
                        children: <Widget>[
                          Text("MODE: ", style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          Text(widget.logData.txnMode, style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                        ],
                      ):new Container(width: 0,height: 0,):new Container(width: 0,height: 0,),

                      widget.logData.txnStatus.toString().compareTo("settle")==0 || widget.logData.txnStatus.toString().compareTo("payee_settle")==0 || widget.logData.txnStatus.toString().compareTo("admin_settle")==0?widget.logData.txnAmount!=null?new Row(
                        children: <Widget>[
                          Text("AMNT: ", style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          Text("\$"+global.formatter.format(widget.logData.txnAmount), style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                        ],
                      ):new Container(width: 0,height: 0,):new Container(width: 0,height: 0,),

                      widget.logData.balanceAmt!=null?new Row(
                        children: <Widget>[
                          Text("BAL: ", style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          Text("\$"+global.formatter.format(widget.logData.balanceAmt), style: TextStyle(fontSize: global.font14,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
                        ],
                      ):new Container(width: 0,height: 0,),

                      Container(
                        padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xffdcdcdc),
                              width: 1.5,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ),
        ]
    );
  }
}
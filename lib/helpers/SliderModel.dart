class SliderModel{

  String imageAssetPath;
  String title;
  String desc;

  SliderModel({this.imageAssetPath,this.title,this.desc});

  void setImageAssetPath(String getImageAssetPath){
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle){
    title = getTitle;
  }

  void setDesc(String getDesc){
    desc = getDesc;
  }

  String getImageAssetPath(){
    return imageAssetPath;
  }

  String getTitle(){
    return title;
  }

  String getDesc(){
    return desc;
  }

}


List<SliderModel> getSlides(){

  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

  //1
  sliderModel.setDesc("Thermal paper making involves BPA chemicals hazrdous to earth and human too");
  sliderModel.setTitle("Go Green with E-receipts");
  sliderModel.setImageAssetPath("assets/Onb-1.png");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //2
  sliderModel.setDesc("Whether at malls or restaurants, split and ssettle your bills with friends");
  sliderModel.setTitle("Split and Settle bills hasslefree");
  sliderModel.setImageAssetPath("assets/Onb-2.png");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //3
  sliderModel.setDesc("Get rewards for staying connected");
  sliderModel.setTitle("Revisiting places rewards you.");
  sliderModel.setImageAssetPath("assets/Onb-3.png");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  return slides;
}
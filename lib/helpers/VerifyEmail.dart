import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class VerifyEmail extends StatefulWidget{

  VerifyEmail({Key key,this.isSel}) : super(key: key);
  final ValueChanged<bool> isSel;
  VerifyEmailState createState()=>VerifyEmailState();
}

class VerifyEmailState extends State<VerifyEmail>
{

  final emailController=new TextEditingController();
  bool emailadd_validate=false;
  bool show_missing_text=false;

  String emailAddress;

  Future<bool> sendEmailForVerification(String email) async
  {
    Future<bool> flag=Future<bool>.value(false);
    VerifyEmailObject verifyEmailObject=new VerifyEmailObject(global.UserID,email);
    var jsonbody=jsonEncode(verifyEmailObject);
    UserService userService=new UserService();
    ResponseBean response=await userService.verifyEmail(jsonbody);
    int statusCode=response.status;
    var payload=response.payLoad;
    if(statusCode==200)
    {
      flag=Future<bool>.value(true);
    }
    else
    {
      flag=Future<bool>.value(false);
    }
    return flag;
  }


  @override
  void initState() {
    // TODO: implement initState
    updateUI();
    super.initState();
  }

  void updateUI()async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    emailAddress=prefs.getString("email");
    setState(() {
      emailController.text=emailAddress;
    });
  }

  @override
  Widget build(BuildContext context) {

    final emailaddressField = TextField(
      onTap: () {
        setState(() {
          emailadd_validate = false;
          show_missing_text=false;
        });
      },

      cursorColor: global.mainColor,
      obscureText: false,
      controller: emailController,
      decoration: !emailadd_validate?InputDecoration(
        filled: true,
        fillColor: Color(0xffF0F0F0),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xffc0c0c0),
          ),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc0c0c0)),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Email address",
        hintStyle: TextStyle( fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ):InputDecoration(
        filled: true,
        fillColor: global.errorTextFieldFillColor,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xffBF4C4C),
          ),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xffc0c0c0)),
          borderRadius: BorderRadius.circular(5.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Email address",
        hintStyle: TextStyle( fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
      ),
    );
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        new Container(
            padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
            child:new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  height:50,
                  child: emailaddressField,
                ),
                SizedBox(height:15),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Offstage(
                      offstage: !show_missing_text,
                      child: new Text(
                        "Missing Mandtory Fields",
                        textAlign: TextAlign.left,
                        style: new TextStyle(
                            fontSize: global.font12,
                            color: global.errorColor,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'BalooChetanRegular'
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5,),
                new Flexible(
                  fit: FlexFit.loose,
                  child: new Text("Tapit will send a verification email to this address. Please open the mail and click on the link inside.", style: TextStyle(fontSize: global.font15,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                ),
              ],
            )
        ),
        SizedBox(height: 20,),
        new Container(
          width: MediaQuery.of(context).size.width,
          decoration: new BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                color: Color(0xffdcdcdc),
                width: 1.0,
              ),
            ),
          ),
        ),
        new Row(
          children: <Widget>[
            new Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                )
            ),
            new Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      child: Text("Send Email", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                      onPressed: () async {

                        if(emailController.text==null || emailController.text.isEmpty || emailController.text.compareTo("")==0)
                        {
                          setState(() {
                            show_missing_text=true;
                            emailadd_validate=true;
                          });
                        }
                        else
                        {
                          bool flag = await sendEmailForVerification(emailController.text);
                          widget.isSel(flag);
                        }
                      },
                    )
                  ],
                )
            )
          ],
        ),
      ],
    );
  }
}

class VerifyEmailObject {
  String id;
  String email;
  VerifyEmailObject(this.id, this.email);

  Map toJson() => {
    'id': id,
    'email': email,
  };
}

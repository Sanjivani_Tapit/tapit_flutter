import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart'as global;
import 'package:url_launcher/url_launcher.dart';

class ListForOffersVer extends StatefulWidget
{
  const ListForOffersVer({Key key}) : super(key: key);
  ListForOffersVerState createState()=>ListForOffersVerState();
}

class ListForOffersVerState extends State<ListForOffersVer>
{
  bool isSelected = false;

  @override
  void initState(){
    super.initState();
  }

  _launchURL() async {
    const url = 'https://www.google.com';
    if (await canLaunch(url))
    {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override

  Widget build(BuildContext context)
  {

    return new Row(
          children: <Widget>[
            Flexible(
                child:Container(
                  margin:EdgeInsets.fromLTRB(0,10, 0, 10),
                  width: MediaQuery.of(context).size.width,
                  color: global.whitecolor,
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          Flexible(child: Text("Thirsty Thursdays", style: TextStyle(fontSize: global.font17,color: global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),)
                        ],
                      ),
                      SizedBox(height: 10,),
                      new Row(
                        children: <Widget>[
                          Flexible(
                            child:
                            new RichText(
                              text: new TextSpan(
                                children: [
                                  new TextSpan(
                                    text: 'Buy 1, Get 1 offer of cocktails/beers throughout the day.Just Come with a group of 4 between 4 & 9PM ',
                                    style: new TextStyle(fontSize: global.font12,color: Color(0xFF808080),fontWeight: FontWeight.normal,
                                        fontFamily: 'BalooChetanRegular'),
                                  ),
                                  new TextSpan(
                                    text: '*T&C',
                                    style: new TextStyle(fontSize: global.font12,color: Color(0xFF0000FF),fontWeight: FontWeight.normal,
                                        fontFamily: 'BalooChetanRegular',decoration: TextDecoration.underline),
                                    recognizer: new TapGestureRecognizer()
                                      ..onTap = () {
                                        _launchURL();
                                      },
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 10,),
                      new Row(
                        children: <Widget>[
                          Flexible(child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: new Container(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(
                                        'assets/receipt_symbol.png',
                                        fit: BoxFit.contain
                                    )
                                ),
                              ),
                              new Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 30, 10),
                                child: new Text("4PM-9PM", style: TextStyle(fontSize: global.font17,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),

                              ),
                            ],
                          )),
                          Flexible(child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: new Container(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(
                                        'assets/receipt_symbol.png',
                                        fit: BoxFit.contain
                                    )
                                ),
                              ),
                              new Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 30, 10),
                                child: new Text("THU", style: TextStyle(fontSize: global.font17,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                              ),
                            ],
                          )),
                          Flexible(child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: new Container(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(
                                        'assets/receipt_symbol.png',
                                        fit: BoxFit.contain
                                    )
                                ),
                              ),
                              new Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 30, 10),
                                child: new Text("4", style: TextStyle(fontSize: global.font17,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                              ),
                            ],
                          ))
                        ],
                      )
                    ],
                  ),
                )
            ),
          ],
    );
  }
}
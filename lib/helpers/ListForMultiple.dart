import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/global.dart' as global;

class ListForMultiple extends StatefulWidget
{
  const ListForMultiple({Key key,this.index, this.userAmountData,this.profileImg,this.amount,this.isSel}) : super(key: key);
  final UserAmountData userAmountData;
  final Uint8List profileImg;
  final int index;
  final double amount;
  final ValueChanged<int> isSel;
  ListForMultipleState createState()=>ListForMultipleState();
}

class ListForMultipleState extends State<ListForMultiple>
{

  FocusNode focusNode = FocusNode();
  final textController = TextEditingController();
  bool isSel= false;
  int count=1;
  double _curAmount=0;
  String subtext="(Me)";
  String hintText="";

  @override
  void initState(){
    super.initState();

    setState(() {
      hintText=widget.userAmountData.sharepart.toString();
    });

    super.initState();

    focusNode.addListener(()
    {
      if (focusNode.hasFocus)
      {
        hintText = '';
      }
      else
      {
        hintText = widget.userAmountData.sharepart.toString();
      }
      setState(() {
      });
    });

    setState(() {
      _curAmount=widget.userAmountData.shareAmount;
      count=widget.userAmountData.sharepart;

      if(widget.index==0)
      {
        subtext="(Me)";
      }
      else
      {
        subtext="";
      }
    });
  }

  void _onRememberMeChanged(String newValue) => setState(()
  {
    textController.selection = TextSelection.collapsed(offset: textController.text.length);
    int prev_val=widget.userAmountData.sharepart.toInt();
    int new_val=int.parse(textController.text);
    int val_diff=new_val-prev_val;

    if(val_diff<0)
    {
      count=count-val_diff;
      global.multiple_parts += val_diff;
      widget.userAmountData.sharepart = count;
      _curAmount = (global.receiptAmount / global.multiple_parts) * count;
      _curAmount=double.parse((_curAmount).toStringAsFixed(2));
      widget.isSel(global.multiple_parts);
    }
    else
    {
      count=count+val_diff;
      global.multiple_parts += val_diff;
      widget.userAmountData.sharepart = count;
      _curAmount = double.parse((global.receiptAmount / global.multiple_parts).toStringAsFixed(2)) * count;
      _curAmount=double.parse((_curAmount).toStringAsFixed(2));
      widget.isSel(global.multiple_parts);
    }

  });

  @override
  Widget build(BuildContext context) {

    final textField=  TextField(

      focusNode: focusNode,
      inputFormatters: [ WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),],
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      onChanged: _onRememberMeChanged,
      onTap: (){
        textController.selection = TextSelection.collapsed(offset: textController.text.length);
      },
      textAlignVertical: TextAlignVertical.center,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),
      enabled: global.isSplitDetails?false:true,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: textController,
      decoration: InputDecoration(
          filled: true,
          fillColor:  Color(0xfffcfcfc),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: Color(0xffc8c8c8),
                width: 0.75
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder:OutlineInputBorder(
            borderSide: const BorderSide(
                color: Color(0xffF57C00),
                width: 0.75
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(5, 5 , 5, 5),
          hintText: hintText,
          hintStyle: TextStyle(fontSize:global.font14,color:global.darkgreycolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular')
      ),
    );

    return new Row(
      children: <Widget>[
        Flexible(
          child: Padding(
            padding:  global.isSplitDetails?EdgeInsets.symmetric(vertical: 2.0,horizontal: 0.0):EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
            child:Container(
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  color: global.whitecolor,
                  border: Border(
                    bottom: BorderSide(
                      color: widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails?Color(0xffe1e1e1):Color(0xffffffff),
                      width: 0.75,
                    ),
                    top:BorderSide(
                      color: widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails?Color(0xffe1e1e1):Color(0xffffffff),
                      width: 0.75,
                    ),
                  ),
                ),
                padding: EdgeInsets.fromLTRB(0,5, 0, 5),
                child: Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Flexible(
                            flex:2,
                            fit: FlexFit.loose,
                            child: new Stack(
                              children: <Widget>[
                                new Container(
                                  margin: EdgeInsets.all(3),
                                  width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                  height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                  child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                    backgroundImage: MemoryImage(widget.profileImg),
                                    backgroundColor: global.imageBackColor,
                                  ):(widget.userAmountData.image!=null?CircleAvatar(
                                    child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                    backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                                  ):CircleAvatar(
                                    backgroundImage: AssetImage('assets/dummy_user.png'),
                                    backgroundColor: global.appbargreycolor,
                                  ))):CircleAvatar(
                                    backgroundImage: AssetImage('assets/dummy_user.png'),
                                    backgroundColor: global.appbargreycolor,
                                  ),
                                ),
                                new Positioned(
                                  top:0.0,
                                  right: 0.0,
                                  child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                    height: 15,
                                    width: 15,
                                    child: Image.asset(
                                        'assets/admin_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                    height: 15,
                                    width: 15,
                                    child: Image.asset(
                                        'assets/payee_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ):new Container(
                                    width: 0,
                                    height: 0,
                                  ),
                                ),
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 6,
                            fit: FlexFit.tight,
                            child: Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                child:new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    widget.userAmountData.name!=null? (widget.userAmountData.userID.compareTo(global.UserID)==0 && global.isSplitDetails? Text(
                                      "You",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    ):
                                    new Text(
                                      widget.userAmountData.name.toString()+subtext,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    )):new Text(
                                      widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    ),
                                    Opacity(
                                      opacity: 0.6,
                                      child: new Text(
                                        "\$ "+global.formatter.format(widget.userAmountData.shareAmount).toString(),
                                        style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                      ),
                                    )
                                  ],
                                )
                            )
                        ),
                        new Flexible(
                            flex:1,
                            fit: FlexFit.tight,
                            child:Container(
                                width:  global.nontapitsymbolSize,
                                height:  global.nontapitsymbolSize,
                                child: !widget.userAmountData.isTapitUser?Image.asset(
                                    'assets/non_tapit_symbol.png',
                                    fit: BoxFit.contain
                                ):new Container(width: 0,height: 0,)
                            )
                        ),

                        new Flexible(
                          flex: 3,
                          fit: FlexFit.tight,
                          child: new Container(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: new Row(
                              mainAxisAlignment: !global.isSplitDetails?MainAxisAlignment.end:MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                !global.isSplitDetails?GestureDetector(
                                  onTap: (){
                                    setState(()
                                    {
                                      if(!global.isSplitDetails)
                                      {
                                        if (count>0 && global.multiple_parts>1)
                                        {
                                          count--;
                                          global.multiple_parts -= 1;
                                          widget.userAmountData.sharepart = count;
                                          textController.text = (count).toString();
                                          _curAmount = (global.receiptAmount / global.multiple_parts) * count;
                                          _curAmount=double.parse((_curAmount).toStringAsFixed(2));
                                          widget.isSel(global.multiple_parts);
                                        }
                                      }
                                    });
                                  },
                                  child: Container(
                                    width: 25,
                                    height: 25,
                                    child: Image.asset(
                                        'assets/sub_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ),
                                ):new Container(width: 0,height: 0,),

                                !global.isSplitDetails?Expanded(
                                  child: new Container(
                                    height: 40,
                                    child: textField,
                                  ),
                                ):new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      child: new Text(widget.userAmountData.sharepart.toString(),style: TextStyle(fontSize: global.font15,color: global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                    ),
                                  ],
                                ),

                                !global.isSplitDetails? GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      if(!global.isSplitDetails) {
                                        if (count >= 0 && global.multiple_parts>=1) {
                                          count++;
                                          global.multiple_parts += 1;
                                          widget.userAmountData.sharepart = count;
                                          textController.text = (count).toString();
                                          _curAmount = double.parse((global.receiptAmount / global.multiple_parts).toStringAsFixed(2)) * count;
                                          _curAmount=double.parse((_curAmount).toStringAsFixed(2));
                                          widget.isSel(global.multiple_parts);
                                        }
                                      }
                                    });
                                  },
                                  child: Container(
                                    width: 25,
                                    height: 25,
                                    child: Image.asset(
                                        'assets/add_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ),
                                ):new Container(width: 0,height: 0,)
                              ],
                            ),
                          ),
                        ),
                      ]
                  ),
                )
            ),
          ),
        )
      ],
    );
  }
}
import 'package:tapit/helpers/SplitGroups.dart';

class BillSplit {
  String groupId;
  int additionalTip;
  String split_type;
  String settlement_status;
  List<SplitGroups> splitGroups;
  List<SplitGroups> unregisteredSplitGroups;


  BillSplit(this.groupId, this.additionalTip,this.split_type,this.settlement_status,this.splitGroups,this.unregisteredSplitGroups);

  Map toJson() => {
    'groupId': groupId,
    'additionalTip': additionalTip,
    'splitType': split_type,
    'settlement_status': settlement_status,
    'splitGroups': splitGroups,
    'unregisteredSplitGroups':unregisteredSplitGroups,
  };
}
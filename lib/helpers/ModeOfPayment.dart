import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';

class ModeOfPayment extends StatefulWidget
{
  ModeOfPayment({Key key,this.dailogType,this.isSel}) : super(key: key);
  final ValueChanged<int> isSel;
  final int dailogType;
  ModeOfPaymentState createState()=>ModeOfPaymentState();
}

class ModeOfPaymentState extends State<ModeOfPayment>
{
  bool pay=true;
  bool venmo=false;
  bool gpay=false;
  bool others=false;
  int selectedMode=0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.dailogType==0? new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap:(){
              setState(() {
                selectedMode=0;
                venmo=false;
                gpay=false;
                pay=true;
                widget.isSel(0);
              });
            },
            child: pay? new Container(
              decoration: BoxDecoration(
                color: global.whitecolor,
                border: Border.all(color: global.mainColor, width: 1.5),
                borderRadius: BorderRadius.all(Radius.circular(13.0)),
              ),
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                      child: Image.asset(
                          'assets/apple.png',
                          fit: BoxFit.contain
                      )
                  ),
                  new Text("pay", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                ],
              ),
            ):new Container(
              decoration: BoxDecoration(
                color: global.whitecolor,
                border: Border.all(color: global.modeColor, width: 1.5),
                borderRadius: BorderRadius.all(Radius.circular(13.0)),
              ),
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                      child: Image.asset(
                          'assets/apple.png',
                          fit: BoxFit.contain,
                          color:global.nameTextColor
                      )
                  ),
                  new Text("pay", style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                ],
              ),
            ),
          ),
          SizedBox(width: 10,),
          GestureDetector(
              onTap: (){
                setState(() {
                  pay=false;
                  gpay=false;
                  venmo=true;
                  selectedMode=1;
                  widget.isSel(1);
                });
              },
              child: venmo?new Container(
                decoration: BoxDecoration(
                  color: global.whitecolor,
                  border: Border.all(color: global.mainColor, width: 1.5),
                  borderRadius: BorderRadius.all(Radius.circular(13.0)),
                ),
                padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text("Venmo", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                  ],
                ),
              ):new Container(
                decoration: BoxDecoration(
                  color: global.whitecolor,
                  border: Border.all(color: global.modeColor, width: 1.5),
                  borderRadius: BorderRadius.all(Radius.circular(13.0)),
                ),
                padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text("Venmo", style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                  ],
                ),
              )
          ),
          SizedBox(width: 10,),
          GestureDetector(
            onTap: (){
              setState(() {
                gpay=true;
                venmo=false;
                pay=false;
                selectedMode=2;
                widget.isSel(2);
              });
            },
            child: gpay? new Container(
              decoration: BoxDecoration(
                color: global.whitecolor,
                border: Border.all(color: global.mainColor, width: 1.5),
                borderRadius: BorderRadius.all(
                    Radius.circular(13.0)),
              ),
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text("Gpay", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                ],
              ),
            ): new Container(
              decoration: BoxDecoration(
                color: global.whitecolor,
                border: Border.all(color: global.modeColor, width: 1.5),
                borderRadius: BorderRadius.all(
                    Radius.circular(13.0)),
              ),
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text("Gpay", style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                ],
              ),
            ),
          ),
        ],
      ):new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child:  GestureDetector(
              onTap:(){
                setState(() {
                  selectedMode=0;
                  others=false;
                  pay=true;
                  widget.isSel(3);
                });
              },
              child: pay? new Container(
                  width: 80,
                  decoration: BoxDecoration(
                    color: global.whitecolor,
                    border: Border.all(
                        color: global.mainColor,
                        width: 1.5),
                    borderRadius: BorderRadius.all(
                        Radius.circular(13.0)),
                  ),
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text("Cash", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                    ],
                  )
              ):new Container(
                width: 80,
                decoration: BoxDecoration(
                  color: global.whitecolor,
                  border: Border.all(color: global.modeColor, width: 1.5),
                  borderRadius: BorderRadius.all(Radius.circular(13.0)),
                ),
                padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text("Cash", style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(width: 10,),
          new Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child:  GestureDetector(
              onTap: (){
                setState(() {
                  pay=false;
                  others=true;
                  selectedMode=4;
                  widget.isSel(selectedMode);
                });
              },
              child: others?new Container(
                  width: 80,
                  decoration: BoxDecoration(
                    color: global.whitecolor,
                    border: Border.all(color: global.mainColor, width: 1.5),
                    borderRadius: BorderRadius.all(Radius.circular(13.0)),
                  ),
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text("Others", style: TextStyle(fontSize: global.font15,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                    ],
                  )
              ):new Container(
                  width: 80,
                  decoration: BoxDecoration(
                    color: global.whitecolor,
                    border: Border.all(color: global.modeColor, width: 1.5),
                    borderRadius: BorderRadius.all(Radius.circular(13.0)),
                  ),
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text("Others", style: TextStyle(fontSize: global.font15,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                    ],
                  )
              ),
            ),
          ),
        ],
      );
  }
}


import 'package:json_annotation/json_annotation.dart';
@JsonSerializable(nullable: false)
class AlertTypeList
{
  AlertTypeList(this.alertType, this.notificationEnabled, this.appNotificationEnabled, this.emailNotificationEnabled, this.messageNotificationEnabled,);

  String alertType;
  bool notificationEnabled;
  bool appNotificationEnabled;
  bool emailNotificationEnabled;
  bool messageNotificationEnabled;

  AlertTypeList.fromJson(Map<String, dynamic> json)
      : alertType = json['alertType'],
        notificationEnabled = json['notificationEnabled'],
        appNotificationEnabled = json['appNotificationEnabled'],
        emailNotificationEnabled = json['emailNotificationEnabled'],
        messageNotificationEnabled = json['messageNotificationEnabled'];

  Map<String, dynamic> toJson() {
    return {
      'alertType':alertType,
      'notificationEnabled':notificationEnabled,
      'appNotificationEnabled':appNotificationEnabled,
      'emailNotificationEnabled':emailNotificationEnabled,
      'messageNotificationEnabled':messageNotificationEnabled,
    };
  }
}
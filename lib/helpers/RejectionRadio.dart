import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';

class RejectionRadio extends StatefulWidget
{
  RejectionRadio({Key key,this.isSel}) : super(key: key);
  final ValueChanged<int> isSel;
  RejectionRadioState createState()=>RejectionRadioState();
}

class RejectionRadioState extends State<RejectionRadio>
{
  int _radioValue=-1;

  void _handleRadioValueChange(int value)
  {
    setState(()
    {
      _radioValue = value;

      switch (_radioValue)
      {
        case 0:
          setState(()
          {
            widget.isSel(_radioValue);
          });
          break;
        case 1:
          setState(()
          {
            widget.isSel(_radioValue);
          });
          break;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  height: 24.0,
                  width: 24.0,
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                  child: new Radio(
                    value: 0,
                    activeColor: global.mainColor,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                Expanded(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text("I want adjustment in my share", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 10,),
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                  height:24,
                  width: 24,
                  child: new Radio(
                    value: 1,
                    activeColor: global.mainColor,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                Expanded(
                  child:  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text("I have already paid in other mode", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(0, 0, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                    ],
                  ),
                )
              ],
            ),
          ],
        )
    );
  }
}


import 'package:flutter/material.dart';
class PointedEdgeClipperTop extends CustomClipper<Path> {
  @override
  Path getClip(Size size)
  {
    Path path = Path();
    path.lineTo(0, size.height);
    var curXPos = 0.0;
    var curYPos = 0.0;
    var increment = size.width / 50;

    while (curXPos < size.width)
    {
      curXPos += increment;
      curYPos = curYPos == size.height ? size.height -8 : size.height;
      path.lineTo(curXPos, curYPos);
    }
    path.lineTo(0,size.width);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
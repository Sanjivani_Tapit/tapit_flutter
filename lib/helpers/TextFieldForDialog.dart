import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';

class TextFieldForDialog extends StatefulWidget
{
  TextFieldForDialog({Key key,this.initialVal,this.isSel}) : super(key: key);
  final ValueChanged<double> isSel;
  final double initialVal;
  TextFieldForDialogState createState()=>TextFieldForDialogState();
}

class TextFieldForDialogState extends State<TextFieldForDialog> {

  final formatter = new NumberFormat("##.##");
  final textController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return
      new Row(
        children: <Widget>[
          new Flexible(
            flex:1,
            child: new Text("an amount of", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
          ),
          new Flexible(
              flex:1,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text("\$",style: TextStyle(fontSize: global.font17,color: global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.fromLTRB(3, 5, 0, 5),
                        width: MediaQuery.of(context).size.width/4,
                        child: TextField(
                          onChanged: (value){
                            widget.isSel(double.parse(value));
                          },
                          onSubmitted: (value){
                            textController.text=value;
                          },
                          inputFormatters: [ WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),],
                          style: TextStyle(fontSize: global.font20,color:global.lightgreytextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          enabled: true,
                          cursorColor: global.mainColor,
                          obscureText: false,
                          controller: textController,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor:  Color(0xffF7F7F7),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xffc8c8c8),
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xffc8c8c8),
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              focusedBorder:OutlineInputBorder(
                                borderSide: const BorderSide(color: Color(0xffc8c8c8)),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              contentPadding: EdgeInsets.fromLTRB(5, 5 , 5, 5),
                              hintText:formatter.format(widget.initialVal),
                              hintStyle: TextStyle(color:Color.fromRGBO(0, 0, 0, 0.7),fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular')
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )
          )
        ],
      );
  }
}


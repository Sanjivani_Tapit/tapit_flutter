import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';
import 'package:tapit/helpers/ListForLog.dart';
import 'package:tapit/helpers/LogData.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class LogListViewDialog extends StatefulWidget
{
  LogListViewDialog({Key key,this.userID,this.receiptID}) : super(key: key);
  final String userID;
  final String receiptID;
  LogListViewDialogState createState()=>LogListViewDialogState();
}

class LogListViewDialogState extends State<LogListViewDialog>
{
  bool logResponse=false;
  List<LogData> finalLogList=new List();

  @override
  void initState()
  {
    super.initState();
    getLog();
  }

  Future<void> getLog() async
  {
    finalLogList.clear();
    UserService userService=new UserService();
    ResponseBean response=await userService.getLogs(widget.userID,global.receiptID);

    if(response!=null)
    {
      int statusCode = response.status;
      List<dynamic> logList = response.payLoad;
      if(statusCode==200 || statusCode ==204)
      {
        if (logList != null)
        {
          var format = DateFormat('HH:mm');

          for (int i = 0; i < logList.length; i++)
          {
            String currentDay;
            double balanceAmt = 0;
            String txnId = logList[i]["txnId"];
            String txnTimestamp = logList[i]["txnDate"];
            String senderName = logList[i]["senderId"];
            String receiverName = logList[i]["receiverId"];
            double txnAmount = logList[i]["txnAmount"];
            String txnStatus = logList[i]["txnStatus"];
            String txnMode = logList[i]["txnMode"];
            String txnComment = logList[i]["comment"];
            balanceAmt = logList[i]["balanceAmount"];

            String day = txnTimestamp.substring(0, 10);
            String time = txnTimestamp.substring(11, 19);

            var dateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse(day + " " + time, true);
            var dateLocal = dateTime.toLocal();
            String alert_time = dateTime.toLocal().toString().substring(11, 16);
            String time_hh_mm = alert_time;

            final now = DateTime.now();
            final lastMidnight = new DateTime(now.year, now.month, now.day);

            int today_midnight = lastMidnight.millisecondsSinceEpoch;
            int current_timeinmillies = dateTime.toLocal().millisecondsSinceEpoch;
            bool timeFlag = false;

            int s = 0;
            while (!timeFlag)
            {
              if (current_timeinmillies > today_midnight)
              {
                if (s == 0)
                {
                  currentDay = "Today" + " " + time_hh_mm;
                  timeFlag = true;
                }
                else if (s == 1)
                {
                  currentDay = "Yesterday" + " " + time_hh_mm;
                  timeFlag = true;
                }
                else
                {
                  currentDay = s.toString() + " days ago" + " " + time_hh_mm;
                  timeFlag = true;
                }
              }
              else
              {
                today_midnight = today_midnight - 86400000;
              }
              s++;
            }

            String logtitle = "";
            if (txnStatus.toString().compareTo("reject") == 0)
            {
              if (senderName.compareTo(global.UserID) == 0)
              {
                logtitle = "You rejected";
              }
              else
              {
                logtitle = global.UserIdAndNameMap[senderName].toString() + " rejected ";
              }
            }
            else if (txnStatus.toString().compareTo("settle") == 0 ||
                txnStatus.toString().compareTo("admin_settle") == 0 ||
                txnStatus.toString().compareTo("payee_settle") == 0 ||
                txnStatus.toString().compareTo("payee_partial_settle") == 0 ||
                txnStatus.toString().compareTo("admin_partial_settle") == 0) {
              if (txnMode != null)
              {
                if (txnMode.compareTo("2") == 0 || txnMode.compareTo("1") == 0 || txnMode.compareTo("0") == 0)
                {
                  if (senderName.compareTo(global.UserID) == 0)
                  {
                    logtitle = "You settled \$" + txnAmount.toString() + " for" + global.UserIdAndNameMap[receiverName].toString();
                  }
                  else
                  {
                    logtitle = global.UserIdAndNameMap[senderName].toString() + " settled \$" + txnAmount.toString() + " for you ";
                  }
                }
                else
                {
                  if (senderName.compareTo(global.UserID) == 0)
                  {
                    if (txnMode.compareTo("3") == 0)
                    {
                      logtitle = "You settled \$" + txnAmount.toString() + " with " + global.UserIdAndNameMap[receiverName].toString() + " for cash";
                    }
                    else
                    {
                      logtitle = "You settled \$" + txnAmount.toString() + " with " + global.UserIdAndNameMap[receiverName].toString();
                    }
                  }
                  else
                  {
                    if (txnMode.compareTo("3") == 0)
                    {
                      logtitle = global.UserIdAndNameMap[senderName].toString() + " settled you with \$" + txnAmount.toString() + " for cash ";
                    }
                    else
                    {
                      logtitle = global.UserIdAndNameMap[senderName].toString() + " settled you with\$" + txnAmount.toString();
                    }
                  }
                }
              }
              else
              {
                logtitle = "null transfer mode";
              }
            }
            else if (txnStatus.toString().compareTo("pending") == 0)
            {
              if (senderName.compareTo(global.UserID) == 0)
              {
                logtitle = "You requested to pay";
              }
              else
              {
                logtitle = global.UserIdAndNameMap[senderName].toString() + " requested to pay";
              }
            }

            if (txnMode.compareTo("0") == 0) {
              txnMode = "Apple Pay";
            }
            else if (txnMode.compareTo("1") == 0) {
              txnMode = "Venmo";
            }
            else if (txnMode.compareTo("2") == 0) {
              txnMode = "GPay";
            }
            else if (txnMode.compareTo("3") == 0) {
              txnMode = "Cash";
            }
            else if (txnMode.compareTo("4") == 0) {
              txnMode = "Other";
            }

            LogData logData = new LogData(txnId: txnId,
              txnTimestamp: currentDay,
              senderName: senderName,
              receiverName: receiverName,
              txnAmount: txnAmount,
              txnStatus: txnStatus,
              txnMode: txnMode,
              txnComment: txnComment,
              logTitle: logtitle,
              balanceAmt: balanceAmt,
            );
            finalLogList.add(logData);
          }
        }
      }
    }

    setState(() {
      logResponse=true;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        padding:  EdgeInsets.fromLTRB(18,0, 18, 0),
        child: logResponse?(finalLogList.length>0?
        ListView.builder(
          itemCount: finalLogList.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Center(
                child:Container(
                  child: ListForLog(index:index,logData:finalLogList[index]),
                )
            );
          },
        ):Center(
            child: new Container(
              child: new Text("No Log Found",style: TextStyle(fontSize:  global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
            )
        )
        ):Center(
          child: new Container(
            height: 35,
            width: 35,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
              backgroundColor: global.mainColor,
              strokeWidth: 5,),
          ),
        )
    );
  }
}


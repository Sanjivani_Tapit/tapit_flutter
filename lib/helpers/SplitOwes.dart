class SplitOwes {
  String userId;
  double amount;
  double balanceAmount;
  SplitOwes(this.userId, this.amount,this.balanceAmount);

  Map toJson() => {
    'userId': userId,
    'amount': amount,
    'balanceAmount': balanceAmount,
  };
}
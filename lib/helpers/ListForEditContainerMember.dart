import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:tapit/helpers/LogData.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ModeOfPayment.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/helpers/RejectionRadio.dart';
import 'package:tapit/helpers/LogListViewDialog.dart';

class ListForEditContainerMember extends StatefulWidget
{
  const ListForEditContainerMember({Key key,this.index, this.userAmountData,this.profileImg,this.isSettledSplit,this.isSel}) : super(key: key);
  final UserAmountData userAmountData;
  final Uint8List profileImg;
  final int index;
  final bool isSettledSplit;
  final ValueChanged<bool> isSel;

  ListForEditContainerMemberState createState()=>ListForEditContainerMemberState();
}

class ListForEditContainerMemberState extends State<ListForEditContainerMember>
{
  final textController = TextEditingController();
  bool isSel= false;
  bool isUserSettled=false;
  int modeOfPayemnt=0;
  String comment="";
  String subtext="(Me)";
  bool logResponse=false;

  List<LogData> finalLogList=new List();

  @override
  void initState(){
    super.initState();
    setState(() {

      if(widget.index==0)
      {
        subtext="(Me)";
      }
      else
      {
        subtext="";
      }
    });
  }


  void ShowLogDialog(){

    if(widget.userAmountData.userID.compareTo(global.UserID)==0)
    {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                  height: 400,
                  padding:  EdgeInsets.fromLTRB(0,8, 0, 10),
                  child: new Column(
                    children: <Widget>[
                      Flexible(
                          flex:1,
                          fit:FlexFit.tight,
                          child:
                          Container(
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  width: 1.0,
                                ),
                              ),
                            ),
                            child:
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        padding:  EdgeInsets.fromLTRB(18,0, 0, 0),
                                        child: new Text("Action Logs", style: TextStyle(fontSize:  global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                      )
                                    ],
                                  ),
                                ),
                                Flexible(
                                    flex:1,
                                    child:new Container(
                                      padding:  EdgeInsets.fromLTRB(0,0, 18, 0),
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                              width: 20,
                                              height: 20,
                                              child: GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).pop();
                                                },
                                                child: Image.asset(
                                                  'assets/black_cross_icon.png',
                                                  fit: BoxFit.contain,
                                                  color:Color.fromRGBO(0, 0, 0, 0.3),
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          )
                      ),
                      Flexible(
                        flex:7,
                        fit:FlexFit.tight,
                        child: LogListViewDialog(userID: widget.userAmountData.userID, receiptID: global.receiptID),
                      )
                    ],
                  )
              ),
            );
          }
      );
    }
  }


  void payupDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {

          modeOfPayemnt=0;
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new Container(
                      padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                      child:new Column(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              Flexible(
                                flex:1,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("Pay to", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ],
                                ),
                              ),
                              Flexible(
                                flex:1,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                        width: 20,
                                        height: 20,
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.of(context).pop();
                                          },
                                          child: Image.asset(
                                            'assets/black_cross_icon.png',
                                            fit: BoxFit.contain,
                                            color:Color.fromRGBO(0, 0, 0, 0.3),
                                          ),
                                        )
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 20),
                          new Stack(
                            children: <Widget>[
                              new Container(
                                width: 50.0,
                                height: 50.0,
                                child: global.adminImage!=null?((global.adminImageType.toString().contains("true"))?
                                CircleAvatar(
                                  backgroundImage: MemoryImage(global.adminImage),
                                  backgroundColor: global.imageBackColor,
                                ):CircleAvatar(
                                  child: Text(global.adminImageString,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                  backgroundColor: global.colors.elementAt(2),
                                )):
                                CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ),
                              ),
                              new Positioned(
                                  top:0.0,
                                  right: 0.0,
                                  child: Container(
                                    height: 15,
                                    width: 15,
                                    child: Image.asset(
                                        'assets/admin_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  )
                              ),
                            ],
                          ),
                          new Text(global.adminName.toString(), style: TextStyle(fontSize: global.font18,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                          new Text("(Admin)", style: TextStyle(fontSize: global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          SizedBox(height:17),
                          new Text("an amount of", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          SizedBox(
                            height: 2,
                          ),
                          new Text("\$ "+global.formatter.format(widget.userAmountData.balanceAmount), style: TextStyle(fontSize: global.font26,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                          SizedBox(
                            height: 20,
                          ),
                          new Text("Mode of Payment", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                          SizedBox(height: 10,),
                          ModeOfPayment(dailogType:0,isSel: (int value)  {
                            setState(() {
                              modeOfPayemnt=value;
                            });

                          },
                          ),
                        ],
                      )
                  ),
                  SizedBox(height: 20,),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffdcdcdc),
                          width: 1.0,
                        ),
                      ),
                    ),
                  ),
                  new Row(
                    children: <Widget>[
                      new Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child:new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                onPressed: (){
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          )
                      ),
                      new Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child:new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                onPressed: () async {
                                  String amt=double.parse((widget.userAmountData.balanceAmount).toStringAsFixed(2)).toString();
                                  bool flag=await global.helperClass.payupAPI(global.receiptID,widget.userAmountData.userID,global.adminUserID,amt,modeOfPayemnt,"member");
                                  if(flag) {
                                    setState(() {
                                      isUserSettled=true;
                                      widget.userAmountData.settlementStatus = "accept";
                                    });
                                  }
                                  Navigator.of(context).pop();
                                  widget.isSel(true);
                                },
                              )
                            ],
                          )
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  void rejectionDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {

          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child:new Column(
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Reject request", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                          width: 20,
                                          height: 20,
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Image.asset(
                                              'assets/black_cross_icon.png',
                                              fit: BoxFit.contain,
                                              color:Color.fromRGBO(0, 0, 0, 0.3),
                                            ),
                                          )
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            new Column(
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("I can't accept this request because", style: TextStyle(fontSize: global.font17,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(height:17),
                            RejectionRadio(isSel: (int value)
                            {
                              if(value==0)
                              {
                                setState(()
                                {
                                  comment="I want adjustment in my share";
                                });
                              }
                              else
                              {
                                setState(()
                                {
                                  comment="I have already paid in other mode";
                                });
                              }
                            },
                            )
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async
                                  {
                                    if(comment!=null && comment.isNotEmpty)
                                    {
                                      bool flag = await global.helperClass.rejectAPI(global.receiptID,global.UserID,global.adminUserID,comment,"member");
                                      if (flag)
                                      {
                                        setState(()
                                        {
                                          widget.userAmountData.settlementStatus = "reject";
                                          isUserSettled = true;
                                        });
                                      }
                                      Navigator.of(context).pop();
                                      widget.isSel(true);
                                    }
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
    );
  }



  @override
  Widget build(BuildContext context) {

    return
      widget.userAmountData.settlementStatus.contains("pending") && widget.index==0 ?
      Expanded(
          child:GestureDetector(
            onTap: ShowLogDialog,
            child: Container(
                decoration: new BoxDecoration(
                  color: global.whitecolor,
                  border: Border(
                    bottom: BorderSide
                      (
                      color: Color(0xffdcdcdc),
                      width: 1.0,
                    ),
                  ),
                ),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(10,10,10,10),
                child:
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Flexible(
                                flex:2,
                                fit: FlexFit.loose,
                                child:
                                new Stack(
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.all(3),
                                      child:global.imageList[0].toString()!=null?(
                                          widget.userAmountData.type.toString().compareTo("true")==0?
                                          CircleAvatar(
                                            backgroundImage: MemoryImage(global.imageList[0]),
                                            backgroundColor: global.imageBackColor,
                                          ):
                                          CircleAvatar(
                                              child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: global.adminTextcolor),),
                                              backgroundColor: global.adminBackcolor
                                          )
                                      ):
                                      CircleAvatar(
                                        backgroundImage: AssetImage('assets/dummy_user.png'),
                                        backgroundColor: global.appbargreycolor,
                                      ),
                                    ),
                                    new Positioned(
                                      top:0.0,
                                      right: 0.0,
                                      child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                        height: 15,
                                        width: 15,
                                        child: Image.asset(
                                            'assets/admin_symbol.png',
                                            fit: BoxFit.contain
                                        ),
                                      ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                        height: 15,
                                        width: 15,
                                        child: Image.asset(
                                            'assets/payee_symbol.png',
                                            fit: BoxFit.contain
                                        ),
                                      ):new Container(
                                        width: 0,
                                        height: 0,
                                      ),
                                    ),
                                  ],
                                )
                            ),
                            Flexible(
                                flex: 11,
                                fit: FlexFit.tight,
                                child:new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            new Flexible(
                                                flex: 6,
                                                fit: FlexFit.tight,
                                                child: Container(
                                                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                    child:new Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: <Widget>[
                                                        Expanded(
                                                            child:new Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: <Widget>[
                                                                widget.index==0?Text(
                                                                  "You",
                                                                  style: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                                                                ):Text(
                                                                  (widget.userAmountData.name==null?widget.userAmountData.cc.toString()+" "+widget.userAmountData.number:widget.userAmountData.name),
                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                ),
                                                              ],
                                                            )
                                                        ),
                                                      ],
                                                    )
                                                )
                                            ),
                                            widget.isSettledSplit?new Flexible(
                                                flex:1,
                                                fit: FlexFit.loose,
                                                child:Container(
                                                    width:  global.nontapitsymbolSize,
                                                    height:  global.nontapitsymbolSize,
                                                    child: !widget.userAmountData.isTapitUser?Image.asset(
                                                        'assets/non_tapit_symbol.png',
                                                        fit: BoxFit.contain
                                                    ):null
                                                )
                                            ):new Container(width: 0,height: 0,),

                                            widget.isSettledSplit?new Flexible(
                                                flex: 6,
                                                fit: FlexFit.tight,
                                                child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0?Container(
                                                    child:new Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        new Flexible(
                                                            flex:1,
                                                            fit: FlexFit.tight,
                                                            child:new Column(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                                children: <Widget>[
                                                                  widget.userAmountData.isPayee?Container(
                                                                    child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                  ):new Container(
                                                                  )
                                                                ]
                                                            )
                                                        ),
                                                        new Flexible(
                                                            flex: 1,
                                                            fit: FlexFit.tight,
                                                            child:  new Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              crossAxisAlignment: CrossAxisAlignment.end,
                                                              children: <Widget>[
                                                                widget.userAmountData.isPayee? Container(
                                                                  child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                  Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                              ],
                                                            )
                                                        ),
                                                      ],
                                                    )
                                                ):GestureDetector(
                                                  onTap: ShowLogDialog,
                                                  child: new Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      new Flexible(
                                                          flex:1,
                                                          fit:FlexFit.tight,
                                                          child:new Container(width:0,height:0)
                                                      ),
                                                      new Flexible(
                                                          flex:1,
                                                          fit: FlexFit.tight,
                                                          child: Center(
                                                            child: widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                            Text("Rejected",
                                                                maxLines: 1,
                                                                overflow: TextOverflow.clip,
                                                                style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                            Text("Settled",
                                                                maxLines: 1,
                                                                overflow: TextOverflow.clip,
                                                                style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                          )
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ):new Flexible(
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: <Widget>[
                                                  new Flexible(
                                                      flex:1,
                                                      fit: FlexFit.loose,
                                                      child:Container(
                                                          width:  global.nontapitsymbolSize,
                                                          height:  global.nontapitsymbolSize,
                                                          child: !widget.userAmountData.isTapitUser?Image.asset(
                                                              'assets/non_tapit_symbol.png',
                                                              fit: BoxFit.contain
                                                          ):new Container(
                                                              width: 0,
                                                              height:0
                                                          )
                                                      )
                                                  )
                                                ],
                                              ),
                                            )
                                          ]
                                      ),
                                      new Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                            child:widget.isSettledSplit?(global.isPayee?(widget.userAmountData.settlementStatus.toString().compareTo("pending")==0?(widget.userAmountData.balanceAmount<0?Text(
                                              "should get \$"+global.formatter.format((widget.userAmountData.balanceAmount).abs()).toString()+" from "+global.adminName,
                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                            ):Text(
                                              "should pay \$"+global.formatter.format((widget.userAmountData.balanceAmount).abs()).toString()+" to "+global.adminName,
                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                            )):Text(
                                              "have received \$"+global.formatter.format(widget.userAmountData.shareAmount).toString()+" from "+global.adminName,
                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                            )):Text(
                                              "should pay \$"+global.formatter.format(widget.userAmountData.shareAmount).toString()+" to "+global.adminName,
                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                            )):new Container(
                                              child:Text(
                                                "will see details here, once admin requests you payback",
                                                maxLines: 2,
                                                style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    ]
                                )
                            ),
                          ]
                      ),
                    ),
                    widget.isSettledSplit && !global.isPayee && !isUserSettled?
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Flexible(
                            flex:2,
                            fit:FlexFit.loose,
                            child:new Container(
                              margin: EdgeInsets.all(3),
                              width: 30,
                              height: 30,
                            )
                        ),
                        new Flexible(
                            flex:10,
                            fit:FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[

                                Container(
                                  width: MediaQuery.of(context).size.width/3,
                                  decoration: BoxDecoration(
                                    color: global.whitecolor,
                                    border: Border.all(
                                        color: global.rejectButtoncolor,
                                        width: 1.0),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(5.0)),
                                  ),
                                  padding: EdgeInsets.fromLTRB(5,2,5,2),
                                  margin: EdgeInsets.fromLTRB(0,20,10,20),
                                  child: FlatButton(
                                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                    child: Text('REJECT',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                    textColor: global.rejectButtoncolor,
                                    onPressed: () {
                                      rejectionDialog();
                                    },
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width/3,
                                  decoration: BoxDecoration(
                                    color: global.payupButtoncolor,
                                    border: Border.all(
                                        color: global.payupButtoncolor,
                                        width: 1.0),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(5.0)),
                                  ),
                                  padding: EdgeInsets.fromLTRB(5,2,5,2),
                                  margin: EdgeInsets.fromLTRB(20,20,0,20),
                                  child: FlatButton(
                                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                    child: Text('PAY UP',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                    textColor: Colors.white,
                                    onPressed: () {
                                      payupDialog();
                                    },
                                  ),
                                ),

                              ],
                            )
                        )

                      ],
                    ):
                    new Container(
                    )
                  ],
                )
            ),
          )
      ):(
          widget.userAmountData.settlementStatus.compareTo("reject")==0 && widget.index==0 ?
          Expanded(
              child:GestureDetector(
                onTap: ShowLogDialog,
                child:
                Container(
                    decoration: new BoxDecoration(
                      color: global.whitecolor,
                      border: Border(
                        bottom: BorderSide
                          (
                          color: Color(0xffdcdcdc),
                          width: 1.0,
                        ),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(10,10, 10,10),
                    child:
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Flexible(
                                    flex:2,
                                    fit: FlexFit.loose,
                                    child:
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.all(3),
                                          child:global.imageList[0].toString()!=null?(
                                              widget.userAmountData.type.toString().compareTo("true")==0?
                                              CircleAvatar(
                                                backgroundImage: MemoryImage(global.imageList[0]),
                                                backgroundColor: global.imageBackColor,
                                              ):
                                              CircleAvatar(
                                                  child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: global.adminTextcolor),),
                                                  backgroundColor: global.adminBackcolor
                                              )
                                          ):
                                          CircleAvatar(
                                            backgroundImage: AssetImage('assets/dummy_user.png'),
                                            backgroundColor: global.appbargreycolor,
                                          ),
                                        ),
                                        new Positioned(
                                          top:0.0,
                                          right: 0.0,
                                          child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                            height: 15,
                                            width: 15,
                                            child: Image.asset(
                                                'assets/admin_symbol.png',
                                                fit: BoxFit.contain
                                            ),
                                          ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                            height: 15,
                                            width: 15,
                                            child: Image.asset(
                                                'assets/payee_symbol.png',
                                                fit: BoxFit.contain
                                            ),
                                          ):new Container(
                                            width: 0,
                                            height: 0,
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                                Flexible(
                                  flex: 11,
                                  fit: FlexFit.tight,
                                  child:new Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              new Flexible(
                                                  flex: 6,
                                                  fit: FlexFit.tight,
                                                  child: Container(
                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 10),
                                                      child:new Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: <Widget>[
                                                          Expanded(
                                                              child:new Column(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: <Widget>[
                                                                  widget.index==0?Text(
                                                                    "You",
                                                                    style: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                                                                  ):Text(
                                                                    (widget.userAmountData.name==null?widget.userAmountData.cc.toString()+" "+widget.userAmountData.number:widget.userAmountData.name),
                                                                    style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                  ),

                                                                ],
                                                              )
                                                          ),
                                                        ],
                                                      )
                                                  )
                                              ),
                                              widget.isSettledSplit?new Flexible(
                                                  flex:1,
                                                  fit: FlexFit.loose,
                                                  child:Container(
                                                      width:  global.nontapitsymbolSize,
                                                      height:  global.nontapitsymbolSize,
                                                      child: !widget.userAmountData.isTapitUser?Image.asset(
                                                          'assets/non_tapit_symbol.png',
                                                          fit: BoxFit.contain
                                                      ):null
                                                  )
                                              ):new Container(width: 0,height: 0,),

                                              widget.isSettledSplit?new Flexible(
                                                  flex: 6,
                                                  fit: FlexFit.tight,
                                                  child:
                                                  GestureDetector(
                                                    onTap: ShowLogDialog,
                                                    child: new Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: <Widget>[
                                                        new Flexible(
                                                            flex:1,
                                                            fit:FlexFit.tight,
                                                            child:new Container(width:0,height:0)
                                                        ),
                                                        new Flexible(
                                                            flex:1,
                                                            fit: FlexFit.tight,
                                                            child: Center(
                                                              child: widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                              Text("Rejected",
                                                                  maxLines: 1,
                                                                  overflow: TextOverflow.clip,
                                                                  style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                              Text("Settled",
                                                                  maxLines: 1,
                                                                  overflow: TextOverflow.clip,
                                                                  style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                            )
                                                        )
                                                      ],
                                                    ),
                                                  )
                                              ):new Flexible(
                                                child: new Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    new Flexible(
                                                        flex:1,
                                                        fit: FlexFit.loose,
                                                        child:Container(
                                                            width:  global.nontapitsymbolSize,
                                                            height:  global.nontapitsymbolSize,
                                                            child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                'assets/non_tapit_symbol.png',
                                                                fit: BoxFit.contain
                                                            ):new Container(
                                                                width: 0,
                                                                height:0
                                                            )
                                                        )
                                                    )
                                                  ],
                                                ),
                                              )
                                            ]),
                                        new Row(
                                          children: <Widget>[
                                            widget.userAmountData.isPayee && widget.userAmountData.balanceAmount<0?new Container(
                                              child:Text(
                                                "admin is waiting for your action",
                                                style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                              ),
                                            ):new Container(
                                              child:Text(
                                                "waiting for admin's action",
                                                style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                              ),
                                            ),
                                          ],
                                        )
                                      ]
                                  ),
                                ),
                              ]
                          ),
                        ),
                      ],
                    )
                ),
              )
          ):
          Expanded(
              child:GestureDetector(
                onTap: widget.index==0?ShowLogDialog:null,
                child:
                Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      color: global.whitecolor,
                      border: Border(
                        bottom: BorderSide
                          (
                          color: widget.index==0?Color(0xffdcdcdc):Color(0xffffffff),
                          width: 1.0,
                        ),
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Flexible(
                                    flex:2,
                                    fit: FlexFit.loose,
                                    child:
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.all(3),
                                          child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                            backgroundImage: MemoryImage(widget.profileImg),
                                            backgroundColor: global.imageBackColor,
                                          ):(widget.userAmountData.image!=null?CircleAvatar(
                                            child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color:  widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                            backgroundColor: widget.index==0?global.adminBackcolor:global.colors.elementAt(widget.userAmountData.colordata),
                                          ):CircleAvatar(
                                            backgroundImage: AssetImage('assets/dummy_user.png'),
                                            backgroundColor: global.appbargreycolor,
                                          ))):CircleAvatar(
                                            backgroundImage: AssetImage('assets/dummy_user.png'),
                                            backgroundColor: global.appbargreycolor,
                                          ),
                                        ),
                                        new Positioned(
                                          top:0.0,
                                          right: 0.0,
                                          child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                            height: 15,
                                            width: 15,
                                            child: Image.asset(
                                                'assets/admin_symbol.png',
                                                fit: BoxFit.contain
                                            ),
                                          ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                            height: 15,
                                            width: 15,
                                            child: Image.asset(
                                                'assets/payee_symbol.png',
                                                fit: BoxFit.contain
                                            ),
                                          ):new Container(
                                            width: 0,
                                            height: 0,
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                                Flexible(
                                    flex: 11,
                                    fit: FlexFit.tight,
                                    child:new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                new Flexible(
                                                    flex: 6,
                                                    fit: FlexFit.tight,
                                                    child: Container(
                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),

                                                      child: new Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          widget.index==0?Text(
                                                            "You",
                                                            style: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                                                          ):Text(
                                                            (widget.userAmountData.name==null?widget.userAmountData.cc.toString()+" "+widget.userAmountData.number:widget.userAmountData.name),
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,
                                                            softWrap: false,
                                                            style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                          ),

                                                        ],
                                                      ),
                                                    )
                                                ),
                                                widget.isSettledSplit?new Flexible(
                                                    flex:1,
                                                    fit: FlexFit.loose,
                                                    child:Container(
                                                        width:  global.nontapitsymbolSize,
                                                        height:  global.nontapitsymbolSize,
                                                        child: !widget.userAmountData.isTapitUser?Image.asset(
                                                            'assets/non_tapit_symbol.png',
                                                            fit: BoxFit.contain
                                                        ):null
                                                    )
                                                ):new Container(width: 0,height: 0,),

                                                widget.isSettledSplit?new Flexible(
                                                    flex: 6,
                                                    fit: FlexFit.tight,
                                                    child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle") == 0?Container(
                                                        child:new Row(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            new Flexible(
                                                                flex:1,
                                                                fit: FlexFit.tight,
                                                                child:new Column(
                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                                    children: <Widget>[
                                                                      widget.userAmountData.isPayee?Container(
                                                                        child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                      ):new Container(
                                                                          width:0,
                                                                          height:0
                                                                      )
                                                                    ]
                                                                )
                                                            ),
                                                            new Flexible(
                                                                flex: 1,
                                                                fit: FlexFit.tight,
                                                                child:  new Column(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                                  children: <Widget>[
                                                                    widget.userAmountData.isPayee? Container(
                                                                      child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                      Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                    ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                    Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                  ],
                                                                )
                                                            ),
                                                          ],
                                                        )
                                                    ):
                                                    GestureDetector(
                                                      onTap: ShowLogDialog,
                                                      child: new Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: <Widget>[
                                                          new Flexible(
                                                              flex:1,
                                                              fit:FlexFit.tight,
                                                              child:new Container(width:0,height:0)
                                                          ),
                                                          new Flexible(
                                                              flex:1,
                                                              fit: FlexFit.tight,
                                                              child:Center(
                                                                child: widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                                Text("Rejected",
                                                                    maxLines: 1,
                                                                    overflow: TextOverflow.clip,
                                                                    style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                Text("Settled",
                                                                    maxLines: 1,
                                                                    overflow: TextOverflow.clip,
                                                                    style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),

                                                              )
                                                          )
                                                        ],
                                                      ),

                                                    )
                                                ):new Flexible(
                                                  child: new Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      new Flexible(
                                                          flex:1,
                                                          fit: FlexFit.loose,
                                                          child:Container(
                                                              width:  global.nontapitsymbolSize,
                                                              height:  global.nontapitsymbolSize,
                                                              child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                  'assets/non_tapit_symbol.png',
                                                                  fit: BoxFit.contain
                                                              ):new Container(
                                                                width: 0,
                                                                height: 0,
                                                              )
                                                          )
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ]),
                                          new Row(
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                child: widget.index==0?(
                                                    widget.userAmountData.isPayee?(widget.userAmountData.balanceAmount!=0?new Container(
                                                      child: widget.userAmountData.settlementStatus.toString().contains("payee_settle") || widget.userAmountData.settlementStatus.toString().contains("payee_partial_settle")?Text(
                                                        "paid \$"+global.formatter.format(((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount).abs()-widget.userAmountData.balanceAmount.abs()).abs()).toString()+" to "+global.adminName.toString(),
                                                        style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                      ):Text(
                                                        "received \$"+global.formatter.format((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount-widget.userAmountData.balanceAmount.abs()).abs()).toString()+" from "+global.adminName.toString(),
                                                        style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                      ),
                                                    ):new Container(
                                                      child:widget.userAmountData.settlementStatus.toString().contains("payee_settle") || widget.userAmountData.settlementStatus.toString().contains("payee_partial_settle")?Text(
                                                        "paid \$"+global.formatter.format((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount).abs()).toString()+" to "+global.adminName.toString(),
                                                        style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                      ):Text(
                                                        "received \$"+global.formatter.format((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount).abs()).toString()+" from "+global.adminName.toString(),
                                                      ),
                                                    )):(
                                                        widget.userAmountData.balanceAmount!=0?new Container(
                                                          child:
                                                          widget.userAmountData.settlementStatus.toString().contains("payee_settle") || widget.userAmountData.settlementStatus.toString().contains("payee_partial_settle")?Text(
                                                            "paid \$"+global.formatter.format(((widget.userAmountData.payeeAmount).abs()-widget.userAmountData.balanceAmount.abs()).abs()).toString()+" to "+global.adminName.toString(),
                                                            style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                          ):Text(
                                                            "received \$"+global.formatter.format((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount-widget.userAmountData.balanceAmount.abs()).abs()).toString()+" from "+global.adminName.toString(),
                                                            style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                          ),
                                                        ):new Container(
                                                          child:widget.userAmountData.settlementStatus.toString().contains("payee_settle") || widget.userAmountData.settlementStatus.toString().contains("payee_partial_settle")?(widget.userAmountData.isPayee?Text(
                                                            "paid \$"+global.formatter.format((widget.userAmountData.payeeAmount-widget.userAmountData.shareAmount).abs()).toString()+" to "+global.adminName.toString(),
                                                            style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                          ):(
                                                              Text(
                                                                "paid \$"+global.formatter.format((widget.userAmountData.shareAmount).abs()).toString()+" to "+global.adminName.toString(),
                                                                style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                              )
                                                          )):Text(
                                                            "paid \$"+global.formatter.format((widget.userAmountData.shareAmount).abs()).toString()+" to "+global.adminName.toString(),
                                                            style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                          ),
                                                        )
                                                    )):new Container(width: 0,height: 0,),
                                              )
                                            ],
                                          )
                                        ])
                                ),

                              ]
                          ),
                        ),
                        widget.isSettledSplit && (widget.userAmountData.settlementStatus.toString().compareTo("pending")==0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle")==0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle")==0)  && !widget.userAmountData.isPayee && widget.index==0?new Row(
                          children: <Widget>[
                            new Flexible(
                                flex:1,
                                fit:FlexFit.tight,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width/3,
                                      decoration: BoxDecoration(
                                        color: global.whitecolor,
                                        border: Border.all(
                                            color: global.rejectButtoncolor,
                                            width: 1.0),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      padding: EdgeInsets.fromLTRB(5,2,5,2),
                                      margin: EdgeInsets.fromLTRB(20,20,20,20),
                                      child: FlatButton(
                                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                        child: Text('REJECT',style: TextStyle(fontSize: global.font14,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                        textColor: global.rejectButtoncolor,
                                        onPressed: () {
                                          rejectionDialog();
                                        },
                                      ),
                                    ),
                                  ],
                                )
                            ),
                            new Flexible(
                                flex:1,
                                fit:FlexFit.tight,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width/3,
                                      decoration: BoxDecoration(
                                        color: global.payupButtoncolor,
                                        border: Border.all(
                                            color: global.payupButtoncolor,
                                            width: 1.0),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      padding: EdgeInsets.fromLTRB(5,2,5,2),
                                      margin: EdgeInsets.fromLTRB(20,20,0,20),
                                      child: FlatButton(
                                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                        child: Text('PAY UP',style: TextStyle(fontSize: global.font16,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                                        textColor: Colors.white,
                                        onPressed: () {
                                          payupDialog();
                                        },
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ):
                        new Container(
                        )
                      ],
                    )
                ),
              )
          )
      );
  }
}
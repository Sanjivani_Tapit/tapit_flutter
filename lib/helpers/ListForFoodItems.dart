import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/helpers/Fooditems.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/MySeparator.dart';

class ListForFoodItems extends StatefulWidget
{
  const ListForFoodItems({Key key,this.index,this.fooditem}) : super(key: key);
  final Fooditems fooditem;
  final int index;
  ListForFoodItemsState createState()=>ListForFoodItemsState();
}

class ListForFoodItemsState extends State<ListForFoodItems>
{

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Row(
      children: <Widget>[
        Flexible(
          child:Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
            child:Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 5,
                            fit: FlexFit.tight,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(widget.fooditem.name.toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text("x"+widget.fooditem.quantity.toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text("\$"+global.formatter.format(widget.fooditem.rate).toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text("\$"+global.formatter.format(widget.fooditem.amount).toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ],
                            ),
                          )
                        ]
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child:  MySeparator(color: Color(0xff979797)),
                    ),
                  ],
                )
            ),
          ),
        )
      ],
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;

class HomeScreenBottomSheet extends StatefulWidget
{
  HomeScreenBottomSheet({Key key,this.scrollController}) : super(key: key);
  ScrollController scrollController;
  HomeScreenBottomSheetState createState()=> HomeScreenBottomSheetState();
}

class HomeScreenBottomSheetState extends State<HomeScreenBottomSheet>
{
  @override
  void initState()
  {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    // TODO: implement build
    return new Container(
        width: MediaQuery.of(context).size.width,
        color: global.whitecolor,
        child:Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 30.0),
            child: CustomScrollView(
                controller: widget.scrollController,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                slivers: <Widget>[
                  SliverList(
                      delegate: SliverChildListDelegate(
                          [
                            Container(
                                decoration: BoxDecoration(
                                  color: global.whitecolor,
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(0.0),
                                    topRight: Radius.circular(0.0),
                                  ),
                                ),
                                child: GestureDetector(
                                  child: Container(
                                      padding: EdgeInsets.fromLTRB(0,0,0,0),
                                      child:new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                            width:20,
                                            margin: EdgeInsets.fromLTRB(0, 15, 0, 10),
                                            height: 4,
                                            decoration: BoxDecoration(
                                              color: Color(0xffd0d0d0),
                                              border: Border.all(color: Color(0xffd0d0d0), width: 1.0),
                                              borderRadius: BorderRadius.all(Radius.circular(2.0)),
                                            ),
                                          ),
                                          Text("How it works?", style: TextStyle(color:global.blackcolor,fontSize: global.font20,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanMedium'),
                                          ),
                                        ],
                                      )
                                  ),
                                )
                            )
                          ]
                      )
                  ),
                  SliverList(
                      delegate: SliverChildListDelegate(
                          [
                            Container(
                                decoration: BoxDecoration(
                                  color: global.whitecolor,
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(0.0),
                                    topRight: Radius.circular(0.0),
                                  ),
                                ),
                                child:
                                GestureDetector(
                                  child:  Container(
                                      padding: EdgeInsets.fromLTRB(0,0,0,0),
                                      child:new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                            margin: EdgeInsets.fromLTRB(0, 15, 0, 10),
                                            height: MediaQuery.of(context).size.width-100,
                                            decoration: BoxDecoration(
                                              image: new DecorationImage(
                                                fit: BoxFit.cover,
                                                image: AssetImage("assets/gif_image.png",
                                                ),
                                              ),
                                              color: Color(0xffd0d0d0),
                                              border: Border.all(color: Color(0xffd0d0d0), width: 0.0),
                                              borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                            ),
                                          ),
                                        ],
                                      )
                                  ),
                                )
                            )
                          ]
                      )
                  ),
                  SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 15,
                        crossAxisSpacing: 15
                    ),
                    delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return Container(
                            color: global.whitecolor,
                            child: BottomSheetElementCard(bottomSheetElement: bottomSheetElementsList[index]),
                          );
                        },
                        childCount: bottomSheetElementsList.length),
                  ),
                ]
            )
        )
    );
  }
}

class BottomSheetElement
{
  const BottomSheetElement({this.title,this.subtitle, this.imagepath});
  final String title;
  final String subtitle;
  final String imagepath;
}

const List<BottomSheetElement> bottomSheetElementsList = const [
  const BottomSheetElement(title: 'At a store',subtitle:'How to download your receipt', imagepath: 'assets/video1.png'),
  const BottomSheetElement(title: 'Wearable Connect',subtitle:'How to tag yourself through wearable',imagepath: 'assets/video2.png'),
  const BottomSheetElement(title: 'Video-3',subtitle:'How to download your receipt', imagepath: 'assets/video1.png'),
  const BottomSheetElement(title: 'Video-4',subtitle:'How to tag yourself through wearable', imagepath: 'assets/video2.png'),
];



class BottomSheetElementCard extends StatelessWidget {
  const BottomSheetElementCard({Key key, this.bottomSheetElement}) : super(key: key);

  final BottomSheetElement bottomSheetElement;

  @override
  Widget build(BuildContext context) {

    return new Row(
      children: <Widget>[
        Flexible(
            child:Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0,horizontal: 0.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Stack(
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                width: MediaQuery.of(context).size.width,
                                height: 81.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: ExactAssetImage(bottomSheetElement.imagepath)
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                )
                            ),
                            Positioned(
                                bottom: 0.0,
                                right: 0.0,
                                left: 0.0,
                                top:0,
                                child:  new Container(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Image.asset(
                                    "assets/play_button.png",
                                    height: 5,
                                  ),
                                )
                            )
                          ],
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(bottomSheetElement.title, style: TextStyle(fontSize: global.font14,fontWeight: FontWeight.normal,color: Color(0xff1f1f1f), fontFamily: 'BalooChetanMedium')),
                            Opacity(
                              opacity: 0.5,
                              child:  Text(bottomSheetElement.subtitle, style: TextStyle(fontSize: global.font10,fontWeight: FontWeight.normal,color: Color(0xff1f1f1f), fontFamily: 'BalooChetanRegular')),
                            )
                          ],
                        )
                      ]),
                )
            )
        )
      ],
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ExtraBillDetails.dart';

class TaxReceipt extends StatefulWidget
{
  const TaxReceipt({Key key,this.index,this.extraBillDetails}) : super(key: key);
  final ExtraBillDetails extraBillDetails;
  final int index;
  TaxReceiptState createState()=>TaxReceiptState();
}

class TaxReceiptState extends State<TaxReceipt>
{
  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Row(
      children: <Widget>[
        Flexible(
          child:Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0,horizontal: 0.0),
            child:Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 5,
                            fit: FlexFit.tight,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(widget.extraBillDetails.name+"("+widget.extraBillDetails.rate.toString()+"%)", style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ],
                            ),
                          ),
                          Flexible(
                              flex: 1,
                              fit: FlexFit.tight,
                              child: new Container(width: 0,height: 0,)
                          ),
                          Flexible(
                              flex: 2,
                              fit: FlexFit.tight,
                              child: new Container(width: 0,height: 0,)
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                new Container(
                                  child:Text("\$"+global.formatter.format(widget.extraBillDetails.amount).toString(), style: TextStyle(fontSize: global.font13,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),

                                )
                              ],
                            ),
                          )
                        ]
                    ),
                  ],
                )
            ),
          ),
        )
      ],
    );
  }
}
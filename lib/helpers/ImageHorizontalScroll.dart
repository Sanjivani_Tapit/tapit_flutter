import 'dart:typed_data';
import 'package:tapit/helpers/ContactData.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart'as global;

class ImageHorizontalScroll extends StatefulWidget
{
  ImageHorizontalScroll({Key key,this.index,this.contactData,this.profileImg,this.flag,this.isSelected}) : super(key: key);
  ContactData contactData;
  Uint8List profileImg;
  int index;
  ValueChanged<bool> isSelected;
  int flag;

  ImageHorizontalScrollState createState()=>ImageHorizontalScrollState();
}

class ImageHorizontalScrollState extends State<ImageHorizontalScroll>
{

  String LOGTAG="ImageHorizontalScroll";
  bool isSelected = false;

  @override
  void initState(){
    super.initState();
  }

  void imageClicked()
  {
    setState(() {
      isSelected = true;
      widget.isSelected(isSelected);
    });
  }

  @override

  Widget build(BuildContext context)
  {
    return SizedBox(
      height: widget.flag==0?35:35,
      width: widget.flag==0?35:35,
      child:Padding( padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 3.0),
          child:Container(
            width: widget.flag==0?35:35,
            color: global.transparent,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Flexible(
                    child: new Container(
                      width: widget.flag==0?32:30,
                      height: widget.flag==0?32:30,
                      child:GestureDetector(
                        onTap: () {
                          imageClicked();
                        },
                        child:widget.profileImg!=null?((widget.contactData.type.toString().contains("true"))?CircleAvatar(
                          backgroundImage: MemoryImage(widget.profileImg),
                          backgroundColor: global.imageBackColor,
                        ):(widget.contactData.image!=null?CircleAvatar(
                          child: Text(widget.contactData.image,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                          backgroundColor: widget.contactData.colordata!=null?global.colors.elementAt(widget.contactData.colordata):global.colors.elementAt(3),
                        ):CircleAvatar(
                          backgroundImage: AssetImage('assets/dummy_user.png'),
                          backgroundColor: global.appbargreycolor,
                        ))):CircleAvatar(
                          backgroundImage: AssetImage('assets/dummy_user.png'),
                          backgroundColor: global.appbargreycolor,
                        ),
                      ),
                    ),
                  ),
                ]
            ),
          )
      ),
    );
  }
}
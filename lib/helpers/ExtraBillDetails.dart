class ExtraBillDetails {
  String name;
  int rate;
  double amount;
  ExtraBillDetails(this.name, this.rate,this.amount);

  Map toJson() => {
    'name': name,
    'rate': rate,
    'amount': amount,
  };
}
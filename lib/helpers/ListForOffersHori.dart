import 'package:flutter/material.dart';
import 'package:tapit/global.dart'as global;

class ListForOffersHori extends StatefulWidget
{
  const ListForOffersHori({Key key}) : super(key: key);
  ListForOffersHoriState createState()=>ListForOffersHoriState();
}

class ListForOffersHoriState extends State<ListForOffersHori>
{
  bool isSelected = false;
  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return SizedBox(
      height: 250,
      width: MediaQuery.of(context).size.width-15,
      child:Padding(
          padding: EdgeInsets.fromLTRB(10,10,5,10),
          child:Container(
            decoration: new BoxDecoration(
              color: global.mainColor,
              image:  DecorationImage(
                image:  AssetImage('assets/Onb-2.png'),
                fit: BoxFit.fill,
                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
              ),
              border: Border.all(
                color: global.whitecolor,
                width: 1.0,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: new Container(
                    padding: EdgeInsets.fromLTRB(10,0,0,10),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Sebu Mexican Joint", style: TextStyle(fontSize: global.font17,color: global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold')),
                        Text("in Restaurents", style: TextStyle(fontSize: global.font14,color: global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                      ],
                    ),
                  )
                ),
              ],
            ),
          )
      ),
    );
  }
}
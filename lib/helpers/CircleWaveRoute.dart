import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:tapit/global.dart' as global;

class CircleWaveRoute extends StatefulWidget {

  CircleWaveRoute({Key key,this.waveRadius}) : super(key: key);
  final double waveRadius;
  @override
  _CircleWaveRouteState createState() => _CircleWaveRouteState();
}

class _CircleWaveRouteState extends State<CircleWaveRoute>
    with TickerProviderStateMixin {
  double waveRadius = 0;
  double waveGap =30.0;
  Animation<double> _animation;
  AnimationController controller;


  int count=0;
  Color animationColor=global.rippleColor;
  Animation<Color> colorAnimation;
  AnimationController colorController;


  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);

    global.controller=controller;
    controller.forward();

    controller.addStatusListener((status) {

      if (status == AnimationStatus.completed)
      {
        if(!global.changeColor)
        {
          controller.reset();
        }
        else
        {
          if(count==0){
            secondAnimation();
          }

        }
      }
      else if (status == AnimationStatus.dismissed)
      {
        if(!global.changeColor)
        {
          controller.forward();
        }
        else
        {
          setState(() {
            global.changeColor=true;
          });
          if(count==0){
            secondAnimation();
          }
        }
      }
    });
  }


  void secondAnimation()
  {
    setState(() {
      global.secondAnimationCalled=true;
    });

    colorController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));
    colorAnimation =
    ColorTween(begin: global.rippleColor, end: global.rippleColor2).animate(
        colorController)
      ..addListener(() {
        setState(() {
          animationColor = colorAnimation.value;
        });
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          if (count < 1) {
            count++;
            colorController.reset();
          }
          else
          {
            animationColor = global.rippleColor2;
          }
        }
        else if (status == AnimationStatus.dismissed) {
          if (count < 1)
          {
            count++;
            colorController.forward();
          }
          else
          {
            animationColor = global.rippleColor2;
          }
        }
      });

    colorController.forward();
  }

  @override
  Widget build(BuildContext context) {


    _animation = Tween(begin: 0.0, end: waveGap).animate(controller)
      ..addListener(() {
        setState(() {
          waveRadius = _animation.value;
        });
      });

    return Scaffold(
      backgroundColor: global.browncolor,
      body: CustomPaint(
        size: Size(double.infinity, double.infinity),
        painter: CircleWavePainter(waveRadius,animationColor),
      ),
    );
  }

  @override
  void dispose() {
    if(controller!=null) {
      controller.dispose();
    }
    if(colorController!=null) {
      colorController.dispose();
    }
    super.dispose();
  }
}

class CircleWavePainter extends CustomPainter {
  final double waveRadius;
  final Color animationColor;
  var wavePaint;
  CircleWavePainter(this.waveRadius,this.animationColor) {
    wavePaint = Paint()
      ..color = global.rippleColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.5
      ..isAntiAlias = true;
  }
  @override
  void paint(Canvas canvas, Size size) {

    double centerX = size.width / 2.0;
    double centerY = size.height / 2.0;
    double maxRadius = hypot(centerX, centerY)-(size.height / 4.5);
    if(!global.changeColor)
    {
      bool firstRadius=false;
      global.lastWaveRadius=global.aniStartRadius+waveRadius;
      var currentRadius = waveRadius;
      double ini_opacity = 0;
      while (currentRadius < maxRadius) {

        if(!firstRadius)
        {
          firstRadius=true;
          currentRadius=global.aniStartRadius+waveRadius;
        }
        double opacity = 0.5-ini_opacity;
        final Color _color = global.rippleColor.withOpacity(opacity);
        wavePaint = Paint()
          ..color = _color
          ..style = PaintingStyle.stroke
          ..strokeWidth = 1.5
          ..isAntiAlias = true;

        canvas.drawCircle(Offset(centerX, centerY), currentRadius, wavePaint);
        currentRadius += 30.0;

        if(ini_opacity<0.75)
        {
          if(currentRadius>(maxRadius*(0.8)))
          {
            ini_opacity += 0.2;
          }
          else
          {
            ini_opacity -= 0.15;
          }
        }
        else
        {
          ini_opacity-=0.15;
          if(ini_opacity<0.3)
          {
            ini_opacity=0.3;
          }
        }
      }
    }
    else
    {
      double currentRadius = global.lastWaveRadius;
      while (currentRadius < maxRadius) {
        wavePaint = Paint()
          ..color = animationColor
          ..style = PaintingStyle.stroke
          ..strokeWidth = 1.5
          ..isAntiAlias = true;

        canvas.drawCircle(Offset(centerX, centerY), currentRadius, wavePaint);
        currentRadius += 30.0;
      }

    }
  }

  @override
  bool shouldRepaint(CircleWavePainter oldDelegate) {
    if(!global.changeColor)
    {
      return oldDelegate.waveRadius != waveRadius;
    }
    else
    {
      return oldDelegate.animationColor != animationColor;
    }
  }

  double hypot(double x, double y) {
    return math.sqrt(x * x + y * y);
  }
}
import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';

class GenderRadio extends StatefulWidget{

  GenderRadio({Key key,this.isSel}) : super(key: key);
  final ValueChanged<String> isSel;
  RGenderRadioState createState()=>RGenderRadioState();
}

class RGenderRadioState extends State<GenderRadio>
{
  int _radioValue=-1;
  void _handleRadioValueChange(int value) {

    setState(() {
      _radioValue = value;
      switch (_radioValue) {
        case 0:
          setState(() {
            widget.isSel("Male");
          });
          break;
        case 1:
          setState(() {
            widget.isSel("Female");
          });
          break;
        case 2:
          setState(() {
            widget.isSel("Others");
          });
          break;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  height: 24.0,
                  width: 24.0,
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                  child: new Radio(
                    value: 0,
                    activeColor: global.mainColor,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                new Text("Male", style: TextStyle(fontSize: global.font16,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
              ],
            ),
            SizedBox(height: 10,),
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                  height:24,
                  width: 24,
                  child: new Radio(
                    value: 1,
                    activeColor: global.mainColor,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                new Text("Female", style: TextStyle(fontSize: global.font16,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
              ],
            ),
            SizedBox(height: 10,),
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                  height:24,
                  width: 24,
                  child: new Radio(
                    value:2,
                    activeColor: global.mainColor,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                new Text("Others", style: TextStyle(fontSize: global.font16,color:global.darkgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
              ],
            ),
          ],
        )
    );
  }
}


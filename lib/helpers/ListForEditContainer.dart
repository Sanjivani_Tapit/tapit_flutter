import 'dart:async';
import 'dart:typed_data';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:tapit/helpers/LogData.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ModeOfPayment.dart';
import 'package:tapit/helpers/RejectionRadio.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/helpers/LogListViewDialog.dart';
import 'package:tapit/helpers/TextFieldForDialog.dart';

class ListForEditContainer extends StatefulWidget
{
  const ListForEditContainer({Key key,this.index, this.userAmountData,this.profileImg,this.isSettledSplit,this.isSel}) : super(key: key);
  final UserAmountData userAmountData;
  final Uint8List profileImg;
  final int index;
  final bool isSettledSplit;
  final ValueChanged<bool> isSel;

  ListForEditContainerState createState()=>ListForEditContainerState();
}

class ListForEditContainerState extends State<ListForEditContainer>
{

  final textController = TextEditingController();

  bool isSel= false;
  bool isRemainderSent=false;
  String subtext="(Me)";
  String comment="";
  int modeOfPayment=0;
  double myAmount=0;
  String settleAmount;
  double loginWidth = 0.0;
  SlidableController slidableController;
  List<LogData> finalLogList=new List();
  final String LOGTAG="ListForEditContainer";
  bool isSlideOpen=false;

  @override
  void initState(){
    super.initState();

    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );


    setState(()
    {
      if(widget.index==0)
      {
        subtext="(Me)";
      }
      else{
        subtext="";
      }
    });
  }

  void handleSlideIsOpenChanged(bool isOpen)
  {
    if(isOpen)
    {
      if(!isSlideOpen)
      {
        setState(()
        {
          isSlideOpen = true;
        });
      }
    }
  }

  void handleSlideAnimationChanged(Animation<double> isOpen)
  {
    if(isOpen==null)
    {
      setState(()
      {
        isSlideOpen = false;
      });
    }
    else if(isOpen!=null && isOpen.value!=0)
    {
      if(!isSlideOpen)
      {
        setState(()
        {
          isSlideOpen = true;
        });
      }
    }

  }

  void ShowLogDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(10.0)),
              child: Container(
                  height: 400,
                  padding:  EdgeInsets.fromLTRB(0,8, 0, 10),
                  child: new Column(
                    children: <Widget>[
                      Flexible(
                          flex:1,
                          fit:FlexFit.tight,
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  width: 1.0,
                                ),
                              ),
                            ),
                            child: new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        padding:  EdgeInsets.fromLTRB(18,0, 0, 0),
                                        child:new Text("Action Logs", style: TextStyle(fontSize:  global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                      )
                                    ],
                                  ),
                                ),
                                Flexible(
                                    flex:1,
                                    child:new Container(
                                      padding:  EdgeInsets.fromLTRB(0,0, 18, 0),
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                              width: 20,
                                              height: 20,
                                              child: GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).pop();
                                                },
                                                child: Image.asset(
                                                  'assets/black_cross_icon.png',
                                                  fit: BoxFit.contain,
                                                  color:Color.fromRGBO(0, 0, 0, 0.3),
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          )
                      ),
                      Flexible(
                        flex:6,
                        fit:FlexFit.tight,
                        child: LogListViewDialog(userID:widget.userAmountData.userID,receiptID:global.receiptID),
                      )
                    ],
                  )
              )
          );
        }
    );
  }


  Future<void> _showSnackBar(BuildContext context, String text,String userType) async
  {
    if(text.toString().compareTo("REMIND")==0)
    {
      bool flag =await global.helperClass.sendRemainder(global.receiptID, global.adminUserID, widget.userAmountData.userID, global.hotelname,"admin");
      if(flag)
      {
        if(widget.userAmountData.name==null || widget.userAmountData.name.isEmpty)
        {
          showInfoFlushbarHelper(context, "Remainder sent to " + widget.userAmountData.number.toString());
        }
        else
        {
          showInfoFlushbarHelper(context, "Remainder sent to " + widget.userAmountData.name.toString());
        }
        setState(() {
          isRemainderSent=true;
        });

        Timer(Duration(seconds: 2), () {
          setState(() {
            isRemainderSent=false;
          });
        });
      }
      else
      {
        showInfoFlushbarHelper(context, "Remainder is already sent to " + widget.userAmountData.name.toString()+".  Please wait for 24hrs.");
      }
    }
    else if(text.toString().compareTo("PAY UP")==0)
    {
      payupDialog(userType);
    }
    else if(text.toString().compareTo("REJECT")==0)
    {
      rejectionDialog();
    }
    else if(text.toString().compareTo("SETTLE")==0)
    {
      settleForDialog(userType);
    }
  }


  void settleForDialog(String userType)
  {
    setState(() {
      settleAmount= double.parse((widget.userAmountData.balanceAmount).toStringAsFixed(2)).toString();
    });

    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          modeOfPayment=3;
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                  child:SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        new Container(
                            padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                            child:new Column(
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Flexible(
                                      flex:1,
                                      child:new Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text("Received from", style: TextStyle(fontSize:  global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex:1,
                                      child:new Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                              width: 20,
                                              height: 20,
                                              child: GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).pop();
                                                },
                                                child: Image.asset(
                                                  'assets/black_cross_icon.png',
                                                  fit: BoxFit.contain,
                                                  color:Color.fromRGBO(0, 0, 0, 0.3),
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 20),
                                new Row(
                                  children: <Widget>[
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                          width: 48.0,
                                          height: 48.0,
                                          child:
                                          widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                            backgroundImage: MemoryImage(widget.profileImg),
                                            backgroundColor: global.imageBackColor,
                                          ):CircleAvatar(
                                            child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                            backgroundColor: global.colors.elementAt(widget.userAmountData.colordata),
                                          )):
                                          CircleAvatar(
                                            backgroundImage: AssetImage('assets/dummy_user.png'),
                                            backgroundColor: global.appbargreycolor,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        widget.userAmountData.name!=null?new Text(widget.userAmountData.name, style: TextStyle(fontSize: global.font16,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'))
                                            :new Text(widget.userAmountData.number.toString(), style: TextStyle(fontSize: global.font16,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                        new Text("("+userType+")", style: TextStyle(fontSize:  global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(height:20),
                                TextFieldForDialog(initialVal:widget.userAmountData.balanceAmount,isSel:(double value){
                                  setState(() {
                                    settleAmount=double.parse((value).toStringAsFixed(2)).toString();
                                  });
                                }),
                                SizedBox(height: 23,),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                        padding: EdgeInsets.fromLTRB(2, 0, 0, 0),
                                        child:   Opacity(
                                          opacity: 0.9,
                                          child:new Text("I received payment by", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                        )
                                    )
                                  ],
                                ),
                                SizedBox(height: 15,),
                                ModeOfPayment(dailogType:1,isSel: (int value)  {
                                  setState(() {
                                    modeOfPayment=value;
                                  });
                                },),
                              ],
                            )
                        ),
                        SizedBox(height: 35,),
                        new Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xffdcdcdc),
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                        new Row(
                          children: <Widget>[
                            new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    FlatButton(
                                      child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                      onPressed: (){
                                        Navigator.of(context).pop();
                                      },
                                    )
                                  ],
                                )
                            ),
                            new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child:new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    FlatButton(
                                      child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                      onPressed: ()async
                                      {
                                        bool flag=await global.helperClass.SettleAPI(global.receiptID,global.UserID,widget.userAmountData.userID,settleAmount,modeOfPayment,"admin");
                                        if(flag)
                                        {
                                          setState(()
                                          {
                                            widget.userAmountData.settlementStatus = "settle";
                                          });
                                        }
                                        Navigator.of(context).pop();
                                        widget.isSel(true);
                                      },
                                    )
                                  ],
                                )
                            )
                          ],
                        ),
                      ],
                    ),
                  )
              ),
            ),
          );
        }
    );
  }


  void rejectionDialog()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(10.0)
            ),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child:new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Reject request", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                          width: 20,
                                          height: 20,
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Image.asset(
                                              'assets/black_cross_icon.png',
                                              fit: BoxFit.contain,
                                              color:Color.fromRGBO(0, 0, 0, 0.3),
                                            ),
                                          )
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            new Column(
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("I can't accept this request because", style: TextStyle(fontSize: global.font17,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(height:17),
                            RejectionRadio(isSel: (int value){
                              if(value==0)
                              {
                                setState(()
                                {
                                  comment="I want adjustment in my share";
                                });
                              }
                              else
                              {
                                setState(()
                                {
                                  comment="I have already paid in other mode";
                                });
                              }
                            },)
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async
                                  {
                                    if(comment!=null && comment.isNotEmpty)
                                    {
                                      bool flag = await global.helperClass.rejectAPI(global.receiptID,global.UserID,widget.userAmountData.userID,comment,"admin");
                                      if (flag)
                                      {
                                        setState(()
                                        {
                                          widget.userAmountData.settlementStatus = "reject";
                                        });
                                      }
                                      Navigator.of(context).pop();
                                      widget.isSel(true);
                                    }
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
    );
  }

  void showInfoFlushbarHelper(BuildContext context,String text)
  {
    Flushbar(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(10),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [global.darkgreycolor, global.darkgreycolor],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      duration: Duration(seconds: 2),

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      message:text,
    )..show(context);
  }


  void payupDialog(String userType)
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          modeOfPayment=0;
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(18, 0, 18, 0),
                        child:new Column(
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Pay to", style: TextStyle(fontSize: global.font20,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                          width: 20,
                                          height: 20,
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.of(context).pop();
                                            },
                                            child: Image.asset(
                                              'assets/black_cross_icon.png',
                                              fit: BoxFit.contain,
                                              color:Color.fromRGBO(0, 0, 0, 0.3),
                                            ),
                                          )
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 20),
                            new Stack(
                              children: <Widget>[
                                new Container(
                                  width: 50.0,
                                  height: 50.0,
                                  child:
                                  widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                    backgroundImage: MemoryImage(widget.profileImg),
                                    backgroundColor: global.imageBackColor,
                                  ):(widget.userAmountData.image!=null?CircleAvatar(
                                    child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                    backgroundColor: global.colors.elementAt(widget.userAmountData.colordata),
                                  ):CircleAvatar(
                                    backgroundImage: AssetImage('assets/dummy_user.png'),
                                    backgroundColor: global.appbargreycolor,
                                  ))):
                                  CircleAvatar(
                                    backgroundImage: AssetImage('assets/dummy_user.png'),
                                    backgroundColor: global.appbargreycolor,
                                  ),
                                ),
                                new Positioned(
                                  top:0.0,
                                  right: 0.0,
                                  child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                    height: 15,
                                    width: 15,
                                    child: Image.asset(
                                        'assets/admin_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                    height: 15,
                                    width: 15,
                                    child: Image.asset(
                                        'assets/payee_symbol.png',
                                        fit: BoxFit.contain
                                    ),
                                  ):new Container(
                                    width: 0,
                                    height: 0,
                                  ),
                                ),
                              ],
                            ),
                            widget.userAmountData.name!=null?new Text(widget.userAmountData.name, style: TextStyle(fontSize: global.font18,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Text(widget.userAmountData.number, style: TextStyle(fontSize: global.font18,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            new Text("("+userType+")", style: TextStyle(fontSize: global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height:17),
                            new Text("an amount of", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height: 2,),
                            new Text("\$ "+global.formatter.format((widget.userAmountData.balanceAmount).abs()).toString(), style: TextStyle(fontSize:  global.font26,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            SizedBox(height: 20,),
                            new Text("Mode of Payment", style: TextStyle(fontSize:  global.font15,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            SizedBox(height: 10,),
                            ModeOfPayment(dailogType:0,isSel: (int value)
                            {
                              setState(() {
                                modeOfPayment=value;
                              });
                            },
                            ),
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font18,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async
                                  {
                                    bool flag=true;
                                    double amount=(widget.userAmountData.balanceAmount).abs();
                                    amount=double.parse((amount).toStringAsFixed(2));
                                    String amt=amount.toString();
                                    flag=await global.helperClass.payupAPI(global.receiptID,global.UserID,widget.userAmountData.userID,amt,modeOfPayment,"admin");
                                    if(flag)
                                    {
                                      setState(()
                                      {
                                        widget.userAmountData.settlementStatus="settle";
                                      });
                                    }

                                    Navigator.of(context).pop();
                                    widget.isSel(true);
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {

    return
      widget.userAmountData.usertype.toString().compareTo("member")==0 && (widget.userAmountData.settlementStatus.toString().compareTo("settle")!=0 && widget.userAmountData.settlementStatus.toString().compareTo("admin_settle")!=0 && widget.userAmountData.settlementStatus.toString().compareTo("payee_settle")!=0) && global.isSettled?
      new Stack(
        children: <Widget>[
          GestureDetector(
              onTap:(){
                ShowLogDialog();
              },
              child: new Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 0.75,
                    decoration:isSlideOpen?BoxDecoration(
                      color:Color(0xfffcfcfc),
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffE1E1E1),
                          width: 0.75,
                        ),
                        top: BorderSide(
                          color: Color(0xffE1E1E1),
                          width: 0.75,
                        ),
                      ),
                    ):BoxDecoration(
                      color:Color(0xffffffff),
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffffffff),
                          width: 0.75,
                        ),
                        top: BorderSide(
                          color: Color(0xffffffff),
                          width: 0.75,
                        ),
                      ),
                    ),
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Slidable(
                        key: ValueKey(widget.index),
                        controller: slidableController,
                        actionPane: SlidableDrawerActionPane(),
                        actions: <Widget>[
                          IconSlideAction(
                            foregroundColor: global.whitecolor,
                            color: global.mainColor,
                            iconWidget: new Text("REMIND",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                            onTap: () {
                              _showSnackBar(context, 'REMIND','member');
                            },
                          ),
                        ],
                        secondaryActions: <Widget>[
                          IconSlideAction(
                              foregroundColor: global.whitecolor,
                              color: global.settledTextcolor,
                              iconWidget: new Text("SETTLE",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                              onTap: () => _showSnackBar(context, 'SETTLE','member')
                          ),
                        ],
                        child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                  child:new Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                                        child:Container(
                                            width: MediaQuery.of(context).size.width,
                                            color:isSlideOpen?Color(0xfffcfcfc):Color(0xffffffff),
                                            padding: EdgeInsets.fromLTRB(10,5, 10, 5),
                                            child: Center(
                                              child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    new Flexible(
                                                        flex:2,
                                                        fit: FlexFit.loose,
                                                        child:
                                                        new Stack(
                                                          children: <Widget>[
                                                            new Container(
                                                              margin: EdgeInsets.all(3),
                                                              child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                                                backgroundImage: MemoryImage(widget.profileImg),
                                                                backgroundColor: global.imageBackColor,
                                                              ):(widget.userAmountData.image!=null?
                                                              CircleAvatar(
                                                                child: Text(widget.userAmountData.image,style: TextStyle(color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                                                backgroundColor: widget.index==0?global.adminBackcolor:global.colors.elementAt(widget.userAmountData.colordata),
                                                              ) :CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ))):CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ),
                                                            ),
                                                            new Positioned(
                                                              top:0.0,
                                                              right: 0.0,
                                                              child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/admin_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/payee_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):new Container(
                                                                width: 0,
                                                                height: 0,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                    ),
                                                    Flexible(
                                                        flex: 11,
                                                        fit: FlexFit.tight,
                                                        child:new Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: <Widget>[
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: Container(
                                                                            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                                            child:new Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                widget.userAmountData.isPayee?widget.userAmountData.name!=null?
                                                                                new Text(
                                                                                  widget.userAmountData.name.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.font15,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):
                                                                                widget.userAmountData.name!=null?
                                                                                Text(
                                                                                  widget.userAmountData.name.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+ widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 20,
                                                                                ),
                                                                              ],
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex:1,
                                                                        fit: FlexFit.loose,
                                                                        child:Container(
                                                                            width: global.nontapitsymbolSize,
                                                                            height:  global.nontapitsymbolSize,
                                                                            child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                                'assets/non_tapit_symbol.png',
                                                                                fit: BoxFit.contain
                                                                            ):new Container(
                                                                              width: 0,
                                                                              height: 0,
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle") == 0?Container(
                                                                            child:new Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                new Flexible(
                                                                                    flex:1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:new Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                                                        children: <Widget>[
                                                                                          widget.userAmountData.isPayee?Container(
                                                                                            child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                          ):new Container(
                                                                                          )
                                                                                        ]
                                                                                    )
                                                                                ),
                                                                                new Flexible(
                                                                                    flex: 1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:  new Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        widget.userAmountData.isPayee? Container(
                                                                                          child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                          Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                        ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                        Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),

                                                                                      ],
                                                                                    )
                                                                                ),
                                                                              ],
                                                                            )
                                                                        ): GestureDetector(
                                                                          onTap: ShowLogDialog,
                                                                          child: new Row(
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            children: <Widget>[
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit:FlexFit.tight,
                                                                                  child:new Container(width:0,height:0)
                                                                              ),
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit: FlexFit.tight,
                                                                                  child:  Center(
                                                                                    child: widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                                                    Text("Rejected",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                    Text("Settled",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                  )
                                                                              )
                                                                            ],
                                                                          ),
                                                                        )
                                                                    )
                                                                  ]
                                                              )
                                                            ]
                                                        )
                                                    ),
                                                  ]
                                              ),
                                            )
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                            ]
                        )
                    ),
                  ),
                  Container(
                    height: 0.75,
                    width: MediaQuery.of(context).size.width,
                    decoration:isSlideOpen?BoxDecoration(
                      color:Color(0xfffcfcfc),
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffE1E1E1),
                          width: 0.75,
                        ),
                        top: BorderSide(
                          color: Color(0xffE1E1E1),
                          width: 0.75,
                        ),
                      ),
                    ):BoxDecoration(
                      color:Color(0xffffffff),
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffffffff),
                          width: 0.75,
                        ),
                        top: BorderSide(
                          color: Color(0xffffffff),
                          width: 0.75,
                        ),
                      ),
                    ),
                  ),
                ],
              )
          ),
          isRemainderSent?
          new Container(
            width:MediaQuery.of(context).size.width,
            height: 60.0,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: global.remainderButtonGradient,
                  tileMode: TileMode.clamp
              ),
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.notifications_none,color: global.whitecolor,),
                SizedBox(width: 0,),
                new Text("REMAINDER SENT",style: TextStyle(fontSize: global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
              ],
            ),
          )
              :new Container(width: 0,height: 0,)
        ],
      ) :(widget.userAmountData.usertype.compareTo("payee")==0 && (widget.userAmountData.settlementStatus.toString().compareTo("settle")!=0 && widget.userAmountData.settlementStatus.toString().compareTo("admin_settle")!=0 && widget.userAmountData.settlementStatus.toString().compareTo("payee_settle")!=0) && global.isSettled?
      (widget.userAmountData.settlementStatus.toString().compareTo("reject")!=0?
      GestureDetector(
          onTap: ShowLogDialog,
          child:
          new Column(
              children: <Widget>[

                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:isSlideOpen?BoxDecoration(
                    color:Color(0xfffcfcfc),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                    ),
                  ):BoxDecoration(
                    color:Color(0xffffffff),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                    ),
                  ),
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    child:
                    Slidable(
                        key: ValueKey(widget.index),
                        controller: slidableController,
                        actionPane: SlidableDrawerActionPane(),
                        actions: <Widget>[

                          widget.userAmountData.balanceAmount<0?IconSlideAction(
                            foregroundColor: global.whitecolor,
                            color: global.rejectedTextcolor,
                            iconWidget: new Text("REJECT",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                            onTap: () => _showSnackBar(context, 'REJECT','payee'),
                          ):
                          IconSlideAction(
                            foregroundColor: global.whitecolor,
                            color: global.mainColor,
                            iconWidget: new Text("REMIND",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                            onTap: () => _showSnackBar(context, 'REMIND','member'),
                          )
                        ],
                        secondaryActions: <Widget>[
                          widget.userAmountData.balanceAmount<0?IconSlideAction(

                              foregroundColor: global.whitecolor,
                              color: global.mainColor,
                              iconWidget: new Text("PAY UP",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                              onTap: () => _showSnackBar(context, 'PAY UP','payee')
                          ):IconSlideAction(
                              foregroundColor: global.whitecolor,
                              color: global.settledTextcolor,
                              iconWidget: new Text("SETTLE",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                              onTap: () => _showSnackBar(context, 'SETTLE','payee')
                          ),
                        ],
                        child:
                        new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                  child:new Column(
                                    children: <Widget>[
                                      Padding( padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                                        child:Container(
                                            width: MediaQuery.of(context).size.width,
                                            color:isSlideOpen?Color(0xfffcfcfc):Color(0xffffffff),
                                            padding: EdgeInsets.fromLTRB(10,5, 10, 5),
                                            child: Center(
                                              child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    new Flexible(
                                                        flex:2,
                                                        fit: FlexFit.loose,
                                                        child:
                                                        new Stack(
                                                          children: <Widget>[
                                                            new Container(
                                                              margin: EdgeInsets.all(3),
                                                              child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                                                backgroundImage: MemoryImage(widget.profileImg),
                                                                backgroundColor: global.imageBackColor,
                                                              ):(widget.userAmountData.image!=null?
                                                              CircleAvatar(
                                                                child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                                                backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                                                              ) :CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ))):CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ),
                                                            ),
                                                            new Positioned(
                                                              top:0.0,
                                                              right: 0.0,
                                                              child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/admin_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/payee_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):new Container(
                                                                width: 0,
                                                                height: 0,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                    ),
                                                    Flexible(
                                                        flex: 11,
                                                        fit: FlexFit.tight,
                                                        child:new Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: <Widget>[
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: Container(
                                                                            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                                            child:new Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                widget.userAmountData.isPayee?widget.userAmountData.name!=null?
                                                                                Text(
                                                                                  widget.userAmountData.name+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):
                                                                                widget.userAmountData.name!=null?
                                                                                Text(
                                                                                  widget.userAmountData.name+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 20,
                                                                                ),
                                                                              ],
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex:1,
                                                                        fit: FlexFit.loose,
                                                                        child:Container(

                                                                            width:  global.nontapitsymbolSize,
                                                                            height:  global.nontapitsymbolSize,
                                                                            child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                                'assets/non_tapit_symbol.png',
                                                                                fit: BoxFit.contain
                                                                            ):new Container(
                                                                              width: 0,
                                                                              height: 0,
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle") == 0?Container(
                                                                            child:new Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                new Flexible(
                                                                                    flex:1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:new Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        widget.userAmountData.isPayee?Container(
                                                                                          child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                        ):new Container(

                                                                                        )
                                                                                      ],
                                                                                    )
                                                                                ),
                                                                                new Flexible(
                                                                                    flex: 1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:  new Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        widget.userAmountData.isPayee? Container(
                                                                                          child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                          Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                        ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                        Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),

                                                                                      ],
                                                                                    )
                                                                                ),
                                                                              ],
                                                                            )
                                                                        ): GestureDetector(
                                                                          onTap: ShowLogDialog,
                                                                          child: new Row(
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            children: <Widget>[
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit:FlexFit.tight,
                                                                                  child:new Container(width:0,height:0)
                                                                              ),
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit: FlexFit.tight,
                                                                                  child:Center(
                                                                                    child:widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                                                    Text("Rejected",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                    Text("Settled",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                  )
                                                                              )
                                                                            ],
                                                                          ),
                                                                        )
                                                                    )
                                                                  ]
                                                              )
                                                            ]
                                                        )
                                                    ),
                                                  ]
                                              ),
                                            )
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                            ]
                        )
                    )
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:isSlideOpen?BoxDecoration(
                    color:Color(0xfffcfcfc),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                    ),
                  ):BoxDecoration(
                    color:Color(0xffffffff),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                    ),
                  ),
                ),
              ]
          )
      ):
      GestureDetector(
          onTap: ShowLogDialog,
          child:
          new Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:isSlideOpen?BoxDecoration(
                    color:Color(0xfffcfcfc),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                    ),
                  ):BoxDecoration(
                    color:Color(0xffffffff),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                    ),
                  ),
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    child:
                    Slidable(
                        key: ValueKey(widget.index),
                        controller: slidableController,
                        actionPane: SlidableDrawerActionPane(),
                        actions: <Widget>[
                        ],
                        secondaryActions: <Widget>[
                          widget.userAmountData.balanceAmount<0?IconSlideAction(
                              foregroundColor: global.whitecolor,
                              color: global.mainColor,
                              iconWidget: new Text("PAY UP",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                              onTap: () => _showSnackBar(context, 'PAY UP','payee')
                          ):IconSlideAction(
                              foregroundColor: global.whitecolor,
                              color: global.settledTextcolor,
                              iconWidget: new Text("SETTLE",style: TextStyle(fontSize:  global.font16,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanSemiBold'),),
                              onTap: () => _showSnackBar(context, 'SETTLE','payee')
                          ),
                        ],
                        child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                  child:new Column(
                                    children: <Widget>[
                                      Padding( padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                                        child:Container(
                                            width: MediaQuery.of(context).size.width,
                                            color:isSlideOpen?Color(0xfffcfcfc):Color(0xffffffff),
                                            padding: EdgeInsets.fromLTRB(10,5, 10, 5),
                                            child: Center(
                                              child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    new Flexible(
                                                        flex:2,
                                                        fit: FlexFit.loose,
                                                        child: new Stack(
                                                          children: <Widget>[
                                                            new Container(
                                                              margin: EdgeInsets.all(3),
                                                              child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                                                backgroundImage: MemoryImage(widget.profileImg),
                                                                backgroundColor: global.imageBackColor,
                                                              ):(widget.userAmountData.image!=null?
                                                              CircleAvatar(
                                                                child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                                                backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                                                              ) :CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ))):CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ),
                                                            ),
                                                            new Positioned(
                                                              top:0.0,
                                                              right: 0.0,
                                                              child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/admin_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/payee_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):new Container(
                                                                width: 0,
                                                                height: 0,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                    ),
                                                    Flexible(
                                                        flex: 11,
                                                        fit: FlexFit.tight,
                                                        child:new Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: <Widget>[
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: Container(
                                                                            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                                            child:new Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                widget.userAmountData.isPayee?widget.userAmountData.name!=null?
                                                                                Text(
                                                                                  widget.userAmountData.name+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):
                                                                                widget.userAmountData.name!=null?
                                                                                Text(
                                                                                  widget.userAmountData.name+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ):new
                                                                                Text(
                                                                                  widget.userAmountData.cc.toString()+" "+widget.userAmountData.number.toString()+subtext,
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  softWrap: false,
                                                                                  style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 20,
                                                                                ),
                                                                              ],
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex:1,
                                                                        fit: FlexFit.loose,
                                                                        child:Container(
                                                                            width:  global.nontapitsymbolSize,
                                                                            height:  global.nontapitsymbolSize,
                                                                            child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                                'assets/non_tapit_symbol.png',
                                                                                fit: BoxFit.contain
                                                                            ):new Container(
                                                                              width: 0,
                                                                              height: 0,
                                                                            )
                                                                        )
                                                                    ),
                                                                    new Flexible(
                                                                        flex: 6,
                                                                        fit: FlexFit.tight,
                                                                        child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle") == 0?Container(
                                                                            child:new Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                new Flexible(
                                                                                    flex:1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:new Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                                                        children: <Widget>[
                                                                                          widget.userAmountData.isPayee?Container(
                                                                                            child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                          ):new Container(
                                                                                          )
                                                                                        ]
                                                                                    )
                                                                                ),
                                                                                new Flexible(
                                                                                    flex: 1,
                                                                                    fit: FlexFit.tight,
                                                                                    child:  new Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        widget.userAmountData.isPayee? Container(
                                                                                          child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                          Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                        ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                        Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(), style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),

                                                                                      ],
                                                                                    )
                                                                                ),
                                                                              ],
                                                                            )
                                                                        ): GestureDetector(
                                                                          onTap: ShowLogDialog,
                                                                          child: new Row(
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            children: <Widget>[
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit:FlexFit.tight,
                                                                                  child:new Container(width:0,height:0)
                                                                              ),
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit: FlexFit.tight,
                                                                                  child:  Center(
                                                                                    child:widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                                                    Text("Rejected",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                    Text("Settled",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.clip,
                                                                                        style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                  )
                                                                              )
                                                                            ],
                                                                          ),
                                                                        )
                                                                    )
                                                                  ]
                                                              )
                                                            ]
                                                        )
                                                    ),
                                                  ]
                                              ),
                                            )
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                            ]
                        )
                    )
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:isSlideOpen?BoxDecoration(
                    color:Color(0xfffcfcfc),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffE1E1E1),
                        width: 0.75,
                      ),
                    ),
                  ):BoxDecoration(
                    color:Color(0xffffffff),
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                      top: BorderSide(
                        color: Color(0xffffffff),
                        width: 0.75,
                      ),
                    ),
                  ),
                ),
              ]
          )
      )):GestureDetector(
          onTap: ShowLogDialog,
          child:
          new Column(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    child:
                    Slidable(
                        key: ValueKey(widget.index),
                        actionPane: SlidableDrawerActionPane(),
                        child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                  child:new Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                                        child:Container(
                                            width: MediaQuery.of(context).size.width,
                                            decoration: new BoxDecoration(
                                              color: global.whitecolor,
                                              border: Border(
                                                bottom: BorderSide
                                                  (
                                                  color: widget.userAmountData.userID.compareTo(global.UserID)==0?Color(0xffffffff):Color(0xffffffff),
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                            padding: EdgeInsets.fromLTRB(10,5, 10,5),
                                            child: Center(
                                              child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    new Flexible(
                                                        flex:2,
                                                        fit: FlexFit.loose,
                                                        child: new Stack(
                                                          children: <Widget>[
                                                            new Container(
                                                              margin: EdgeInsets.all(3),
                                                              child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                                                                backgroundImage: MemoryImage(widget.profileImg),
                                                                backgroundColor: global.imageBackColor,
                                                              ):(widget.userAmountData.image!=null?
                                                              CircleAvatar(
                                                                child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                                                                backgroundColor: widget.index==0?global.adminBackcolor:global.colors.elementAt(widget.userAmountData.colordata),
                                                              ):CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ))):CircleAvatar(
                                                                backgroundImage: AssetImage('assets/dummy_user.png'),
                                                                backgroundColor: global.appbargreycolor,
                                                              ),
                                                            ),
                                                            new Positioned(
                                                              top:0.0,
                                                              right: 0.0,
                                                              child: widget.userAmountData.usertype.toString().compareTo("admin")==0?Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/admin_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):(widget.userAmountData.usertype.toString().compareTo("payee")==0)?new Container(
                                                                height: 15,
                                                                width: 15,
                                                                child: Image.asset(
                                                                    'assets/payee_symbol.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              ):new Container(
                                                                width: 0,
                                                                height: 0,
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                    ),

                                                    Flexible(
                                                        flex: 11,
                                                        fit: FlexFit.tight,
                                                        child:new Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[

                                                                  new Flexible(
                                                                      flex: 6,
                                                                      fit: FlexFit.tight,
                                                                      child: Container(
                                                                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                                          child:new Column(
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: <Widget>[
                                                                              widget.userAmountData.isPayee?
                                                                              widget.userAmountData.name!=null?
                                                                              (widget.userAmountData.userID.compareTo(global.UserID)==0?Text(
                                                                                "You",
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.font16,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                                                                              ):Text(
                                                                                widget.userAmountData.name+subtext,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                              )):new Text(
                                                                                widget.userAmountData.cc.toString()+" "+widget.userAmountData.number+subtext,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.nameTextFont, fontFamily: 'BalooChetanMedium'),
                                                                              ):
                                                                              widget.userAmountData.name!=null?
                                                                              (widget.userAmountData.userID.compareTo(global.UserID)==0?Text(
                                                                                "You",
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.font16,color:global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),
                                                                              ):Text(
                                                                                widget.userAmountData.name+subtext,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                              )):
                                                                              new Text(
                                                                                widget.userAmountData.cc.toString()+" "+widget.userAmountData.number+subtext,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                softWrap: false,
                                                                                style: TextStyle(fontSize: global.nameTextFont,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                              ),

                                                                              SizedBox(
                                                                                width: 0,
                                                                              ),
                                                                            ],
                                                                          )
                                                                      )
                                                                  ),
                                                                  new Flexible(
                                                                      flex:1,
                                                                      fit: FlexFit.loose,
                                                                      child:Container(
                                                                          width:  global.nontapitsymbolSize,
                                                                          height:  global.nontapitsymbolSize,
                                                                          child: !widget.userAmountData.isTapitUser?Image.asset(
                                                                              'assets/non_tapit_symbol.png',
                                                                              fit: BoxFit.contain
                                                                          ):new Container(
                                                                            width: 0,
                                                                            height: 0,
                                                                          )
                                                                      )
                                                                  ),
                                                                  new Flexible(
                                                                      flex: 6,
                                                                      fit: FlexFit.tight,
                                                                      child: widget.userAmountData.settlementStatus.toString().compareTo("pending") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("payee_partial_settle") == 0 || widget.userAmountData.settlementStatus.toString().compareTo("admin_partial_settle") == 0?Container(
                                                                          child:new Row(
                                                                            mainAxisAlignment: MainAxisAlignment.center,

                                                                            children: <Widget>[
                                                                              new Flexible(
                                                                                  flex:1,
                                                                                  fit: FlexFit.tight,
                                                                                  child:new Column(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        widget.userAmountData.isPayee?Container(
                                                                                          child: Text("\$"+widget.userAmountData.payeeAmount.toString(), style: TextStyle(fontSize: global.amtFont,color: global.bluetextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                        ):new Container(
                                                                                        )
                                                                                      ]
                                                                                  )
                                                                              ),
                                                                              new Flexible(
                                                                                  flex: 1,
                                                                                  fit: FlexFit.tight,
                                                                                  child:  new Column(
                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                                                    children: <Widget>[
                                                                                      widget.userAmountData.isPayee? Container(
                                                                                        child: widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(),
                                                                                            style: TextStyle(fontSize: global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                        Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount).toString(),
                                                                                            style: TextStyle(fontSize:  global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                      ): widget.userAmountData.balanceAmount<0?Text("-\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(),
                                                                                          style: TextStyle(fontSize:  global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                      Text("\$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString(),
                                                                                          style: TextStyle(fontSize:  global.amtFont,color: global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                    ],
                                                                                  )
                                                                              ),
                                                                            ],
                                                                          )
                                                                      ): GestureDetector(
                                                                        onTap: ShowLogDialog,
                                                                        child: new Row(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment: CrossAxisAlignment.end,
                                                                          children: <Widget>[
                                                                            new Flexible(
                                                                                flex:1,
                                                                                fit:FlexFit.tight,
                                                                                child:new Container(width:0,height:0)
                                                                            ),
                                                                            new Flexible(
                                                                                flex:1,
                                                                                fit: FlexFit.tight,
                                                                                child:  Center(
                                                                                  child:widget.userAmountData.settlementStatus.toString().compareTo("reject")==0?
                                                                                  Text("Rejected",
                                                                                      maxLines: 1,
                                                                                      overflow: TextOverflow.clip,
                                                                                      style: TextStyle(fontSize: global.font14,color: global.rejectedTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')):
                                                                                  Text("Settled",
                                                                                      maxLines: 1,
                                                                                      overflow: TextOverflow.clip,
                                                                                      style: TextStyle(fontSize: global.font14,color: global.settledTextcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                                                                                )
                                                                            )
                                                                          ],
                                                                        ),
                                                                      )
                                                                  )

                                                                ],
                                                              ),
                                                              new Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[
                                                                  new Container(
                                                                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                                    child:

                                                                    widget.userAmountData.userID.compareTo(global.UserID)==0?(
                                                                        widget.isSettledSplit?(
                                                                            widget.userAmountData.balanceAmount<0 && global.unsettledUserCount!=0?new Text(
                                                                              "owed \$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString()+" from "+global.unsettledUserCount.toString()+" users",
                                                                              maxLines: 2,
                                                                              overflow: TextOverflow.ellipsis,
                                                                              softWrap: false,
                                                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                            ):
                                                                            widget.userAmountData.balanceAmount!=0 && global.unsettledPayeeCount!=0?new Text(
                                                                              "owe \$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString()+" to "+global.unsettledPayeeCount.toString()+" payee",
                                                                              maxLines: 2,
                                                                              overflow: TextOverflow.ellipsis,
                                                                              softWrap: false,
                                                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                            ):new Container(width:0,height:0)
                                                                        ):(
                                                                            widget.userAmountData.balanceAmount!=0?new Text(
                                                                              "will get back \$"+global.formatter.format(widget.userAmountData.balanceAmount.abs()).toString()+" once settled",
                                                                              maxLines: 2,
                                                                              overflow: TextOverflow.ellipsis,
                                                                              softWrap: false,
                                                                              style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                                                            ):new Container(width:0,height: 0,)
                                                                        )

                                                                    ):new Container(width:0,height:0),
                                                                  )
                                                                ],
                                                              )

                                                            ]
                                                        )
                                                    )
                                                  ]
                                              ),
                                            )
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                            ]
                        )
                    )
                ),
              ]
          )
      )
      );
  }
}
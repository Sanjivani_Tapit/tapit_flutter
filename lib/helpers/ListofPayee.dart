import 'dart:typed_data';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart'as global;

class ListofPayee extends StatefulWidget
{
  const ListofPayee({Key key,this.index,this.userAmountData,this.profileImg,this.isSelected}) : super(key: key);
  final UserAmountData userAmountData;
  final Uint8List profileImg;
  final int index;
  final ValueChanged<bool> isSelected;

  ListofPayeeState createState()=>ListofPayeeState();
}

class ListofPayeeState extends State<ListofPayee>
{
  bool isSelected = false;

  @override
  void initState(){
    super.initState();
  }

  void imageClicked()
  {
    setState(()
    {
      isSelected = true;
      widget.isSelected(isSelected);
    });
  }

  @override

  Widget build(BuildContext context)
  {

    return widget.userAmountData.isPayee && widget.userAmountData.payeeAmount!=0?
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: 40,
          child:Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
              child:Container(
                width: 40,
                color: Colors.white,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Flexible(
                        child: new Container(
                          width: 30,
                          height: 30,
                          child:GestureDetector(
                            onTap: () {
                              imageClicked();
                            },
                            child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                              backgroundImage: MemoryImage(widget.profileImg),
                              backgroundColor: global.imageBackColor,
                            ):CircleAvatar(
                              child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color:widget.index==0?global.adminTextcolor: new Color(0xffffffff)),),
                              backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                            )):CircleAvatar(
                              backgroundImage: AssetImage('assets/dummy_user.png'),
                              backgroundColor: global.appbargreycolor,
                            ),
                          ),
                        ),
                      ),
                    ]
                ),
              )
          ),
        ),
        Opacity(
            opacity: 0.7,
            child:new Text("\$ "+global.formatter.format(widget.userAmountData.payeeAmount).toString(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: global.font10,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'))
        )
      ],
    ):new Container(width: 0,height: 0,);
  }
}
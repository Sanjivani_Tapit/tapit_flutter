import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:tapit/global.dart' as global;

class AnimatedGradientClass2 extends StatefulWidget
{
  @override
  AnimatedClassGradient2State createState() => AnimatedClassGradient2State();
}

class AnimatedClassGradient2State extends State<AnimatedGradientClass2> with SingleTickerProviderStateMixin
{
  Animation animation;
  AnimationController animationController;

  @override
  void initState()
  {
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    animationController.repeat(reverse: true);
    animation = Tween(begin: 0.0, end: 1.0).animate(animationController)..addListener(() {
      setState(() {
      });
    });
    super.initState();
  }

  @override
  void dispose()
  {
    if(animationController!=null)
    {
      animationController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      body: Container(
        height: 5,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient:
            LinearGradient(
                begin:Alignment(animation.value-0.7,animation.value-0.7),
                end:Alignment(animation.value+0.1,animation.value+0.1),
                colors: global.ereceiptRunningGradient
            )
        ),
      ),
    );
  }
}
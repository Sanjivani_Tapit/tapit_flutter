import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(nullable: false)
class ContactData
{
  ContactData({this.userID,this.cc,this.isTapitUser,this.type,this.colordata,this.image,this.name,this.number,this.isSelected});

  String userID;
  String cc;
  bool isTapitUser;
  String type;
  int colordata;
  String image;
  String name;
  String number;
  bool isSelected;

  ContactData.fromJson(Map<String, dynamic> json)
      : userID = json['userID'],
        cc = json['cc'],
        isTapitUser = json['isTapitUser'],
        type = json['type'],
        colordata = json['colordata'],
        image = json['img'],
        name = json['name'],
        number = json['number'],
        isSelected=json['isSelected'];

  Map<String, dynamic> toJson() {
    return {
      'userID':userID,
      'cc':cc,
      'isTapitUser':isTapitUser,
      'type':type,
      'colordata':colordata,
      'img': image,
      'name': name,
      'number': number,
      'isSelected': isSelected,

    };
  }
}
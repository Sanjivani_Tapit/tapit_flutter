import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/global.dart' as global;

class ListofWhoPaid extends StatefulWidget
{
  ListofWhoPaid({Key key,this.index, this.userAmountData,this.profileImg,this.amount,this.isSel}) : super(key: key);
  UserAmountData userAmountData;
  Uint8List profileImg;
  int index;
  double amount;
  ValueChanged<double> isSel;
  ListofWhoPaidState createState()=>ListofWhoPaidState();
}

class ListofWhoPaidState extends State<ListofWhoPaid>
{
  bool userSel=false;
  bool isSel= false;
  bool isCheckboxSel=false;

  final textController = TextEditingController();
  final String LOGTAG="ListofWhoPaid";

  @override
  void initState()
  {
    super.initState();
    setState(()
    {
      if(widget.userAmountData.isPayee && widget.userAmountData.payeeAmount!=0)
      {
        isCheckboxSel =true;
      }
    });
  }

  void _onRememberMeChanged(String newValue) => setState(()
  {
    widget.isSel(double.parse(newValue));
  });

  void _onCheckboxChanged(bool newValue)
  {
    setState(()
    {
      if (!newValue)
      {
        widget.isSel(0);
        widget.userAmountData.payeeAmount = 0;
      }
      else
      {
        widget.isSel(widget.userAmountData.payeeAmount);
      }
      isCheckboxSel = newValue;
    });

  }


  @override
  Widget build(BuildContext context) {

    final textField=  TextField(
      inputFormatters: [ WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),],
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      style: TextStyle(fontSize:global.font14,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular'),
      onChanged: _onRememberMeChanged,
      textAlignVertical: TextAlignVertical.center,
      textAlign: TextAlign.center,
      enabled: true,
      cursorColor: global.mainColor,
      obscureText: false,
      controller: textController,
      decoration: InputDecoration(
          filled: true,
          fillColor:  Color(0xffffffff),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffc8c8c8),
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffc8c8c8),
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          focusedBorder:OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xffF57C00)),
            borderRadius: BorderRadius.circular(5.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(5, 5 , 5, 5),
          hintText: widget.userAmountData.isPayee?global.formatter.format(widget.userAmountData.payeeAmount):global.formatter.format(0),
          hintStyle: TextStyle(fontSize:global.font14,color:global.darkgreycolor,fontWeight: FontWeight.normal, fontFamily: 'BalooChetanRegular')
      ),
    );

    return new Row(
      children: <Widget>[
        Flexible(
          child: Padding( padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
            child:Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(0,10, 0, 0),
                child: Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Flexible(
                            flex: 1,
                            fit: FlexFit.loose,
                            child:GestureDetector(
                              onTap: (){
                                _onCheckboxChanged(!isCheckboxSel);
                              },
                              child: new Container(
                                  width: 25,
                                  height: 25,
                                  child: isCheckboxSel?Image.asset(
                                      'assets/voilet_checkbox.png',
                                      fit: BoxFit.contain
                                  ):Image.asset(
                                      'assets/empty_checkbox.png',
                                      fit: BoxFit.contain
                                  )
                              ),
                            )
                        ),
                        new Flexible(
                          flex:2,
                          fit: FlexFit.loose,
                          child: new Container(
                            width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                            height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                            child:widget.profileImg!=null?((widget.userAmountData.type.toString().contains("true"))?CircleAvatar(
                              backgroundImage: MemoryImage(widget.profileImg),
                              backgroundColor: global.imageBackColor,
                            ):CircleAvatar(
                              child: Text(widget.userAmountData.image,style: TextStyle(fontSize:global.font14,color: widget.index==0?global.adminTextcolor:new Color(0xffffffff)),),
                              backgroundColor: widget.index==0?global.adminBackcolor:(widget.userAmountData.colordata!=null?global.colors.elementAt(widget.userAmountData.colordata):global.colors.elementAt(3)),
                            )):CircleAvatar(
                              backgroundImage: AssetImage('assets/dummy_user.png'),
                              backgroundColor: global.appbargreycolor,
                            ),
                          ),
                        ),
                        new Flexible(
                            flex: 5,
                            fit: FlexFit.tight,
                            child: Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child:new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    widget.userAmountData.name!=null?
                                    new Text(
                                      widget.userAmountData.name,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      style: TextStyle(fontSize: global.font16,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    ):new Text(
                                      widget.userAmountData.cc.toString()+" "+widget.userAmountData.number,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      style: TextStyle(fontSize: global.font16,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    ),
                                  ],
                                )
                            )
                        ),
                        new Flexible(
                            flex:1,
                            fit: FlexFit.loose,
                            child:Container(
                                width: global.nontapitsymbolSize,
                                height: global.nontapitsymbolSize,
                                child: !widget.userAmountData.isTapitUser?Image.asset(
                                    'assets/non_tapit_symbol.png',
                                    fit: BoxFit.contain
                                ):new Container(width: 0,height: 0,)
                            )
                        ),
                        new Flexible(
                          flex: 2,
                          fit: FlexFit.tight,
                          child: isCheckboxSel?new Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text("\$",style: TextStyle(fontSize: global.font17,color: global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                  Expanded(
                                    child:  new Container(
                                      padding: EdgeInsets.fromLTRB(3, 0, 0, 0),
                                      width: 60,
                                      height: 40,
                                      child: textField,
                                    ),
                                  )
                                ],
                              )
                          ):new Container(width: 0,height: 40,),
                        ),
                      ]
                  ),
                )
            ),
          ),
        )
      ],
    );
  }
}
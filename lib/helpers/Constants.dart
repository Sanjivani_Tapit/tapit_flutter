import 'dart:io';

class Constants {
  static const String BASE_URL = "http://3.21.202.26:8080/tapitserver";
  static const String NEW_BASE_URL = "http://3.21.202.26:8080/tapitserver/api/v1/test/";
  static  String BASE_PLATFORM = Platform.isAndroid ? "android" : "ios";
  static  String RETRO_BASE_URL = Constants.BASE_URL + '/api/v1/' + Constants.BASE_PLATFORM + '/';
}

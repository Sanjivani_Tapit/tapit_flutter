import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/helpers/ContactData.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/SubstringHighlight.dart';

class ContactList extends StatefulWidget
{
  const ContactList({Key key,this.index, this.contactData,this.profileImg,this.isSel}) : super(key: key);
  final ContactData contactData;
  final Uint8List profileImg;
  final int index;
  final ValueChanged<bool> isSel;

  ContactListState createState()=>ContactListState();
}

class ContactListState extends State<ContactList> {

  bool isSel= false;
  bool checkboxVal = false;

  void _onRememberMeChanged(bool newValue) => setState(() {

    checkboxVal = newValue;
    widget.contactData.isSelected=newValue;

    isSel = newValue;
    widget.isSel(isSel);
  });

  @override
  Widget build(BuildContext context) {

    return new Row(
      children: <Widget>[
        Flexible(
          child:
          Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10,7, 10, 7),
              child: Center(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Flexible(
                          flex:1,
                          fit: FlexFit.tight,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                height: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/10:MediaQuery.of(context).size.width/13,
                                child:widget.profileImg!=null?((widget.contactData.type.toString().contains("true"))?CircleAvatar(
                                  backgroundImage: MemoryImage(widget.profileImg),
                                  backgroundColor: global.imageBackColor,
                                ):(widget.contactData.image!=null?CircleAvatar(
                                  child: Text(
                                    widget.contactData.image,style: TextStyle(fontSize:global.font14,color: new Color(0xffffffff)),),
                                  backgroundColor: widget.contactData.colordata!=null?global.colors.elementAt(widget.contactData.colordata):global.colors.elementAt(3),
                                ):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ))):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ),
                              ),
                            ],
                          )
                      ),
                      new Flexible(
                          flex: 4,
                          fit: FlexFit.tight,
                          child: Container(
                              padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              child:new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  widget.contactData.name!=null?
                                  SubstringHighlightTapit(
                                    text: widget.contactData.name,
                                    term: global.searchitem,
                                    textStyle: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    textStyleHighlight: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                  ):new Container(
                                    width: 0,
                                    height: 0,
                                  ),
                                  widget.contactData.name!=null? Opacity(
                                    opacity: 0.6,
                                    child: SubstringHighlightTapit(
                                      text: widget.contactData.cc!=null?widget.contactData.cc+" "+widget.contactData.number:widget.contactData.number,
                                      term: global.searchitem,
                                      textStyle: TextStyle(fontSize: global.font13,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                      textStyleHighlight: TextStyle(fontSize: global.font13,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                    ),
                                  ):SubstringHighlightTapit(
                                    text: widget.contactData.cc!=null?widget.contactData.cc+" "+widget.contactData.number:widget.contactData.number,
                                    term: global.searchitem,
                                    textStyle: !global.searchItemNumber?TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'):TextStyle(fontSize: global.font18,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),// user typed "et"
                                    textStyleHighlight: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),
                                  ),
                                ],
                              )
                          )
                      ),
                      new Flexible(
                          flex:1,
                          fit: FlexFit.tight,
                          child:new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  width:  global.nontapitsymbolSize,
                                  height:  global.nontapitsymbolSize,
                                  child: !widget.contactData.isTapitUser?Image.asset(
                                      'assets/non_tapit_symbol.png',
                                      fit: BoxFit.contain
                                  ):null
                              )
                            ],
                          )
                      ),
                      new Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child:new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              widget.contactData.type.toString().contains("admin")?Container(
                                  width: 23,
                                  height: 23,
                                  child: Image.asset(
                                      'assets/admin_lock_symbol.png',
                                      fit: BoxFit.contain
                                  )):GestureDetector(
                                onTap: (){
                                  _onRememberMeChanged(!widget.contactData.isSelected);
                                },
                                child: new Container(
                                    width: 23,
                                    height: 23,
                                    child: widget.contactData.isSelected?Image.asset(
                                        'assets/orange_checkbox.png',
                                        fit: BoxFit.contain
                                    ):Image.asset(
                                        'assets/empty_checkbox.png',
                                        fit: BoxFit.contain
                                    )
                                ),
                              )
                            ],
                          )
                      ),
                    ]
                ),
              )
          ),
        ),
      ],
    );
  }
}
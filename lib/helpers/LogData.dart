import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(nullable: false)
class LogData
{
  LogData({this.txnId,this.senderName,this.receiverName,this.txnTimestamp,this.txnAmount,this.txnStatus,this.txnMode,this.txnComment,this.logTitle,this.balanceAmt});

  String txnId;
  String txnTimestamp;
  String senderName;
  String receiverName;
  double txnAmount;
  String txnStatus;
  String txnMode;
  String txnComment;
  String logTitle;
  double balanceAmt;

  LogData.fromJson(Map<String, dynamic> json)
      : txnId = json['txnId'],
        senderName = json['senderName'],
        receiverName = json['receiverName'],
        txnTimestamp = json['txnTimestamp'],
        txnAmount = json['txnAmount'],
        txnStatus = json['txnStatus'],
        txnMode = json['txnMode'],
        txnComment=json['txnComment'],
        logTitle=json['logTitle'],
        balanceAmt=json['balanceAmt'];

  Map<String, dynamic> toJson() {
    return {
      'txnId':txnId,
      'senderName':senderName,
      'receiverName':receiverName,
      'txnTimestamp':txnTimestamp,
      'txnAmount': txnAmount,
      'txnStatus': txnStatus,
      'txnMode': txnMode,
      'txnComment': txnComment,
      'logTitle': logTitle,
      'balanceAmt': balanceAmt,
    };
  }
}
import 'package:flutter/cupertino.dart';
import 'package:tapit/global.dart' as global;
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class NotificationPrefList extends StatefulWidget
{
  NotificationPrefList({Key key,this.index,this.title,this.isSelected,this.isSel}) : super(key: key);
  final int index;
  final String title;
  final bool isSelected;
  final ValueChanged<bool> isSel;

  NotificationPrefListState createState()=>NotificationPrefListState();
}

class NotificationPrefListState extends State<NotificationPrefList>
{
  bool iniValue;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      iniValue=widget.isSelected;
    });
  }

  void _onRememberMeChanged(bool newValue) => setState(()
  {
    setState(() {
      iniValue=newValue;
    });
    widget.isSel(newValue);
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: new Container(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(widget.title, style: TextStyle(fontSize: global.font14,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
            ),
          ),
          widget.index==0?new Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child:new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    child: FlutterSwitch(
                     width: 50,
                      height: 25.0,
                      activeColor: global.mainColor,
                      inactiveColor: global.lightgreycolor,
                      value: true,
                      showOnOff: false,
                      onToggle: _onRememberMeChanged,
                    ),
                  )
                ],
              )
          ):
          new Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child:new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    child: FlutterSwitch(
                      width: 50,
                      height: 25.0,
                      activeColor: global.mainColor,
                      inactiveColor: global.lightgreycolor,
                      value: iniValue,
                      showOnOff: false,
                      onToggle: _onRememberMeChanged,
                    ),
                  )
                ],
              )
          ),
        ],
      ),
    );
  }
}



import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/http.dart';
import 'package:tapit/helpers/Constants.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:tapit/utils/responsebean.dart';


part 'api_client.g.dart';

@RestApi(baseUrl: Constants.BASE_URL)
abstract class ApiClient {
  factory ApiClient(Dio dio, String baseUrl) {
    dio.options = BaseOptions(receiveTimeout: 5000, connectTimeout: 10000);
    return _ApiClient(dio, baseUrl: baseUrl);
  }

  @GET("/api/v1/{devicetype}/user/user")
  @Headers(<String, dynamic>{
    //Static header
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> getAllUsers(@Path("devicetype") String devicetype,);

  @POST("/api/v1/{devicetype}/login/user/otp/generate")
  @Headers(<String, dynamic>{
    //Static header
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> otpGenerate(@Path("devicetype") String devicetype,@Body() String jsonbody);

  @PUT("/api/v1/{devicetype}/login/user/otp/mobile/verify")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> otpVerify(@Path("devicetype") String devicetype,@Body() String jsonbody);

  @GET("/api/v1/{devicetype}/user/user/mobile")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> checkMobileExist(@Path("devicetype") String devicetype, @Queries() Map<String, dynamic> queries);

  @POST("/api/v1/{devicetype}/user/user")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> UpdateUserData(@Path("devicetype") String devicetype,@Body() String jsonbody);

  @GET("/api/v1/{devicetype}/users/{userid}/counter")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getCounter(@Path("devicetype") String devicetype,@Path("userid") String id,@Header("Authorization") String authToken);

  @POST("/api/v1/{devicetype}/users/{userid}/contacts")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> postContactToCloud(@Path("devicetype") String devicetype,@Body() String jsonbody,@Path("userid") String id,@Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/users/{userid}/contacts")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getContactSync(@Path("devicetype") String devicetype,@Path("userid") String id,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/{userid}/receipts/history")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getHistoryData(
      @Path("devicetype") String devicetype,
      @Path("userid") String id,
      @Queries() Map<String, dynamic> queries,
      @Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/users/{userid}/alerts")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getAlertData(
      @Path("devicetype") String devicetype,
      @Path("userid") String id,
      @Queries() Map<String, dynamic> queries,
      @Header("Authorization") String authToken);


  @PUT("/api/v1/{devicetype}/users/{userid}/alert/{alertid}")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> updateAlertStatus(@Path("devicetype") String devicetype, @Path("userid") String userid,@Path("alertid") String alertid,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/{userid}/groups")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getGroups(@Path("devicetype") String devicetype,@Path("userid") String id,@Header("Authorization") String authToken);


  @PUT("/api/v1/{devicetype}/users/{userid}/group/{groupid}")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> updateGroup(@Path("devicetype") String devicetype,@Path("userid") String userid,@Path("groupid") String groupid,@Body() String jsonbody,@Header("Authorization") String authToken);


  @POST("/api/v1/{devicetype}/users/{userid}/group")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> addGroup(@Path("devicetype") String devicetype,@Body() String jsonbody,@Path("userid") String id,@Header("Authorization") String authToken);

  @POST("/api/v1/{devicetype}/users/{userid}/initiate/billsplit/receipt/{receiptid}")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> initiateBillSplit(@Path("devicetype") String devicetype,@Body() String jsonbody,@Path("userid") String userid,@Path("receiptid") String receiptid,@Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/users/receipt/{receiptid}/billsplit/payment/settle")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> settleAPI(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid, @Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/receipt/{receiptid}/billsplit/payment/reject")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> rejectAPI(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid, @Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/receipt/{receiptid}/billsplit/payment/payup")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> payupAPI(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid, @Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/receipt/{receiptid}/payment/reminder")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> reminderAPI(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid, @Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/users/receipt/{receiptid}")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getReceipt(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/users/users/billsplit/requestsettlement/receipt/{receiptid}")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> requestSettlement(@Path("devicetype") String devicetype,@Path("receiptid") String receiptid,@Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/users/users/{userid}/receipt/{receiptid}/logs")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getLogs(@Path("devicetype") String devicetype,@Path("userid") String userid,@Path("receiptid") String receiptid,@Header("Authorization") String authToken);


  @PUT("/api/v1/{devicetype}/user/user/email/update-and-verify")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> verifyEmail(@Path("devicetype") String devicetype,@Body() String jsonbody,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/user/user/get/{userid}")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getUserData(@Path("devicetype") String devicetype,@Path("userid") String id,@Header("Authorization") String authToken);


  @GET("/api/v1/{devicetype}/user/user/profile-pic/pre-signed-url/get")
  @Headers(<String, dynamic>{
    'accept': '*/*'
  })
  Future<ResponseBean> getImageMethod(@Path("devicetype") String devicetype,@Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);


  @PUT("/api/v1/{devicetype}/user/user/profile-pic/s3/update")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> putImageMethod(@Path("devicetype") String devicetype,@Body() String jsonbody,@Header("Authorization") String authToken);


  @PUT("/api/v1/test/parser/save")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> getTapAPI(@Body() String jsonbody,@Header("Authorization") String authToken);

  @GET("/api/v1/{devicetype}/merchant/search/eprinter/get")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> getMerchantInfo(@Path("devicetype") String devicetype,@Queries() Map<String, dynamic> queries,@Header("Authorization") String authToken);


  @POST("/api/v1/{devicetype}/users/receipts")
  @Headers(<String, dynamic>{
    'Content-Type': 'application/json',
    'accept': '*/*'
  })
  Future<ResponseBean> acceptReceipt(@Path("devicetype") String devicetype,@Body() String jsonbody,@Header("Authorization") String authToken);



}

@JsonSerializable()
class User {
  String id;
  String name;
  String password;
  String role;
  String picture;
  String email;
  String mobileNo;
  String countryCode;
  String uuid;
  String deviceId;
  String deviceType;
  String fcmToken;
  bool isEmailVerified = false;
  bool isMobileVerified = false;
  bool isActive = true;

//    @Transient
  String otp;
  String registrationStatus;

  User(
      {this.id,
      this.name,
      this.password,
      this.role,
      this.picture,
      this.email,
      this.mobileNo,
      this.countryCode,
      this.uuid,
      this.deviceId,
      this.deviceType,
      this.fcmToken,
      this.isEmailVerified,
      this.isMobileVerified,
      this.isActive,
      this.otp,
      this.registrationStatus});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class OTPGenerate {
  String mobileNo;
  String countryCode;

  OTPGenerate(
      {this.mobileNo,
        this.countryCode});
  factory OTPGenerate.fromJson(Map<String, dynamic> json) => _$OTPGenerateFromJson(json);
  Map<String, dynamic> toJson() => _$OTPGenerateToJson(this);
}

@JsonSerializable()
class OTPVerify {
  String mobileNo;
  String otp;
  String countryCode;

  OTPVerify(
      {this.mobileNo,
        this.otp,
        this.countryCode});
  factory OTPVerify.fromJson(Map<String, dynamic> json) => _$OTPVerifyFromJson(json);
  Map<String, dynamic> toJson() => _$OTPVerifyToJson(this);
}

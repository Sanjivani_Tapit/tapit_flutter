import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tapit/Activities/EditContainerActivity.dart';
import 'package:tapit/Activities/NotificationsScreen.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class ReceiptsFragment extends StatefulWidget
{
  ReceiptsFragmentState createState()=> ReceiptsFragmentState();
}

class ReceiptsFragmentState extends State<ReceiptsFragment>
{
  bool receivedResponse=false;
  int paginationTotalCount=0;
  int paginationCount=0;
  int notiCount=0;
  ScrollController scrollController = new ScrollController();
  List<Receipt> list_of_receipts=new List();
  String LOGTAG="ReceiptsFragment";

  @override
  void initState()
  {
    super.initState();
    getReceipt();
  }

  Future<void> getReceipt()
  {
    setState(()
    {
      notiCount=global.unreadNotiCounter;
    });
    paginationTotalCount=(global.totalReceiptCount/10).toInt();
    int remain=(global.totalReceiptCount%10);

    getReceipts();

    setState(()
    {
      list_of_receipts.clear();
    });

    scrollController.addListener(()
    {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent)
      {
        if(remain==0)
        {
          if (paginationCount < paginationTotalCount)
          {
            getData(paginationCount, 10);
          }
        }
        else
        {
          if (paginationCount <= paginationTotalCount)
          {
            getData(paginationCount, 10);
          }
        }
      }
    });
  }

  void getReceipts() async
  {
    setState(()
    {
      list_of_receipts.clear();
      receivedResponse=false;
    });
    bool flag=await getData(0,10);
    if(mounted)
    {
      setState(()
      {
        receivedResponse = true;
      });
    }

  }

  Future<bool> getData(int start,int end) async
  {
    Future<bool> flag=Future<bool>.value(true);
    List<Receipt> receiptsList=new List();
    UserService userService=new UserService();
    ResponseBean response=await userService.getHistoryData(global.UserID,start.toString(),end.toString());
    if(response!=null)
    {
      int statusCode = response.status;
      if (statusCode == 200)
      {
        List<dynamic> payloadList = response.payLoad;
        if (payloadList != null)
        {
          if (payloadList.length > 0)
          {
            paginationCount++;
            for (int i = 0; i < payloadList.length; i++)
            {
              bool splitgrpPresence = false;
              String receiptId = "";
              String hotelName = "";
              String groupName = "";
              String currentDay = "";
              String isUser = "member";
              String userSettlements = "";
              String userMemberStatus = "reject";
              double payeeBalAmt=0;
              String merchantURL;
              bool receiptSettlementStatus = false;

              int receiptType=payloadList[i]['receiptType'];
              receiptId = payloadList[i]['id'];
              double totalAmount = payloadList[i]['totalAmount'];
              String timestamp = payloadList[i]['timestamp'];
              groupName = payloadList[i]['groupName'];
              receiptSettlementStatus = payloadList[i]['settlementStatus'];
              List<dynamic> splitGroups = payloadList[i]['splitGroups'];

              var merchantobj=payloadList[i]['merchant'];
              if(merchantobj!=null)
              {
                merchantURL = payloadList[i]['merchant']['merchantUrl'];
                hotelName = payloadList[i]['merchant']['merchantName'];
              }

              if(receiptType!=null)
              {
                if (receiptType == 1)
                {
                  hotelName = payloadList[i]['merchantName'];
                  if (hotelName == null)
                  {
                    hotelName = "Any Restaurent";
                  }
                }
              }
              else
              {
                receiptType=2;
              }

              if (merchantURL == null || merchantURL.toString().compareTo("/url") == 0)
              {
                merchantURL="https://image.freepik.com/free-photo/chicken-steak-with-lemon-tomato-chili-carrot-white-plate_1150-25886.jpg";
              }

              if (totalAmount == 0)
              {
                totalAmount = 60;
              }

              if (splitGroups.length > 0)
              {
                splitgrpPresence = true;
                int settledUserCount = 0;
                isUser = "member";
                for (int f = 0; f < splitGroups.length; f++)
                {
                  String userid = splitGroups[f]['userId'];
                  int userRole = splitGroups[f]['userRole'];
                  bool isPayee = splitGroups[f]['payee'];
                  String memberStatus = splitGroups[f]['memberStatus'];
                  double balAmt=splitGroups[f]['splitOwes'][0]['balanceAmount'];

                  if (memberStatus.toString().compareTo("settle") == 0 || memberStatus.toString().compareTo("admin_settle") == 0 || memberStatus.toString().compareTo("payee_settle") == 0)
                  {
                    settledUserCount++;
                  }

                  if (userid.compareTo(global.UserID) == 0 && userRole == 1)
                  {
                    userMemberStatus = memberStatus;
                    if (isUser.compareTo("member") == 0)
                    {
                      isUser = "admin";
                    }
                  }
                  else if (userid.compareTo(global.UserID) == 0 && isPayee)
                  {
                    userMemberStatus = memberStatus;
                    if (isUser.compareTo("member") == 0)
                    {
                      payeeBalAmt=balAmt;
                      isUser = "payee";
                    }
                  }
                  else
                  {
                    if(userid.compareTo(global.UserID) == 0)
                    {
                      userMemberStatus = memberStatus;
                    }
                    if(isUser.compareTo("member") == 0)
                    {
                      isUser = "member";
                    }
                  }
                }

                if (isUser.toString().compareTo("admin") == 0)
                {
                  if (settledUserCount == splitGroups.length)
                  {
                    userSettlements = "all";
                  }
                  else
                  {
                    userSettlements = settledUserCount.toString() + "/" + splitGroups.length.toString();
                  }
                }
                else if (isUser.toString().compareTo("payee") == 0)
                {
                  if (settledUserCount == splitGroups.length)
                  {
                    userSettlements = "all";
                  }
                  else if(userMemberStatus.toString().compareTo("reject") == 0)
                  {
                    userSettlements = "reject";
                  }
                  else if(userMemberStatus.toString().compareTo("settle") == 0 || userMemberStatus.toString().compareTo("admin_settle") == 0 || userMemberStatus.toString().compareTo("payee_settle") == 0)
                  {
                    userSettlements = "settle";
                  }
                  else if(payeeBalAmt>0)
                  {
                    userSettlements = "pay";
                  }
                  else
                  {
                    userSettlements = settledUserCount.toString() + "/" + splitGroups.length.toString();
                  }
                }
                else
                {
                  if (settledUserCount == splitGroups.length)
                  {
                    userSettlements = "all";
                  }
                  else
                  {
                    if(userMemberStatus.toString().compareTo("settle") == 0 || userMemberStatus.toString().compareTo("admin_settle") == 0 || userMemberStatus.toString().compareTo("payee_settle") == 0)
                    {
                      userSettlements = "settle";
                    }
                    else if(userMemberStatus.toString().compareTo("admin_partial_settle") == 0 || userMemberStatus.toString().compareTo("payee_partial_settle") == 0 || userMemberStatus.toString().compareTo("pending") == 0)
                    {
                      userSettlements = "pending";
                    }
                    else
                    {
                      userSettlements = "reject";
                    }
                  }
                }
              }
              else
              {
                isUser = "admin";
              }

              if (hotelName == null || hotelName.isEmpty)
              {
                hotelName = "Any Restaurent";
              }

              if (groupName == null || groupName.isEmpty)
              {
                groupName = "New Group";
              }

              if (timestamp != null)
              {
                String day = timestamp.substring(0, 10);
                String time = timestamp.substring(11, 19);

                var dateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse(day + " " + time, true);
                String alert_time = dateTime.toLocal().toString().substring(11, 16);
                String time_hh_mm = alert_time;

                final now = DateTime.now();
                final lastMidnight = new DateTime(now.year, now.month, now.day);

                int today_midnight = lastMidnight.millisecondsSinceEpoch;
                int current_timeinmillies = dateTime.toLocal().millisecondsSinceEpoch;

                bool timeFlag = false;
                int s = 0;
                while (!timeFlag)
                {
                  if (current_timeinmillies > today_midnight)
                  {
                    if (s == 0)
                    {
                      currentDay = "Today," + " " + time_hh_mm;
                      timeFlag = true;
                    }
                    else if (s == 1)
                    {
                      currentDay = "Yesterday," + " " + time_hh_mm;
                      timeFlag = true;
                    }
                    else
                    {

                      String formattedDate = DateFormat('dd MMM').format(dateTime);
                      currentDay=formattedDate+ ", " + time_hh_mm;
                      timeFlag = true;
                    }
                  }
                  else
                  {
                    today_midnight = today_midnight - 86400000;
                  }
                  s++;
                }
              }
              Receipt receipt = new Receipt(receiptId: receiptId,
                  title: hotelName,
                  amount: totalAmount,
                  timestamp: currentDay,
                  imagepath: merchantURL,
                  groupName: groupName,
                  isUser: isUser,
                  isSplitGroupPresent: splitgrpPresence,
                  receiptSettlementStatus: receiptSettlementStatus,
                  userSettlements: userSettlements);
              receiptsList.add(receipt);
            }
          }
        }
      }
      if (mounted) {
        setState(() {
          list_of_receipts.addAll(receiptsList);
        });
      }
    }
    return flag;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding:  EdgeInsets.fromLTRB(15,0,0,0),
                child: Image.asset(
                  'assets/logo.png',
                  fit: BoxFit.contain,
                  height: 24,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(8,0,8,0),
                child:  Image.asset(
                  'assets/justap_logo.png',
                  fit: BoxFit.contain,
                  height: 22,
                ),
              )
            ],

          ),
          actions: <Widget>[
            new Stack(
              children: <Widget>[
                new IconButton(
                    icon: new Image.asset('assets/notification_symbol.png'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NotificationsScreen()),
                      ).then((value) async => {
                        await global.helperClass.getCount(),
                        if(mounted){
                          setState(() {
                            notiCount = global.unreadNotiCounter;
                          })
                        }
                      });
                    }),
                notiCount != 0 ? new Positioned(
                  right: 9,
                  top: 9,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: global.mainColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      notiCount<100?notiCount.toString():"99+",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ) : new Container(width: 0,height: 0,)
              ],
            ),
          ],
          backgroundColor: global.appbarBackColor),
      body:
      receivedResponse?RefreshIndicator(
        child:Container(
          color: Colors.white,
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child:Center(
              child: ListView.builder(
                controller: scrollController,
                itemCount: list_of_receipts.length,
                itemBuilder: (context, index) {
                  return Center(
                      child:Container(
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color:Color(0xffc8c8c8))),
                        ),
                        child: ChoiceCard(receipt: list_of_receipts[index],isSel: (bool value){
                          if(value)
                          {
                            getReceipt();
                          }
                        },),
                      )
                  );
                },
              ),
            ),
          ),
        ),
        onRefresh: getReceipt,
      ):new Container(
          child:Center(
            child:new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          )
      ),
    );
  }
}

class Receipt {
  const Receipt({this.receiptId,this.title,this.amount,this.timestamp, this.imagepath,this.groupName,this.isUser,this.isSplitGroupPresent,this.receiptSettlementStatus,this.userSettlements});

  final String receiptId;
  final String title;
  final double amount;
  final String timestamp;
  final String imagepath;
  final String groupName;
  final String isUser;
  final bool isSplitGroupPresent;
  final bool receiptSettlementStatus;
  final String userSettlements;

}


class ChoiceCard extends StatelessWidget
{
  const ChoiceCard({Key key, this.receipt,this.isSel}) : super(key: key);

  final Receipt receipt;
  final ValueChanged<bool> isSel;

  @override
  Widget build(BuildContext context)
  {
    return new Row(
      children: <Widget>[
        new Container(
            child: Flexible(
              child:Padding( padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 0.0),
                  child:Stack(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          global.receiptAmount=0;
                          global.receiptID=receipt.receiptId;
                          global.hotelname=receipt.title;
                          global.receiptLastScreen=true;
                          global.ereceiptToContainer=false;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  EditContainerActivity(),
                            ),
                          ).then((value) => {
                            isSel(true),
                          });
                        },
                        child:  Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.white,
                            child: Center(
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    new Flexible(
                                        flex:2,
                                        fit: FlexFit.loose,
                                        child: new Stack(
                                          children: <Widget>[
                                            new Container(
                                                width: MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/9:MediaQuery.of(context).size.width/10,
                                                height:MediaQuery.of(context).size.width<600?MediaQuery.of(context).size.width/9:MediaQuery.of(context).size.width/10,
                                                margin: EdgeInsets.all(5),
                                                decoration: new BoxDecoration(
                                                  color: global.imageBackColor,
                                                  image: new DecorationImage(
                                                    fit: BoxFit.fill,
                                                    image: NetworkImage(receipt.imagepath),
                                                  ),
                                                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                                                )
                                            ),
                                            Positioned(
                                              top: 0,
                                              right: 0,
                                              child: receipt.isUser.compareTo("admin")==0?Container(
                                                height: 15,
                                                width: 15,
                                                child: Image.asset(
                                                    'assets/admin_symbol.png',
                                                    fit: BoxFit.contain
                                                ),
                                              ):(receipt.isUser.compareTo("payee")==0)?new Container(
                                                height: 15,
                                                width: 15,
                                                child: Image.asset(
                                                    'assets/payee_symbol.png',
                                                    fit: BoxFit.contain
                                                ),
                                              ):new Container(
                                                width: 0,
                                                height: 0,
                                              ),
                                            )
                                          ],
                                        )
                                    ),
                                    new Flexible(
                                        flex: 5,
                                        fit: FlexFit.tight,
                                        child: Container(
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child:new Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                    receipt.title,
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(fontSize: global.font16,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')
                                                ),
                                                Text(
                                                    "\$"+global.formatter.format(receipt.amount).toString(),
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(fontSize: global.font16,color:Color.fromRGBO(0, 0, 0, 0.54),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')
                                                ),
                                              ],
                                            )
                                        )
                                    ),
                                    new Flexible(
                                        flex: 2,
                                        fit: FlexFit.tight,
                                        child: Container(
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child:new Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[

                                                receipt.isUser.toString().compareTo("admin")==0?(
                                                    receipt.isSplitGroupPresent?new Container(
                                                      child: receipt.receiptSettlementStatus?(
                                                          new Container(
                                                              child: receipt.userSettlements.toString().compareTo("all")!=0?(
                                                                  new Container(
                                                                    padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                    child: Text(receipt.userSettlements, style: TextStyle(fontSize: global.font15,color:Color(0xff333333),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                  )
                                                              ):new Container(
                                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                height: 25,
                                                                width: 25,
                                                                child: Image.asset(
                                                                    'assets/green_tick.png',
                                                                    fit: BoxFit.contain
                                                                ),
                                                              )
                                                          )):new Container(
                                                        height: 18,
                                                        width: 18,
                                                        child: Image.asset(
                                                            'assets/save_draft_symbol.png',
                                                            fit: BoxFit.contain
                                                        ),
                                                      ),
                                                    ):new Container(width: 18,height: 18,)
                                                ):(
                                                    receipt.isUser.toString().compareTo("payee")==0?(
                                                        receipt.receiptSettlementStatus?new Container(
                                                            child: receipt.userSettlements.toString().compareTo("all")!=0?(
                                                                receipt.userSettlements.toString().compareTo("reject")==0?(
                                                                    new Container(
                                                                      padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                      child: Text("PAY >", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(250, 100, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                    )
                                                                ):receipt.userSettlements.toString().compareTo("pay")==0?(
                                                                    new Container(
                                                                      padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                      child: Text("PAY >", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(250, 100, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                    )
                                                                ):receipt.userSettlements.toString().compareTo("settle")==0?(
                                                                    new Container(
                                                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                      height: 25,
                                                                      width: 25,
                                                                      child: Image.asset(
                                                                          'assets/green_tick.png',
                                                                          fit: BoxFit.contain
                                                                      ),
                                                                    )
                                                                ):
                                                                new Container(
                                                                  padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                  child: Text(receipt.userSettlements, style: TextStyle(fontSize: global.font15,color:Color(0xff333333),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                )
                                                            ):new Container(
                                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                              height: 25,
                                                              width: 25,
                                                              child: Image.asset(
                                                                  'assets/green_tick.png',
                                                                  fit: BoxFit.contain
                                                              ),
                                                            )
                                                        ):new Container(
                                                          height: 18,
                                                          width: 18,
                                                          child: Image.asset(
                                                              'assets/save_draft_symbol.png',
                                                              fit: BoxFit.contain
                                                          ),
                                                        )
                                                    ):(
                                                        receipt.receiptSettlementStatus?new Container(
                                                            child:  receipt.userSettlements.toString().compareTo("settle")==0?(
                                                                new Container(
                                                                  height: 25,
                                                                  width: 25,
                                                                  child: Image.asset(
                                                                      'assets/green_tick.png',
                                                                      fit: BoxFit.contain
                                                                  ),)
                                                            ):
                                                            receipt.userSettlements.toString().compareTo("reject")==0?(
                                                                new Container(
                                                                  padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                  child: Text("PAY >", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(250, 100, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                )
                                                            ): receipt.userSettlements.toString().compareTo("all")==0?(
                                                                new Container(
                                                                  height: 25,
                                                                  width: 25,
                                                                  child: Image.asset(
                                                                      'assets/green_tick.png',
                                                                      fit: BoxFit.contain
                                                                  ),)
                                                            ):(
                                                                new Container(
                                                                  padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                                  child: Text("PAY >", style: TextStyle(fontSize: global.font15,color:Color.fromRGBO(250, 100, 0, 0.9),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                                )
                                                            )
                                                        ):new Container(
                                                          height: 18,
                                                          width: 18,
                                                          child: Image.asset(
                                                              'assets/save_draft_symbol.png',
                                                              fit: BoxFit.contain
                                                          ),
                                                        )
                                                    )
                                                ),
                                                Container(
                                                    padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                                    child: Opacity(
                                                      opacity: 0.4,
                                                      child: Text(
                                                          receipt.timestamp,
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')
                                                      ),
                                                    )
                                                )
                                              ],
                                            )
                                        )
                                    )
                                  ]
                              ),
                            )
                        ),
                      )
                    ],
                  )
              ),
            )
        )
      ],
    );
  }
}

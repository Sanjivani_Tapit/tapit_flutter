import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/Activities/EditContactActivity.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ListForMultiple.dart';
import 'package:tapit/helpers/UserAmountData.dart';

typedef void TotalMatchCallback(bool value);

class MultipleFragment extends StatefulWidget
{
  MultipleFragmentState createState()=> MultipleFragmentState();
  TotalMatchCallback totalMatchCallback;
  void setOnMatchStateListener(TotalMatchCallback callback)
  {
    this.totalMatchCallback = callback;
  }
}

class MultipleFragmentState extends State<MultipleFragment>
{
  Uint8List MainprofileImg;
  List<UserAmountData> sel_contact_list=new List();
  List<double> _totalAmountList=new List();
  double _totalAmount=0;
  double amount=0;
  int totalParts=0;

  @override
  void initState()
  {
    setState(()
    {
      sel_contact_list.addAll(global.finalUserList);
      amount=global.receiptAmount/(sel_contact_list.length);
      amount=double.parse((amount).toStringAsFixed(2));
      totalParts=global.multiple_parts;

      double offset=(global.receiptAmount-(amount*sel_contact_list.length));
      offset=double.parse((offset).toStringAsFixed(2));

      for(int j=0;j<sel_contact_list.length;j++)
      {
        UserAmountData userAmountData=global.finalUserList.elementAt(j);
        if(global.multiple_parts==sel_contact_list.length)
        {
          double testAmt;
          if(j==0)
          {
            testAmt = double.parse((amount+offset).toStringAsFixed(2));
          }
          else
          {
            testAmt=amount;
          }
          userAmountData.shareAmount = testAmt;
          userAmountData.sharepart=1;
          _totalAmountList.add(amount);
        }
        else
        {
          _totalAmountList.add(userAmountData.shareAmount);
        }
      }
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: null,
        body: Container(
          color: global.whitecolor,
          child: Stack(
            children: <Widget>[
              new Container(
                color: global.whitecolor,
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverList(delegate: SliverChildListDelegate(
                        [
                          Container(
                            child: new Column(
                              children: <Widget>[
                                new Container(
                                    height: kBottomNavigationBarHeight,
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Flexible(
                                          flex:2,
                                          fit:FlexFit.loose,
                                          child: new Container(
                                              width: 40,
                                              height:40
                                          ),
                                        ),
                                        new Flexible(
                                            flex:7,
                                            fit: FlexFit.tight,
                                            child: new Container(
                                              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                              child: global.grpname!=null?new Text(global.grpname, style: TextStyle(fontSize: global.font14,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Container(width:0,height:0),
                                            )),
                                        new Flexible(
                                          flex: 3,
                                          fit: FlexFit.tight,
                                          child: new Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Container(
                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                child: new Container(
                                                  decoration: BoxDecoration(
                                                    color: global.mainColor,
                                                    border: Border.all(
                                                        color: global.mainColor,
                                                        width: 1.0),
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(10.0)),
                                                  ),
                                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                                  child: new Text(totalParts.toString()+" parts", style: TextStyle(fontSize: global.font12,color:global.whitecolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Flexible(
                                        flex:2,
                                        fit:FlexFit.loose,
                                        child: new Container(
                                            width:40,
                                            child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Opacity(
                                                    opacity:0.5,
                                                    child: new Text("Pic", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                  )
                                                ]
                                            )
                                        )
                                    ),
                                    new Flexible(
                                        flex:6,
                                        fit:FlexFit.tight,
                                        child:new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Container(
                                                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                child: Opacity(
                                                  opacity:0.5,
                                                  child: new Text("Share holder", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                )
                                            )
                                          ],
                                        )
                                    ),
                                    new Flexible(
                                        flex:1,
                                        fit:FlexFit.tight,
                                        child:new Container(
                                          width: 20,
                                          height: 20,
                                        )
                                    ),
                                    new Flexible(
                                      flex: 3,
                                      fit: FlexFit.tight,
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: Opacity(
                                                opacity:0.5,
                                                child: new Text("Split share", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                              )
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ]
                    )),
                    SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {

                          return
                            Container(
                              child: ListForMultiple(index:index,userAmountData:global.finalUserList[index],profileImg: global.imageList[index],amount:_totalAmountList[index],isSel: (int value)  {

                                setState(() {
                                  totalParts=value;
                                });

                                double finalTotal=0;

                                for(int k=0;k<_totalAmountList.length;k++)
                                {
                                  double val=(global.receiptAmount/global.multiple_parts)*global.finalUserList.elementAt(k).sharepart;
                                  double mod = pow(10.0, 2);
                                  val=(val * mod).round().toDouble() / mod;
                                  finalTotal=finalTotal+val;
                                }
                                double offset=global.receiptAmount-finalTotal;
                                offset=double.parse((offset).toStringAsFixed(2));

                                for(int i=0;i<_totalAmountList.length;i++)
                                {
                                  double val=(global.receiptAmount/global.multiple_parts)*global.finalUserList.elementAt(i).sharepart;
                                  double mod = pow(10.0, 2);
                                  val=(val * mod).round().toDouble() / mod;

                                  if(i==0)
                                  {
                                    if(val!=0)
                                    {
                                      val = val + offset;
                                    }
                                  }
                                  setState(()
                                  {
                                    _totalAmountList[i]=val;
                                  });
                                }
                                for(int i=0;i<_totalAmountList.length;i++)
                                {
                                  if(_totalAmountList[i].compareTo(0)==0)
                                  {
                                    UserAmountData ud=global.finalUserList.elementAt(i);
                                    ud.shareAmount=_totalAmountList[i];
                                  }
                                  else
                                  {
                                    setState(()
                                    {
                                      UserAmountData ud=global.finalUserList.elementAt(i);
                                      ud.shareAmount=_totalAmountList[i];
                                    });
                                  }
                                }
                              },
                              ),
                            );
                        }, childCount: global.finalUserList.length)),
                    SliverList(delegate: SliverChildListDelegate(
                        [
                          Container(
                            padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  color: !global.isSplitDetails?Color(0xffdcdcdc):Color(0xffffffff),
                                  width: 1.5,
                                ),
                              ),
                            ),
                          ),
                        ]
                    )),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          !global.isSplitDetails?SizedBox(
                              height: 50,
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  color: global.whitecolor,
                                  child: GestureDetector(
                                    onTap: (){
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              EditContactActivity(sourceID:0,groupID:global.grpid,groupname:global.grpname,sel_contact_list:global.sel_contact_list),
                                        ),
                                      );
                                    },
                                    child: new Row(
                                      children: <Widget>[
                                        Image.asset(
                                          'assets/add_remove_member_orange.png',
                                          fit: BoxFit.contain,
                                          height: 15,
                                        ),
                                        Container(
                                            color: global.whitecolor,
                                            padding: const EdgeInsets.fromLTRB(20,0,0,0),
                                            child:  Text("Add/Remove member",style: new TextStyle(
                                                fontSize: global.font16,
                                                color: global.mainColor,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: 'BalooChetanMedium'
                                            )
                                            ))
                                      ],
                                    ),
                                  )
                              )
                          ):new Container(width: 0,height: 0,),
                        ],
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          SizedBox(
                              height: 70,
                              child: Container(
                                width: 0,
                                height: 0,
                              )
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:tapit/Activities/AddManualReceipt.dart';
import 'package:tapit/Activities/EReceiptClass.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/Activities/NotificationsScreen.dart';
import 'package:quiver/async.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:tapit/helpers/HomeScreenBottomSheet.dart';

class HomeFragmentClass extends StatefulWidget
{
  HomeFragmentState createState()=> HomeFragmentState();
}

class HomeFragmentState extends State<HomeFragmentClass>
{
  int _start = 5;
  int counter = 5;
  int notiCounter;
  int notiCount;
  SolidController _controller = SolidController();

  @override
  void initState()
  {
    super.initState();

    if(global.unreadNotiCounter.toString().compareTo("null")!=0)
    {
      notiCounter = global.unreadNotiCounter;
    }
    startTimer();
  }

  void startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      if(mounted) {
        setState(() {
          if(global.unreadNotiCounter.toString().compareTo("null")!=0)
          {
            notiCount = global.unreadNotiCounter;
          }
        });
      }
    });

    sub.onDone(() {
      sub.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    if(Platform.isAndroid)
    {
      FlutterStatusbarcolor.setStatusBarColor(global.browncolor);
    }
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding:  EdgeInsets.fromLTRB(15,0,0,0),
                child: Image.asset(
                  'assets/logo.png',
                  fit: BoxFit.contain,
                  height: 24,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(8,0,8,0),
                child:  Image.asset(
                  'assets/justap_logo.png',
                  fit: BoxFit.contain,
                  height: 22,
                ),
              )
            ],
          ),
          backgroundColor: global.appbarBackColor,
          actions: <Widget>[
            new Stack(
              children: <Widget>[
                new IconButton(
                    icon: new Image.asset('assets/notification_symbol.png'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NotificationsScreen()),
                      ).then((value) async => {
                        await global.helperClass.getCount(),
                        if(mounted){
                          setState(() {
                            if(global.unreadNotiCounter.toString().compareTo("null")!=0)
                            {
                              notiCount = global.unreadNotiCounter;
                            }
                          })
                        }
                      });
                    }
                ),
                global.unreadNotiCounter != 0 ? new Positioned(
                  right: 9,
                  top: 9,
                  child: notiCount!=null?new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: global.mainColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      notiCount<100?notiCount.toString():"99+",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ):new Container(width: 0,height: 0,),
                ) : new Container(width: 0,height: 0,)
              ],
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                decoration: new BoxDecoration(gradient: new LinearGradient(
                    colors: global.homeScreenGradientColors,
                    tileMode: TileMode.clamp
                ),
                ),
                child:new Column(
                  children: <Widget>[
                    Flexible(
                        flex:6,
                        fit:FlexFit.tight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: new Container(
                                color: global.transparent,
                                child: Image.asset(
                                  'assets/lightining.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ],
                        )
                    ),
                    Flexible(
                      flex:2,
                      fit:FlexFit.tight,
                      child:  new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height:36,
                            width: (MediaQuery.of(context).size.width/3)*1.5,
                            decoration: BoxDecoration(
                              color: global.browncolor.withOpacity(0.7),
                              border: Border.all(color: global.browncolor.withOpacity(0.7), width: 0.0),
                              borderRadius: BorderRadius.all(Radius.circular(18.0)),
                            ),
                            margin: EdgeInsets.fromLTRB(0,0,0,10),
                            padding: EdgeInsets.fromLTRB(0,7,0,7),
                            child: FlatButton(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              child: Text('+ Tapceipt',style: TextStyle(fontSize: global.font15,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),),
                              textColor: global.whitecolor,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => EReceiptClass()),
                                );
                              },
                            ),
                          ),
                          Container(
                            height:36,
                            width: (MediaQuery.of(context).size.width/3)*1.5,
                            decoration: BoxDecoration(
                              color: global.transparent,
                              border: Border.all(color: global.whitecolor, width: 2.0),
                              borderRadius: BorderRadius.all(Radius.circular(18.0)),
                            ),
                            margin: EdgeInsets.fromLTRB(0,0,0,0),
                            padding: EdgeInsets.fromLTRB(0,7,0,7),
                            child: FlatButton(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              child: Text('Add Receipt manually',style: TextStyle(fontSize: global.font15,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium'),),
                              textColor: global.whitecolor,
                              onPressed: ()
                              {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AddManualReceipt()),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      flex: 2,
                      fit: FlexFit.tight,
                      child: new Container(
                        width: 0,
                        height:0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            DraggableScrollableSheet(
              initialChildSize: 0.2,
              minChildSize: 0.2,
              maxChildSize: 1.0,
              builder: (BuildContext context, ScrollController scrollController){
                return HomeScreenBottomSheet(scrollController: scrollController,);
              },
            )
          ],
        )
    );
  }
}

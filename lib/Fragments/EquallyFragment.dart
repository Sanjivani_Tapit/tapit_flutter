import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/UserAmountData.dart';
import 'package:tapit/helpers/ListForEqually.dart';
import 'package:tapit/Activities/EditContactActivity.dart';

typedef void TotalMatchCallback(bool value);

class EquallyFragment extends StatefulWidget
{
  EquallyFragmentState createState()=> EquallyFragmentState();
  TotalMatchCallback totalMatchCallback;
  void setOnMatchStateListener(TotalMatchCallback callback)
  {
    this.totalMatchCallback = callback;
  }

}

class EquallyFragmentState extends State<EquallyFragment>
{
  Uint8List MainprofileImg;
  double amount=0;
  bool flag=true;
  List<UserAmountData> sel_contact_list=new List();

  @override
  void initState()
  {
    super.initState();
    getData();
  }

  void getData()
  {
    setState(() {

      sel_contact_list.addAll(global.finalUserList);
      amount = global.receiptAmount / (sel_contact_list.length);
      amount=double.parse((amount).toStringAsFixed(2));
      double offset=(global.receiptAmount-(amount*sel_contact_list.length));
      offset=double.parse((offset).toStringAsFixed(2));

      for (int i = 0; i < global.finalUserList.length; i++)
      {
        UserAmountData userAmountData = global.finalUserList.elementAt(i);
        if(i==0)
        {
          userAmountData.shareAmount = double.parse((amount+offset).toStringAsFixed(2));
        }
        else
        {
          userAmountData.shareAmount = amount;
        }
        userAmountData.sharepart = 1;
      }
    }
    );
    global.multiple_parts=global.finalUserList.length;

  }


  @override
  Widget build(BuildContext context)
  {
    // TODO: implement build
    return Scaffold(
        appBar: null,
        body: Container(
          color: global.whitecolor,
          child: Stack(
            children: <Widget>[
              new Container(
                color: global.whitecolor,
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverList(delegate: SliverChildListDelegate(
                        [
                          Container(
                            child: new Column(
                              children: <Widget>[
                                new Container(
                                    height: kBottomNavigationBarHeight,
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Flexible(
                                          flex:2,
                                          fit:FlexFit.loose,
                                          child: new Container(
                                              width: 40,
                                              height:40
                                          ),
                                        ),
                                        new Flexible(
                                            flex: 9,
                                            fit:FlexFit.tight,
                                            child: new Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                                  child: global.grpname!=null?new Text(global.grpname, style: TextStyle(fontSize: global.font14,color:global.blackcolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')):new Container(width: 0,height: 0,),
                                                ),
                                              ],
                                            )
                                        )
                                      ],
                                    )
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Flexible(
                                        flex:2,
                                        fit:FlexFit.loose,
                                        child: new Container(
                                            width:40,
                                            child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Opacity(
                                                    opacity:0.5,
                                                    child: new Text("Pic", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                  )
                                                ]
                                            )
                                        )
                                    ),
                                    new Flexible(
                                        flex:6,
                                        fit:FlexFit.tight,
                                        child:new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Container(
                                                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                child: Opacity(
                                                  opacity:0.5,
                                                  child: new Text("Share holder", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                                )
                                            )
                                          ],
                                        )
                                    ),
                                    new Flexible(
                                        flex:1,
                                        fit:FlexFit.tight,
                                        child:new Container(
                                          width: 20,
                                          height: 20,
                                        )
                                    ),
                                    new Flexible(
                                      flex: 2,
                                      fit: FlexFit.tight,
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: Opacity(
                                                opacity:0.5,
                                                child: new Text("Split share", style: TextStyle(fontSize: global.font12,color:Color.fromRGBO(0, 0, 0, 0.87),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                              )
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ]
                    )),
                    SliverList(
                        delegate: SliverChildBuilderDelegate((context, index) {
                          return flag?
                          Container(
                            child: ListForEqually(index:index,userAmountData:global.finalUserList[index],profileImg: global.imageList[index],amount:amount,isSel: (bool value)  {

                            },),
                          ):new Container(height: 0,width: 0,);
                        }, childCount: global.finalUserList.length)
                    ),
                    SliverList(delegate: SliverChildListDelegate(
                        [
                          Container(
                            padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                bottom: BorderSide(
                                  color: !global.isSplitDetails?Color(0xffdcdcdc):Color(0xffffffff),
                                  width: 1.5,
                                ),
                              ),
                            ),
                          ),
                        ]
                    )),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          !global.isSplitDetails?SizedBox(
                              height: 50,
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  color: global.whitecolor,
                                  child: GestureDetector(
                                    onTap: (){
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              EditContactActivity(sourceID:0,groupID:global.grpid,groupname:global.grpname,sel_contact_list:global.sel_contact_list),
                                        ),
                                      );
                                    },
                                    child: new Row(
                                      children: <Widget>[
                                        Image.asset(
                                          'assets/add_remove_member_orange.png',
                                          fit: BoxFit.contain,
                                          height: 15,
                                        ),
                                        Container(
                                            color: global.whitecolor,
                                            padding: const EdgeInsets.fromLTRB(20,0,0,0),
                                            child:  Text("Add/Remove member",style: new TextStyle(
                                                fontSize: global.font16,
                                                color: global.mainColor,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: 'BalooChetanMedium'
                                            )
                                            ))
                                      ],
                                    ),
                                  )
                              )
                          ):new Container(width: 0,height: 0,),
                        ],
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          !global.isSplitDetails?SizedBox(
                              height: 70,
                              child: Container(
                                width: 0,
                                height: 0,
                              )
                          ):new Container(width: 0,height: 0,),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}
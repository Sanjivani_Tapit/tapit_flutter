import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tapit/Activities/ProfileActivity.dart';
import 'package:tapit/Activities/GroupsActivity.dart';
import 'package:tapit/Activities/NotificationsScreen.dart';
import 'package:tapit/Activities/SignInClass.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/VerifyEmail.dart';
import 'package:tapit/utils/UserService.dart';
import 'package:tapit/utils/responsebean.dart';

class AccountFragment extends StatefulWidget
{
  AccountFragment({Key key}) : super(key: key);
  AccountFragmentState createState()=> AccountFragmentState();
}

class AccountFragmentState extends State<AccountFragment> with WidgetsBindingObserver
{
  Uint8List profileImg;
  String image,firstname,lastname;
  String username="";
  String imageType="false";
  String emailVerified="false";
  int notiCount;
  bool isResponseReceived=false;

  String LOGTAG="AccountFragment";

  @override
  void initState()
  {
    getData();
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {

    if(state == AppLifecycleState.resumed)
    {
      checkEmailVerified();
    }else if(state == AppLifecycleState.inactive)
    {
    }else if(state == AppLifecycleState.paused)
    {
    }
  }

  Future<void> checkEmailVerified()async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    emailVerified=prefs.getString("emailVerified");
    if(emailVerified.toString().compareTo("false")==0 || emailVerified.toString().compareTo("pending")==0)
    {
      UserService userService = new UserService();
      ResponseBean response = await userService.getUserInfo(global.UserID);
      if(response!=null)
      {
        var payload = response.payLoad;
        var emailver = payload['emailVerified'];
        if (emailver)
        {
          if(mounted)
          {
            setState(() {
              emailVerified = "true";
              global.isEmailVerified="true";
              prefs.setString("emailVerified", "true");
            });
          }
        }
      }
    }
  }

  void getData() async
  {
    setState(()
    {
      notiCount=global.unreadNotiCounter;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    image=prefs.getString("UserImage");
    firstname=prefs.getString("CurrentFirstName");
    lastname=prefs.getString("CurrentLastName");
    emailVerified=prefs.getString("emailVerified");

    if(emailVerified==null || emailVerified.isEmpty)
    {
      setState(()
      {
        emailVerified="false";
        global.isEmailVerified="false";
      });
    }

    if(image==null || image.isEmpty)
    {
      setState(() {
        username=firstname+" "+lastname;
        if(firstname!=null && firstname.isNotEmpty)
        {
          image=firstname.substring(0,1);
        }
        if(lastname!=null && lastname.isNotEmpty)
        {
          image=image+lastname.substring(0,1);
        }
        imageType="false";
        profileImg=new Uint8List(0);
      });

    }
    else
    {
      setState(() {
        username=firstname+" "+lastname;
        profileImg=base64Decode(image);
        imageType="true";
      });
    }
    checkEmailVerified();
    setState(()
    {
      isResponseReceived=true;
    });
  }

  void signoutPopup()
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    new Container(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child:new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text("Are you sure you want to Sign Out?", style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                              ],
                            ),
                          ],
                        )
                    ),
                    SizedBox(height: 20,),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xffdcdcdc),
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Cancel", style: TextStyle(fontSize: global.font16,color:global.lightgreycolor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            )
                        ),
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child:new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  child: Text("Confirm", style: TextStyle(fontSize: global.font16,color:global.mainColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                  onPressed: () async
                                  {
                                    SharedPreferences prefs = await SharedPreferences.getInstance();
                                    prefs.setBool("UserLoggedInStatus", false);
                                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                        SignInClass(
                                            main_text: "Hi " + firstname + " " + lastname,
                                            sub_text: "Mobile verification is mandatory")), (Route<dynamic> route) => false);
                                  },
                                )
                              ],
                            )
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });

  }


  void verifyEmailAddressPopup()
  {
    showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 0),
                child: VerifyEmail(isSel: (bool value) async {
                  if(value)
                  {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    setState(() {
                      global.isEmailVerified="pending";
                      emailVerified="pending";
                      prefs.setString("emailVerified", "pending");
                    });
                    Navigator.pop(context);
                  }
                }),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding:  EdgeInsets.fromLTRB(15,0,0,0),
                child: Image.asset(
                  'assets/logo.png',
                  fit: BoxFit.contain,
                  height: 24,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(8,0,8,0),
                child:  Image.asset(
                  'assets/justap_logo.png',
                  fit: BoxFit.contain,
                  height: 22,
                ),
              )
            ],
          ),
          actions: <Widget>[
            new Stack(
              children: <Widget>[
                new IconButton(
                    icon: new Image.asset('assets/notification_symbol.png'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NotificationsScreen()),
                      ).then((value) async => {
                        await global.helperClass.getCount(),
                        if(mounted){
                          setState(() {
                            notiCount = global.unreadNotiCounter;
                          })
                        }
                      });
                    }),
                global.unreadNotiCounter != 0 ? new Positioned(
                  right: 9,
                  top: 9,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: global.mainColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      notiCount<100?notiCount.toString():"99+",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ) : new Container(width: 0,height: 0,)
              ],
            ),
          ],
          backgroundColor: global.appbarBackColor),
      body: isResponseReceived?Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          color: global.whitecolor,
          child:
          CustomScrollView(
              slivers: <Widget>[
                SliverList(delegate: SliverChildListDelegate(
                    [
                      new Container(
                        height: MediaQuery.of(context).size.height/6,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              flex:1,
                              fit:FlexFit.loose,
                              child: new Container(
                                width:MediaQuery.of(context).size.width/6,
                                height:MediaQuery.of(context).size.width/6,
                                child:  profileImg!=null?((imageType.toString().contains("true"))?CircleAvatar(
                                  backgroundImage: MemoryImage(profileImg),
                                  backgroundColor: global.imageBackColor,
                                ):(image!=null?CircleAvatar(
                                  child: Text(
                                    image,style: TextStyle(color: global.adminTextcolor),),
                                  backgroundColor: global.adminBackcolor,
                                ):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ))):CircleAvatar(
                                  backgroundImage: AssetImage('assets/dummy_user.png'),
                                  backgroundColor: global.appbargreycolor,
                                ),
                              ),
                            ),
                            Flexible(
                                flex:3,
                                fit:FlexFit.tight,
                                child: new Container(
                                  padding: EdgeInsets.fromLTRB(15,0,0,0),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text(username,style:TextStyle(fontSize: global.font18,color:Color(0xff353535),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                                      Opacity(
                                        opacity: 0.4,
                                        child:  new Text("Joined on 26/7/2020",style: TextStyle(fontSize: global.font13,color:Color(0xff353535),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular'),),
                                      )
                                    ],
                                  ),
                                )
                            ),
                            Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child:  GestureDetector(
                                  onTap:(){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProfileActivity(),
                                      ),
                                    ).then((value) {
                                      getData();
                                    });
                                  },
                                  child:new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[

                                      new Container(
                                        width: 20,
                                        height: 20,
                                        child: Image.asset(
                                          'assets/setting.png',
                                          fit: BoxFit.fill,
                                        ),
                                      )

                                    ],
                                  ),
                                )
                            ),
                          ],
                        ),
                      ),
                    ])),
                SliverList(delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding:EdgeInsets.fromLTRB(10, 0,10, 0),
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xff979797).withOpacity(0.3),
                              width: 1,
                            ),
                          ),
                        ),
                      ),])),
                SliverList(delegate: SliverChildListDelegate(
                  [
                    global.isEmailVerified.toString().compareTo("false")==0?new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 15,),
                        Opacity(
                          opacity: 0.5,
                          child:  Text("You might be missing important notifications via emails from your friends", style: TextStyle(fontSize: global.font13,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                        ),
                        SizedBox(height: 5,),
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            GestureDetector(
                              onTap: (){
                                verifyEmailAddressPopup();
                              },
                              child: Text("Verify email >", style: TextStyle(fontSize: global.font14,color:Color(0xfffa6400),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                            ),
                          ],
                        ),
                        Container(
                          padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xff979797).withOpacity(0.3),
                                width: 1,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ):new Container(width: 0,height: 0,),

                    global.isEmailVerified.toString().compareTo("pending")==0?
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height:15),
                        Text("Email verification is pending", style: TextStyle(fontSize: global.font14,color:Color(0xfffa6400),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanMedium')),
                        SizedBox(height: 5,),
                        Opacity(
                          opacity: 0.5,
                          child:  Text("Click on the link we have sent you in the email, subject \"Verify your email\" from no-reply@tapit.live", style: TextStyle(fontSize: global.font13,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                        ),
                        Container(
                          padding:EdgeInsets.fromLTRB(10, 10,10, 0),
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xff979797).withOpacity(0.3),
                                width: 1,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ):new Container(width: 0,height: 0,),

                    SizedBox(height: 20),
                    GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  GroupsActivity(source:1),
                            ),
                          );
                        },
                        child:new Row(
                          children: <Widget>[
                            Flexible(
                              flex:1,
                              child: new Container(
                                padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                                child:  Image.asset(
                                  'assets/group_symbol.png',
                                  fit: BoxFit.contain,
                                  height: 20,
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 2,
                              child: new Container(
                                padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                                child:Text("My groups", style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ),

                            )
                          ],
                        )
                    ),
                    SizedBox(height:20),
                    new Row(
                      children: <Widget>[
                        Flexible(
                          flex:1,
                          child: new Container(
                            padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                            child:  Image.asset(
                              'assets/rewards_symbol.png',
                              fit: BoxFit.contain,
                              height: 20,
                              color: Color.fromRGBO(0, 0, 0, 0.87).withOpacity(0.4),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 2,
                          child: GestureDetector(
                            child: new Container(
                              padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                              child:Text("My Rewards", style: TextStyle(fontSize: global.font16,color:Color.fromRGBO(0, 0, 0, 0.87).withOpacity(0.4),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height:20),
                    new Row(
                      children: <Widget>[
                        Flexible(
                          flex:1,
                          child: new Container(
                            padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                            child:
                            Image.asset(
                              'assets/financial_analysis_symbol.png',
                              fit: BoxFit.contain,
                              height: 20,
                              color: Color.fromRGBO(0, 0, 0, 0.87).withOpacity(0.4),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 2,
                          child: GestureDetector(
                            child: new Container(
                              padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                              child:Text("Financial Analysis", style: TextStyle(fontSize: global.font16,color:Color.fromRGBO(0, 0, 0, 0.87).withOpacity(0.4),fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height:20),
                    GestureDetector(
                        onTap: (){
                          signoutPopup();
                        },
                        child:new Row(
                          children: <Widget>[
                            Flexible(
                              flex:1,
                              child: new Container(
                                padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                                child:  Image.asset(
                                  'assets/logout.png',
                                  fit: BoxFit.contain,
                                  height: 18,
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 2,
                              child: new Container(
                                padding: EdgeInsets.fromLTRB(15,  0, 0, 0),
                                child:Text("Sign Out", style: TextStyle(fontSize: global.font16,color:global.nameTextColor,fontStyle: FontStyle.normal,fontFamily: 'BalooChetanRegular')),
                              ),

                            )
                          ],
                        )
                    ),
                  ],
                ),
                )
              ]
          )
      ):new Container(
          child:Center(
            child:new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          )
      ),
    );
  }
}
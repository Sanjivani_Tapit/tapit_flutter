import 'package:flutter/material.dart';
import 'package:tapit/Activities/NotificationsScreen.dart';
import 'package:tapit/global.dart' as global;
import 'package:tapit/helpers/ListForOffersHori.dart';
import 'package:tapit/helpers/ListForOffersVer.dart';

class OffersFragment extends StatefulWidget
{
  OffersFragmentState createState()=> OffersFragmentState();
}

class OffersFragmentState extends State<OffersFragment>
{
  bool receivedResponse=false;
  @override
  void initState()
  {
    super.initState();
    getOfferList();
  }

  void getOfferList() async
  {
    setState(()
    {
      receivedResponse=false;
    });

    bool flag=await getData();

    setState(()
    {
      receivedResponse=true;
    });
  }

  Future<bool> getData() async
  {
    Future<bool> flag=Future<bool>.value(true);
    return flag;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                'assets/logo.png',
                fit: BoxFit.contain,
                height: 30,
              ),
              Container(
                  padding: const EdgeInsets.all(8.0), child:  Text("Offers",style: new TextStyle(
                  fontSize: global.font18,
                  color: global.appbarTextColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'BalooChetanSemiBold'
              )
              ))
            ],
          ),
          actions: <Widget>[
            new Stack(
              children: <Widget>[
                new IconButton(icon: Icon(Icons.notifications_none,color: Colors.black,), onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NotificationsScreen()),
                  );
                }),
                global.unreadNotiCounter != 0 ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: global.mainColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      global.unreadNotiCounter.toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ) : new Container(width: 0,height: 0,)
              ],
            ),
          ],
          backgroundColor: global.appbarBackColor),
      body: receivedResponse?new Column(
        children: <Widget>[
          new Container(
            height: 250,
            width:  MediaQuery.of(context).size.width,
            child:  CustomScrollView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: false,
                slivers: <Widget>[
                  SliverList(
                      delegate: SliverChildBuilderDelegate((context, index)
                      {
                        return Center(
                          child:Container(
                            color: global.whitecolor,
                            child: ListForOffersHori(),
                          ),
                        );
                      }, childCount: 2)
                  )
                ]
            ),
          ),
          Flexible(
              child:new Container(
                color: global.whitecolor,
                height: MediaQuery.of(context).size.height-250,
                child: CustomScrollView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: false,
                    slivers: <Widget>[
                      SliverList(
                          delegate: SliverChildBuilderDelegate((context, index)
                          {
                            return Center(
                              child:Container(
                                margin: EdgeInsets.fromLTRB(20,0, 20, 0),
                                decoration: BoxDecoration(
                                  color: global.whitecolor,
                                  border: Border(bottom: BorderSide(color:Color(0xffc8c8c8))),
                                ),
                                child: ListForOffersVer(),
                              ),
                            );
                          }, childCount: 3)
                      )

                    ]
                ),
              )
          )
        ],
      ):
      new Container(
          child:Center(
            child:new Container(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(global.appbargreycolor),
                backgroundColor: global.mainColor,
                strokeWidth: 5,),
            ),
          )
      ),
    );
  }
}

class Offer {
  const Offer({this.storename,this.offername,this.title,this.count,this.distance, this.imagepath});

  final int count;
  final String storename;
  final String offername;
  final String title;
  final String distance;
  final String imagepath;
}

const List<Offer> list_of_offers = const [
  const Offer(storename:'Sebu Mexican Joint',offername:'Thirsty Thursdays',title: 'Buy one,get one offer for throught out the day.T&C*',count:1,distance:'2mi', imagepath: 'assets/video1.png'),
  const Offer(storename:'KFC',offername:'Happy Hours',title: 'Buy one,get one offer for throught out the day.T&C*',count:10,distance:'2mi', imagepath: 'assets/video2.png'),
  const Offer(storename:'Holliysters fabric',offername:'Mothers Day Special',title: 'Buy one,get one offer for throught out the day.T&C*',count:3,distance:'2mi', imagepath: 'assets/video2.png'),
  const Offer(storename:'Twinkal Indian resto',offername:'Happy Sunday',title: 'Buy one,get one offer for throught out the day.T&C*',count:4,distance:'2mi', imagepath: 'assets/video1.png'),

];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.offer}) : super(key: key);

  final Offer offer;

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      height: 150,
      child:Padding( padding: EdgeInsets.symmetric(vertical: 8.0,horizontal: 10.0),
          child:Card(
            child:Container(
                padding: EdgeInsets.all(20),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                child: Center(
                    child:Column(
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Flexible(
                                flex:2,
                                fit: FlexFit.tight,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child:Text(offer.storename, style: TextStyle(fontSize: global.font15,fontWeight: FontWeight.normal,color: global.mainColor)),
                                )
                            ),
                            new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child:Text(offer.distance, style: TextStyle(fontSize: global.font15,fontWeight: FontWeight.normal,color: Color(0xff505050))),
                                )
                            ),
                            new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child:Text(offer.count.toString(), style: TextStyle(fontSize: global.font15,fontWeight: FontWeight.normal,color: Color(0xff505050))),
                                )
                            )
                          ],
                        ),
                        new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              new Flexible(
                                  flex: 3,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(offer.offername, style: TextStyle(fontSize: global.font18,fontWeight: FontWeight.bold,color: Color(0xff505050))),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(offer.title, style: TextStyle(fontSize: global.font13,fontWeight: FontWeight.normal,color: Color(0xff505050))),
                                    ],
                                  )
                              ),
                              new Flexible(
                                flex:1,
                                child:new Center(
                                  child:new Container(
                                      width: 40.0,
                                      height: 40.0,
                                      padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                      decoration: new BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                          image: new DecorationImage(
                                              fit: BoxFit.fill,
                                              image: ExactAssetImage(offer.imagepath)
                                          )
                                      )),
                                ),
                              ),
                            ]),
                      ],
                    )
                )
            ),
          )
      ),
    );
  }
}
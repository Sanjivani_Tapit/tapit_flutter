// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    name: json['name'] as String,
    password: json['password'] as String,
    role: json['role'] as String,
    picture: json['picture'] as String,
    email: json['email'] as String,
    mobileNo: json['mobileNo'] as String,
    countryCode: json['countryCode'] as String,
    uuid: json['uuid'] as String,
    deviceId: json['deviceId'] as String,
    deviceType: json['deviceType'] as String,
    fcmToken: json['fcmToken'] as String,
    isEmailVerified: json['isEmailVerified'] as bool,
    isMobileVerified: json['isMobileVerified'] as bool,
    isActive: json['isActive'] as bool,
    otp: json['otp'] as String,
    registrationStatus: json['registrationStatus'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'password': instance.password,
      'role': instance.role,
      'picture': instance.picture,
      'email': instance.email,
      'mobileNo': instance.mobileNo,
      'countryCode': instance.countryCode,
      'uuid': instance.uuid,
      'deviceId': instance.deviceId,
      'deviceType': instance.deviceType,
      'fcmToken': instance.fcmToken,
      'isEmailVerified': instance.isEmailVerified,
      'isMobileVerified': instance.isMobileVerified,
      'isActive': instance.isActive,
      'otp': instance.otp,
      'registrationStatus': instance.registrationStatus,
    };

OTPGenerate _$OTPGenerateFromJson(Map<String, dynamic> json) {
  return OTPGenerate(
    mobileNo: json['mobileNo'] as String,
    countryCode: json['countryCode'] as String,
  );
}

Map<String, dynamic> _$OTPGenerateToJson(OTPGenerate instance) =>
    <String, dynamic>{
      'mobileNo': instance.mobileNo,
      'countryCode': instance.countryCode,
    };

OTPVerify _$OTPVerifyFromJson(Map<String, dynamic> json) {
  return OTPVerify(
    mobileNo: json['mobileNo'] as String,
    otp: json['otp'] as String,
    countryCode: json['countryCode'] as String,
  );
}

Map<String, dynamic> _$OTPVerifyToJson(OTPVerify instance) => <String, dynamic>{
      'mobileNo': instance.mobileNo,
      'otp': instance.otp,
      'countryCode': instance.countryCode,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiClient implements ApiClient {
  _ApiClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    this.baseUrl ??= 'http://3.21.202.26:8080/tapitserver';
  }

  final Dio _dio;

  String baseUrl;

  @override
  getAllUsers(devicetype) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*'
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  otpGenerate(devicetype, jsonbody) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/login/user/otp/generate',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*'
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  otpVerify(devicetype, jsonbody) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/login/user/otp/mobile/verify',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*'
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  checkMobileExist(devicetype, queries) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(queries, 'queries');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user/mobile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{r'accept': '*/*'},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  UpdateUserData(devicetype, jsonbody) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*'
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getCounter(devicetype, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$id/counter',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  postContactToCloud(devicetype, jsonbody, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result =
        await _dio.request('/api/v1/$devicetype/users/$id/contacts',
            queryParameters: queryParameters,
            options: RequestOptions(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'accept': '*/*',
                  r'Authorization': authToken
                },
                extra: _extra,
                contentType: 'application/json',
                baseUrl: baseUrl),
            data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getContactSync(devicetype, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$id/contacts',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getHistoryData(devicetype, id, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$id/receipts/history',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getAlertData(devicetype, id, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$id/alerts',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  updateAlertStatus(devicetype, userid, alertid, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(userid, 'userid');
    ArgumentError.checkNotNull(alertid, 'alertid');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$userid/alert/$alertid',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getGroups(devicetype, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$id/groups',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  updateGroup(devicetype, userid, groupid, jsonbody, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(userid, 'userid');
    ArgumentError.checkNotNull(groupid, 'groupid');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$userid/group/$groupid',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  addGroup(devicetype, jsonbody, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result =
        await _dio.request('/api/v1/$devicetype/users/$id/group',
            queryParameters: queryParameters,
            options: RequestOptions(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'accept': '*/*',
                  r'Authorization': authToken
                },
                extra: _extra,
                contentType: 'application/json',
                baseUrl: baseUrl),
            data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  initiateBillSplit(devicetype, jsonbody, userid, receiptid, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(userid, 'userid');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/$userid/initiate/billsplit/receipt/$receiptid',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  settleAPI(devicetype, receiptid, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/receipt/$receiptid/billsplit/payment/settle',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  rejectAPI(devicetype, receiptid, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/receipt/$receiptid/billsplit/payment/reject',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  payupAPI(devicetype, receiptid, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/receipt/$receiptid/billsplit/payment/payup',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  reminderAPI(devicetype, receiptid, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/receipt/$receiptid/payment/reminder',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getReceipt(devicetype, receiptid, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/receipt/$receiptid',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  requestSettlement(devicetype, receiptid, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/users/billsplit/requestsettlement/receipt/$receiptid',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getLogs(devicetype, userid, receiptid, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(userid, 'userid');
    ArgumentError.checkNotNull(receiptid, 'receiptid');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/users/users/$userid/receipt/$receiptid/logs',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  verifyEmail(devicetype, jsonbody, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user/email/update-and-verify',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getUserData(devicetype, id, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user/get/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getImageMethod(devicetype, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user/profile-pic/pre-signed-url/get',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  putImageMethod(devicetype, jsonbody, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/v1/$devicetype/user/user/profile-pic/s3/update',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{
              r'Content-Type': 'application/json',
              r'accept': '*/*',
              r'Authorization': authToken
            },
            extra: _extra,
            contentType: 'application/json',
            baseUrl: baseUrl),
        data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getTapAPI(jsonbody, authToken) async {
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result =
        await _dio.request('/api/v1/test/parser/save',
            queryParameters: queryParameters,
            options: RequestOptions(
                method: 'PUT',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'accept': '*/*',
                  r'Authorization': authToken
                },
                extra: _extra,
                contentType: 'application/json',
                baseUrl: baseUrl),
            data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  getMerchantInfo(devicetype, queries, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(queries, 'queries');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries ?? <String, dynamic>{});
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result =
        await _dio.request('/api/v1/$devicetype/merchant/search/eprinter/get',
            queryParameters: queryParameters,
            options: RequestOptions(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'accept': '*/*',
                  r'Authorization': authToken
                },
                extra: _extra,
                contentType: 'application/json',
                baseUrl: baseUrl),
            data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }

  @override
  acceptReceipt(devicetype, jsonbody, authToken) async {
    ArgumentError.checkNotNull(devicetype, 'devicetype');
    ArgumentError.checkNotNull(jsonbody, 'jsonbody');
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = jsonbody;
    final Response<Map<String, dynamic>> _result =
        await _dio.request('/api/v1/$devicetype/users/receipts',
            queryParameters: queryParameters,
            options: RequestOptions(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'accept': '*/*',
                  r'Authorization': authToken
                },
                extra: _extra,
                contentType: 'application/json',
                baseUrl: baseUrl),
            data: _data);
    final value = ResponseBean.fromJson(_result.data);
    return value;
  }
}
